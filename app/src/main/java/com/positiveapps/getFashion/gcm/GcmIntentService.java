/**
 * 
 */
package com.positiveapps.getFashion.gcm;

import java.util.Random;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.media.AudioManager;
import android.media.RingtoneManager;
import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import android.support.v4.app.NotificationCompat;
import com.positiveapps.getFashion.App;
import com.positiveapps.getFashion.MainActivity;
import com.positiveapps.getFashion.R;
import com.positiveapps.getFashion.SplashActivity;
import com.positiveapps.getFashion.backend.Prefs;

/**
 * @author Maor
 *
 */
public class GcmIntentService extends IntentService {

	public static final int NOTIFICATION_ID = 100;
	public static final String CUSTOM_DATA = "CustomData";
	public static final String BUSINESS_ID = "BID";
	public static final String RESERVATION_ID = "ReservationID";
	public static final String MESSAGE = "message";

	public static final int TYPE_RESERVATION_APPROVE = 10;
	public static final int TYPE_RESERVATION_REQUEST = 20;
	public static final int SIMPLE_ALERT = 1;
	public static final int BUSINNES_ALERT = 2;
	public static final int ADMIN_RESERVATION_ALERT = 3;
	public static final int USER_RESERVATION_ALERT = 4;
	public static final String KEY_TYPE = "type";

	// public static final int TYPE_RESERVATION_REQUEST = 20;

	/**
	 * @param name
	 */
	public GcmIntentService() {
		super("GcmIntentService");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.app.IntentService#onHandleIntent(android.content.Intent)
	 */
	@Override
	protected void onHandleIntent(Intent intent) {
		Bundle extras = intent.getExtras();
		GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(this);

		String messageType = gcm.getMessageType(intent);

		Log.e("pushlog", "extras: " + intent.getExtras().describeContents());

		for (String key : intent.getExtras().keySet()) {
			Object value = intent.getExtras().get(key);
			Log.d("pushlog", String.format("%s %s (%s)", key, value.toString(),
					value.getClass().getName()));
		}
		try {
			if (extras != null) {
				
				Prefs.setFromNotification(true);
				
				String businessID = extras.getString(BUSINESS_ID);
				String message = extras.getString(MESSAGE);
				
				
				Prefs.setNotificationMessage(message);
				
				if(businessID.isEmpty()){
					Prefs.setFromNotificationType(SIMPLE_ALERT);
				}else{
					Prefs.setNotificationBusinessId(Long.parseLong(businessID));
					Prefs.setFromNotificationType(BUSINNES_ALERT);
				} 
				
				if(MainActivity.mainActivityIsOn){
					MainActivity.instance.handleNotifications();
				}else{
					getBusinessInfoThenNotify(businessID, message, 0);
				}
				
//				String type = extras.getString("Type");
//				int reservationId = Integer.valueOf(extras
//						.getString(RESERVATION_ID));
				
//
//				int reservationType = TYPE_RESERVATION_REQUEST;
//				// reservation notification!
//				if (type == null) {
//					reservationType = TYPE_RESERVATION_APPROVE;
//					DBHandler.getInstance(getApplicationContext())
//							.insertOrUpdateReservation(reservationId,
//									businessID, _Business.RES_STATUS_APPROVED);
//					
//					Log.e("pushlog", "message type: load buisiness  id:"
//							+ businessID);
//
//					
//				}

				
			}
		} catch (Exception e) {

		}

		// Release the wake lock provided by the WakefulBroadcastReceiver.
		GcmBroadcastReceiver.completeWakefulIntent(intent);

	}

	private void getBusinessInfoThenNotify(final String businessID,
			final String message, final int reservationType) {
		
		Log.e("pushlog", "getBusinessInfoThenNotify reservationType:"
				+ reservationType + " bid " + businessID);
		
		NotificationManager mNotificationManager = (NotificationManager) App.applicationContext.getSystemService(Context.NOTIFICATION_SERVICE);

		Intent intent = new Intent(App.applicationContext, SplashActivity.class);

		intent.setAction("actionstring" + System.currentTimeMillis());
		intent.putExtra(KEY_TYPE, reservationType);
		intent.putExtra(BUSINESS_ID, businessID);
		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP);

		PendingIntent contentIntent = PendingIntent.getActivity(App.applicationContext, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

		NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(App.applicationContext).setSmallIcon(R.mipmap.ic_launcher)
				.setLargeIcon(BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher)).setContentText(message)
				.setContentTitle(App.applicationContext.getString(R.string.app_name))

				.setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION), AudioManager.STREAM_NOTIFICATION).setAutoCancel(true);
		
		
		mBuilder.setContentIntent(contentIntent);
		Notification notification = mBuilder.build();
		notification.flags = Notification.FLAG_SHOW_LIGHTS | Notification.FLAG_AUTO_CANCEL;

		mNotificationManager.notify(new Random().nextInt(5000), notification);

	}
 

}
