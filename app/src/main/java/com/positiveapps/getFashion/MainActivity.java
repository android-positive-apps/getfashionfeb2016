package com.positiveapps.getFashion;

import android.app.Dialog;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.SimpleOnPageChangeListener;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import android.widget.ToggleButton;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.ProfileTracker;
import com.facebook.login.LoginManager;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.location.LocationServices;
import com.positiveapps.getFashion.adapters.MainPagerAdapter;
import com.positiveapps.getFashion.backend.APIManager;
import com.positiveapps.getFashion.backend.BusinessRepository;
import com.positiveapps.getFashion.backend.Categories;
import com.positiveapps.getFashion.backend.Prefs;
import com.positiveapps.getFashion.backend.UserManager;
import com.positiveapps.getFashion.dialogs.MessageDialog.DialogListener;
import com.positiveapps.getFashion.interfaces.AppuserListener;
import com.positiveapps.getFashion.interfaces.FilterSetListener;
import com.positiveapps.getFashion.jsonmodels.Appuser;
import com.positiveapps.getFashion.jsonmodels.Category;
import com.positiveapps.getFashion.jsonmodels.apiresponse.CategoryGetList;
import com.positiveapps.getFashion.jsonmodels.apiresponse.UserResponse;
import com.positiveapps.getFashion.util.ActivityStarter;
import com.positiveapps.getFashion.util.AnimationHelper;
import com.positiveapps.getFashion.util.DialogHelper;
import com.positiveapps.getFashion.util.ImageLoader;
import com.positiveapps.getFashion.views.SlidingTabLayout;
import com.positiveapps.getFashion.views.SwitchButton;

import java.util.ArrayList;
import java.util.Collections;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class MainActivity extends BaseActivity implements AppuserListener, ConnectionCallbacks, OnConnectionFailedListener {
	public static final String STATE_VIEWPAGER_PAGE_INDEX = "viewpagerpageindex";

	public static final int SIMPLE_ALERT = 1;
	public static final int BUSINNES_ALERT = 2;
	public static final int ADMIN_RESERVATION_ALERT = 3;
	public static final int USER_RESERVATION_ALERT = 4;

	public static boolean mainActivityIsOn;
	public static MainActivity instance;

	private PageListener pageListener;
	private static final String TAG = "MainActivity";
	private boolean mGpsDialogDisplayed = false;
	public static AppuserListener appuserListener;
	public static int pagePosition;
	private ActionBarDrawerToggle mDrawerToggle;
	private DrawerLayout mDrawerLayout;
	private Toolbar mToolbar;
	private ImageView btnSearch;
	private ImageView filterer;
	private AutoCompleteTextView txtSearch;
	private ViewPager mainPager;
	private SlidingTabLayout categoryTabs;

	private FragmentStatePagerAdapter mPagerAdapter;
	private boolean mIsSearchExpanded = false;
	private int mSearchViewWidth;

	private View profileMenuItem;
	private ImageView imgProfilePic;
	private TextView txtUsername;
	private TextView txtFilter;
	private TextView txtLogin;
	private TextView txtLogout;
	private TextView txtSwitchLanguage;
	private TextView txtManageReservations;
	private TextView txtManagePromos;
	private TextView txtManageProducts;

	public static SearchData searchData = new SearchData();

	private ProfileTracker mFacebookProfileTracker;
	private CallbackManager callbackManager;
	private GoogleApiClient mGoogleApiClient;
	private OnClickListener menuClickListener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			menuItemClick(v);
		}
	};
	private LinearLayout mainParentLayout;


	private ToggleButton button_A;
	private ToggleButton button_B;
	private ToggleButton button_C;
	private ToggleButton button_D;
	private ToggleButton button_E;
	public FilterSetListener filterSetListener;
	private SwitchButton switchOpenNow;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		findViews();
		initActionBar();
		initDrawer();
		getCategories();
		initFacebookSdk();
		buildGoogleApiClient();

		makeUIforUser(UserManager.getAppUser());
		instance = this;


	}
	public void setFilterSetListener(FilterSetListener filterSetListener){
		this.filterSetListener=filterSetListener;
	}

	private void makeUIforUser(Appuser appUser) {
		if (appUser != null && appUser.IsAuthed) {
			userLoggedIn(appUser);
		} else {
			userLoggedOut();
		}
		appuserListener = this;
	}

	private void initFacebookSdk() {
		callbackManager = CallbackManager.Factory.create();

//		mFacebookProfileTracker = new ProfileTracker() {
//
//			@Override
//			protected void onCurrentProfileChanged(Profile oldProfile, Profile currentProfile) {
//				if(currentProfile!=null){
//					Appuser user = UserManager.getAppUser();
//
//					user.FirstName = currentProfile.getFirstName();
//					user.LastName = currentProfile.getLastName();
//					user.Avatar = currentProfile.getLastName();
//				}
//
//			}
//		};
	}

	protected void loginUserToServer(AccessToken accessToken) {

	}
 
	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		super.onRestoreInstanceState(savedInstanceState);
		int pagerIndex = savedInstanceState.getInt(STATE_VIEWPAGER_PAGE_INDEX);
		mainPager.setCurrentItem(pagerIndex, false);
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putInt(STATE_VIEWPAGER_PAGE_INDEX, mainPager.getCurrentItem());
	}

	@Override
	protected void onStart() {
		super.onStart();
		mGoogleApiClient.connect();
		mainActivityIsOn = true;
	}

	protected synchronized void buildGoogleApiClient() {
		mGoogleApiClient = new GoogleApiClient.Builder(this).addConnectionCallbacks(this).addOnConnectionFailedListener(this)
				.addApi(LocationServices.API).build();
	}

	@Override
	protected void onPause() {
		super.onPause();
		Log.d(TAG, "onPause()");
	}

	@Override
	protected void onStop() {
		super.onStop();
		Log.d(TAG, "onStop()");
		if (mGoogleApiClient.isConnected()) {
			mGoogleApiClient.disconnect();
		}
		mainActivityIsOn = false;
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		Log.d(TAG, "onDestroy()");
		instance = null;
	}
	
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		mDrawerLayout.closeDrawers();
		handleNotifications();
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == ActivityStarter.RESULT_FILTER) {
	        if(resultCode == RESULT_OK){
	            String result = data.getStringExtra("result");
	            initPagerAndTabs();
	        }
	    }
		callbackManager.onActivityResult(requestCode, resultCode, data);
	}

	private void findViews() {
		mToolbar = (Toolbar) findViewById(R.id.actionbar);
		mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
		btnSearch = (ImageView) findViewById(R.id.btnSearch);
		filterer = (ImageView) findViewById(R.id.filterer);
		txtSearch = (AutoCompleteTextView) findViewById(R.id.txtSearch);
		mainPager = (ViewPager) findViewById(R.id.mainPager);
		categoryTabs = (SlidingTabLayout) findViewById(R.id.categoryTabs);

		profileMenuItem = findViewById(R.id.profile);
		txtUsername = (TextView) findViewById(R.id.username);
		txtFilter = (TextView) findViewById(R.id.filter);
		txtLogin = (TextView) findViewById(R.id.login);
		txtLogout = (TextView) findViewById(R.id.logout);
		txtSwitchLanguage = (TextView) findViewById(R.id.languages);
		txtManageReservations = (TextView) findViewById(R.id.manageOrders);
		txtManageProducts = (TextView) findViewById(R.id.manageProducts);
		txtManagePromos = (TextView) findViewById(R.id.managePromotions);
		imgProfilePic = (ImageView) findViewById(R.id.profilePic);
		mainParentLayout = (LinearLayout) findViewById(R.id.mainParentLayout);

		if (BuildConfig.FLAVOR.equals("getCoffeeShops")){
			txtSwitchLanguage.setVisibility(View.GONE);
		}
	}

	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		mDrawerToggle.syncState();
	}

	private void initActionBar() {
		setSupportActionBar(mToolbar);
		mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, 0, 0);
		mDrawerLayout.setDrawerListener(mDrawerToggle);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setHomeButtonEnabled(true);
		getSupportActionBar().setDisplayShowTitleEnabled(false);

		mSearchViewWidth = getDimensionPixelSize(R.dimen.searchViewWidth);

		btnSearch.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				searchButtonClick();
			}
		});

		filterer.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				showFilter();
			}
		});

		
		txtSearch.setOnEditorActionListener(new OnEditorActionListener() {
			@Override
			public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
				if (EditorInfo.IME_ACTION_SEARCH == actionId) {
					searchData.keyword = txtSearch.getText().toString();
					if (TextUtils.isEmpty(searchData.keyword))
						return false;

					preformSearch();
					return true;
				}
				return false;
			}
		});
	}

	private void initPagerAndTabs() {
		mPagerAdapter = new MainPagerAdapter(this, Categories.getCategories(), getSupportFragmentManager());
		mainPager.setAdapter(mPagerAdapter);
		categoryTabs.setViewPager(mainPager);
		categoryTabs.setSelectedIndicatorColors(Color.WHITE);
		categoryTabs.setBackgroundColor(getResources().getColor(R.color.itemBackground));
		categoryTabs.setDistributeEvenly(true);
	}


	private void showFilter() {
		View popupView = LayoutInflater.from(this).inflate(R.layout.filter_menu, null);

		Button set = (Button) popupView.findViewById(R.id.set);
		 switchOpenNow= (SwitchButton)popupView.findViewById(R.id.switchOpenNow);
		 button_A= (ToggleButton) popupView.findViewById(R.id.buttonCategory1);
		 button_B= (ToggleButton)  popupView.findViewById(R.id.buttonCategory2);
		 button_C= (ToggleButton)  popupView.findViewById(R.id.buttonCategory3);
		 button_D= (ToggleButton)  popupView.findViewById(R.id.buttonCategory4);
		 button_E= (ToggleButton)  popupView.findViewById(R.id.buttonCategory5);
		setFilterButtons();
		final PopupWindow popupWindow = new PopupWindow(popupView,
				ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT, true);

		popupWindow.setAnimationStyle(R.style.AnimationPopup);

		popupWindow.setOutsideTouchable(true);
        popupWindow.setBackgroundDrawable(new BitmapDrawable());
		set.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				String button_a_checked = button_A.isChecked() ? "0" : "1";
				String button_b_checked = button_B.isChecked() ? "0" : "1";
				String button_c_checked = button_C.isChecked() ? "0" : "1";
				String button_d_checked = button_D.isChecked() ? "0" : "1";
				String button_e_checked = button_E.isChecked() ? "0" : "1";
				String openNow 	= switchOpenNow.isChecked() ? "0" : "1";
//				"Custom1": "Medical",
//						"Custom2": "Recreational",
//						"Custom3": "Club",
//						"Custom4": "Coffeeshop"

				String checked = button_a_checked + ","
						+ button_b_checked + ","
						+ button_c_checked + ","
						+ button_d_checked + ","
						+ button_e_checked + ","
						+ openNow
						;

				Prefs.setMenuFilterButtons(checked);

				//refresh:


				filterSetListener.onFilterSet();


				popupWindow.dismiss();
			}
		});



		popupWindow.setTouchable(true);
		popupWindow.setFocusable(true);
		popupWindow.showAsDropDown(mToolbar);



	}

	private void setFilterButtons() {

		String checkedButtonsPrefs = Prefs.getMenuFilterButtons();
		String[] filterButtons = checkedButtonsPrefs.split(",");
		button_A.setChecked(filterButtons[0].contains("0"));
		button_B.setChecked(filterButtons[1].contains("0"));
		button_C.setChecked(filterButtons[2].contains("0"));
		button_D.setChecked(filterButtons[3].contains("0"));
		button_E.setChecked(filterButtons[4].contains("0"));
		switchOpenNow.setChecked(filterButtons[5].contains("0"));


	}


	private void searchButtonClick() {

		if (txtSearch.getText().toString().isEmpty()) {
			toggleSearchBox();
		} else {
			preformSearch();
		}
	}

	private void preformSearch() {
		searchData.keyword = txtSearch.getText().toString();
		mainPager.setCurrentItem(Categories.getSearchFragmentPosition(), true);
	}

	private void toggleSearchBox() {
		int targetWidth = mIsSearchExpanded ? 0 : mSearchViewWidth;
		AnimationHelper.resizeWidth(txtSearch, targetWidth, 350);
		mIsSearchExpanded = !mIsSearchExpanded;
		mToolbar.invalidate();
	}

	private void initDrawer() {
		txtFilter.setOnClickListener(menuClickListener);
		txtLogin.setOnClickListener(menuClickListener);
		txtLogout.setOnClickListener(menuClickListener);
		txtSwitchLanguage.setOnClickListener(menuClickListener);
		txtManageReservations.setOnClickListener(menuClickListener);
		txtManageProducts.setOnClickListener(menuClickListener);
		txtManagePromos.setOnClickListener(menuClickListener);

		System.out.println("isManager -> " + Prefs.isManager());
		setIsManager(Prefs.isManager());
	}

	private void setIsManager(boolean isManager) {
		if (isManager) {
			txtManageReservations.setVisibility(View.VISIBLE);
			txtManageProducts.setVisibility(View.VISIBLE);
			txtManagePromos.setVisibility(View.VISIBLE);

		} else {
			txtManageReservations.setVisibility(View.GONE);
			txtManageProducts.setVisibility(View.GONE);
			txtManagePromos.setVisibility(View.GONE);
		}
	}

	@Override
	public void onAppuserLoginSuccess(Appuser appUser) {
		if (appUser == null || !appUser.IsAuthed)
			return;

		userLoggedIn(appUser);
	}

	private void userLoggedIn(Appuser appUser) {
		profileMenuItem.setVisibility(View.VISIBLE);
		txtLogin.setVisibility(View.GONE);
		txtLogout.setVisibility(View.VISIBLE);
 
 
		if (!appUser.FirstName.isEmpty() || !appUser.LastName.isEmpty()) {
			String name = appUser.FirstName + " " + appUser.LastName;
			txtUsername.setText(name);
		} else {
			txtUsername.setText(appUser.Email);
		}


		if(!appUser.FBID.matches("0")){
			ImageLoader.loadUserProfilePic(this, appUser.Avatar, imgProfilePic);
		}else{
			ImageLoader.loadUserProfilePic(this, appUser.Image, imgProfilePic);
		}

	}

	@Override
	public void onAppuserLoginFail(RetrofitError e) {
		userLoggedOut();
	}

	private void userLoggedOut() {
		txtLogin.setVisibility(View.VISIBLE);
		txtLogout.setVisibility(View.GONE);
		profileMenuItem.setVisibility(View.GONE);
		setIsManager(false);
		return;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (mDrawerToggle.onOptionsItemSelected(item))
			return true;

		return true;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		return true;
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		mDrawerToggle.onConfigurationChanged(newConfig);
	}
	
	private void restartActivity() {
		Intent intent = getIntent();
		finish();
		startActivity(intent);
	}

	protected void menuItemClick(View v) {
		switch (v.getId()) {
		case R.id.login:
			DialogHelper.loginDialog(getSupportFragmentManager(), this);
			break;
		case R.id.logout:
			promptLogout();
			break;
		case R.id.filter:
			Intent i = new Intent(MainActivity.this, FilterActivity.class); 
			startActivityForResult(i, ActivityStarter.RESULT_FILTER);
			break;
		case R.id.languages: 
			changeLang();
			break;
		case R.id.manageOrders:
			
			ActivityStarter.manage(this, ActivityStarter.MANAGE_ORDERS);
			mDrawerLayout.closeDrawers();
			break;
		case R.id.manageProducts:
			
			ActivityStarter.manage(this, ActivityStarter.MANAGE_PRODUCT);
			mDrawerLayout.closeDrawers();
			break;
		case R.id.managePromotions:
			
			ActivityStarter.manage(this, ActivityStarter.MANAGE_PROMOTION);
			mDrawerLayout.closeDrawers();
			break;
		}
	}

	private void promptLogout() {
		DialogHelper.messageDialog(getSupportFragmentManager(), R.string.dialog_logout_title, R.string.dialog_logout_message, new DialogListener() {
			@Override
			public void onOk(Dialog d) {
				d.dismiss();
				preformLogout();
			}

			@Override
			public void onCancel(Dialog d) {
				d.dismiss();
			}
		});
	}

	protected void preformLogout() {
		userLoggedOut();
		APIManager.logout(new Callback<UserResponse>() {
			@Override
			public void success(UserResponse result, Response arg1) {
				UserManager.setAppuser(result.data.Appuser);
				UserManager.setBaseAppuser(result.data.Appuser);
				LoginManager.getInstance().logOut();
				Prefs.setIsConnected(false);
			}

			@Override
			public void failure(RetrofitError arg0) {
				Appuser user = new Appuser();
				user.IsAuthed = false;
				UserManager.setAppuser(user);
				UserManager.setBaseAppuser(user);
				Prefs.setIsConnected(false);
				LoginManager.getInstance().logOut();
				
			}
		});
	}

	public static class SearchData {
		public String keyword;
	}

	@Override
	public void onConnected(Bundle arg0) {
		Location mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
		if (mLastLocation != null) {
			Appuser user = UserManager.getAppUser();
			user.Lat = mLastLocation.getLatitude()+"";
			user.Lng = mLastLocation.getLongitude()+"";
			UserManager.setAppuser(user);
		} else {
//			if (!mGpsDialogDisplayed) {
//				displayGpsDialog();
//			}
			mGpsDialogDisplayed = true;
			Log.e(TAG, "Location is null, prompt user");
		}

	}

	@Override
	public void onConnectionSuspended(int arg0) {
		mGoogleApiClient.connect();
	}

	@Override
	public void onConnectionFailed(ConnectionResult arg0) {}

	private void displayGpsDialog() {
		DialogHelper.messageDialog(getSupportFragmentManager(), getString(R.string.location_dialog_title),
				getString(R.string.location_dialog_message), new DialogListener() {

					@Override
					public void onOk(Dialog d) {
						ActivityStarter.openGPSSettings(MainActivity.this);
						d.dismiss();
					}

					@Override
					public void onCancel(Dialog d) {
						d.dismiss();
					}
				});
	}

	@Override
	public void onCheckManager(boolean isManager) {
		setIsManager(isManager);
	}
 
	
	private class PageListener extends SimpleOnPageChangeListener{
        public void onPageSelected(int position) {
            Log.i(TAG, "page selected " + position);
            pagePosition = position;
    }
	}

	private void changeLang() {
		System.out.println("Language -> " + Prefs.getAppLanguage());
		if (Prefs.getAppLanguage().matches("he")
				|| Prefs.getAppLanguage().matches("iw")) {
			Prefs.setAppLanguage("en");
		} else if (Prefs.getAppLanguage().matches("en")) {
			Prefs.setAppLanguage("he");
		}
		App.initAppLanguage();
		APIManager.initApp(this, new Callback<UserResponse>() {
		@Override
		public void success(UserResponse result, Response response) {
			UserManager.setAppuser(result.data.Appuser);
			restartActivity();
		}	

		@Override
		public void failure(RetrofitError arg0) {
 
		}
	});
		

	}
	
	private void getCategories() {
		APIManager.api.getCategories(APIManager.makeUserParams(), new Callback<CategoryGetList>() {
			@Override
			public void success(CategoryGetList result, Response response) {
				ArrayList<Category> categories = result.data.Categories;
//				if (categories!=null){
//					for (Category c:categories)
//						System.out.println("niv cate "+c.Name);
//				}

				Collections.sort(categories);
				Categories.setCategories(categories);
				initPagerAndTabs();
			}

			@Override
			public void failure(RetrofitError retrofitError) {

			}
		}); 
		
	}
	
	public void handleNotifications() {
		if (Prefs.getFromNotification()) {
			String message = Prefs.getNotificationMessage();
			long businessId = Prefs.getNotificationBusinessId();
			
			switch (Prefs.getFromNotificationType()) {
			case SIMPLE_ALERT:
				showDialogMessage(getString(R.string.message), message, 0, true);
				break;
			case BUSINNES_ALERT:
				showDialogMessage(getString(R.string.message), message, businessId , false);
				break;
			case ADMIN_RESERVATION_ALERT:
				 
				break;
			case USER_RESERVATION_ALERT:
				 
				break;
			default:
				break;
			}
			
			Prefs.setNotificationBusinessId(0);
			Prefs.setNotificationMessage("");
			Prefs.setFromNotification(false);
			cancelNotification();
		}

	}
	
	private void showDialogMessage(String title, String message, final long businessID, boolean showOk) {
		DialogHelper.messageDialog(getSupportFragmentManager(), title, message, showOk, new DialogListener() {
			@Override
			public void onOk(Dialog d) {
				BusinessRepository.tempBusiness = BusinessRepository.findBussinessById(businessID);
				ActivityStarter.viewBusiness(MainActivity.this, businessID, 0);
				d.dismiss(); 
			}

			@Override
			public void onCancel(Dialog d) {
				d.dismiss();
			}
		});
	}
	
	 
	public void cancelNotification() {
	    String ns = Context.NOTIFICATION_SERVICE;
	    NotificationManager nMgr = (NotificationManager)getSystemService(ns);
//	    nMgr.cancel(notifyId);
	    nMgr.cancelAll();
	}

	@Override
	public void onBackPressed() {
		if (mDrawerLayout.isDrawerOpen(Gravity.LEFT)) {
			mDrawerLayout.closeDrawer(Gravity.LEFT);
		}else {
			super.onBackPressed();
		}
	}
}