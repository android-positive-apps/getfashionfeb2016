/**
 * 
 */
package com.positiveapps.getFashion.util;

import android.content.Context;
import android.os.Build;
import android.provider.Settings.Secure;

public class DeviceUtil {
	
	
	public static String getDeviceUDID(Context context) {

		return Secure.getString(context.getContentResolver(),
				Secure.ANDROID_ID);
	}
	
	public static String getDeviceName() {
		String manufacturer = Build.MANUFACTURER;
		String model = Build.MODEL;
		if (model.startsWith(manufacturer)) {
			return capitalize(model);
		} else {
			return capitalize(manufacturer) + " " + model;
		}
	}

	public static String getDeviceVersion () {
		
		String myVersion = "";
		if (android.os.Build.VERSION.RELEASE != null){
			myVersion = android.os.Build.VERSION.RELEASE;
		}
		return myVersion;
	}
	
	private static String capitalize(String s) {
		if (s == null || s.length() == 0) {
			return "";
		}
		char first = s.charAt(0);
		if (Character.isUpperCase(first)) {
			return s;
		} else {
			return Character.toUpperCase(first) + s.substring(1);
		}
	} 
	
	
	
	

}
