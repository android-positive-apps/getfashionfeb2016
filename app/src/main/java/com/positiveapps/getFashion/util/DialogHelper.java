package com.positiveapps.getFashion.util;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.widget.Toast;

import com.positiveapps.getFashion.dialogs.CooserDialog;
import com.positiveapps.getFashion.dialogs.DialogCallback;
import com.positiveapps.getFashion.dialogs.LoginDialog;
import com.positiveapps.getFashion.dialogs.MessageDialog;
import com.positiveapps.getFashion.dialogs.MessageDialog.DialogListener;
import com.positiveapps.getFashion.dialogs.ProductDescriptionDialog;
import com.positiveapps.getFashion.dialogs.PromotionDialog;
import com.positiveapps.getFashion.dialogs.RegisterDialog;
import com.positiveapps.getFashion.dialogs.ReservationDialog;
import com.positiveapps.getFashion.interfaces.AppuserListener;
import com.positiveapps.getFashion.jsonmodels.Business;
import com.positiveapps.getFashion.jsonmodels.Product;
import com.positiveapps.getFashion.jsonmodels.Promotion;

public class DialogHelper {
	public static void promotionReservationDialog(FragmentManager fm, Business business, Promotion promotion) {
		ReservationDialog.withPromotion(business, promotion).show(fm, ReservationDialog.TAG);
	}
	
	public static void bussinessReservationDialog(FragmentManager fm, Business business, Promotion promotion) {
		ReservationDialog.withPromotion(business, promotion).show(fm, ReservationDialog.TAG);
	}

	public static void productReservationDialog(FragmentManager fm, Business business, Product product) {
		ReservationDialog.withProduct(business, product).show(fm, ReservationDialog.TAG);
	}

	public static void promotionDescriptionDialog(FragmentManager fm, Business business, Promotion promotion) {
		ProductDescriptionDialog.withPromotion(business, promotion).show(fm, ProductDescriptionDialog.TAG);
	}

	public static void productDescriptionDialog(FragmentManager fm, Business business, Product product) {
		ProductDescriptionDialog.withProduct(business, product).show(fm, ProductDescriptionDialog.TAG);
	}
	
	public static void registerDialog(FragmentManager fm, AppuserListener listener){
		RegisterDialog.newInstance(listener).show(fm, RegisterDialog.TAG);
	}
	
	public static void loginDialog(FragmentManager fm, AppuserListener listener){
		LoginDialog.newInstance(listener).show(fm, RegisterDialog.TAG);
	}
	
	public static void messageDialog(FragmentManager fm, String title, String message, DialogListener listener){
		MessageDialog.newInstance(title, message, listener).show(fm, MessageDialog.TAG);
	}
	
	public static void messageDialog(FragmentManager fm, int titleRes, int messageRes, DialogListener listener){
		MessageDialog.newInstance(titleRes, messageRes, listener).show(fm, MessageDialog.TAG);
	}
	 
	
	public static void messageDialog(FragmentManager fm, String title, String message, boolean hideOkButton, DialogListener listener){
		MessageDialog.newInstance(title, message, hideOkButton , listener).show(fm, MessageDialog.TAG);
	}
	
	public static void error(Context context, String message) {
		Toast.makeText(context, message, Toast.LENGTH_LONG).show();
	}
	public static void error(Context context, int message) {
		Toast.makeText(context, message, Toast.LENGTH_LONG).show();
	}

	public static void message(Context context, int message) {
		Toast.makeText(context, message, Toast.LENGTH_LONG).show();
	}
	public static void message(Context context, String message) {
		Toast.makeText(context, message, Toast.LENGTH_LONG).show();
	}
	
	public static void showDialogChooser(Fragment parentFragment, String[] optionsText,
			DialogCallback callback){
		
		CooserDialog dialog = new CooserDialog(parentFragment,optionsText,callback);
		dialog.show(parentFragment.getFragmentManager(), CooserDialog.DIALOG_NAME);
	}
	
	// din added
		public static void promotionDialog(FragmentManager fm, Business business, Promotion promotion, Bitmap bitmap) {
			PromotionDialog.newInstance(promotion,business, bitmap).show(fm, PromotionDialog.TAG);
		}
}
