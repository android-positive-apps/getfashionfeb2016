/**
 * 
 */
package com.positiveapps.getFashion.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import com.positiveapps.getFashion.R;

import android.content.Context;
import android.util.Log;

/**
 * @author Nati Gabay
 *
 */
public class DateUtil {

	private static String[] months = { "January", "February", "March", "April",
			"May", "June", "July", "August", "September", "October",
			"November", "December" };
	
	
	public static Calendar calendarFromString(String dateString, String pattern){
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat format = new SimpleDateFormat(pattern);
		Date date;
		try {
			date = format.parse(dateString);
			cal.setTime(date);
		} catch (ParseException e) {
			cal=null;
		}
		
		
		return cal;
	}
	
	public static String changeStringFormat(String Date){
		SimpleDateFormat fromUser = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		SimpleDateFormat myFormat = new SimpleDateFormat("dd.MM.yy");
		String reformattedStr = "";
		try {

			reformattedStr = myFormat.format(fromUser.parse(Date));
		} catch (ParseException e) {
		    e.printStackTrace();
		}
		return reformattedStr;

	}
	
	public static Date stringHour2Millisecond(String time) {
		SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
		Calendar cal = Calendar.getInstance();
		try {
			Date date = sdf.parse(time);
			int year = cal.get(Calendar.YEAR);
			int month = cal.get(Calendar.MONTH); 
			int day = cal.get(Calendar.DAY_OF_MONTH); 
			cal.setTime(date);
			cal.set(year, month, day);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
		}
		return cal.getTime();
	}

	public static String getCurrentDateAsString(String format) {
		Calendar calendar = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		return sdf.format(calendar.getTime());
	}
	
	public static String formatCalendar(Calendar calendar, String pattern){
		SimpleDateFormat format=new SimpleDateFormat(pattern);
		return format.format(calendar.getTime());
	}
	
	public static Date dateFromString(String dateString, String pattern){
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat format = new SimpleDateFormat(pattern);
		Date date;
		try {
			return format.parse(dateString);
		} catch (ParseException e) {
			e.printStackTrace();
			
		}
		
		return null;
	}
	
	public static String formatDate(Date date, String pattern){
		SimpleDateFormat format=new SimpleDateFormat(pattern);
		return format.format(date);
	}
	

	public static String getCurrentTimeAsString() {
		Calendar calendar = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy - hh:mm:ss");
		Log.e("timelog", "currentTime = " + calendar.getTimeInMillis());
		return sdf.format(calendar.getTime());
	}

	public static String getCurrentTimeAsServerString() {
		Calendar calendar = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		Log.e("timelog", "currentTime = " + calendar.getTimeInMillis());
		return sdf.format(calendar.getTime());
	}

	public static String getDateAsStringByMilli(String dilimeter, long milli,
			boolean time) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTimeInMillis(milli);
		SimpleDateFormat sdf = null;
		if (!time) {
			sdf = new SimpleDateFormat("dd" + dilimeter + "MM" + dilimeter
					+ "yyyy");
		} else {
			sdf = new SimpleDateFormat("dd" + dilimeter + "MM" + dilimeter
					+ "yyyy hh:mm:ss");
		}

		return sdf.format(calendar.getTime());
	}
	
	public static String getDateAsStringByMilliNoSeconds(String dilimeter, long milli) {
		Calendar calendar = Calendar.getInstance();
		
		calendar.setTimeInMillis(milli);
		SimpleDateFormat sdf = null;
	
			sdf = new SimpleDateFormat("hh:mm   dd" + dilimeter + "MM" + dilimeter
					+ "yyyy");


		return sdf.format(calendar.getTime());
	}

	public static String getTimeAsStringByMilli(long milli) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTimeInMillis(milli);
		SimpleDateFormat sdf = new SimpleDateFormat("hh" + ":" + "mm");
		return sdf.format(calendar.getTime());
	}

	public static String getClosestFutureSringDateByDayNumber(int dayNumber) {
		Calendar today = Calendar.getInstance();
		Calendar futureDate = Calendar.getInstance();

		if (dayNumber >= 1 && dayNumber <= 7) {
			futureDate.set(Calendar.DAY_OF_WEEK, dayNumber);
			if (futureDate.before(today)) {
				futureDate.add(Calendar.DATE, 7);
			}
		}

		return getDateAsStringWithoutYearByMilli("/",
				futureDate.getTimeInMillis(), false);
	}

	public static Calendar getClosestFutureCalendarDateByDayNumber(int dayNumber) {
		Calendar today = Calendar.getInstance();
		Calendar futureDate = Calendar.getInstance();

		if (dayNumber >= 1 && dayNumber <= 7) {
			futureDate.set(Calendar.DAY_OF_WEEK, dayNumber);
			if (futureDate.before(today)) {
				futureDate.add(Calendar.DATE, 7);
			}
		}

		return futureDate;
	}

	public static String getDateAsStringWithoutYearByMilli(String dilimeter,
			long milli, boolean time) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTimeInMillis(milli);
		SimpleDateFormat sdf = null;
		if (!time) {
			sdf = new SimpleDateFormat("dd" + dilimeter + "MM");
		} else {
			sdf = new SimpleDateFormat("dd" + dilimeter + "MM" + " hh:mm:ss");
		}

		return sdf.format(calendar.getTime());
	}

	public static String getMonthName(int month) {
		return months[month - 1];
	}

	public static String getTimeAsString(int hour, int mintue) {
		String h = hour + "";
		String m = mintue + "";
		if (hour < 10) {
			h = "0" + h;
		}
		if (mintue < 10) {
			m = "0" + m;
		}
		String time = h + ":" + m;
		return time;
	}

	public static String getDateAsString(int day, int month, int yeers) {
		String d = day + "";
		String m = (month + 1) + "";
		String y = yeers + "";
		if (day < 10) {
			d = "0" + d;
		}
		if (month < 10) {
			m = "0" + m;
		}
		String date = d + "/" + m + "/" + y;
		return date;
	}

	public static long getDateInMilli(int day, int month, int yeers) {
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.YEAR, yeers);
		calendar.set(Calendar.MONTH, month - 1);
		calendar.set(Calendar.DAY_OF_MONTH, day);

		return calendar.getTimeInMillis();
	}

	public static String dayName(Context context, int day) {
		String strDay = "";
		switch (day) {
		case 1:
			strDay = context.getResources().getString(
					R.string.day_1);
			break;

		case 2:
			strDay = context.getResources().getString(
					R.string.day_2);
			break;

		case 3:
			strDay = context.getResources().getString(
					R.string.day_3);
			break;

		case 4:
			strDay =context.getResources().getString(
					R.string.day_4);
			break;

		case 5:
			strDay = context.getResources().getString(
					R.string.day_5);
			break;

		case 6:
			strDay = context.getResources().getString(
					R.string.day_6);
			break;

		case 7:
			strDay = context.getResources().getString(
					R.string.day_7);
			break;

		default:
			break;
		}

		return strDay;
	}

	public static long getTimeInMilli(int hower, int seconds) {
		long h = hower * 360000;
		long s = seconds * 60000;
		return h + s;
	}

	public static int[] getBestDate(String date) {

		Calendar calendar = Calendar.getInstance();
		int year = calendar.get(Calendar.YEAR);
		int month = calendar.get(Calendar.MONTH) + 1;
		int day = calendar.get(Calendar.DAY_OF_MONTH);

		String curretnSetDate[] = date.split("/");
		if (curretnSetDate.length == 3) {
			year = Integer.parseInt(curretnSetDate[2]);
			month = Integer.parseInt(curretnSetDate[1]) - 1;
			day = Integer.parseInt(curretnSetDate[0]);
		}

		int[] result = { day, month, year };
		return result;
	}

	public static int[] getBestTime(String time) {
		Calendar calendar = Calendar.getInstance();
		int hour = calendar.get(Calendar.HOUR_OF_DAY);
		int minute = calendar.get(Calendar.MINUTE);

		String[] curretnSetTime = time.split(":");
		if (curretnSetTime.length == 2) {
			hour = Integer.parseInt(curretnSetTime[0]);
			minute = Integer.parseInt(curretnSetTime[1]);
		}
		int[] result = { hour, minute };
		return result;
	}

	public static long getCurrentDateInMilli() {
		Calendar calendar = Calendar.getInstance();
		return calendar.getTimeInMillis();
	}

	public static String getDateDescriptionByDate(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		String month = months[calendar.get(Calendar.MONTH)];

		return month + " " + calendar.get(Calendar.DAY_OF_MONTH) + "."
				+ calendar.get(Calendar.YEAR);
	}

	public static String getRangBetweenTwoDatesDescription(Date date1,
			Date date2) {

		SimpleDateFormat sdf = new SimpleDateFormat("hh:mm");

		String from = sdf.format(date1);
		String to = sdf.format(date2);
		return from + " - " + to;
	}

	public static String getRangBetweenTwoDates(Date date1, Date date2) {
		long from = date1.getTime();
		long to = date2.getTime();
		long disTance = to - from;
		if (disTance <= 0) {
			return String.valueOf(0);
		}
		long result = disTance / 60000;
		return String.valueOf(result);
	}

	public static long getDateMillisByServerDate(String serverDate) {
		long result = 0;
		String[] date = serverDate.split(" ");
		Calendar calendar = Calendar.getInstance();

		if (date.length > 0) {
			String[] justDate = date[0].split("-");
			int years = Integer.parseInt(justDate[0]);
			int munts = Integer.parseInt(justDate[1]) - 1;
			int days = Integer.parseInt(justDate[2]);

			String[] justTime = date[1].split(":");
			int hours = Integer.parseInt(justTime[0]);
			int minutes = Integer.parseInt(justTime[1]);
			int seconds = Integer.parseInt(justTime[2]);

			// GetEatApp.Log("t: " + years + "-" + munts + "-" + days + " "
			// + hours + ":" + minutes + ":" + seconds);

			calendar.set(years, munts, days, hours, minutes, seconds);
			result = calendar.getTimeInMillis();
		}
		/*
		 * GetEatApp.Log("calculated milis :" + result + "\n"
		 * +"current:         :" + Calendar.getInstance().getTimeInMillis());
		 */
		return result;
	}

	public static long getTodaysMillisByTime(String time) {
		long result = 0;
		String[] timeArr = time.split(":");
		Calendar calendar = Calendar.getInstance();

		if (timeArr.length > 0) {
			int hours = Integer.parseInt(timeArr[0]);
			int minutes = Integer.parseInt(timeArr[1]);
			int seconds = Integer.parseInt(timeArr[2]);

//			 App.Log("t: " + calendar.get(Calendar.YEAR) + "-" + calendar.get(Calendar.MONTH) + "-" + calendar.get(Calendar.DAY_OF_MONTH) + " "
//			 + hours + ":" + minutes + ":" + seconds);

			calendar.set(calendar.get(Calendar.YEAR),
					calendar.get(Calendar.MONTH),
					calendar.get(Calendar.DAY_OF_MONTH), hours, minutes,
					seconds);
			
//			App.Log("promo time: "+calendar.toString());
			result = calendar.getTimeInMillis();
		}
		/*
		 * GetEatApp.Log("calculated milis :" + result + "\n"
		 * +"current:         :" + Calendar.getInstance().getTimeInMillis());
		 */
		return result;
	}

	public static int getDaysOfServerDate(String serverDate) {
		int result = 0;
		String[] date = serverDate.split(" ");
		if (date.length > 0) {
			String[] jastDate = date[0].split("-");
			result = Integer.parseInt(jastDate[2]);
		}
		return result;
	}

	public static String getServerDateAsString(String serverDate) {
		if (serverDate.isEmpty()) {
			return "";
		}
		String[] dateAndTime = serverDate.split("T");
		if (dateAndTime.length == 1) {
			return serverDate;
		} else {
			return dateAndTime[0] + " " + dateAndTime[1];
		}
	}

}
