package com.positiveapps.getFashion.util;

import android.content.Context;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;

import com.positiveapps.getFashion.R;
import com.squareup.picasso.Picasso;

public class ImageLoader {
	public static final int NO_IMAGE_RES = R.drawable.img_no_items;
	public static final int PLACEHOLDER_RES = R.drawable.img_no_items;

	public static void loadBusinessLogo(Context context, String path, ImageView target) {
		if(TextUtils.isEmpty(path))
			return;
		
		int targetWidth = dpToPx(context, 50);
		int targetHeight = dpToPx(context, 50);
		Picasso.with(context).load(path).resize(targetWidth, targetHeight).into(target);
	}
	
	public static void loadUserProfilePic(Context context, String path, ImageView target) {
		if(TextUtils.isEmpty(path))
			return;
		
		int targetWidth = dpToPx(context, 50);
		int targetHeight = dpToPx(context, 50);
		Picasso.with(context).load(path).resize(targetWidth, targetHeight).into(target);
	}

	public static void loadProductImage(Context context, String path, ImageView target) {
		
		if(TextUtils.isEmpty(path))
			return;
		Picasso.with(context).load(path).error(R.mipmap.ic_launcher).placeholder(R.mipmap.ic_launcher).into(target);
	}
	
	public static void loadDefaultProductImage(Context context, ImageView target) {
		
		target.setScaleType(ScaleType.CENTER_INSIDE);
		Picasso.with(context).load(PLACEHOLDER_RES).into(target);
	}

	public static int dpToPx(Context c, int dp) {
		
		DisplayMetrics displayMetrics = c.getResources().getDisplayMetrics();
		return (int) ((dp * displayMetrics.density) + 0.5);
	}
}
