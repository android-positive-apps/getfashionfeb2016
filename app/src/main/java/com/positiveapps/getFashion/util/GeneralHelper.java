/**
 * 
 */
package com.positiveapps.getFashion.util;

import java.io.File;
import java.util.concurrent.atomic.AtomicInteger;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Toast;

import com.positiveapps.getFashion.R;

/**
 * Class with some utility methods that can be useful
 * 
 * @author Maor
 *
 */
public class GeneralHelper {
	
	public static final int GALLARY_REQUEST_CODE = 1001;
	public static final int CAMERA_REQUEST_CODE = 1002;
	public static final int CONTACT_REQUEST = 9003;
	
	public static final String APP_FOLDER =  "/photos";
	public static final String CAMERA_PROFILE_PATH =  APP_FOLDER + "/bussiness.jpg";
	
	
	public static boolean ValidateOrPaintRed(EditText... editTexts){
		for (EditText editText : editTexts) {
			if(editText.getText().toString().trim().isEmpty()){
				editText.setHintTextColor(Color.RED);
				return false;
			}	
		}
		return true;
	}

	public static void createSDFolder(){
		File folder = new File(Environment.getExternalStorageDirectory() + APP_FOLDER);
		if (!folder.exists()) {
			folder.mkdir();
		}
	}
	
	public static String getProfileImageAsBase64(Bitmap selectedImage){
//		String path = VaadApp.userProfil.getImagePathUri();
//		System.out.println("PATH " + path);
		if (selectedImage != null){
//			Bitmap selectedImage = BitmapFactory.decodeFile(path);
//			selectedImage = BitmapUtil.crupAndScale(selectedImage, 300);
			String path = BitmapUtil.getImageAsStringEncodedBase64(selectedImage);
			return path;
		}
		return "";
	}
	
	public static void openCamera(Fragment fragment) {
		createSDFolder();
		Intent takePicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		Uri imageFileUri = Uri.parse("file:///sdcard/" + CAMERA_PROFILE_PATH);
		takePicture.putExtra(android.provider.MediaStore.EXTRA_OUTPUT, imageFileUri);
		fragment.startActivityForResult(takePicture,CAMERA_REQUEST_CODE);
	}

	public static void openGallery(Fragment fragment) {
		Intent pickPhoto = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
		fragment.startActivityForResult(pickPhoto, GALLARY_REQUEST_CODE);

	}
	private static final AtomicInteger sNextGeneratedId = new AtomicInteger(1);

	public static int generateViewId() {
		for (;;) {
			final int result = sNextGeneratedId.get();
			// aapt-generated IDs have the high byte nonzero; clamp to the range
			// under that.
			int newValue = result + 1;
			if (newValue > 0x00FFFFFF)
				newValue = 1; // Roll over to 1, not 0.
			if (sNextGeneratedId.compareAndSet(result, newValue)) {
				return result;
			}
		}
	}

	public static boolean isAppInstalledOrNot(Context context, String uri) {
		PackageManager pm = context.getPackageManager();
		boolean app_installed = false;
		try {
			pm.getPackageInfo(uri, PackageManager.GET_ACTIVITIES);
			app_installed = true;
		} catch (PackageManager.NameNotFoundException e) {
			app_installed = false;
		}
		return app_installed;
	}

	public static void sendMessageViaWhatsapp(Context context, String message) {

		try {
			if (!isAppInstalledOrNot(context, "com.whatsapp")) {
				Toast.makeText(
						context,
						context.getString(
								R.string.no_whatsapp), Toast.LENGTH_LONG)
						.show();
			} else {

				Intent waIntent = new Intent(Intent.ACTION_SEND);
				waIntent.setType("text/plain");
				waIntent.setPackage("com.whatsapp");
				waIntent.putExtra(Intent.EXTRA_TEXT, message);
				context.startActivity(waIntent);

			}
		} catch (Exception ex) {
			Toast.makeText(context, "Could not open Whatsapp!",
					Toast.LENGTH_LONG).show();
		}

	}

	public static void findLocationViaWaze(Context context, String address) {
		if (!isAppInstalledOrNot(context, "com.waze")) {
			Toast.makeText(context, "Waze is not installed on this device!",
					Toast.LENGTH_LONG).show();
		} else {
			try {
				String url = "waze://?q=" + address;
				Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
				context.startActivity(intent);
			} catch (ActivityNotFoundException ex) {
				Toast.makeText(context, "Could not open Waze!",
						Toast.LENGTH_LONG).show();
				/*
				 * Intent intent = new Intent( Intent.ACTION_VIEW, Uri.parse(
				 * "market://details?id=com.waze" ) );
				 * context.startActivity(intent);
				 */
			}
		}
	}

	public static void openGetTaxi(Context context) {
		try {
			if (!isAppInstalledOrNot(context, "com.gettaxi.android")) {
				Toast.makeText(
						context,
						context.getString(
								R.string.no_gettaxi), Toast.LENGTH_LONG).show();
			} else {

				Intent LaunchIntent = context.getPackageManager()
						.getLaunchIntentForPackage("com.gettaxi.android");
				context.startActivity(LaunchIntent);

			}
		} catch (Exception ex) {
			Toast.makeText(context, "Could not open GetTaxi!",
					Toast.LENGTH_LONG).show();
		}
	}

	public static void openBrowserLink(Context context, String link) {
		Intent linkIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(repairURL(link)));
		linkIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		try {
			context.startActivity(linkIntent);
		} catch (ActivityNotFoundException e) {
			Toast.makeText(context, R.string.cant_open_site, Toast.LENGTH_SHORT).show();
		}
	}

	private static String repairURL(String url) {
		if (!url.startsWith("http://") && !url.startsWith("https://")) {
			url = "http://" + url;
		}
		return url;
	}
	
	public static void actionSend(Activity activity, String title, String body){
		Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT,title);
        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, body);
        activity.startActivity(Intent.createChooser(sharingIntent, "Share via"));
	}
	
	public static void sendEmail(Context context, String address, String subject, String body){
		Intent i = new Intent(Intent.ACTION_SEND);
		i.setType("message/rfc822");
		i.putExtra(Intent.EXTRA_EMAIL  , new String[]{address});
		i.putExtra(Intent.EXTRA_SUBJECT, subject);
		i.putExtra(Intent.EXTRA_TEXT   , body);
		try {
			context.startActivity(Intent.createChooser(i, "���� ����..."));
		} catch (android.content.ActivityNotFoundException ex) {
		    Toast.makeText(context, "There are no email clients installed.", Toast.LENGTH_SHORT).show();
		}
	}
	
	public static void openDialer(Context context, String tel){
		Intent intent = new Intent(Intent.ACTION_DIAL);
		intent.setData(Uri.parse("tel:" +tel));
		context.startActivity(intent); 
	}
	public static void closeKeyboard(Context context, EditText... myEditText){
    	for (EditText editText : myEditText) {
    		InputMethodManager imm = (InputMethodManager)context.getSystemService(
    	    	      Context.INPUT_METHOD_SERVICE);
    	    	imm.hideSoftInputFromWindow(editText.getWindowToken(), 0);
		}
    }
	
	public static double distance(double lat1, double lon1, double lat2, double lon2) {
	      double theta = lon1 - lon2;
	      double dist = Math.sin(deg2rad(lat1)) * Math.sin(deg2rad(lat2)) + Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * Math.cos(deg2rad(theta));
	      dist = Math.acos(dist);
	      dist = rad2deg(dist);
	      dist = dist * 60 * 1.1515;
	       return (dist);
	    }
	
	public static double deg2rad(double deg) {
	      return (deg * Math.PI / 180.0);
	    }
	public static double rad2deg(double rad) {
	      return (rad * 180.0 / Math.PI);
	    }
	
	
}
