package com.positiveapps.getFashion.util;

import android.content.Context;

import com.positiveapps.getFashion.R;
import com.positiveapps.getFashion.backend.Prefs;
import com.positiveapps.getFashion.jsonmodels.Business;

public class StringFormatUtils {
	public static String getBusinessFormattedAddress(String language, Business b) {
		String formattedAddress = "";

		if (Prefs.LANGUAGE_HEB.equals(language)) {
			formattedAddress = b.AddressStreet + " " + b.AddressHouse + ", " + b.AddressCity;
		} else if (Prefs.LANGUAGE_ENG.equals(language)) {
			formattedAddress = b.AddressHouse + " " + b.AddressStreet + "st. " + b.AddressCity;
		}

		return formattedAddress;
	}

	public static String getFormattedPrice(Context context, double price) {
		double d = price;
		String string = "";
		if(d % 1 == 0){
			int p = (int) price;
			string = context.getString(R.string.price_format, p);
		}else{
			string =  context.getString(R.string.nis) + price;
		}

		return string;
	}
	
	public static String getFormattedDiscount(Context context, double oldPrice, double newPrice) {
		float oldPriceF = (float) oldPrice;
		float newPriceF = (float) newPrice;
		float discount = (oldPriceF - newPriceF)/oldPriceF  *100;
		
		return  Math.round(discount) + "%";
	}
	
	public static String getAmountLeft(Context context, int amount) {
		return context.getString(R.string.promotion_amount_left, amount);
	}	
	
	public static String getPromotionEndsIn(Context context, long millisUntilFinished) {
		int hours = (int) (((millisUntilFinished / 1000) / 60) / 60);
		int minutes = (int) ((millisUntilFinished / 60000) % 60);
		int seconds = (int) ((millisUntilFinished / 1000) % 60);
		String hourString = hours <= 9 ? "0" + hours : "" + hours;
		String minutesString = minutes <= 9 ? "0" + minutes : "" + minutes;
		String secondsString = seconds <= 9 ? "0" + seconds : "" + seconds;

		String timeStr = hourString + ":" + minutesString + ":" + secondsString;
		
		return context.getString(R.string.promotion_ends_in, timeStr);
	}
}
