/**
 * 
 */
package com.positiveapps.getFashion.util;




import com.positiveapps.getFashion.ManageActivity;
import com.positiveapps.getFashion.R;
import com.positiveapps.getFashion.ViewBusinessActivity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.v4.app.Fragment;
import android.widget.Toast;

/**
 * @author natiapplications
 *
 */
public class ActivityStarter {
	
	public static final String MANAGE = "manageExtra";
	
	public static final int MANAGE_ORDERS = 1;
	public static final int MANAGE_PROMOTION = 2;
	public static final int MANAGE_PRODUCT = 3;

	public static final int RESULT_FILTER = 1001;
	
 
	public static void viewBusiness(Context context, long businessId, long catId){
		Intent intent =new Intent(context, ViewBusinessActivity.class);
		intent.putExtra(ViewBusinessActivity.EXTRA_BUSINESS_ID, businessId);
		intent.putExtra(ViewBusinessActivity.EXTRA_CATEGORY_ID, catId);
		context.startActivity(intent);
	}
	
	public static void manage(Context context, int extra){
		Intent intent =new Intent(context, ManageActivity.class);
		intent.putExtra(MANAGE, extra);
		context.startActivity(intent);
	}
	
	public static void start(Context context, Class<?> clazz){
		context.startActivity(new Intent(context, clazz));
	}
	
	public static void share(Context context, String message){
		Intent intent = new Intent(Intent.ACTION_SEND);
		intent.putExtra(Intent.EXTRA_TEXT, message);
		intent.setType("text/plain");
		
		context.startActivity(Intent.createChooser(intent, "Select application:"));
	}
	
	public static void openLocationChooser (Activity activity,String lat,String lon) {
		Uri location = Uri.parse("geo:0,0?q="+lat + "," + lon);
		 Intent intent = new Intent(Intent.ACTION_VIEW,location);
        activity.startActivity(intent);
	}
	
	public static void openSMSScreen (Activity activity,String message,String address) {
        Intent intent = new Intent("android.intent.action.VIEW");
        Uri data = Uri.parse("sms:"+address);
        intent.setData(data);
        intent.putExtra("address", address);
        intent.putExtra("sms_body", message);
       
        
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        activity.startActivity(intent);
	}
	
	
	public static void openGallery (Fragment fragment,int requestCode){
		Intent pickPhoto = new Intent(Intent.ACTION_PICK);
		pickPhoto.setType("image/*");
		fragment.startActivityForResult(pickPhoto, requestCode);
	}
	
	public static void openCamera (Fragment fragment){
		Intent takePicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		fragment.startActivity(takePicture);
	}
	
	public static void openCameraForResult (Fragment fragment,int requestCode){
		Intent takePicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		fragment.startActivityForResult(takePicture, requestCode);
	}
	
	public static void openCameraForResultWithUri (Fragment fragment,int requestCode,Uri uri){
		Intent takePicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		takePicture.setData(uri);
		fragment.startActivityForResult(takePicture,requestCode);
	}
	
	public static void openImageInGallery (Activity activity,String phth) {
		Intent intent = null;
		intent = new Intent(android.content.Intent.ACTION_VIEW);
		intent.setDataAndType(Uri.parse("file://" + phth), "image/*");
		activity.startActivity(intent);
	}
	
	public static void makePhoneCall (Fragment fragment,String phone) {
		Uri callUri = Uri.parse("tel:" + phone);
		Intent callIntent = new Intent(Intent.ACTION_CALL,callUri);
		callIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_NO_USER_ACTION);
		fragment.startActivity(callIntent);
		
		
	}

	
	public static void dialer (Context context ,String phone) {
		Uri callUri = Uri.parse("tel:" + phone);
		Intent callIntent = new Intent(Intent.ACTION_DIAL,callUri);
//		callIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_NO_USER_ACTION);
		context.startActivity(callIntent);
	}
	
	public static void openGPSSettings (Fragment fragment) {
		fragment.startActivity(new Intent(
				Settings.ACTION_LOCATION_SOURCE_SETTINGS));
	}
	
	public static void openGPSSettings (Activity activiy) {
		activiy.startActivity(new Intent(
				Settings.ACTION_LOCATION_SOURCE_SETTINGS));
	}
	
	public static void openGPSSettingsForResult (Activity activity,int requestCode) {
		activity.startActivityForResult(new Intent(
				Settings.ACTION_LOCATION_SOURCE_SETTINGS),requestCode);
	}
	
	public static void openGPSSettingsForResult (Fragment fragment,int requestCode) {
		fragment.startActivityForResult(new Intent(
				Settings.ACTION_LOCATION_SOURCE_SETTINGS),requestCode);
	}
	
	public static void openBrowser (Fragment fragment,String url){
		Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
		fragment.getActivity().startActivity(browserIntent);
	}
	
	public static void openBrowser (Activity activity,String url){
		Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
		activity.startActivity(browserIntent);
	}
	
	public static void openWaze (Activity activity,String address) {
		
		try
		{
			Uri location = Uri.parse("waze://?q="+address);
			Intent intent = new Intent(Intent.ACTION_VIEW,location);
			activity.startActivity(intent);
		}
		catch ( Exception ex  )
		{
		   Intent intent =
		    new Intent( Intent.ACTION_VIEW, Uri.parse( "market://details?id=com.waze" ) );
		   activity.startActivity(intent);
		}
	}
	
	public static void sendEmail(Context context, String address, String subject, String body){
		Intent i = new Intent(Intent.ACTION_SEND);
		i.setType("message/rfc822");
		i.putExtra(Intent.EXTRA_EMAIL  , new String[]{address});
		i.putExtra(Intent.EXTRA_SUBJECT, subject);
		i.putExtra(Intent.EXTRA_TEXT   , body);
		try {
			context.startActivity(Intent.createChooser(i, context.getString(R.string.open_with)));
		} catch (android.content.ActivityNotFoundException ex) {
		    Toast.makeText(context, "There are no email clients installed.", Toast.LENGTH_SHORT).show();
		}
	}

}
