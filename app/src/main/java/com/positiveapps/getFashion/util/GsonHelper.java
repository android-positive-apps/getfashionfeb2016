package com.positiveapps.getFashion.util;

import java.io.IOException;
import java.util.Date;

import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.google.gson.stream.JsonWriter;

public class GsonHelper {
	public static class BooleanAsIntAdapter extends TypeAdapter<Boolean> {
		@Override
		public void write(JsonWriter out, Boolean value) throws IOException {
			if (value == null) {
				out.nullValue();
			} else {
				out.value(value);
			}
		}

		@Override
		public Boolean read(JsonReader in) throws IOException {
			JsonToken peek = in.peek();
			switch (peek) {

			case BOOLEAN:
				return in.nextBoolean();
			case NULL:
				in.nextNull();
				return null;
			case NUMBER:
				return in.nextInt() != 0;
			case STRING:
				return Boolean.parseBoolean(in.nextString());
			default:
				throw new IllegalStateException("Expected BOOLEAN or NUMBER but was " + peek);
			}
		}
	};

	public static class DateTypeAdapter extends TypeAdapter<Date> {
		String mDateFormat;
		
		public DateTypeAdapter(String dateFormat) {
			mDateFormat = dateFormat;
		}
		
		@Override
		public void write(JsonWriter out, Date value) throws IOException {
			if (value == null) {
				out.nullValue();
			} else {
				out.value(DateUtil.formatDate(value, mDateFormat));
			}
		}

		@Override
		public Date read(JsonReader in) throws IOException {
			JsonToken peek = in.peek();
			String str = in.nextString();

			if (str == null || str.isEmpty()) {
				return null;
			}

			Date date = DateUtil.dateFromString(str, mDateFormat);

			if (date == null)
				throw new IllegalStateException("Expected date of pattern " + mDateFormat + "  but was " + peek);

			return date;

		}
	};
}
