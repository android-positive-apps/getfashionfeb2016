package com.positiveapps.getFashion.util;

import android.view.View;
import android.view.animation.Animation;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Transformation;
import android.widget.LinearLayout.LayoutParams;

public class AnimationHelper {
	public static void expand(final View v) {
		v.measure(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);

		final int targetHeight = v.getMeasuredHeight();
		v.setVisibility(View.VISIBLE);
		v.getLayoutParams().height = 0;

		Animation a = new Animation() {
			@Override
			protected void applyTransformation(float interpolatedTime,
					Transformation t) {

				v.getLayoutParams().height = interpolatedTime == 1 ? targetHeight
						: (int) (targetHeight * interpolatedTime);

//				v.setLayoutParams(new LayoutParams(
//						android.view.ViewGroup.LayoutParams.MATCH_PARENT,
//						(int) (targetHeight * interpolatedTime)));
				
				v.requestLayout();
			}

			@Override
			public boolean willChangeBounds() {
				return true;
			}

		};

		// 1dp/ms
		a.setDuration(250);
		v.startAnimation(a);
	}

	public static void collapse(final View v) {
		final int initialHeight = v.getMeasuredHeight();

		Animation a = new Animation() {
			@Override
			protected void applyTransformation(float interpolatedTime,
					Transformation t) {
				if (interpolatedTime == 1) {
					// v.setVisibility(View.INVISIBLE);
					v.getLayoutParams().height = 0;
					v.requestLayout();
				} else {
					v.getLayoutParams().height = initialHeight
							- (int) (initialHeight * interpolatedTime);
					v.requestLayout();
				}
			}

			@Override
			public boolean willChangeBounds() {
				return true;
			}

		};

		a.setDuration(250);
		v.startAnimation(a);
	}
	
	public static void resizeWidth(final View v, final int targetWidth, int duration){
		final android.view.ViewGroup.LayoutParams lp = v.getLayoutParams();
		final int initialWidth = lp.width;
		final int widthDif = targetWidth - initialWidth;
		
		Animation a = new Animation() {
			@Override
			protected void applyTransformation(float interpolatedTime,
					Transformation t) {
				
				lp.width = interpolatedTime == 1 ? targetWidth
						: initialWidth
						+ (int) (widthDif * interpolatedTime);
				
				v.requestLayout();
			}
			
			
			
			@Override
			public boolean willChangeBounds() {
				return true;
			}

		};
		
		a.setInterpolator(new DecelerateInterpolator());
		a.setDuration(duration);
		v.startAnimation(a);
		
	}
}
