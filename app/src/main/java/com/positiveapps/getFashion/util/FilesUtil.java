package com.positiveapps.getFashion.util;

import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

import android.content.Context;

public class FilesUtil {

	public static byte[] getByteArrayFromFile(File f)
			throws FileNotFoundException, IOException {
		FileInputStream is = new FileInputStream(f);
		byte[] ba = convertStreamToByteArray(is);

		// in case we already have a byte array and the close function is
		// throwing, this won't exit the function
		try {
			is.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return ba;
	}

	public static byte[] getByteArrayFromPath(String path)
			throws FileNotFoundException, IOException {
		return convertStreamToByteArray(new FileInputStream(path));
	}

	public static byte[] convertStreamToByteArray(InputStream is)
			throws IOException {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		byte[] buff = new byte[10240];
		int i = Integer.MAX_VALUE;
		while ((i = is.read(buff, 0, buff.length)) > 0) {
			baos.write(buff, 0, i);
		}

		return baos.toByteArray(); // be sure to close InputStream in calling
									// function
	}

	public static void saveToFile(Context context, String content, String path)
			throws IOException {

		File requestFile = new File(path);

		// Toast.makeText(this, requestFile.getAbsolutePath(),
		// Toast.LENGTH_LONG).show();

		if (!requestFile.exists()) {
			requestFile.createNewFile();
		}

		FileWriter writer = null;

		writer = new FileWriter(requestFile);

		BufferedWriter bw = new BufferedWriter(writer);

		bw.write(content);
		bw.close();
	}

	@SuppressWarnings("unchecked")
	public static <T extends Serializable> T instaciateObjectFromFile(
			String filePath) throws ClassCastException, ClassNotFoundException,
			IOException {
		
//		File dir = new File(filePath);
//		dir.mkdirs(); //create folders where write files
//		File file = new File(dir, filePath);
		
		
		File file = new File(filePath);
		FileInputStream fileInStream = null;
		ObjectInputStream objInStream = null;

		T obj;

		fileInStream = new FileInputStream(file);
		objInStream = new ObjectInputStream(fileInStream);

		obj = (T) objInStream.readObject();

		try {
			objInStream.close();
		} catch (IOException | NullPointerException e) {
			e.printStackTrace();
		}

		return obj;
	}

	public static void saveObjectToFile(Serializable obj, String filePath) throws IOException {
		File file = new File(filePath);
		FileOutputStream fileOutStream = null;
		ObjectOutputStream objOutStream = null;

		fileOutStream = new FileOutputStream(file);
		objOutStream = new ObjectOutputStream(fileOutStream);
		objOutStream.writeObject(obj);

		try {
			objOutStream.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
