package com.positiveapps.getFashion;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.positiveapps.getFashion.backend.APIManager;
import com.positiveapps.getFashion.backend.Prefs;
import com.positiveapps.getFashion.backend.SettingManager;
import com.positiveapps.getFashion.backend.UserManager;
import com.positiveapps.getFashion.dialogs.MessageDialog;
import com.positiveapps.getFashion.gcm.GCMManager;
import com.positiveapps.getFashion.jsonmodels.Appuser;
import com.positiveapps.getFashion.jsonmodels.apiresponse.SettingGet;
import com.positiveapps.getFashion.jsonmodels.apiresponse.UserResponse;
import com.positiveapps.getFashion.util.BitmapUtil;
import com.positiveapps.getFashion.util.DialogHelper;
import com.positiveapps.getFashion.util.ImageLoader2;

import java.io.File;

public class SplashActivity extends BaseActivity {

	//TODO no internet alert
	private static final int SPLASH_MIN_MILLIS = 5000;

	/* GCM final Variables*/
	private static final String TAG = "GCM log";
	private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 1000;

	private CountDownTimer mTimer;
	private boolean isResumed = false;
	private boolean taskCountDownFinished = false;
	private boolean taskCategoryGetFinished = false;
	private boolean taskInitAppFinished = false;
	private boolean hasError = false;
	private ProgressBar splashProgressBar;
	private int i;
	private ImageView backgroundImage;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_splash);
		checkNetwork();
		splashProgressBar= (ProgressBar)findViewById(R.id.splashProgressBar);

		if (BuildConfig.FLAVOR.equals("getCoffeeShops")) {
			backgroundImage = (ImageView) findViewById(R.id.splashBack);
			if (Prefs.getSplashUri() != null) {

				File f = new File(Prefs.getSplashUri());
				if (f.exists()) {
					ImageLoader2.byFile(f, backgroundImage);
					Bitmap myBitmap = BitmapFactory.decodeFile(f.getAbsolutePath());
					backgroundImage.setImageBitmap(myBitmap);
					System.out.println("Niv test File");
				}
			} else {
				backgroundImage.setImageResource(R.drawable.splash);
				System.out.println("Niv test backgroundImage");

			}
		}
		if (checkPlayServices()) {
        	checkForGCMRegistrationID();
        	setPushToken();
        	}

		startCountDown();
//		getCategories();
		getSettingIfNeeded();



        // TODO: 1/25/2016 make setting and visible the following code:
//        String url = SettingManager.getSetting().url_googleplay;
//        if (!Prefs.getSplashLoaded().matches(url)) {
//            Bitmap bm = BitmapFactory.decodeResource(this.getResources(), R.drawable.splash);
//            String path = BitmapUtil.saveImageToSDCard("SplashPhoto", bm);
//            Prefs.setSplashUri(path);
//            Prefs.setSplashLoaded(url);
//            System.out.println("splash "+ path);
//        }else {
//            Uri path = Uri.parse(Prefs.getSplashUri());
//            System.out.println("splash "+ path);
//
//
//            FrameLayout frameLayout= (FrameLayout) findViewById(R.id.relative_splash);
//            File f = new File(String.valueOf(path));
//            if(f .exists()){
//                Bitmap myBitmap = BitmapFactory.decodeFile(f.getAbsolutePath());
//                Drawable d = new BitmapDrawable(getResources(), myBitmap);
//                System.out.println("splash "+ "bit");
//
//                frameLayout.setBackgroundDrawable(d);
////                frameLayout.setBackground(d);
//            }
//
//
//        }
                //todo end
	}

	private void setPushToken() {
		APIManager.api.setPushToken(Prefs.getGCMKey(), "android", new Callback<UserResponse>() {

			@Override
			public void success(UserResponse arg0, Response arg1) {
				// TODO Auto-generated method stub

			}

			@Override
			public void failure(RetrofitError arg0) {
				// TODO Auto-generated method stub

			}
		});
	}

	@Override
	protected void onResume() {
		super.onResume();
		isResumed = true;

		checkEverythingFinished();
	}

	@Override
	protected void onPause() {
		super.onPause();
		isResumed = false;
	}

	private void checkEverythingFinished() {
		if (!isResumed) {
			return;
		}
		if (hasError){
			MessageDialog.DialogListener dl=new MessageDialog.DialogListener() {
				@Override
				public void onOk(Dialog d) {
					finish();
					Intent restart = new Intent(SplashActivity.this,SplashActivity.class);
					startActivity(restart);

				}

				@Override
				public void onCancel(Dialog d) {
					finish();
				}
			};
			DialogHelper.messageDialog(getSupportFragmentManager(),getString(R.string.error),getString(R.string.connection_error_splash),dl);
		}


		if (taskCountDownFinished && taskInitAppFinished) {
			startActivity(new Intent(SplashActivity.this, MainActivity.class));
			finish();
		}
	}

	private void getSettingIfNeeded() {

		APIManager.api.getSettings(new Callback<SettingGet>() {

			@Override
			public void success(SettingGet result, Response arg1) {
				SettingManager.setSetting(result.data.settings);
				initAppIfNeeded();

				if (BuildConfig.FLAVOR.equals(("getCoffeeShops")))
				{
//					ImageLoader2.ByUrl(SettingManager.getSetting().splash_screen_image, backgroundImage, R.drawable.splash);
					ImageLoader2.loadImageAndSaveItOnStorage("SplashPhoto", SettingManager.getSetting().splash_screen_image, new ImageLoader2.LoadImageCallback() {
						@Override
						public void onLoadImage(boolean success, final Bitmap image, String fileName) {
							final Drawable d = new BitmapDrawable(getResources(), image);
							runOnUiThread(new Runnable() {
								@Override
								public void run() {

									backgroundImage.setBackgroundDrawable(d);

								}
							});

//								backgroundImage.setImageBitmap(image);
								Prefs.setSplashUri(BitmapUtil.saveImageToSDCard("SplashPhoto", image));
								System.out.println("Niv test SettingManager");


						}
					});
				}
			}

			@Override
			public void failure(RetrofitError arg0) {
				onError();
			}
		});
	}

	private void initAppIfNeeded() {
		Appuser user = UserManager.getAppUser();

		// if there's a saved user, no need to do anything
		if (user != null) {
			taskInitAppFinished = true;
			return;
		}

		APIManager.initApp(this, new Callback<UserResponse>() {
			@Override
			public void success(UserResponse result, Response response) {
				UserManager.setAppuser(result.data.Appuser);
				taskInitAppFinished = true;
				checkEverythingFinished();
			}

			@Override
			public void failure(RetrofitError arg0) {
				onError();
			}
		});

	}

	// Checks network connection
	public boolean isNetworkAccess() {
		ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo netInfo = cm.getActiveNetworkInfo();
		if (netInfo != null && netInfo.isAvailable() && netInfo.isConnected()) {
			return true;
		}
		return false;
	}

	private void checkNetwork() {
		if (!isNetworkAccess()) {
			showDialog();
		}
	}

	private void showDialog() {
		new AlertDialog.Builder(this)
				.setMessage(getString(R.string.no_network))
				.setPositiveButton(android.R.string.ok,
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,
									int which) {
								finish();
							}
						});
	}

//	private void getCategories() {
//		APIManager.api.getCategories(APIManager.makeUserParams(), new Callback<CategoryGetList>() {
//			@Override
//			public void success(CategoryGetList result, Response response) {
//				ArrayList<Category> categories = result.data.Categories;
//				Collections.sort(categories);
//				Categories.setCategories(categories);
//				taskCategoryGetFinished = true;
//				checkEverythingFinished();
//			}
//
//			@Override
//			public void failure(RetrofitError retrofitError) {
//				onError();
//			}
//		});
//	}

	protected void onError() {
		hasError = true;
	}

	private void startCountDown() {
		 i=0;
		splashProgressBar.setProgress(i);

		mTimer = new CountDownTimer(SPLASH_MIN_MILLIS, 1000) {
			@Override
			public void onTick(long millisUntilFinished) {
				i++;
				splashProgressBar.setProgress(i);


			}

			@Override
			public void onFinish() {
				i++;
				splashProgressBar.setProgress(i);
				taskCountDownFinished = true;
				checkEverythingFinished();
			}
		};

		mTimer.start();
	}

	private boolean checkPlayServices() {
		int resultCode = GooglePlayServicesUtil
				.isGooglePlayServicesAvailable(this);
		if (resultCode != ConnectionResult.SUCCESS) {
			if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
				GooglePlayServicesUtil.getErrorDialog(resultCode, this,
						PLAY_SERVICES_RESOLUTION_REQUEST).show();
			} else {
				Log.i(TAG, "This device is not supported.");
				finish();
			}
			return false;
		}
		return true;
	}

	private void checkForGCMRegistrationID() {
		// if(WingsApplication.getGcmManager().isRegistrationIDEmpty()){
		GCMManager.getInstace(this).registerInBackground();
		// }
	}

}
