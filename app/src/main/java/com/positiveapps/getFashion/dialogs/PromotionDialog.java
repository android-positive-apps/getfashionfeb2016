package com.positiveapps.getFashion.dialogs;

import android.app.Dialog;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.positiveapps.getFashion.App;
import com.positiveapps.getFashion.R;
import com.positiveapps.getFashion.backend.Prefs;
import com.positiveapps.getFashion.backend.UserManager;
import com.positiveapps.getFashion.jsonmodels.Business;
import com.positiveapps.getFashion.jsonmodels.Promotion;
import com.positiveapps.getFashion.util.Coordinates;
import com.positiveapps.getFashion.util.GeneralHelper;
import com.positiveapps.getFashion.util.ImageLoader;
import com.positiveapps.getFashion.util.StringFormatUtils;

public class PromotionDialog extends BaseDialogFragment implements OnClickListener {
	
	// the TAG
	public static final String TAG = "PromotionDialog";
	
	private Business mBusiness;
	private Promotion mPromotion;
	Bitmap bitmap;
	private Coordinates coordinates;
	
	private String Param1 = "";
	private String Param2 = "";
	private String Param3 = "";
	private String Param4 = "";
	private String Param5 = "";
	
	// new instance
	public static PromotionDialog newInstance(Promotion mPromotion, Business mBusiness, Bitmap bitmap) {
		PromotionDialog dialog = new PromotionDialog();
		dialog.mPromotion = mPromotion;
		dialog.mBusiness = mBusiness;
		dialog.bitmap = bitmap;
		return dialog;
	}
	
	// constructor
	public PromotionDialog() {
	}
	
	//******************************************************************************\\


//	@Override
//	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
//		View view = inflater.inflate(R.layout.dialog_promotion,
//				container, false);
//		return view;
//	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		Log.i(TAG, "onViewCreated");
		
		getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
		getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));



		// creating and getting the views
		mPromotionViewHolder holder = new mPromotionViewHolder(view);
		// setting the views listeners
		setViewsListeners(holder);
		
		handleViews(holder);
		
		
		
	}

	@Override
	public void onResume() {
		super.onResume();
//		Window window = getDialog().getWindow();
//		int dialogWidth= 300;
//		int dialogHeight= 420;
//		window.setLayout(dialogWidth, dialogHeight);
//		window.setGravity(Gravity.CENTER);
	}

	@Override
	public void onClick(View v) {
		Log.i(TAG, "onClick");
		dismiss();
//		switch (v.getId()) {


//		case R.id.btn_home_screen:
//			dismiss();
//			getActivity().finish();
//			break;

//		case R.id.btn_detailed_view:
//				dismiss();
//			break;
//		case R.id.btn_done:
//			dismiss();
//			break;
//		case R.id.txtShare:
//				ActivityStarter.share(getActivity(), mBusiness.Name);
//			break;
//
//		case R.id.txtCallStore:
//				ActivityStarter.dialer(getActivity(), mBusiness.Phone);
//			break;
//		}

	}

	
	////////////////////////////////////////////////////////////////////////////////
	
	private void setViewsListeners(mPromotionViewHolder holder) {
		Log.i(TAG, "setViewsListeners");
		holder.promotionRelative.setOnClickListener(this);
		holder.btnReserveItem.setOnClickListener(this);
//		holder.btn_home_screen.setOnClickListener(this);
//		holder.btn_detailed_view.setOnClickListener(this);
//		holder.btn_done.setOnClickListener(this);
//		holder.txtShare.setOnClickListener(this);
//		holder.txtCallStore.setOnClickListener(this);
	}

	//********* mPromotionViewHolder, creating and getting the views *********\\
	private static class mPromotionViewHolder {
		
		public TextView txtDiscount;
		public TextView txtTitle;
//		public TextView txtHeartsCounter;
		public TextView txtDistanceFromUser;
		public TextView txtAddress;
//		public TextView txtDesc;
//		public TextView txtAvailableColors;
//		public TextView txtAvailableSizes;
		public TextView txtPrice;
		public TextView txtOldPrice;
//		public TextView txtItemsLeft;
//		public TextView txtCallStore;
		public TextView txtNavigate;
//		public TextView txtShare;
		public TextView txtReport;
		public ImageView imgSaleImage;
//		public ImageView imgHeartIcon;
		public ImageView imgLogo;
		public Button btnReserveItem;
		
//		public TextView btn_home_screen;
//		public TextView btn_detailed_view;
//		public TextView btn_done;
		
//		public View mPromotionInfoLayout;
//		public View mPromotionRibbonFrame;
//		public View mPromotionPricesView;
		public RelativeLayout promotionRelative;
//		private TextView param1;
//		private TextView param2;
//		private TextView param3;
//		private TextView param4;
//		private TextView param5;
//		private TextView param1Txt;
//		private TextView param2Txt;
//		private TextView param3Txt;
//		private TextView param4Txt;
//		private TextView param5Txt;
//		private TextView txtOpenHour;
		

		public mPromotionViewHolder(View convertView) {
			Log.i(TAG, "mPromotionViewHolder");
			promotionRelative = (RelativeLayout) convertView.findViewById(R.id.promotionRelative);
			txtDiscount = (TextView) convertView.findViewById(R.id.txtDiscount);
			txtTitle = (TextView) convertView.findViewById(R.id.txtTitle);
//			txtHeartsCounter = (TextView) convertView.findViewById(R.id.txtHeartsCount);
			txtDistanceFromUser = (TextView) convertView.findViewById(R.id.txtDistance);
			txtAddress = (TextView) convertView.findViewById(R.id.txtAddress);
//			txtDesc = (TextView) convertView.findViewById(R.id.txtDesc);
//			txtAvailableColors = (TextView) convertView.findViewById(R.id.txtAvailableColors);
//			txtAvailableSizes = (TextView) convertView.findViewById(R.id.txtAvailableSizes);
			txtPrice = (TextView) convertView.findViewById(R.id.txtPrice);
			txtOldPrice = (TextView) convertView.findViewById(R.id.txtOldPrice);
//			txtItemsLeft = (TextView) convertView.findViewById(R.id.txtItemsLeft);
//			txtCallStore = (TextView) convertView.findViewById(R.id.txtCallStore);
//			txtNavigate = (TextView) convertView.findViewById(R.id.txtNavigate);
//			txtShare = (TextView) convertView.findViewById(R.id.txtShare);
			txtReport = (TextView) convertView.findViewById(R.id.txtReport);
			imgSaleImage = (ImageView) convertView.findViewById(R.id.imgProduct);
//			imgHeartIcon = (ImageView) convertView.findViewById(R.id.imgHeartIcon);
			imgLogo = (ImageView) convertView.findViewById(R.id.imgLogo);
			btnReserveItem = (Button) convertView.findViewById(R.id.btnReserveItem2);
			txtOldPrice.setPaintFlags(txtOldPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
//			txtOpenHour = (TextView) convertView.findViewById(R.id.txtOpenHour);
			
//			btn_home_screen = (TextView) convertView.findViewById(R.id.btn_home_screen);
//			btn_detailed_view = (TextView) convertView.findViewById(R.id.btn_detailed_view);
//			btn_done = (TextView) convertView.findViewById(R.id.btn_done);
			
//			param1 = (TextView) convertView.findViewById(R.id.param1);
//			param2 = (TextView) convertView.findViewById(R.id.param2);
//			param3 = (TextView) convertView.findViewById(R.id.param3);
//			param4 = (TextView) convertView.findViewById(R.id.param4);
//			param5 = (TextView) convertView.findViewById(R.id.param5);
//			param1Txt = (TextView) convertView.findViewById(R.id.param1Txt);
//			param2Txt = (TextView) convertView.findViewById(R.id.param2Txt);
//			param3Txt = (TextView) convertView.findViewById(R.id.param3Txt);
//			param4Txt = (TextView) convertView.findViewById(R.id.param4Txt);
//			param5Txt = (TextView) convertView.findViewById(R.id.param5Txt);
//
//			mPromotionInfoLayout = convertView.findViewById(R.id.promotionInfoLayout);
//			mPromotionPricesView = convertView.findViewById(R.id.promotionPricesLayout);
//			mPromotionRibbonFrame = convertView.findViewById(R.id.promotionRibbonFrame);
			
		}
	}
	
	////////////////////// layout ID //////////////////////////////
	@Override
	public int layoutId() {
		return R.layout.dialog_promotion;
	}
	
	private void handleViews(final mPromotionViewHolder holder) {
		Log.i(TAG, "handleViews");
		
		
		for (int i = 0; i < mBusiness.Products.size(); i++) {
			for (int k = 0; k < mBusiness.Promotions.size(); k++) {
				if (mBusiness.Promotions.get(k).ProductID == mBusiness.Products
						.get(i).ID) {
					if (mBusiness.Products.get(i).ProductItems != null) {
						for (int j = 0; j < mBusiness.Products.get(i).ProductItems
								.size(); j++) {
							Param1 = mBusiness.Products.get(i).ProductItems
									.get(j).Param1;
							Param2 = mBusiness.Products.get(i).ProductItems
									.get(j).Param2;
							Param3 = mBusiness.Products.get(i).ProductItems
									.get(j).Param3;
							Param4 = mBusiness.Products.get(i).ProductItems
									.get(j).Param4;
							Param5 = mBusiness.Products.get(i).ProductItems
									.get(j).Param5;
						}
					}
				}
			}
		}
		 
		
//		for (String key: SettingManager.getSetting().params.keySet()) {
//			if(key.matches("Param1")){
//				holder.param1.setText(SettingManager.getSetting().params.get(key));
//				holder.param1.setVisibility(View.VISIBLE);
//				if(!Param1.isEmpty()){
//					holder.param1Txt.setText(Param1);
//					holder.param1Txt.setVisibility(View.VISIBLE);
//				}
//			}
//			if(key.matches("Param2")){
//				holder.param2.setText(SettingManager.getSetting().params.get(key));
//				holder.param2.setVisibility(View.VISIBLE);
//				if(!Param2.isEmpty()){
//					holder.param2Txt.setText(Param1);
//					holder.param2Txt.setVisibility(View.VISIBLE);
//				}
//			}
//			if(key.matches("Param3")){
//				holder.param3.setText(SettingManager.getSetting().params.get(key));
//				holder.param3.setVisibility(View.VISIBLE);
//				if(!Param3.isEmpty()){
//					holder.param3Txt.setText(Param1);
//					holder.param3Txt.setVisibility(View.VISIBLE);
//				}
//			}
//			if(key.matches("Param4")){
//				holder.param4.setText(SettingManager.getSetting().params.get(key));
//				holder.param4.setVisibility(View.VISIBLE);
//				if(!Param4.isEmpty()){
//					holder.param4Txt.setText(Param1);
//					holder.param4Txt.setVisibility(View.VISIBLE);
//				}
//			}
//			if(key.matches("Param5")){
//				holder.param5.setText(SettingManager.getSetting().params.get(key));
//				holder.param5.setVisibility(View.VISIBLE);
//				if(!Param5.isEmpty()){
//					holder.param5Txt.setText(Param1);
//					holder.param5Txt.setVisibility(View.VISIBLE);
//				}
//			}
//		}
		
		
		holder.txtTitle.setText(mPromotion.Title);
//		holder.txtDesc.setText(mPromotion.Description);
		holder.txtDiscount.setText(StringFormatUtils.getFormattedDiscount(getActivity(), 
				mPromotion.ProductPrice, 
				mPromotion.DiscountPrice));
		holder.txtOldPrice.setText(StringFormatUtils.getFormattedPrice(getActivity(), mPromotion.ProductPrice));
		holder.txtPrice.setText(StringFormatUtils.getFormattedPrice(getActivity(), mPromotion.DiscountPrice));
		holder.txtAddress.setText(StringFormatUtils.getBusinessFormattedAddress(Prefs.getAppLanguage(), mBusiness));
//		holder.txtItemsLeft.setText(App.applicationContext.getResources().getString(R.string.number_of_item_in_storage) + " " + mPromotion.NumUnits);

		ImageLoader.loadBusinessLogo(getActivity(), mBusiness.Logo, holder.imgLogo);
		ImageLoader.loadProductImage(getActivity(), mPromotion.Image, holder.imgSaleImage);
//
//		holder.txtShare.setOnClickListener(new OnClickListener() {
//			public void onClick(View v) {
//				ActivityStarter.share(getActivity(), mBusiness.Name);
//			}
//		});
//
//		holder.txtCallStore.setOnClickListener(new OnClickListener() {
//			public void onClick(View v) {
//				ActivityStarter.dialer(getActivity(), mBusiness.Phone);
//			}
//		});
//
//		holder.txtNavigate.setOnClickListener(new OnClickListener() {
//			public void onClick(View v) {
//				ActivityStarter.openWaze(getActivity(), holder.txtAddress.getText().toString());
//			}
//		});

//		holder.txtReport.setOnClickListener(new OnClickListener() {
//			public void onClick(View v) {
//				ActivityStarter.sendEmail(getActivity(), "", "", "");
//			}
//		});

//		holder.txtOpenHour.setOnClickListener(new OnClickListener() {
//			public void onClick(View v) {
//				openHoursDialog();
//			}
//		});
		
		
		coordinates = new Coordinates(mBusiness.Lat, mBusiness.Lng);
		double distance = GeneralHelper.distance(coordinates.lat, coordinates.lon, Double.parseDouble(UserManager.getAppUser().Lat), Double.parseDouble(UserManager.getAppUser().Lng));
		String distanceInString = String.format("%.1f", distance);
		holder.txtDistanceFromUser.setText(
				distanceInString + " " +
				App.applicationContext.getResources().getString(R.string.km) );
		
		
		if(bitmap != null){
			holder.imgSaleImage.setImageBitmap(bitmap);
		}else{
			ImageLoader.loadProductImage(getActivity(), mPromotion.Image,holder.imgSaleImage); 
		}
	}
	
	private void openHoursDialog() {
		 
		final Dialog dialog = new Dialog(getActivity(), R.style.myBackgroundStyle);
		final View view = getActivity().getLayoutInflater().inflate(R.layout.dialog_opening_hours, null);
		
		TextView midDays = (TextView) view.findViewById(R.id.midDays);
		TextView friDays = (TextView) view.findViewById(R.id.friDays);
		TextView satDays = (TextView) view.findViewById(R.id.satDays);
		Button closeDialog = (Button) view.findViewById(R.id.closeDialog);
		 

		closeDialog.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) { 
				dialog.dismiss();
			}
		}); 
		
		StringBuilder midTime = new StringBuilder();
		StringBuilder friTime = new StringBuilder();
		StringBuilder satTime = new StringBuilder();
		
		midTime.append(getString(R.string.sun_thur));
		midTime.append(": ");
		if(mBusiness.OpeningTime.matches("00:00:00") && mBusiness.ClosingTime.matches("00:00:00")){
			midTime.append(getString(R.string.close));
		}else{ 
			midTime.append(mBusiness.OpeningTime);
			midTime.append(" - ");
			midTime.append(mBusiness.ClosingTime);
		}

		friTime.append(getString(R.string.fri));
		friTime.append(": ");
		if(mBusiness.OpeningTimeFriday.matches("00:00:00") && mBusiness.ClosingTimeFriday.matches("00:00:00")){
			friTime.append(getString(R.string.close));
		}else{
			friTime.append(mBusiness.OpeningTimeFriday);
			friTime.append(" - ");
			friTime.append(mBusiness.ClosingTimeFriday);
		}
		
		
		satTime.append(getString(R.string.sat));
		satTime.append(": ");
		if(mBusiness.OpeningTimeSaturday.matches("00:00:00") && mBusiness.ClosingTimeSaturday.matches("00:00:00")){
			satTime.append(getString(R.string.close));
		}else{
			satTime.append(mBusiness.OpeningTimeSaturday);
			satTime.append(" - ");
			satTime.append(mBusiness.ClosingTimeSaturday);
		}
		
		midDays.setText(midTime.toString());
		friDays.setText(friTime.toString());
		satDays.setText(satTime.toString());
		
		dialog.setContentView(view);
		dialog.show();
	}
 
}
