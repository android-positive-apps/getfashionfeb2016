package com.positiveapps.getFashion.dialogs;

import android.os.Bundle;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.PopupMenu.OnMenuItemClickListener;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.positiveapps.getFashion.R;
import com.positiveapps.getFashion.backend.APIManager;
import com.positiveapps.getFashion.backend.Prefs;
import com.positiveapps.getFashion.backend.SettingManager;
import com.positiveapps.getFashion.backend.UserManager;
import com.positiveapps.getFashion.jsonmodels.Business;
import com.positiveapps.getFashion.jsonmodels.Product;
import com.positiveapps.getFashion.jsonmodels.ProductItem;
import com.positiveapps.getFashion.jsonmodels.Promotion;
import com.positiveapps.getFashion.jsonmodels.apiresponse.ReservationGetList;
import com.positiveapps.getFashion.util.ActivityStarter;
import com.positiveapps.getFashion.util.DialogHelper;
import com.positiveapps.getFashion.views.SwitchButton;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class ReservationDialog extends BaseDialogFragment implements OnClickListener {

	public static final String TAG = "ReservationDialog";

	public static final int TYPE_BUSSINESS = 1;
	public static final int TYPE_PROMOTION = 2;
	public static final int TYPE_PRODUCT = 3;
	public static final int PURCHASE_TYPE_PICKUP = 3;
	public static final int PURCHASE_TYPE_DELIVERY = 2;
	public static final int PURCHASE_TYPE_RESERVE = 1;
	

	private int dialogType;

	private Business mBusiness;
	private Product mProduct;
	private Promotion mPromotion;

	private TextView txtProductTitle;

	private boolean noColorSelection = false;
	private boolean noSizeSelection = false;
	
	private boolean canMakeReservation = false;
	
	private EditText etxtComment;
	private EditText etxtAmount;
	private TextView txtSubmit;
	private TextView txtCall;

	private SwitchButton swtIsClubMember;
	private SwitchButton swtPurchaseType;
	private EditText etPurchaseType;
	private EditText etPhoneNumber;
	
	private boolean mIsClubMember = false;
	private int mPurchaseType = PURCHASE_TYPE_PICKUP;

	private long productID;
	private int saleType = 3;
	
	private PopupMenu mParam1PopupMenu;
	private PopupMenu mParam2PopupMenu;
	private PopupMenu mParam3PopupMenu;
	private PopupMenu mParam4PopupMenu;
	private PopupMenu mParam5PopupMenu;
	private PopupMenu mProductsPopupMenu;

	private TextView txtParam1;
	private TextView txtParam3;
	private TextView txtParam4;
	private TextView txtParam5;
	private TextView txtParam2;
	private TextView productList;
	
	private LinearLayout clubbingLayout;
	private LinearLayout deliveryLayout;

	private String param1;

	private String param2;

	private String param3;

	private String param4;

	private String param5;
	private int tempProductId=-1;


	public ReservationDialog() {}

	public static ReservationDialog withProduct(Business business, Product product) {
		ReservationDialog dialog = new ReservationDialog();
		dialog.dialogType = TYPE_PRODUCT;
		dialog.mProduct = product;
		dialog.mBusiness = business;
		return dialog;
	}

	public static ReservationDialog withPromotion(Business business, Promotion promotion) {
		ReservationDialog dialog = new ReservationDialog();
		dialog.dialogType = TYPE_PROMOTION;
		dialog.mPromotion = promotion;
		dialog.mBusiness = business;
		dialog.mProduct = business.getProductById(promotion.ProductID);
		return dialog;
	}
	
//	public static ReservationDialog withBussiness(Business business) {
//		ReservationDialog dialog = new ReservationDialog();
//		dialog.dialogType = TYPE_BUSSINESS;
//		dialog.mPromotion = promotion;
//		dialog.mBusiness = business;
//		dialog.mProduct = business.getProductById(promotion.ProductID);
//		return dialog;
//	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

		findViews(view);
		populateViews();
//		initMenus();
		initSwitches();


	}

	private void initSwitches() {
		// in order for the values to be correct in english and hebrew the sides
		// are the same!
		// if needed to make language specific adjustments, preform the check using
		// Prefs.getAppLanguage() and set booleans accordingly
		swtIsClubMember.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				mIsClubMember = !isChecked;
				Log.d(TAG, "is club member switch: " + mIsClubMember);
			}
		});
		
		swtPurchaseType.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				mPurchaseType = isChecked? PURCHASE_TYPE_DELIVERY:PURCHASE_TYPE_PICKUP;
				if(mPurchaseType==PURCHASE_TYPE_DELIVERY){
					Log.d(TAG, "purchase type switch: delivery");
					etPurchaseType.setVisibility(View.VISIBLE);
				}
				if(mPurchaseType==PURCHASE_TYPE_PICKUP){
					Log.d(TAG, "purchase type switch: pickup");
					etPurchaseType.setVisibility(View.GONE);
					
				}
					
			}
		});
		
		swtIsClubMember.setChecked(false, true);
		swtPurchaseType.setChecked(false, true);
	}

	private void populateViews() {

		txtParam1.setOnClickListener(this);
		txtParam2.setOnClickListener(this);
		txtParam3.setOnClickListener(this);
		txtParam4.setOnClickListener(this);
		txtParam5.setOnClickListener(this);
		productList.setOnClickListener(this);
		txtSubmit.setOnClickListener(this);

		if (mBusiness.hasPhone()) {
			txtCall.setOnClickListener(this);
		} else {
			txtCall.setVisibility(View.INVISIBLE);
		}

		switch (dialogType) {
		case TYPE_PRODUCT:
			txtProductTitle.setText(Prefs.getAppLanguage().matches("en") ? 
					SettingManager.getSetting().order_promotion_EN : SettingManager.getSetting().order_promotion_HE); 
			
			txtParam1.setVisibility(View.GONE);
			txtParam2.setVisibility(View.GONE);
			txtParam3.setVisibility(View.GONE);
			txtParam4.setVisibility(View.GONE);
			txtParam5.setVisibility(View.GONE);
		 
			break;

		case TYPE_PROMOTION:
			txtProductTitle.setText(mPromotion.Title);
			productList.setVisibility(View.GONE);
			if(mProduct != null){
				productID = mProduct.ID;
			}else{
				Toast.makeText(getActivity(), "Oops, Product not Correctly defined.", Toast.LENGTH_SHORT).show();
				dismiss();
			}
			break;
			
		}

		String number = Prefs.getLastNumber();
		if (number!=null){
			etPhoneNumber.setText(number);
		}

	}

	private void findViews(View view) {
		txtProductTitle = (TextView) view.findViewById(R.id.txtTitle);
		txtParam1 = (TextView) view.findViewById(R.id.param1);
		txtParam2 = (TextView) view.findViewById(R.id.param2);
		txtParam3 = (TextView) view.findViewById(R.id.param3);
		txtParam4 = (TextView) view.findViewById(R.id.param4);
		txtParam5 = (TextView) view.findViewById(R.id.param5); 
		productList = (TextView) view.findViewById(R.id.productList); 
		
		txtProductTitle = (TextView) view.findViewById(R.id.txtTitle);

		etxtComment = (EditText) view.findViewById(R.id.etxtComment);
		etxtAmount = (EditText) view.findViewById(R.id.etxtAmount);
		etPurchaseType = (EditText) view.findViewById(R.id.etPurchaseType);
		etPhoneNumber = (EditText) view.findViewById(R.id.etPhoneNumber);
		
		txtSubmit = (TextView) view.findViewById(R.id.txtSubmit);
		txtCall = (TextView) view.findViewById(R.id.txtCall);
		
		swtIsClubMember = (SwitchButton) view.findViewById(R.id.swtIsClubMember);
		swtPurchaseType = (SwitchButton) view.findViewById(R.id.swtPurchaseType);
		clubbingLayout = (LinearLayout) view.findViewById(R.id.clubbing_layout);
		deliveryLayout = (LinearLayout) view.findViewById(R.id.delivery_layout);
		
		if(mBusiness.ClubMembership == 0){
			clubbingLayout.setVisibility(View.GONE);
		}
		if (mBusiness.Delivery == 0) {
			deliveryLayout.setVisibility(View.GONE);
			swtPurchaseType.setChecked(false);
		}
		
		
		
		initMenus();

	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.param1:
			mParam1PopupMenu.show();
			break;
		case R.id.param2:
			mParam2PopupMenu.show();
			break;
		case R.id.param3:
			mParam3PopupMenu.show();
			break;
		case R.id.param4:
			mParam4PopupMenu.show();
			break;
		case R.id.param5:
			mParam5PopupMenu.show();
			break;
		case R.id.productList:
			mProductsPopupMenu.show();
			break;
		case R.id.txtCall:
			ActivityStarter.dialer(getActivity(), mBusiness.Phone);
			
		case R.id.txtSubmit:
			placeReservation();
		default:
			break;
		}
	}

	private void placeReservation() {
		if(!isFormValid())
			return;
		
		long quantity = Long.parseLong(etxtAmount.getText().toString());
		String product = txtParam5.getText().toString();
		String Comments = etxtComment.getText().toString() + "";
		String phoneNumber = etPhoneNumber.getText().toString() + "";
		String address = etPurchaseType.getText().toString() + "";
		
		StringBuilder comments = new StringBuilder();
		if(mProduct != null){
		comments.append(getString(R.string.interesing_in));
			comments.append(" ");
		comments.append(mProduct.Title);
		}
/*		comments.append("\n");
		comments.append(getString(R.string.phone));
		comments.append(": ");
		comments.append(phoneNumber);*/
		comments.append("\n");
		comments.append(getString(R.string.customer_comments));
		comments.append(" ");
		if(!Comments.isEmpty()){
			comments.append(Comments);
		}else{
			comments.append(getString(R.string.no_comments));
		}
		
		
 
		if(swtPurchaseType.isChecked()){
			saleType = PURCHASE_TYPE_DELIVERY;
		}else{
			saleType = PURCHASE_TYPE_PICKUP;
		}
		
		if(dialogType == TYPE_PROMOTION){
			param1 = txtParam1.getText().toString();
			param2 = txtParam2.getText().toString();
			param3 = txtParam3.getText().toString();
			param4 = txtParam4.getText().toString();
			param5 = txtParam5.getText().toString();
		}
		
		if(dialogType == TYPE_PRODUCT){
			
			if(productList.getText().toString().matches(getString(R.string.choose_product))){
				DialogHelper.error(getActivity(), R.string.please_choose_product);
				return;
			}
			
			productID = getProductIdByTitle(productList.getText().toString());
			
		}
		String number = etPhoneNumber.getText().toString();
		Prefs.setLastNumber(number);
		
		
		
		showLoadingDialog();
		System.out.println("niv "+UserManager.getAppUser().ID+"."+productID+"."+
				saleType+" ."+param1+param2+param3+param4+param5+phoneNumber+".");
		APIManager.api.saveReservation(
				0, 
				UserManager.getAppUser().ID, 
				productID,
				saleType,
				param1, 
				param2, 
				param3, 
				param4, 
				param5,
				phoneNumber,
				quantity,
				comments.toString(), 
				mIsClubMember ? "1" : "0", 
				address, 
				"", 
				"", 
				"", 
				new Callback<ReservationGetList>() {

					@Override
					public void failure(RetrofitError arg0) {
						// TODO Auto-generated method stub
						Toast.makeText(getActivity(), "reservation failed", Toast.LENGTH_LONG).show();
						dismissLoadingDialog();
					}

					@Override
					public void success(ReservationGetList arg0, Response arg1) {
						// TODO Auto-generated method stub
						Toast.makeText(getActivity(), "success", Toast.LENGTH_LONG).show();
						dismissLoadingDialog();
					dismiss();
					}
				});



	}
	
	private boolean isFormValid(){
		String amount = etxtAmount.getText().toString();
		String phone = etPhoneNumber.getText().toString();
		
		if(TextUtils.isEmpty(amount)){
			DialogHelper.error(getActivity(), R.string.reservation_dialog_amount_error);
			return false;
		}
		
		if(TextUtils.isEmpty(phone)){
			DialogHelper.error(getActivity(), R.string.reservation_dialog_phone_error);
			return false;
		}
	 
		return true;
		
	}

	private void initMenus() {
		mParam1PopupMenu = new PopupMenu(getActivity(), txtParam1);
		mParam2PopupMenu = new PopupMenu(getActivity(), txtParam2);
		mParam3PopupMenu = new PopupMenu(getActivity(), txtParam3);
		mParam4PopupMenu = new PopupMenu(getActivity(), txtParam4);
		mParam5PopupMenu = new PopupMenu(getActivity(), txtParam5);
		mProductsPopupMenu = new PopupMenu(getActivity(), productList);
		
		Menu param1eMenu = mParam1PopupMenu.getMenu();
		Menu param2eMenu = mParam2PopupMenu.getMenu();
		Menu param3eMenu = mParam3PopupMenu.getMenu();
		Menu param4eMenu = mParam4PopupMenu.getMenu();
		Menu param5eMenu = mParam5PopupMenu.getMenu();
		Menu productsMenu = mProductsPopupMenu.getMenu();
		
		ProductItem currentItem;
		
		for (int i = 0; i < mBusiness.Products.size(); i++) {
			productsMenu.add(0, (int) mBusiness.Products.get(i).ID,0,mBusiness.Products.get(i).Title);

		}
		
		if(mProduct != null){
		for (int i = 0; i < mProduct.ProductItems.size(); i++) {
			currentItem = mProduct.ProductItems.get(i);

			if (!TextUtils.isEmpty(currentItem.Param1)) {
				param1eMenu.add(currentItem.Param1);
			}
			if (!TextUtils.isEmpty(currentItem.Param2)) {
				param2eMenu.add(currentItem.Param2);
			}
			if (!TextUtils.isEmpty(currentItem.Param3)) {
				param3eMenu.add(currentItem.Param3);
			}
			if (!TextUtils.isEmpty(currentItem.Param4)) {
				param4eMenu.add(currentItem.Param4);
			}
			if (!TextUtils.isEmpty(currentItem.Param5)) {
				param5eMenu.add(currentItem.Param5);
			}
 
		}
		}
		
		for (String key: SettingManager.getSetting().params.keySet()) {
			if(key.matches("Param1")){
				txtParam1.setText(SettingManager.getSetting().params.get(key));
				txtParam1.setVisibility(View.VISIBLE);
			}
			if(key.matches("Param2")){
				txtParam2.setText(SettingManager.getSetting().params.get(key));
				txtParam2.setVisibility(View.VISIBLE);
			}
			if(key.matches("Param3")){
				txtParam3.setText(SettingManager.getSetting().params.get(key));
				txtParam3.setVisibility(View.VISIBLE);
			}
			if(key.matches("Param4")){
				txtParam4.setText(SettingManager.getSetting().params.get(key));
				txtParam4.setVisibility(View.VISIBLE);
			}
			if(key.matches("Param5")){
				txtParam5.setText(SettingManager.getSetting().params.get(key));
				txtParam5.setVisibility(View.VISIBLE);
			}
		}

		
		
//
//		if (colorMenu.size() == 0) {
//			noColorSelection = true;
//			txtColor.setAlpha(0.4f);
//		}
//
//		if (sizeMenu.size() == 0) {
//			noSizeSelection = true;
//			txtSize.setAlpha(0.4f);
//		}

		mParam1PopupMenu.setOnMenuItemClickListener(new OnMenuItemClickListener() {
			@Override
			public boolean onMenuItemClick(MenuItem item) {
				txtParam1.setText(item.getTitle());
				return true;
			}
		});

		mParam2PopupMenu.setOnMenuItemClickListener(new OnMenuItemClickListener() {
			@Override
			public boolean onMenuItemClick(MenuItem item) {
				txtParam2.setText(item.getTitle());
				return true;
			}
		});
		mParam3PopupMenu.setOnMenuItemClickListener(new OnMenuItemClickListener() {
			@Override
			public boolean onMenuItemClick(MenuItem item) {
				txtParam3.setText(item.getTitle());
				return true;
			}
		});
		mParam4PopupMenu.setOnMenuItemClickListener(new OnMenuItemClickListener() {
			@Override
			public boolean onMenuItemClick(MenuItem item) {
				txtParam4.setText(item.getTitle());
				return true;
			}
		});
		mParam5PopupMenu.setOnMenuItemClickListener(new OnMenuItemClickListener() {
			@Override
			public boolean onMenuItemClick(MenuItem item) {
				txtParam5.setText(item.getTitle());
				return true;
			}
		});
		
		mProductsPopupMenu.setOnMenuItemClickListener(new OnMenuItemClickListener() {
			@Override
			public boolean onMenuItemClick(MenuItem item) {
				productList.setText(item.getTitle());
				tempProductId= item.getItemId();
				return true;
			}
		});

	}


	@Override
	public int layoutId() {
		return R.layout.dialog_reservations;
	}
	
	private long getProductIdByTitle(String title) {
		for (int i = 0; i < mBusiness.Products.size(); i++) {
			System.out.println("mBusiness.Products.get(i).Title - > " + mBusiness.Products.get(i).Title);
			System.out.println("title - > " + title);
			if (mBusiness.Products.get(i).Title!=null) {
				if (mBusiness.Products.get(i).Title.matches(title)) {
					return mBusiness.Products.get(i).ID;
				}
			}else {
				if (tempProductId!=-1)
				return tempProductId;
			}
		}
		return 0;

	}

}
