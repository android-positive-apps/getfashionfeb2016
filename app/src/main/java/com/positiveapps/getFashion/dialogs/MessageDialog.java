package com.positiveapps.getFashion.dialogs;

import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

import com.positiveapps.getFashion.R;

public class MessageDialog extends BaseDialogFragment {

	public static final String TAG = "MessageDialog";
	private TextView txtMessage;
	private TextView txtTitle;
	private TextView txtCancel;
	private TextView txtOK;

	private boolean hideConfirmButton;
	
	private String strOK = null;
	private String strCancel = null;

	private String mMessage = null;
	private int mMessageRes = 0;

	private String mTitle = null;
	private int mTitleRes = 0;

	private DialogListener mListener;

	public static MessageDialog newInstance(String title, String message, DialogListener listener) {
		MessageDialog dialog = new MessageDialog();
		dialog.mMessage = message;
		dialog.mTitle = title;
		dialog.mListener = listener;

		return dialog;
	}

	public static MessageDialog newInstance(int titleRes, int messageRes, DialogListener listener) {
		MessageDialog dialog = new MessageDialog();
		dialog.mMessageRes = messageRes;
		dialog.mTitleRes = titleRes;
		dialog.mListener = listener;

		return dialog;
	}
	
	public static MessageDialog newInstance(String title, String message, boolean hideConfirmButton ,DialogListener listener) {
		MessageDialog dialog = new MessageDialog();
		dialog.mMessage = message;
		dialog.mTitle = title;
		dialog.mListener = listener;
		dialog.hideConfirmButton = hideConfirmButton;
		return dialog;
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		findViews(view);
		populateDialog();
	}

	private void populateDialog() {
		if (mTitleRes == 0)
			txtTitle.setText(mTitle);
		else
			txtTitle.setText(mTitleRes);
		
		if (mMessageRes == 0)
			txtMessage.setText(mMessage);
		else
			txtMessage.setText(mMessageRes);
		
		if (mListener == null) {
			txtOK.setVisibility(View.GONE);
			txtCancel.setVisibility(View.GONE);
			return;
		}

		if (strOK != null) {
			txtOK.setText(strOK);
		}

		if (strCancel != null) {
			txtCancel.setText(strCancel);
		}

		txtOK.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				mListener.onOk(getDialog());
			}
		});

		txtCancel.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				mListener.onCancel(getDialog());
			}
		});
	}

	private void findViews(View view) {
		txtTitle = (TextView) view.findViewById(R.id.txtTitle);
		txtMessage = (TextView) view.findViewById(R.id.txtMessage);
		txtOK = (TextView) view.findViewById(R.id.txtOK);
		txtCancel = (TextView) view.findViewById(R.id.txtCancel);
		
		if(hideConfirmButton){
			txtOK.setVisibility(View.GONE);
			txtOK.setText(getString(R.string.more_info));
		}
	}

	public MessageDialog() {

	}

	public void setOKButtonText(String text) {
		strOK = text;
	}

	public void setCancelButtonText(String text) {
		strCancel = text;
	}

	public static interface DialogListener {
		public void onOk(Dialog d);

		public void onCancel(Dialog d);
	}

	@Override
	public int layoutId() {
		return R.layout.dialog_message;
	}
}
