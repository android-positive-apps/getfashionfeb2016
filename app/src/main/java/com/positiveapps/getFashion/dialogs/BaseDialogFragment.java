package com.positiveapps.getFashion.dialogs;

import com.positiveapps.getFashion.R;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;

public abstract class BaseDialogFragment extends DialogFragment{
	public abstract int layoutId();
	private ProgressDialog mProgressDialog;
	private Dialog loadingDialog;
	
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		
		return inflater.inflate(layoutId(), container, false);
	}
	
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		
	}
	
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		Dialog dialog= super.onCreateDialog(savedInstanceState);
		dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
		dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
		return dialog;
	}
	
	protected void showProgressDialog() {
		mProgressDialog = ProgressDialog.show(getActivity(), "", getString(R.string.please_wait), true, false);
	}

	protected void dismissProgressDialog() {
		if (mProgressDialog != null) {
			mProgressDialog.dismiss();
			mProgressDialog = null;
		}
	}
	
	public void showLoadingDialog(){
		loadingDialog = new Dialog(getActivity(), R.style.loadingDialog);
		final View view = getActivity().getLayoutInflater().inflate(R.layout.dialog_loading, null);
		loadingDialog.setContentView(view);
		loadingDialog.setCancelable(false);
		loadingDialog.show();
	}
	
	public void showLoadingDialog(String text){
		loadingDialog = new Dialog(getActivity(), R.style.loadingDialog);
		final View view = getActivity().getLayoutInflater().inflate(R.layout.dialog_loading, null);
		((TextView) view.findViewById(R.id.loading_text)).setText(text);
		loadingDialog.setContentView(view);
		loadingDialog.setCancelable(false);
		loadingDialog.show();
	}
	
	public void dismissLoadingDialog(){ 
		loadingDialog.dismiss();
	}
}
