package com.positiveapps.getFashion.dialogs;

import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

import com.positiveapps.getFashion.R;
import com.positiveapps.getFashion.backend.Prefs;
import com.positiveapps.getFashion.backend.SettingManager;
import com.positiveapps.getFashion.jsonmodels.Business;
import com.positiveapps.getFashion.jsonmodels.Product;
import com.positiveapps.getFashion.jsonmodels.ProductItem;
import com.positiveapps.getFashion.jsonmodels.Promotion;
import com.positiveapps.getFashion.util.ActivityStarter;
import com.positiveapps.getFashion.util.DialogHelper;
import com.positiveapps.getFashion.util.StringFormatUtils;

public class ProductDescriptionDialog extends BaseDialogFragment implements OnClickListener {
	public static final String TAG = "ProductDescriptionDialog";

	public static final int TYPE_PROMOTION = 2;
	public static final int TYPE_PRODUCT = 3;
	public static final String COMMA = ", ";

	private int dialogType;

	private Product mProduct;
	private Business mBusiness;
	private Promotion mPromotion;

	private TextView txtProductDescription;
	private TextView txtProductTitle;
	
	private String Param1 = "";
	private String Param2 = "";
	private String Param3 = "";
	private String Param4 = "";
	private String Param5 = "";
	
	private TextView param1;
	private TextView param2;
	private TextView param3;
	private TextView param4;
	private TextView param5;
	private TextView param1Txt;
	private TextView param2Txt;
	private TextView param3Txt;
	private TextView param4Txt;
	private TextView param5Txt;
	
	private TextView txtReserve;
	
	private TextView txtCall;

	private ProductItem currentItem;

	public ProductDescriptionDialog() {}

	public static ProductDescriptionDialog withProduct(Business business, Product product) {
		ProductDescriptionDialog dialog = new ProductDescriptionDialog();

		dialog.dialogType = TYPE_PRODUCT;
		dialog.mProduct = product;
		dialog.mBusiness = business;

		return dialog;
	}

	public static ProductDescriptionDialog withPromotion(Business business, Promotion promotion) {
		ProductDescriptionDialog dialog = new ProductDescriptionDialog();
		dialog.dialogType = TYPE_PROMOTION;

		dialog.mBusiness = business;
		dialog.mPromotion = promotion;
		dialog.mProduct = business.getProductById(promotion.ProductID);

		return dialog;
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		findViews(view);
		populateViews();
	}

	private void populateViews() {

		if(mBusiness.EnableReservations == 0){
			txtReserve.setVisibility(View.INVISIBLE);
		}
		txtReserve.setOnClickListener(this);
		
//		for (int i = 0; i < mProduct.ProductItems.size(); i++) {
//			currentItem = mProduct.ProductItems.get(i);

			for (int i = 0; i < mBusiness.Products.size(); i++) {
				for (int k = 0; k < mBusiness.Promotions.size(); k++) {
					if (mBusiness.Promotions.get(k).ProductID == mBusiness.Products
							.get(i).ID) {
						if (mBusiness.Products.get(i).ProductItems != null) {
							for (int j = 0; j < mBusiness.Products.get(i).ProductItems
									.size(); j++) {
								Param1 += mBusiness.Products.get(i).ProductItems
										.get(j).Param1 + ", ";
								Param2 += mBusiness.Products.get(i).ProductItems
										.get(j).Param2 + ", ";
								Param3 += mBusiness.Products.get(i).ProductItems
										.get(j).Param3 + ", ";
								Param4 += mBusiness.Products.get(i).ProductItems
										.get(j).Param4 + ", ";
								Param5 += mBusiness.Products.get(i).ProductItems
										.get(j).Param5 + ", ";
							}
						}
					}
				}
		 
			
//			if (!TextUtils.isEmpty(currentItem.Param1)) {
//				param1Txt.setText(currentItem.Param1); 
//			}
//			if (!TextUtils.isEmpty(currentItem.Param2)) {
//				param2Txt.setText(currentItem.Param2); 
//			}
//			if (!TextUtils.isEmpty(currentItem.Param3)) {
//				param3Txt.setText(currentItem.Param3); 
//			}
//			if (!TextUtils.isEmpty(currentItem.Param4)) {
//				param4Txt.setText(currentItem.Param4); 
//			}
//			if (!TextUtils.isEmpty(currentItem.Param5)) {
//				param5Txt.setText(currentItem.Param5); 
//			}
 
		}
 
		if(dialogType == TYPE_PROMOTION){
		for (String key: SettingManager.getSetting().params.keySet()) {
			if(key.matches("Param1")){
				param1.setText(SettingManager.getSetting().params.get(key));
				param1.setVisibility(View.VISIBLE);
				if(!Param1.isEmpty()){
					Param1 = checkEndingString(Param1);
					param1Txt.setText(Param1);
					param1Txt.setVisibility(View.VISIBLE);
				}
			}
			if(key.matches("Param2")){
				param2.setText(SettingManager.getSetting().params.get(key));
				param2.setVisibility(View.VISIBLE);
				if(!Param2.isEmpty()){
					Param2 = checkEndingString(Param2);
					param2Txt.setText(Param2);
					param2Txt.setVisibility(View.VISIBLE);
				}
			}
			if(key.matches("Param3")){
				param3.setText(SettingManager.getSetting().params.get(key));
				param3.setVisibility(View.VISIBLE);
				if(!Param3.isEmpty()){
					Param3 = checkEndingString(Param3);
					param3Txt.setText(Param3);
					param3Txt.setVisibility(View.VISIBLE);
				}
			}
			if(key.matches("Param4")){
				param4.setText(SettingManager.getSetting().params.get(key));
				param4.setVisibility(View.VISIBLE);
				if(!Param4.isEmpty()){
					Param4 = checkEndingString(Param4);
					param4Txt.setText(Param4);
					param4Txt.setVisibility(View.VISIBLE);
				}
			}
			if(key.matches("Param5")){
				param5.setText(SettingManager.getSetting().params.get(key));
				param5.setVisibility(View.VISIBLE);
				if(!Param5.isEmpty()){
					Param5 = checkEndingString(Param5);
					param5Txt.setText(Param5);
					param5Txt.setVisibility(View.VISIBLE);
				}
			}
		}
		
		txtProductDescription.setText(mProduct.Description);
		}
		if (mBusiness.hasPhone()) {
			txtCall.setOnClickListener(this);
		} else {
			txtCall.setVisibility(View.INVISIBLE);
		}

		switch (dialogType) {
		case TYPE_PRODUCT:
			txtProductTitle.setText(mBusiness.Name);
			txtProductDescription.setText(StringFormatUtils.getBusinessFormattedAddress(Prefs.getAppLanguage(), mBusiness)
					+ "\n\n" +openHours());
			
			break;
		case TYPE_PROMOTION:
			txtProductTitle.setText(mPromotion.Title);
			break;
		}

	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.txtCall:
			ActivityStarter.dialer(getActivity(), mBusiness.Phone);
		case R.id.txtReserve:
			reservationDialog();
			break;
		}
	}

	private void reservationDialog() {
		switch (dialogType) {
		case TYPE_PRODUCT:
			DialogHelper.productReservationDialog(getFragmentManager(), mBusiness, mProduct);
			break;
		case TYPE_PROMOTION:
			DialogHelper.promotionReservationDialog(getFragmentManager(), mBusiness, mPromotion);
			break;
		}
		dismiss();
	}

	private void findViews(View view) {
		txtProductDescription = (TextView) view.findViewById(R.id.txtProductDescription);
		txtCall = (TextView) view.findViewById(R.id.txtCall);
		txtProductTitle = (TextView) view.findViewById(R.id.txtTitle);
		txtReserve = (TextView) view.findViewById(R.id.txtReserve);
		
		param1 = (TextView) view.findViewById(R.id.d_param1);
		param2 = (TextView) view.findViewById(R.id.d_param2);
		param3 = (TextView) view.findViewById(R.id.d_param3);
		param4 = (TextView) view.findViewById(R.id.d_param4);
		param5 = (TextView) view.findViewById(R.id.d_param5);
		param1Txt = (TextView) view.findViewById(R.id.d_param1Txt);
		param2Txt = (TextView) view.findViewById(R.id.d_param2Txt);
		param3Txt = (TextView) view.findViewById(R.id.d_param3Txt);
		param4Txt = (TextView) view.findViewById(R.id.d_param4Txt);
		param5Txt = (TextView) view.findViewById(R.id.d_param5Txt);
		
		if(!Prefs.isConnected()){
			txtReserve.setVisibility(View.INVISIBLE);
		}
	}

	@Override
	public int layoutId() {
		return R.layout.dialog_product_description;
	}
	
	private String openHours() {
 
		StringBuilder midTime = new StringBuilder();
		StringBuilder friTime = new StringBuilder();
		StringBuilder satTime = new StringBuilder();
		
		midTime.append(getString(R.string.sun_thur));
		midTime.append(": ");
		if(mBusiness.OpeningTime.matches("00:00:00") && mBusiness.ClosingTime.matches("00:00:00")){
			midTime.append(getString(R.string.close));
		}else{ 
			midTime.append(mBusiness.OpeningTime);
			midTime.append(" - ");
			midTime.append(mBusiness.ClosingTime);
		}

		friTime.append(getString(R.string.fri));
		friTime.append(": ");
		if(mBusiness.OpeningTimeFriday.matches("00:00:00") && mBusiness.ClosingTimeFriday.matches("00:00:00")){
			friTime.append(getString(R.string.close));
		}else{
			friTime.append(mBusiness.OpeningTimeFriday);
			friTime.append(" - ");
			friTime.append(mBusiness.ClosingTimeFriday);
		}
		
		
		satTime.append(getString(R.string.sat));
		satTime.append(": ");
		if(mBusiness.OpeningTimeSaturday.matches("00:00:00") && mBusiness.ClosingTimeSaturday.matches("00:00:00")){
			satTime.append(getString(R.string.close));
		}else{
			satTime.append(mBusiness.OpeningTimeSaturday);
			satTime.append(" - ");
			satTime.append(mBusiness.ClosingTimeSaturday);
		}
		
		return midTime.toString() + "\n" + friTime.toString() + "\n" + satTime.toString() + "\n";
 
	}
	
	private String checkEndingString(String param) {
		System.out.println("ProductDescriptionDialog: param " + param.charAt(param.length()-1));
		System.out.println("ProductDescriptionDialog: param " + param.charAt(param.length()-2));
		if(param.length() > 0 && param.charAt(param.length()-2)==','){
			param = param.substring(0, param.length()-2);
			System.out.println("ProductDescriptionDialog: param " + param);
			return param;
		}
		return param;
	}

}
