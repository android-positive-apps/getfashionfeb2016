package com.positiveapps.getFashion.dialogs;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.Profile;
import com.facebook.ProfileTracker;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.positiveapps.getFashion.R;
import com.positiveapps.getFashion.backend.APIManager;
import com.positiveapps.getFashion.backend.Prefs;
import com.positiveapps.getFashion.backend.UserManager;
import com.positiveapps.getFashion.interfaces.AppuserListener;
import com.positiveapps.getFashion.jsonmodels.Appuser;
import com.positiveapps.getFashion.jsonmodels.apiresponse.UserResponse;
import com.positiveapps.getFashion.util.DialogHelper;
import com.positiveapps.getFashion.util.TextUtil;

import java.util.Arrays;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;

public class LoginDialog extends BaseDialogFragment implements OnClickListener {

	public static final String TAG = "GeteatLoginDialog";

	private TextView txtSubmit;
	private TextView txtRegister;
	private TextView txtLoginWithFacebook;

	private EditText etxtEmail;
	private EditText etxtPassword;

	private CallbackManager mCallbackManager;
	private AppuserListener mListener;


	public static LoginDialog newInstance(AppuserListener listener) {
		LoginDialog dialog = new LoginDialog();
		dialog.mListener = listener;
		return dialog;
	}

	public LoginDialog() {

	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

		txtSubmit = (TextView) view.findViewById(R.id.txtSubmit);
		txtLoginWithFacebook = (TextView) view.findViewById(R.id.txtLoginWithFacebook);
		txtRegister = (TextView) view.findViewById(R.id.txtRegister);
		etxtEmail = (EditText) view.findViewById(R.id.etxtEmail);
		etxtPassword = (EditText) view.findViewById(R.id.etxtPassword);

		txtRegister.setOnClickListener(this);
		txtSubmit.setOnClickListener(this);
		txtLoginWithFacebook.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.txtRegister:
			DialogHelper.registerDialog(getFragmentManager(), mListener);
			dismiss();
			break;

		case R.id.txtSubmit:
			if (TextUtil.baseFieldsValidation(etxtEmail,etxtPassword))
			loginEmail();
			break;
		case R.id.txtLoginWithFacebook:
			loginFacebook();
			break;
		}
	}

	private void loginEmail() {
		String email = etxtEmail.getText().toString();
		String password = etxtPassword.getText().toString();
		
		showProgressDialog();
		APIManager.emailLogin(email, password, new Callback<UserResponse>() {
			@Override
			public void success(UserResponse result, Response arg1) {
				dismissProgressDialog();

				Prefs.setIsConnected(true);
				UserManager.setAppuser(result.data.Appuser);
				UserManager.setBaseAppuser(result.data.Appuser);
				System.out.println("niv Appuser"+result.data.Appuser.filteredRadius);
				mListener.onAppuserLoginSuccess(result.data.Appuser);
				System.out.println("result.data.Businesses.length() " + result.data.Businesses.size());
				mListener.onCheckManager(result.data.Businesses.size() > 0);
				Prefs.setIsManager(result.data.Businesses.size() > 0);
				dismiss();
			}

			@Override
			public void failure(RetrofitError arg0) {
				dismissProgressDialog();
				DialogHelper.error(getActivity(), R.string.login_invalid_cardentials);
			}
		});
	}

	private void loginFacebook() {
		mCallbackManager = CallbackManager.Factory.create();
		LoginManager.getInstance().registerCallback(mCallbackManager, new FacebookCallback<LoginResult>() {

			private ProfileTracker mProfileTracker;

			@Override
			public void onSuccess(final LoginResult result) {
				System.out.println("onSuccess"+ result.getAccessToken().getUserId()+".."+ result.getAccessToken().getToken()

				);

				mProfileTracker = new ProfileTracker() {
					@Override
					protected void onCurrentProfileChanged(Profile profile, Profile profile2) {
						Log.i(TAG, "onSuccess: onCurrentProfileChanged");
//						loginToServer(result);
//						Prefs.setIsConnected(true);
						mProfileTracker.stopTracking();

					}

				};
				mProfileTracker.startTracking();
				loginToServer(result);
				Prefs.setIsConnected(true);
				if (mProfileTracker.isTracking()) {
					Log.i(TAG, "onSuccess: isTracking");
				} else {
					Log.i(TAG, "onSuccess: isNotTracking");
				}
				dismiss();
			}

			@Override
			public void onError(FacebookException e) {
				dismissProgressDialog();
				DialogHelper.error(getActivity(), R.string.login_facebook_error);
				Log.v("facebook - onError", e.getMessage());
			}

			@Override
			public void onCancel() {
				dismissProgressDialog();
				Log.v("facebook - onCancel", "cancelled");
			}
		});

		LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile"));
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		mCallbackManager.onActivityResult(requestCode, resultCode, data);
	}

	private void loginToServer(LoginResult result) {
		showProgressDialog();

		APIManager.serverFacebookLogin(result.getAccessToken().getUserId(), result.getAccessToken().getToken(), new Callback<UserResponse>() {
			@Override
			public void success(UserResponse result, Response arg1) {
				dismissProgressDialog();
				Appuser user = result.data.Appuser;
				System.out.println();
				Profile profile = Profile.getCurrentProfile();
				user.FirstName = profile.getFirstName();
				user.LastName = profile.getLastName();
				user.Avatar = profile.getProfilePictureUri(200, 200).toString();
				UserManager.setAppuser(user);
				UserManager.setBaseAppuser(user);

				mListener.onAppuserLoginSuccess(user);
				mListener.onCheckManager(result.data.Businesses.size() > 0);
				Prefs.setIsManager(result.data.Businesses.size() > 0);
				dismiss();
			}

			@Override
			public void failure(RetrofitError arg0) {
				dismissProgressDialog();
				DialogHelper.error(getActivity(), getResources().getString(R.string.login_server_error));
				try {
					String errMsg =  new String(((TypedByteArray)arg0.getResponse().getBody()).getBytes()).toString();
					Toast.makeText(getContext(),errMsg,Toast.LENGTH_LONG).show();
				} catch (Exception e) {
					e.printStackTrace();
				}

			}
		});
	}

	private void showKeyboard(View v) {
		InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.showSoftInput(v, InputMethodManager.SHOW_FORCED);
		// imm.hideSoftInputFromWindow(v.getWindowToken(), 0);

		// getActivity().getWindow().setSoftInputMode(LayoutParams.SOFT_INPUT_STATE_VISIBLE);
	}

	@Override
	public int layoutId() {
		return R.layout.dialog_login;
	}

}
