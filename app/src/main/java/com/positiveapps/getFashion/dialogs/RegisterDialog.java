package com.positiveapps.getFashion.dialogs;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;

import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.positiveapps.getFashion.R;
import com.positiveapps.getFashion.backend.APIManager;
import com.positiveapps.getFashion.backend.Prefs;
import com.positiveapps.getFashion.backend.UserManager;
import com.positiveapps.getFashion.interfaces.AppuserListener;
import com.positiveapps.getFashion.jsonmodels.apiresponse.UserResponse;
import com.positiveapps.getFashion.util.DialogHelper;
import com.positiveapps.getFashion.util.TextUtil;

public class RegisterDialog extends BaseDialogFragment {

	private static final int MIN_PASSWORD_LENGTH = 6;
	private static final int MAX_PASSWORD_LENGTH = 14;

	public static final String TAG = "RegisterDialog";

	private TextView txtSubmit;
	private EditText etxtEmail;
	private EditText etxtPassword;
	private EditText etxtFirstname;
	private EditText etxtLastname;

	

	private AppuserListener mListener;

	public static RegisterDialog newInstance(AppuserListener listener) {
		RegisterDialog dialog = new RegisterDialog();
		dialog.mListener = listener;
		return dialog;
	}

	public RegisterDialog() {}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		findViews(view);
		
		txtSubmit.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				if (TextUtil.baseFieldsValidation(etxtEmail, etxtPassword,etxtFirstname,etxtLastname))
				register();
			}
		});
	}

	private void findViews(View view) {
		etxtEmail = (EditText) view.findViewById(R.id.etxtEmail);
		etxtPassword = (EditText) view.findViewById(R.id.etxtPassword);
		etxtFirstname = (EditText) view.findViewById(R.id.etxtFirstname);
		etxtLastname = (EditText) view.findViewById(R.id.etxtLastname);
		txtSubmit = (TextView) view.findViewById(R.id.btnSubmit);
	}



	private void register() {
		if(!isFormValid())
			return;
		
		showProgressDialog();
		String email = etxtEmail.getText().toString();
		String password = etxtPassword.getText().toString();
		String firstname = etxtFirstname.getText().toString();
		String lastname = etxtLastname.getText().toString();
		
		
		APIManager.emailRegister(email, password, firstname, lastname, new Callback<UserResponse>() {
			@Override
			public void success(UserResponse response, Response arg1) {
				dismissProgressDialog();
				if(response.error == 1){

					Toast.makeText(getContext(),"success with error",Toast.LENGTH_LONG).show();
					mListener.onAppuserLoginFail(null);
					return;
				}
				Prefs.setIsConnected(true);
				UserManager.setAppuser(response.data.Appuser);
				mListener.onAppuserLoginSuccess(response.data.Appuser);
				Log.d(TAG, "Register success");

				dismiss();
				System.out.println("niv success");

			}


			@Override
			public void failure(RetrofitError arg0) {
				dismissProgressDialog();
				String errMsg =  new String(((TypedByteArray)arg0.getResponse().getBody()).getBytes()).toString();
				errMsg = errMsg.replace("{\"error\":1,\"errdesc\":\"", "").replace("\"}","");
				Toast.makeText(getContext(),errMsg,Toast.LENGTH_LONG).show();
//				Toast.makeText(getContext(),"there is an account registered by this email, try log in",Toast.LENGTH_LONG).show();
				mListener.onAppuserLoginFail(arg0);
				Log.d(TAG, "Register fail");
			}
		});
	}

	private boolean isFormValid() {
		String email = etxtEmail.getText().toString();
		if (!isValidEmail(email)) {
			DialogHelper.error(getActivity(), R.string.register_invalid_email);
			return false;
		}

		String password = etxtPassword.getText().toString();
		if (password.length() < MIN_PASSWORD_LENGTH || password.length() > MAX_PASSWORD_LENGTH) {
			DialogHelper.error(getActivity(), R.string.register_invalid_password);
			return false;
		}

		return true;
	}

	public static boolean isValidEmail(String target) {
		return !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
	}

	

	@Override
	public int layoutId() {
		return R.layout.dialog_signup_;
	}

}
