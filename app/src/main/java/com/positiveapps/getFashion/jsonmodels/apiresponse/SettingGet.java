package com.positiveapps.getFashion.jsonmodels.apiresponse;


public class SettingGet {
	public boolean error;
	public Data data;

	public static class Data {
		public Setting settings;

	}

}
