package com.positiveapps.getFashion.jsonmodels.apiresponse;

import java.util.ArrayList;

import com.positiveapps.getFashion.jsonmodels.Appuser;


public class UserResponse{
	public int error;
	public String errdesc;

	public Data data;

	public static class Data{
		public Appuser Appuser;
		public ArrayList<Integer> Businesses;
	}
}
