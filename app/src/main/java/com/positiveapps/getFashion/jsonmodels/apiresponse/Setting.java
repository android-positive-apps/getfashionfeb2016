package com.positiveapps.getFashion.jsonmodels.apiresponse;

import java.io.Serializable;
import java.util.Map;

public class Setting implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2L;
	public String app_name;
	public String cool_down;
	public String max_categories;
	public String max_products;
	public String messagebox_promotion_EN;
	public String messagebox_promotion_HE;
	public String notification_email;
	public String order_product_EN;
	public String order_product_HE;
	public String order_promotion_EN;
	public String order_promotion_HE;
	public String promotion_time_limit;
	public String url_googleplay;
	public String splash_screen_image;
	public Map<String, String> params;
	public Map<String, String> sale_types;
	public Map<String, String> custom_filters;

}
