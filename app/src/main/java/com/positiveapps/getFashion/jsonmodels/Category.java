package com.positiveapps.getFashion.jsonmodels;

import java.util.Date;

public class Category implements Comparable<Category> {
 
	public long ID;
	public int DisplayOrder;
	public String Name;
	public int Accessible;

	public Date Created;
	public Date Modified;

	@Override
	public int compareTo(Category another) {
		return Integer.valueOf(DisplayOrder).compareTo(another.DisplayOrder);
	}
	
	@Override
	public String toString() {
		return Name;
	}
}
