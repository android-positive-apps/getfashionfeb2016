package com.positiveapps.getFashion.jsonmodels;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import com.positiveapps.getFashion.backend.SettingManager;

public class Promotion{ 
 
	public long BusinessID;
	public long ProductID;
	public long ID;

	public String Title;
	public String TitleHE;
	public String TitleEN;
	public String Description;
	public String DescriptionHE;
	public String DescriptionEN;
	public String ProductTitle;
	
	public int Status;
	public int NumUnits; 
	
	public double ProductPrice;
	public double DiscountPrice;

	public String Image;
	public String Videos;

	 public Date TimeStart;
	 public Date TimeEnd;
	 public Date RepeatTo;

	 public String TimeStartString;
	 
	/** Not a boolean, 0 is not repeated, any positive number indicates
	 * repetition in days */
	public int Repeated;

	// public Date Created;
	// public Date Modified;
	
	long timeLeft = 0;
	public boolean isOnGoing(){
		long now = System.currentTimeMillis();
		long end = TimeEnd.getTime();
		long start = TimeStart.getTime();
		
		Calendar cal = Calendar.getInstance(); 
		Date nowDate = cal.getTime();
//		System.out.println(milli2String(TimeStart)+ " -> " + milli2String(nowDate) + " <-" + milli2String(TimeEnd));
		if(now>start && now<end){
			
			int dayToday = cal.get(Calendar.DAY_OF_WEEK);
			cal.setTime(TimeStart);
			int dayPromotion = cal.get(Calendar.DAY_OF_WEEK);
//			System.out.println("dayToday -> " + dayToday + " dayPromotion -> " + dayPromotion);
			if(dayToday == dayPromotion){
				String promTime = SettingManager.getSetting().promotion_time_limit;
//				System.out.println("promTime -> " + promTime);
				if(promTime != null){
					if(!promTime.isEmpty()){
						int maxPromotionTime = Integer.parseInt(promTime);  
						timeLeft = getTimeInBetween(nowDate, TimeStart, maxPromotionTime);
						return true; 
					}
				}
				
			}
		}
		return false; 
	}
	
	private String milli2String(Date date) { 
		DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss"); 
		return df.format(date);
	}
	
	
	private long getTimeInBetween(Date date1, Date date2, int limitTime) { 
		
//		System.out.println("getTimeInBetween(): limitTime " + limitTime);
//		System.out.println("getTimeInBetween(): date1 " + milli2String(date1));
//		System.out.println("getTimeInBetween(): date2 " + milli2String(date2));
		
		Calendar calendar1 = Calendar.getInstance(); 
		calendar1.setTime(date1); 
		long hours1 = calendar1.get(Calendar.HOUR_OF_DAY);
		long minutes1 =calendar1.get(Calendar.MINUTE);   
		long seconds1 = calendar1.get(Calendar.SECOND);  
		long total1 = (hours1*3600)+(minutes1*60)+(seconds1);
//		System.out.println("getTimeInBetween(): hours1: " + hours1 + " minutes1: " + minutes1 + " seconds1: " + seconds1 + " total -> " + total1);
		
		Calendar calendar2 = Calendar.getInstance(); 
		calendar2.setTime(date2); 
		long hours2 = calendar2.get(Calendar.HOUR_OF_DAY);
		long minutes2 =calendar2.get(Calendar.MINUTE);   
		long seconds2 = calendar2.get(Calendar.SECOND);  
		long total2 = (hours2*3600)+(minutes2*60)+(seconds2);
//		System.out.println("getTimeInBetween(): hours2: " + hours2 + " minutes2: " + minutes2 + " seconds2: " + seconds2 + " total -> " + total2);
 
		long differnce = (limitTime*3600) - (total1 - total2);
//		System.out.println("getTimeInBetween(): differnce: " + differnce);
//		System.out.println("getTimeInBetween(): differnce > 0 && differnce < (limitTime*3600): " + (differnce > 0 && differnce < (limitTime*3600)));
		if(differnce > 0 && differnce < (limitTime*3600)){
			return differnce * 1000;
		}
		return -1;
	}
	 
	
	public long millisLeft(){
		
		if(!isOnGoing()){
			return -1;
		}
		
		if(timeLeft == -1){
			return timeLeft;
		}
		
		return timeLeft;
	}

	
	public String getTitleHE() {
		return TitleHE;
	}

	public void setTitleHE(String titleHE) {
		TitleHE = titleHE;
	}

	public String getTitleEN() {
		return TitleEN;
	}

	public void setTitleEN(String titleEN) {
		TitleEN = titleEN;
	}

	public String getDescriptionHE() {
		return DescriptionHE;
	}

	public void setDescriptionHE(String descriptionHE) {
		DescriptionHE = descriptionHE;
	}

	public String getDescriptionEN() {
		return DescriptionEN;
	}

	public void setDescriptionEN(String descriptionEN) {
		DescriptionEN = descriptionEN;
	}

	public double getProductPrice() {
		return ProductPrice;
	}

	public void setProductPrice(double productPrice) {
		ProductPrice = productPrice;
	}

	public String getTimeStartString() {
		return TimeStartString;
	}

	public void setTimeStartString(String timeStartString) {
		TimeStartString = timeStartString;
	}

	public double getPrice() {
		return ProductPrice;
	}

	public void setPrice(double price) {
		ProductPrice = price;
	}

	public long getBusinessID() {
		return BusinessID;
	}

	public void setBusinessID(long businessID) {
		BusinessID = businessID;
	}

	public long getProductID() {
		return ProductID;
	}

	public void setProductID(long productID) {
		ProductID = productID;
	}

	public long getID() {
		return ID;
	}

	public void setID(long iD) {
		ID = iD;
	}

	public String getTitle() {
		return Title;
	}

	public void setTitle(String title) {
		Title = title;
	}

	public String getDescription() {
		return Description;
	}

	public void setDescription(String description) {
		Description = description;
	}

	public int getStatus() {
		return Status;
	}

	public void setStatus(int status) {
		Status = status;
	}

	public int getNumUnits() {
		return NumUnits;
	}

	public void setNumUnits(int numUnits) {
		NumUnits = numUnits;
	}

	public double getDiscountPrice() {
		return DiscountPrice;
	}

	public void setDiscountPrice(double discountPrice) {
		DiscountPrice = discountPrice;
	}

	public String getImage() {
		return Image;
	}

	public void setImage(String image) {
		Image = image;
	}

	public String getVideos() {
		return Videos;
	}

	public void setVideos(String videos) {
		Videos = videos;
	}

	public Date getTimeStart() {
		return TimeStart;
	}

	public void setTimeStart(Date timeStart) {
		TimeStart = timeStart;
	}

	public Date getTimeEnd() {
		return TimeEnd;
	}

	public void setTimeEnd(Date timeEnd) {
		TimeEnd = timeEnd;
	}

	public Date getRepeatTo() {
		return RepeatTo;
	}

	public void setRepeatTo(Date repeatTo) {
		RepeatTo = repeatTo;
	}

	public int getRepeated() {
		return Repeated;
	}

	public void setRepeated(int repeated) {
		Repeated = repeated;
	}

	@Override
	public String toString() {
		return "Promotion [ID=" + ID + ", Title=" + Title + ", ProductPrice="
				+ ProductPrice + ", DiscountPrice=" + DiscountPrice
				+ ", Image=" + Image + "]";
	}


	
	
}
