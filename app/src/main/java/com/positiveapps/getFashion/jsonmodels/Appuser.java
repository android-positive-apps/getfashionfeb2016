package com.positiveapps.getFashion.jsonmodels;

import java.io.Serializable;
import java.util.Date;

/** @author Ilia Kaplounov */
public class Appuser implements Serializable{

	private static final long serialVersionUID = 3739177136413104437L;
	
	
	public long ID;
	public int Status;
	public long MPS_ID;
	public boolean IsAuthed;
	public String AuthToken;
	
	public String UDID;
	public String Device;
	public String FBID;
	
	public String Email;
	public String FirstName;
	public String LastName;
	public String Avatar;
	public String Phone;
	public String Name;

	public String Image;
	public String AddressCity;
	public String AddressStreet;
	public String AddressHouse;
	public String AddressApt;
	public String AddressZip;
	public String Lat;
	public String Lng;

	/*filter*/
	public boolean isFiltered;
	public String filteredCategories;
	public int filteredRadius;
	public double filteredLng;
	public double filteredLat;
	public String filteredAddress;

	public String filteredCustom1;
	public String filteredCustom2;
	public String filteredCustom3;
	public String filteredCustom4;
	public String filteredCustom5;
	public boolean filteredBusinesOpen;

	
	public Date LastLocationUpdate;
	
	public String Gender; 
 
	
	
}
