package com.positiveapps.getFashion.jsonmodels.apiresponse;

import java.util.ArrayList;

import com.positiveapps.getFashion.jsonmodels.Promotion;

public class PromotionGetList { 
	
		public boolean error;
		public Data data;

		public static class Data {
			public ArrayList<Promotion> Promotions;
		}
	}
 