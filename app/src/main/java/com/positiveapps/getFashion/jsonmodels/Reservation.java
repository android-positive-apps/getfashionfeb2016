package com.positiveapps.getFashion.jsonmodels;

public class Reservation { 
	
	public long ID;
	public long AppuserID;
	public long ProductID;
	public long BusinessID;
	public long ProductItemID;
	public long Status;
	public String UserName;
	public String UserPhone;
	public long Quantity;
	public String Param1;
	public String Param2;
	public String Param3;
	public String Param4;
	public String Param5;
	public int SaleType;
	public String ReservationDate;
	public String Comments;
	public String Created;
	public String Modified;
	public String AppuserName;
	public String AppuserAddress;
	public String AppuserImage;
	public int ClubMember;
}
