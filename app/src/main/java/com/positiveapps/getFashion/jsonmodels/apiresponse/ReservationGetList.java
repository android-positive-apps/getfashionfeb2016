package com.positiveapps.getFashion.jsonmodels.apiresponse;

import java.util.ArrayList;

import com.positiveapps.getFashion.jsonmodels.Reservation;

public class ReservationGetList {

	public boolean error;
	public Data data;

	public static class Data {
		public ArrayList<Reservation> Reservations;
	}
	
}
