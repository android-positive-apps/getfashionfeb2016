package com.positiveapps.getFashion.jsonmodels.apiresponse;

import java.util.ArrayList;

import com.positiveapps.getFashion.jsonmodels.Business;
 

public class BusinessAsManager {

	public boolean error;
	public Data data;

	public static class Data {
		public ArrayList<Business> businesses;
	}
	
}
 