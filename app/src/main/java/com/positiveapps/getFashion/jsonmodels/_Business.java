/**
 * This class represents a Pub
 */
package com.positiveapps.getFashion.jsonmodels;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;

import org.json.JSONObject;

import android.content.Context;

import com.positiveapps.getFashion.util.DateUtil;

/**
 * This class represents Pub/Bar
 * 
 * @author Maor
 * 
 */
public class _Business {

	public static final int RES_STATUS_NO_RESERVATION = -1;
	public static final int RES_STATUS_PENDING = 0;
	public static final int RES_STATUS_APPROVED = 1;	
	
	private String mID;
	private String mName;
	private String mBusinessDesc;
	private String mCategory;
	private String mLat;
	private String mLng;
	private String mDistance;
	private String mLogoImage;
	private String mCity;
	private String mStreet;
	private String mHouseNum;
	private String mOpening;
	private String mPhone;
	private String mPhone2;
	private String mFax;
	private String mWebsite;
	private String mFacebookLink;

	public int reservationStatus;

	//public boolean isNoPromos = false;
	//private Promotion mClosestPromo = null;
	public WorkDay mTodaysWorkDay = null;

	private boolean mIsKosher;
	private boolean mDeliveries;

	private ArrayList<String> mExtraImages;
	private ArrayList<String> mExtraImageDescriptions;

	private ArrayList<_Promotion> mPromos;

	private ArrayList<WorkDay> mWorkDays;
//	private locationCoordinates mCoordinates;

	public boolean isExpanded = false;

	// private double mDistance = -1; // initialize to 'false' value for 1st
	// time
	private int mLastPageIndex;

	public _Business(JSONObject jsonBusiness, Context context) {
//		try {
//			this.mID = jsonBusiness.optString(APIManager.KEY_ID);
//			this.mName = jsonBusiness.optString(APIManager.KEY_NAME);
//			this.mBusinessDesc = jsonBusiness
//					.optString(APIManager.KEY_BUSINESS_DESC);
//			this.mCategory = jsonBusiness.optString(APIManager.KEY_CATEGORY);
//			this.mLat = jsonBusiness.optString(APIManager.KEY_LAT);
//			this.mLng = jsonBusiness.optString(APIManager.KEY_LOGO_IMAGE);
//			this.mDistance = jsonBusiness.optString(APIManager.KEY_DISTANCE);
//			this.mLogoImage = jsonBusiness.optString(APIManager.KEY_LOGO_IMAGE);
//			this.mCity = jsonBusiness.optString(APIManager.KEY_CITY);
//			this.mStreet = jsonBusiness.optString(APIManager.KEY_STREET);
//			this.mHouseNum = jsonBusiness.optString(APIManager.KEY_HOUSE_NUM);
//			this.mOpening = jsonBusiness.optString(APIManager.KEY_OPENING);
//			this.mPhone = jsonBusiness.optString(APIManager.KEY_PHONE);
//			this.mPhone2 = jsonBusiness.optString(APIManager.KEY_PHONE_2);
//			this.mFax = jsonBusiness.optString(APIManager.KEY_FAX);
//			this.mWebsite = jsonBusiness.optString(APIManager.KEY_WEBSITE);
//			this.mFacebookLink = jsonBusiness
//					.optString(APIManager.KEY_FACEBOOK_LINK);
//
//			this.setIsKosher(jsonBusiness.optInt(APIManager.KEY_IS_KOSHER) == 1);
//			this.setDeliveries(jsonBusiness.optInt(APIManager.KEY_DELIVERIES) == 1);
//
//			mExtraImages = new ArrayList<String>();
//			mExtraImageDescriptions = new ArrayList<String>();
//
//			reservationStatus = DBHandler.getInstance(context)
//					.getReservationStatus(mID);
//
//			JSONArray jsonExtraImages = jsonBusiness
//					.getJSONArray(APIManager.KEY_EXTRA_IMAGES);
//
//			mPromos = parsePromos(jsonBusiness);
//			mWorkDays = parseWorkDays(jsonBusiness);
//
//
//			// Extra images
//			for (int i = 0; i < jsonExtraImages.length(); i++) {
//				mExtraImages.add(jsonExtraImages.getJSONObject(i).optString(
//						APIManager.KEY_URL));
//				mExtraImageDescriptions.add(jsonExtraImages.getJSONObject(i)
//						.optString(APIManager.KEY_EXTRA_IMAGE_DESC));
//			}
//
//		} catch (JSONException e) {
//			App
//					.Log("'Restaurant' Constructor - Error while trying to parse json object");
//		}

	}

	public boolean isOpenNow() {
		if (!mTodaysWorkDay.isOpen)
			return false;

		Date now = Calendar.getInstance().getTime();
		Date open = mTodaysWorkDay.openTime.getTime();
		Date close = mTodaysWorkDay.closeTime.getTime();

		if (now.after(open) && now.after(close))
			return true;

		return false;
	}
	
	
	public _Promotion getClosestPromo(){
		return getClosestPromo(mPromos);
	}
	
	private _Promotion getClosestPromo(ArrayList<_Promotion> promos) {
		final long THREE_HOURS = 10800000;
		final long MINUS_THREE_HOURS = -10800000;
		final long FORTY_EIGHT_HOURS = 172800000;
		ArrayList<_Promotion> futurePromos = new ArrayList<>();
		long curMilis = DateUtil.getCurrentDateInMilli();

		for (int i = 0; i < promos.size(); i++) {
			if (promos.get(i).promoMilisStart - curMilis > MINUS_THREE_HOURS) {
				futurePromos.add(promos.get(i));

				if (promos.get(i).promoMilisStart - curMilis < 0) {
					promos.get(i).isOnGoing = true;
					promos.get(i).promoMilisEnd = promos.get(i).promoMilisStart
							+ THREE_HOURS;
				}

				if (promos.get(i).promoMilisStart - curMilis < FORTY_EIGHT_HOURS) {
					promos.get(i).isLessThan48HoursAway = true;
				}
			}
		}

		if (!futurePromos.isEmpty())
			return Collections.min(futurePromos);

		return null;
	}

	private ArrayList<WorkDay> parseWorkDays(JSONObject json) {

		ArrayList<WorkDay> workDays = new ArrayList<_Business.WorkDay>();
//		final int NUMBER_OF_DAYS = 7;
//
//		for (int i = 0; i < NUMBER_OF_DAYS; i++) {
//			String keyIsOpen = APIManager.KEY_OPEN_DAY_PRE + (i + 1);
//
//			String keyOpenStart = APIManager.KEY_OPEN_DAY_PRE + (i + 1)
//					+ APIManager.KEY_OPEN_DAY_START_SUF;
//
//			String keyOpenEnd = APIManager.KEY_OPEN_DAY_PRE + (i + 1)
//					+ APIManager.KEY_OPEN_DAY_END_SUF;
//
//			try {
//				boolean isOpen = json.getString(keyIsOpen).equals("1");
//				String openStartStr = json.getString(keyOpenStart);
//				String openEndStr = json.getString(keyOpenEnd);
//
//				WorkDay workDay = new WorkDay();
//				workDay.isOpen = isOpen;
//
//				workDay.openTime = hoursAndMinutesToCalendat(openStartStr);
//				workDay.closeTime = hoursAndMinutesToCalendat(openEndStr);
//				workDays.add(workDay);
//
//				if (Calendar.getInstance().get(Calendar.DAY_OF_WEEK) == i+1) {
//					mTodaysWorkDay = workDay;
//				}
//
//			} catch (JSONException e) {
//				e.printStackTrace();
//			}
//
//		}

		return workDays;
	}

	private Calendar hoursAndMinutesToCalendat(String hAndM) {
		String time[] = hAndM.split(":");
		Calendar cal = Calendar.getInstance();
		int hour, minute;
		
		cal.clear();
		if (time.length < 2)
			return cal;
		
		try{
			hour = Integer.valueOf(time[0]);
			minute = Integer.valueOf(time[1]);
			
		}catch(NumberFormatException|ArrayIndexOutOfBoundsException e){
			return cal;
		}

		cal.set(Calendar.HOUR_OF_DAY, Integer.valueOf(time[0]));
		cal.set(Calendar.MINUTE, Integer.valueOf(time[1]));

		return cal;
	}

	private ArrayList<_Promotion> parsePromos(JSONObject json) {
		ArrayList<_Promotion> promos = new ArrayList<>();
//		// int dayOfWeek = Calendar.getInstance().get(Calendar.DAY_OF_WEEK);
//		final int NUMBER_OF_PROMOS = 7;
//		
//		
//		for (int i = 0; i < NUMBER_OF_PROMOS; i++) {
//			String pre = APIManager.KEY_PROMO_PRE + (i + 1);
//			_Promotion t = new _Promotion();
//			try {
//				/*
//				 * t.title = json.getJSONArray(APIManager.KEY_MAIN_IMAGES)
//				 * .getJSONObject(i).optString(APIManager.KEY_IMAGE_DESC);
//				 */
//				
//				JSONObject imageObject= json.getJSONArray(APIManager.KEY_MAIN_IMAGES)
//						.getJSONObject(i);
//				
//				t.promoImage = imageObject.optString(APIManager.KEY_URL);
//				
//				t.title = imageObject.optString(APIManager.KEY_PROMO_TITLE);
//
//				t.description = imageObject.optString(APIManager.KEY_IMAGE_DESC);
//				
//			} catch (JSONException e) {
//				e.printStackTrace();
//			}
//			
//			//old version
//			//t.title = json.optString(pre + APIManager.KEY_PROMO_TITLE_SUF);
//
//			String getEatPrice = json.optString(pre
//					+ APIManager.KEY_PROMO_GETEAT_PRICE_SUF);
//			String regularPrice = json.optString(pre
//					+ APIManager.KEY_PROMO_REGULAR_PRICE_SUF);
//
//			if (getEatPrice.indexOf(".") != -1)
//				t.getEatPrice = getEatPrice.substring(0,
//						getEatPrice.indexOf("."));
//
//			if (regularPrice.indexOf(".") != -1)
//				t.regularPrice = regularPrice.substring(0,
//						regularPrice.indexOf("."));
//
//			t.amountLimit = json.optInt(pre
//					+ APIManager.KEY_PROMO_AMOUNT_LIMIT_SUF, 0);
//			t.amountLeft = json.optInt(pre
//					+ APIManager.KEY_PROMO_AMOUNT_ORDERED_SUF, 0);
//
//			t.amountOrdered = t.amountLimit - t.amountLeft;
//
//			t.startTime = json.optString(pre + APIManager.KEY_PROMO_TIME_SUF);
//
//			if (!t.startTime.equals("00:00:00") && !t.startTime.isEmpty()) {
//				t.promoMilisStart = getPromoMilis(t.startTime, i + 1);
//				t.promoDateNoYear = DateUtil
//						.getClosestFutureSringDateByDayNumber(i + 1);
//			}
//
//			promos.add(i, t);
//		}

		return promos;
	}

	private long getPromoMilis(String time, int dayNum) {
		String timeParts[] = time.split(":");
		int h = Integer.parseInt(timeParts[0]);
		int m = Integer.parseInt(timeParts[1]);
		int s = Integer.parseInt(timeParts[2]);

		Calendar promoDate = DateUtil
				.getClosestFutureCalendarDateByDayNumber(dayNum);
		promoDate.set(promoDate.get(Calendar.YEAR),
				promoDate.get(Calendar.MONTH),
				promoDate.get(Calendar.DAY_OF_MONTH), h, m);
		return promoDate.getTimeInMillis();
	}

	/**
	 * @return the mID
	 */
	public String getID() {
		return mID;
	}

	/**
	 * @param mID
	 *            the mID to set
	 */
	public void setID(String mID) {
		this.mID = mID;
	}

	/**
	 * @return the mName
	 */
	public String getName() {
		return mName;
	}

	/**
	 * @param mName
	 *            the mName to set
	 */
	public void setName(String mName) {
		this.mName = mName;
	}

	/**
	 * @return the mBusinessDesc
	 */
	public String getBusinessDesc() {
		return mBusinessDesc;
	}

	/**
	 * @param mBusinessDesc
	 *            the mBusinessDesc to set
	 */
	public void setBusinessDesc(String mBusinessDesc) {
		this.mBusinessDesc = mBusinessDesc;
	}

	/**
	 * @return the mCategory
	 */
	public String getCategory() {
		return mCategory;
	}

	/**
	 * @param mCategory
	 *            the mCategory to set
	 */
	public void setCategory(String mCategory) {
		this.mCategory = mCategory;
	}

	/**
	 * @return the mLat
	 */
	public String getLat() {
		return mLat;
	}

	/**
	 * @param mLat
	 *            the mLat to set
	 */
	public void setLat(String mLat) {
		this.mLat = mLat;
	}

	/**
	 * @return the mLng
	 */
	public String getLng() {
		return mLng;
	}

	/**
	 * @param mLng
	 *            the mLng to set
	 */
	public void setLng(String mLng) {
		this.mLng = mLng;
	}

	/**
	 * @return the mLogoImage
	 */
	public String getLogoImage() {
		return mLogoImage;
	}

	/**
	 * @param mLogoImage
	 *            the mLogoImage to set
	 */
	public void setLogoImage(String mLogoImage) {
		this.mLogoImage = mLogoImage;
	}

	/**
	 * @return the City of this pub
	 */
	public String getCity() {
		return mCity;
	}

	/**
	 * @param mCity
	 *            the City to set
	 */
	public void setCity(String mCity) {
		this.mCity = mCity;
	}

	/**
	 * @return the Street
	 */
	public String getStreet() {
		return mStreet;
	}

	/**
	 * @param mStreet
	 *            the street to set
	 */
	public void setStreet(String mStreet) {
		this.mStreet = mStreet;
	}

	/**
	 * @return the House Number
	 */
	public String getHouseNum() {
		return mHouseNum;
	}

	/**
	 * @param mHouseNum
	 *            the mHouseNum to set
	 */
	public void setHouseNum(String mHouseNum) {
		this.mHouseNum = mHouseNum;
	}

	/**
	 * @return the Opening
	 */
	public String getOpening() {
		return mOpening;
	}

	/**
	 * @param mOpening
	 *            the Opening to set
	 */
	public void setOpening(String mOpening) {
		this.mOpening = mOpening;
	}

	/**
	 * @return the Phone of this pub
	 */
	public String getPhone() {
		return mPhone;
	}

	/**
	 * @param mPhone
	 *            the phone to set
	 */
	public void setPhone(String mPhone) {
		this.mPhone = mPhone;
	}

	/**
	 * @return the mPhone2
	 */
	public String getPhone2() {
		return mPhone2;
	}

	/**
	 * @param mPhone2
	 *            the mPhone2 to set
	 */
	public void setPhone2(String mPhone2) {
		this.mPhone2 = mPhone2;
	}

	/**
	 * @return the mFax
	 */
	public String getFax() {
		return mFax;
	}

	/**
	 * @param mFax
	 *            the mFax to set
	 */
	public void setFax(String mFax) {
		this.mFax = mFax;
	}

	/**
	 * @return the website of this pub
	 */
	public String getWebsite() {
		return mWebsite;
	}

	/**
	 * @param mWebsite
	 *            the website address to set
	 */
	public void setWebsite(String mWebsite) {
		this.mWebsite = mWebsite;
	}

	/**
	 * @return the FacebookLink
	 */
	public String getFacebookLink() {
		return mFacebookLink;
	}

	/**
	 * @param mFacebookLink
	 *            the FacebookLink to set
	 */
	public void setFacebookLink(String mFacebookLink) {
		this.mFacebookLink = mFacebookLink;
	}

	/**
	 * @return the mFullAddress
	 */
	public String getFullAddress() {

//		String appLocale = App.appPreference.getAppLocale();
//
//		if (appLocale.equalsIgnoreCase("en")) {
//			return null;
//		}

		return null;
	}

	/**
	 * @return mImages - the images of this pub
	 */
	// public ArrayList<String> getImages() {
	// return mImages;
	// }
	//
	// /**
	// * @param mImages
	// * the images to set
	// */
	// public void setImages(ArrayList<String> mImages) {
	// this.mImages = mImages;
	// }
	//
	// /**
	// * @return the mImageDescriptions - the image descriptions
	// */
	// public ArrayList<String> getImageDescriptions() {
	// return mImageTitles;
	// }
	//
	// /**
	// * @param mImageDescriptions
	// * - the image descriptions to set
	// */
	// public void setImageDescriptions(ArrayList<String> mImageDescriptions) {
	// this.mImageTitles = mImageDescriptions;
	// }

	/**
	 * @return the mExtraImages - the extra images of this pub
	 */
	public ArrayList<String> getExtraImages() {
		return mExtraImages;
	}

	/**
	 * @param mExtraImages
	 *            the extra images of this pub to set
	 */
	public void setExtraImages(ArrayList<String> mExtraImages) {
		this.mExtraImages = mExtraImages;
	}

	/**
	 * @return the mExtraImageDescriptions
	 */
	public ArrayList<String> getExtraImageDescriptions() {
		return mExtraImageDescriptions;
	}

	/**
	 * @param mExtraImageDescriptions
	 *            the mExtraImageDescriptions to set
	 */
	public void setExtraImageDescriptions(
			ArrayList<String> mExtraImageDescriptions) {
		this.mExtraImageDescriptions = mExtraImageDescriptions;
	}

	/**
	 * @return the mCoordinates of this pub
	 */
//	public locationCoordinates getCoordinates() {
//		return mCoordinates;
//	}
//
//	/**
//	 * @param mCoordinates
//	 *            the coordinates of this pub to set
//	 */
//	public void setCoordinates(locationCoordinates mCoordinates) {
//		this.mCoordinates = mCoordinates;
//	}

	/**
	 * @return the mDistance - the distance of this pub
	 */
	public String getDistance() {

		try {
			String[] splitParts = mDistance.split("\\.");

			return splitParts[0] + "." + splitParts[1].charAt(0);

		} catch (Exception ex) {

		}
		return null;
	}

	/**
	 * @param mDistance
	 *            - the distance of this pub to set
	 */
	public void setDistance(String mDistance) {
		this.mDistance = mDistance;
	}

	/**
	 * @return the mLastPageIndex - the Last Page Index of this pub item
	 */
	public int getLastPageIndex() {
		return mLastPageIndex;
	}

	/**
	 * @param mLastPageIndex
	 *            - the Last Page Index of this pub item
	 */
	public void setLastPageIndex(int mLastPageIndex) {
		this.mLastPageIndex = mLastPageIndex;
	}

	public boolean isKosher() {
		return mIsKosher;
	}

	public void setIsKosher(boolean mIsKosher) {
		this.mIsKosher = mIsKosher;
	}

	public boolean isDeliveries() {
		return mDeliveries;
	}

	public void setDeliveries(boolean mDeliveries) {
		this.mDeliveries = mDeliveries;
	}

	public ArrayList<_Promotion> getPromos() {
		return mPromos;
	}


	public class WorkDay {
		boolean isOpen;
		Calendar openTime;
		Calendar closeTime;

	}

}
