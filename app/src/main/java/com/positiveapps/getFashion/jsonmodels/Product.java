package com.positiveapps.getFashion.jsonmodels;

import java.util.ArrayList;

public class Product {
 
	public long ID;
	public long BusinessID;

	

	public String Title;
	public String Image;
	public String Description;

	public int Status;
	
	public boolean AvailableForSale;
	public int DisplayOrder;
	public double Price;
	
	public String TitleHE;
	public String TitleEN;
	public String DescriptionHE;
	public String DescriptionEN; 
 
	public ArrayList<String> Categories;
	public ArrayList<ProductItem> ProductItems;

	public ArrayList<PriceLabel> Prices;



	public boolean noSizes(){
		ArrayList<?> sizes= getSizes();
		return sizes == null || sizes.size() == 0;
	}
	
	public boolean noColors(){
		ArrayList<?> colors= getColors();
		return colors == null || colors.size() == 0;
	}
	
	public ArrayList<String> getSizes() {
		ArrayList<String> sizes = new ArrayList<String>();
		ProductItem current;

		if (noItems())
			return null;

		for (int i = 0; i < ProductItems.size(); i++) {
			current = ProductItems.get(i);

//			if (current.Size != null && !current.Size.isEmpty())
//				sizes.add(current.Size);
		}

		return sizes;
	}

 
	
	public String getTitleHE() {
		return TitleHE;
	}

	public void setTitleHE(String titleHE) {
		TitleHE = titleHE;
	}

	public String getTitleEN() {
		return TitleEN;
	}

	public void setTitleEN(String titleEN) {
		TitleEN = titleEN;
	}

	public String getDescriptionHE() {
		return DescriptionHE;
	}

	public void setDescriptionHE(String descriptionHE) {
		DescriptionHE = descriptionHE;
	}

	public String getDescriptionEN() {
		return DescriptionEN;
	}

	public void setDescriptionEN(String descriptionEN) {
		DescriptionEN = descriptionEN;
	}

	public ArrayList<String> getColors() {
		ArrayList<String> colors = new ArrayList<String>();
		ProductItem current;
		
		if (noItems())
			return colors;

		for (int i = 0; i < ProductItems.size(); i++) {
			current = ProductItems.get(i);
//
//			if (current.Color != null && !current.Color.isEmpty())
//				colors.add(current.Color);
		}

		return colors;
	}

	public boolean noItems() {
		return ProductItems == null || ProductItems.isEmpty();
	}

	public long getID() {
		return ID;
	}

	public void setID(long iD) {
		ID = iD;
	}

	public long getBusinessID() {
		return BusinessID;
	}

	public void setBusinessID(long businessID) {
		BusinessID = businessID;
	}

	public ArrayList<PriceLabel> getPrices() {
		return Prices;
	}

	public void setPrices(ArrayList<PriceLabel> prices) {
		Prices = prices;
	}

	public String getTitle() {
		return Title;
	}

	public void setTitle(String title) {
		Title = title;
	}

	public String getImage() {
		return Image;
	}

	public void setImage(String image) {
		Image = image;
	}

	public String getDescription() {
		return Description;
	}

	public void setDescription(String description) {
		Description = description;
	}

	public int getStatus() {
		return Status;
	}

	public void setStatus(int status) {
		Status = status;
	}

	public boolean isAvailableForSale() {
		return AvailableForSale;
	}

	public void setAvailableForSale(boolean availableForSale) {
		AvailableForSale = availableForSale;
	}

	public int getDisplayOrder() {
		return DisplayOrder;
	}

	public void setDisplayOrder(int displayOrder) {
		DisplayOrder = displayOrder;
	}

	public double getPrice() {
		return Price;
	}

	public void setPrice(double price) {
		Price = price;
	}

	public ArrayList<ProductItem> getProductItems() {
		return ProductItems;
	}

	public void setProductItems(ArrayList<ProductItem> productItems) {
		ProductItems = productItems;
	}

	@Override
	public String toString() {
		return "Product [ID=" + ID + ", Title=" + Title + ", ProductItems="
				+ ProductItems + "]";
	}
	
	
	
	// public Date Created;
	// public Date Modified;
}
