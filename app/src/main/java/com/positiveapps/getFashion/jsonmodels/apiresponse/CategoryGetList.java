package com.positiveapps.getFashion.jsonmodels.apiresponse;

import java.util.ArrayList;

import com.positiveapps.getFashion.jsonmodels.Category;

public class CategoryGetList {
	public boolean error;
	public Data data;

	public static class Data {
		public ArrayList<Category> Categories;
	}
}
