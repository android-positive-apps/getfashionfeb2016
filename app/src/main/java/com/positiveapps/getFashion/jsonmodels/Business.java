package com.positiveapps.getFashion.jsonmodels;

import android.util.Log;

import java.io.Serializable;
import java.util.ArrayList;

public class Business implements Serializable{
 
	private static final long serialVersionUID = -3727497992425148346L;
	
	public long ID;
	public int Status;
	public long tempCategory; 
	public int EnableReservations;
	public int Recommended;
	public int Promoted;
	public int Delivery;
	public int ClubMembership;
	public int Custom1;
	public int Custom2;
	public int Custom3;
	public int Custom4;
	public int Custom5;
//	"Custom1": 0,
//			"Custom2": 1,
//			"Custom3": 0,
//			"Custom4": 1,
//			"Custom5": 0,

	public String Name;
	public String NameHE;
	public String NameEN;
	public String ContactName;
	public String ContactEmail;
	public String ContactPhone;
	public String Phone;
	public String Fax;

	public String AddressCity;
	public String AddressStreet;
	public String AddressHouse;
	public String AddressApt;
	public String AddressZip;
	
	public String OpeningTime;
	public String ClosingTime;
	public String OpeningTimeFriday;
	public String ClosingTimeFriday;
	public String OpeningTimeSaturday;
	public String ClosingTimeSaturday;
 
	public double Lat;
	public double Lng;

	public String CoverImage;
	public String Logo;
	public String Website;
	// public Date Created;
	// public Date Modified;

	public ArrayList<Promotion> Promotions;
	public ArrayList<Product> Products;

	public Promotion getNearestPromotion() {
		Promotion nearest;

		if (noPromotions())
			return null;

		nearest = Promotions.get(0);

		// for(int i=1; i<Promotions.size(); i++){
		// Promotion current = Promotions.get(i);
		// if(current.TimeStart.before(nearest.TimeStart)){
		// nearest = current;
		// }
		// }

		return nearest;
	}

	public Promotion getPromotionById(long promotionId) {
		Promotion current = null;
		int size;

		if (noPromotions())
			return null;

		size = Promotions.size();

		for (int i = 0; i < size; i++) {
			current = Promotions.get(i);
			if (current.ID == promotionId) {
				return current;
			}
		}

		return null;
	}

	public Product getProductById(long productId) {
		Product current = null;
		int size;

		if (noProducts())
			return null;

		size = Products.size();

		for (int i = 0; i < size; i++) {
			current = Products.get(i);
			if (current.ID == productId) {
				return current;
			}
		}

		return null;
	}

	public boolean noPromotions() {
		return Promotions == null || Promotions.isEmpty();
	}
	public boolean hasPromotions() {
		return Promotions != null && !Promotions.isEmpty();
	}

	public boolean noProducts() {
		Log.e("CAT", "Products " + Products);
		return Products == null || Products.isEmpty();
	}

	public int getPromotionCount() {
		if (Promotions == null)
			return 0;

		return Promotions.size();
	}

	public int getProductCount() {
		if (Products == null)
			return 0;

		return Products.size();
	}

	public boolean hasPhone() {
		return Phone != null && !Phone.isEmpty();
	}

	public ArrayList<Product> getProductsForCategory(long categoryID) {
		Log.v("getProductsForCategory", "categoryID " + categoryID);
		ArrayList<Product> productsForCategory=null;

		if (noProducts()){
			Log.v("getProductsForCategory", "noProducts");
			return null;
		}

		for (int i = 0; i < getProductCount(); i++) {
			if(Products.get(i).Categories != null){
			for (int j = 0; j < Products.get(i).Categories.size(); j++) {
				if (Long.parseLong(Products.get(i).Categories.get(j)) == categoryID) {
					
					//only create the array list if thre's at least 1 product, to save memory
					if (productsForCategory == null)
						productsForCategory = new ArrayList<Product>();
					
					Log.v("getProductsForCategory", "the product " + Products.get(i).Title);
					Log.v("getProductsForCategory", "the product id " + Products.get(i).Categories.get(j));
					
					productsForCategory.add(Products.get(i));
				}
			}
			}
		}

		return productsForCategory;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (ID ^ (ID >>> 32));
		return result;
	}
	
	

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Business other = (Business) obj;
		if (ID != other.ID)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Business [Products=" + Products + "]";
	}

	
	
}
