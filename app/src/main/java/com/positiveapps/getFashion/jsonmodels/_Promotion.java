package com.positiveapps.getFashion.jsonmodels;


public class _Promotion implements Comparable<_Promotion> {
	public int amountOrdered = 0;
	public String regularPrice = "";
	public String getEatPrice = "";
	public int amountLeft = 0;
	public int amountLimit = 0;
	public String startTime = "";
	public String title = "";
	public String description = "";
	public String promoImage = "";
	public long promoMilisStart = 0;
	public long promoMilisEnd = 0;
	public String promoDateNoYear = "";

	public boolean isOnGoing = false;
	public boolean isLessThan48HoursAway = false;

	@Override
	public int compareTo(_Promotion another) {
		if (promoMilisStart > another.promoMilisStart)
			return 1;
		
		if (promoMilisStart < another.promoMilisStart)
			return -1;

		return 0;
	}
	
	
}
