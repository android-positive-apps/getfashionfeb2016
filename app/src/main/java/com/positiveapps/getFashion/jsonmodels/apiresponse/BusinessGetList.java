package com.positiveapps.getFashion.jsonmodels.apiresponse;

import java.util.ArrayList;

import com.positiveapps.getFashion.jsonmodels.Business;

public class BusinessGetList {
	public boolean error;
	public Data data;
	
	public static class Data{
		public int  TotalRecords;
		public int Page;
		public int PageSize;
		public int TotalPages;
		public ArrayList<Business> Records;		
	}
}
