package com.positiveapps.getFashion.jsonmodels.apiresponse;

import com.positiveapps.getFashion.jsonmodels.Business;

public class BusinessGet {
	
	public boolean error;
	public Data data;
	
	public static class Data{
		public Business Business;
	}
}
