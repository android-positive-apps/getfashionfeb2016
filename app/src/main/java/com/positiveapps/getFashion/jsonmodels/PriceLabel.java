package com.positiveapps.getFashion.jsonmodels;

/**
 * Created by Niv on 5/16/2016.
 */
public class PriceLabel {
    private String label;
    private String value;

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
