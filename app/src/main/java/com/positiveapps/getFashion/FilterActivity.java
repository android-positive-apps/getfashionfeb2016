package com.positiveapps.getFashion;

import android.app.Dialog;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.location.LocationServices;
import com.positiveapps.getFashion.adapters.CategotySwitchFilterAdapter;
import com.positiveapps.getFashion.backend.Categories;
import com.positiveapps.getFashion.backend.UserManager;
import com.positiveapps.getFashion.dialogs.MessageDialog.DialogListener;
import com.positiveapps.getFashion.jsonmodels.Appuser;
import com.positiveapps.getFashion.jsonmodels.Category;
import com.positiveapps.getFashion.util.ActivityStarter;
import com.positiveapps.getFashion.util.Coordinates;
import com.positiveapps.getFashion.util.DialogHelper;
import com.positiveapps.getFashion.util.GeneralHelper;
import com.positiveapps.getFashion.util.LocationUtil;
import com.positiveapps.getFashion.util.LocationUtil.LocationUtilCallbac;
import com.positiveapps.getFashion.views.ExpandableHeightGridView;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

public class FilterActivity extends BaseActivity implements OnClickListener, ConnectionCallbacks, OnConnectionFailedListener, OnSeekBarChangeListener {

	
	private Toolbar mToolbar;
	private LayoutInflater mInflater;
	private FilterActivity filterActivity;
	private boolean isActivityOn;
	private TextView txtTitle;
	private ExpandableHeightGridView gridView;
	private CategotySwitchFilterAdapter adapter;
	private Button btnSet;
	private EditText etxtLocation;
	private Coordinates coordinates;
	private LinearLayout btnCurrentLocation;
	private GoogleApiClient mGoogleApiClient;
	private boolean mGpsDialogDisplayed = false;
	private SeekBar seekBarFilter;
	private TextView seekBarProggress;
	private TextView btnSearch;
	private TextView btn_cancelFilter;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_filter);
		
		
		filterActivity = this;
		isActivityOn = true;
		mInflater = LayoutInflater.from(this);
		findViews();
		initActionbar();
		buildGoogleApiClient();
		if(Categories.getCategories() != null){
			if(Categories.getCategories().size() != 0){
			ArrayList<Category> categories = Categories.getRealCategories();
			adapter = new CategotySwitchFilterAdapter(FilterActivity.this, categories, convertStringToArrayList(UserManager.getAppUser().filteredCategories));
			gridView.setAdapter(adapter);
			}
		}
		 initUI();
 
	
	}


	private void findViews() {
		mToolbar = (Toolbar) findViewById(R.id.actionbar2);	
		txtTitle = (TextView) findViewById(R.id.manageTitle);
		gridView = (ExpandableHeightGridView) findViewById(R.id.gridView_categories);
		btnSet = (Button) findViewById(R.id.btn_Set);
		etxtLocation = (EditText) findViewById(R.id.etxt_location);
		btnCurrentLocation = (LinearLayout) findViewById(R.id.btn_CurrentLocation);
		seekBarFilter = (SeekBar) findViewById(R.id.seekBar_filter);
		seekBarProggress = (TextView) findViewById(R.id.seekBarProggress);
		btnSearch = (TextView) findViewById(R.id.btn_Search);
		btn_cancelFilter = (TextView) findViewById(R.id.btn_cancelFilter);

		btnSearch.setOnClickListener(this);
		btn_cancelFilter.setOnClickListener(this);
		seekBarFilter.setOnSeekBarChangeListener(this);
		gridView.setExpanded(true);
		btnCurrentLocation.setOnClickListener(this);
		btnSet.setOnClickListener(this);
	}


	private void initActionbar() {
		setSupportActionBar(mToolbar);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setDisplayShowTitleEnabled(false);
		
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			saveData();
			return true;
		}	
		return super.onOptionsItemSelected(item);
	}
	
	 
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		filterActivity = null;
	}


	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btn_Set:
			if (GeneralHelper.ValidateOrPaintRed(etxtLocation)) {
				LocationUtil.getPickupLocationByPickupAddress(
						FilterActivity.this, etxtLocation.getText().toString(),
						new LocationUtilCallbac() {

							@Override
							public void onDataReceived(boolean success,
									Coordinates coordinates) {
								if (isActivityOn) {
									FilterActivity.this.coordinates = coordinates;
									showtoast();
								}
							}
						});
			}
			break;
		case R.id.btn_CurrentLocation:
//			Location mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
//			if (mLastLocation != null) {
//				coordinates = new Coordinates(mLastLocation.getLatitude(), mLastLocation.getLongitude());
				coordinates = new Coordinates(UserManager.getAppUser().Lat, UserManager.getAppUser().Lng);

				FilterActivity.this.coordinates = coordinates;
				showtoast();
//			}
			break;
		case R.id.btn_Search:
			saveData();
			break;
		case R.id.btn_cancelFilter:
			resetData();
		    break;
		default:
			break;
		}

	}




	@Override
	protected void onStart() {
		super.onStart();
		mGoogleApiClient.connect();
	}

	protected synchronized void buildGoogleApiClient() {
		mGoogleApiClient = new GoogleApiClient.Builder(this).addConnectionCallbacks(this).addOnConnectionFailedListener(this)
				.addApi(LocationServices.API).build();
	}

	@Override
	protected void onPause() {
		super.onPause();
		isActivityOn = false;
	}

	@Override
	protected void onStop() {
		super.onStop();
		if (mGoogleApiClient.isConnected()) {
			mGoogleApiClient.disconnect();
		}
	}
	
	private void displayGpsDialog() {
		DialogHelper.messageDialog(getSupportFragmentManager(), getString(R.string.location_dialog_title),
				getString(R.string.location_dialog_message), new DialogListener() {

					@Override
					public void onOk(Dialog d) {
						ActivityStarter.openGPSSettings(FilterActivity.this);
						d.dismiss();
					}

					@Override
					public void onCancel(Dialog d) {
						d.dismiss();
					}
				});
	}


	@Override
	public void onConnected(Bundle arg0) {
		// TODO Auto-generated method stub
		Location mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
		if (mLastLocation != null) {
			coordinates = new Coordinates(mLastLocation.getLatitude(), mLastLocation.getLongitude());
			FilterActivity.this.coordinates = coordinates;
		} else {
//			if (!mGpsDialogDisplayed) {
//				displayGpsDialog();
//			}
			mGpsDialogDisplayed = true;
		}
	}


	@Override
	public void onConnectionSuspended(int arg0) {
		// TODO Auto-generated method stub
		mGoogleApiClient.connect();
	}


	@Override
	public void onConnectionFailed(ConnectionResult arg0) {
		// TODO Auto-generated method stub
		
	}
	private void resetData() {
		Appuser user = UserManager.getAppUser();
		user.isFiltered=false;
		user.filteredCategories = adapter.resetCategoriesArray();
		user.filteredRadius=100;
		UserManager.setAppuser(user);
		Intent returnIntent = new Intent();
		setResult(RESULT_OK,returnIntent);
		finish();
	}
	
	private void saveData() {
		int radius = seekBarFilter.getProgress();
		Appuser user = UserManager.getAppUser();
		user.filteredCategories = adapter.getCategoriesArray();
		user.filteredRadius = radius;

		user.filteredAddress = etxtLocation.getText().toString();
		if(coordinates != null) {
			user.filteredLng = coordinates.lon;
			user.filteredLat = coordinates.lat;
		}
		System.out.println("filteredLng " + user.filteredLng + "/"  + user.filteredLat);
		user.isFiltered = true;
		UserManager.setAppuser(user);
		
		
		Intent returnIntent = new Intent();
		setResult(RESULT_OK,returnIntent);
		finish();
	}


	@Override
	public void onProgressChanged(SeekBar seekBar, int progress,
			boolean fromUser) {
		// TODO Auto-generated method stub
		seekBarProggress.setText(progress + " " + getString(R.string.km));
	}


	@Override
	public void onStartTrackingTouch(SeekBar seekBar) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void onStopTrackingTouch(SeekBar seekBar) {
		// TODO Auto-generated method stub
		
	}
	
	private void showtoast() {
		Toast.makeText(this, getString(R.string.your_location_save), Toast.LENGTH_LONG).show();
	}
	
	private void initUI() {
		if(UserManager.getAppUser().filteredAddress != null){
			etxtLocation.setHint(UserManager.getAppUser().filteredAddress);
		}
		seekBarFilter.setProgress(UserManager.getAppUser().filteredRadius); 
		seekBarProggress.setText(UserManager.getAppUser().filteredRadius + " " + getString(R.string.km));
	}

	public ArrayList<String> convertStringToArrayList(String string) {
		ArrayList<String> temp = new ArrayList<>();
		if (string != null) {
			JSONArray jsonArray;
			try {
				jsonArray = new JSONArray(string);
				for (int i = 0; i < jsonArray.length(); i++) {
					temp.add(jsonArray.getInt(i) + "");
				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return temp;
	}
}
