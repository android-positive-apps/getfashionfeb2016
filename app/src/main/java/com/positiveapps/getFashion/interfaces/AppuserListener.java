package com.positiveapps.getFashion.interfaces;

import retrofit.RetrofitError;

import com.positiveapps.getFashion.jsonmodels.Appuser;

public interface AppuserListener{
	public void onAppuserLoginSuccess(Appuser user);
	public void onAppuserLoginFail(RetrofitError e);
	public void onCheckManager(boolean isManager);
}