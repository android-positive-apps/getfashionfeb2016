package com.positiveapps.getFashion.interfaces;

/**
 * Created by Niv on 2/17/2016.
 */
public interface ManagePromationResumeListener {
    public void onPromotionsResumed();
}
