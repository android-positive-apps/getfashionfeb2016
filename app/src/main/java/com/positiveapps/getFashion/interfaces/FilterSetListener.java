package com.positiveapps.getFashion.interfaces;

/**
 * Created by Niv on 2/1/2016.
 */
public interface FilterSetListener {
    public void onFilterSet();
}
