package com.positiveapps.getFashion.interfaces;

import java.util.ArrayList;

import com.positiveapps.getFashion.jsonmodels.Business;

public interface BusinessRepositoryCallbacks {
	public void onFinishedLoadingBusiness(ArrayList<Business> businesses);

	public void onNoMoreBusinessResults();

	public void onProgressLoadingBusinesses();

	public void onErrorLoadingBusinesses(Throwable throwable);
}