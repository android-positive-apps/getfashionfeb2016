package com.positiveapps.getFashion;

import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.widget.TextView;

import com.positiveapps.getFashion.fragments.manage.ManageOrdersFragment;
import com.positiveapps.getFashion.fragments.manage.ManageProductsFragment;
import com.positiveapps.getFashion.fragments.manage.ManagePromotionFragment;
import com.positiveapps.getFashion.interfaces.ManagePromationResumeListener;
import com.positiveapps.getFashion.util.ActivityStarter;

public class ManageActivity extends BaseActivity implements ManagePromationResumeListener{
	
	private Toolbar mToolbar;
	private LayoutInflater mInflater;
	public static ManageActivity manageActivity;
	public static boolean isActivityOn;
	public TextView txtTitle;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_manage);
		manageActivity = this;
		isActivityOn = true;
		mInflater = LayoutInflater.from(this);
		findViews();
		
		if(getIntent().getExtras() != null){
			goToFragment(getIntent().getExtras().getInt(ActivityStarter.MANAGE));
		}else{
			finish();
		}
		initActionbar();
	}
	
	private void goToFragment(int which) {
		switch (which) {
		case ActivityStarter.MANAGE_ORDERS:
			getSupportFragmentManager().beginTransaction().replace(R.id.manageLayout, ManageOrdersFragment.newInstance(), "MANAGE_ORDERS").commit();
			txtTitle.setText(getString(R.string.drawermenu_manage_reservations));
			break;
		case ActivityStarter.MANAGE_PROMOTION:
			if(getIntent().getLongArrayExtra("ManageArgs") != null){
				getSupportFragmentManager().beginTransaction().replace(R.id.manageLayout, ManagePromotionFragment.newInstance(getIntent().getLongArrayExtra("ManageArgs")), "MANAGE_PROMOTION").commit();
			}else{
				getSupportFragmentManager().beginTransaction().replace(R.id.manageLayout, ManagePromotionFragment.newInstance(), "MANAGE_PROMOTION").commit();
			}
			txtTitle.setText(getString(R.string.drawermenu_manage_promos));
			break;
		case ActivityStarter.MANAGE_PRODUCT:
			getSupportFragmentManager().beginTransaction().replace(R.id.manageLayout, ManageProductsFragment.newInstance(), "MANAGE_PRODUCT").commit();
			txtTitle.setText(getString(R.string.drawermenu_manage_products));
			break;
		default:
			finish();
			break;
		}

	}

	private void findViews() {
		mToolbar = (Toolbar) findViewById(R.id.actionbar1);	
		txtTitle = (TextView) findViewById(R.id.manageTitle);
		setSupportActionBar(mToolbar);
		mToolbar.setNavigationIcon(R.drawable.ic_left_arrow); 
	}

	private void initActionbar() {
		setSupportActionBar(mToolbar);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setDisplayShowTitleEnabled(false);
		
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}	
		return super.onOptionsItemSelected(item);
	}
	
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		isActivityOn = false;
	}
	
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		manageActivity = null;
	}

	@Override
	public void onPromotionsResumed() {
		txtTitle.setText(getString(R.string.drawermenu_manage_promos));
	}
}
