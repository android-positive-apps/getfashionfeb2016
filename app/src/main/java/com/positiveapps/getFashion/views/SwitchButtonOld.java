/**
 * 
 */
package com.positiveapps.getFashion.views;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.GestureDetector;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.positiveapps.getFashion.R;

/** iPhone like Switch Button implementation for android
 * 
 * @author Maor */
public class SwitchButtonOld extends FrameLayout implements AnimationListener, View.OnClickListener {

	private static final int USER_ACTION_EVENT = 1;
	private static final int SET_CHECKED_EVENT = 2;

	private boolean checkbale = true;
	private Drawable mSwitchImageChecked;
	private Drawable mSwitchImageNotChecked;
	private Drawable mSliderButtonImage;
	private ImageView mSwitchButton;
	private ImageView mSliderButton;
	private int mAnimDuration = 200;
	private boolean mChecked = false;
	private int mSlidingButtonOriginalWidth;
	private int mSwitchOriginalWidth;
	private MyTranslationAnimation mTranslateAnim;
	// private MyScaleAnimation mScaleAnim;
	private TextView mTxtChecked;
	private TextView mTxtNotChecked;
	private OnSwipeTouchListener mOnSwipeListener;
	private OnSwitchChangeListener onSwitchListener;
	private boolean enableClick = true;

	/** @param context */
	public SwitchButtonOld(Context context) {
		this(context, null);
	}

	public SwitchButtonOld(Context context, AttributeSet attrs) {
		super(context, attrs);

		// Optional - initialize the view with some drawables
		mSwitchImageChecked = getResources().getDrawable(R.drawable.switch_shape);
		mSwitchImageNotChecked = getResources().getDrawable(R.drawable.switch_shape);
		mSliderButtonImage = getResources().getDrawable(R.drawable.switch_toggle_shape);

		mSlidingButtonOriginalWidth = mSliderButtonImage.getMinimumWidth();
		mSwitchOriginalWidth = mSwitchImageChecked.getMinimumWidth();

		mSwitchButton = new ImageView(context);
		mSwitchButton.setImageDrawable(mSwitchImageNotChecked);

		mTxtChecked = new TextView(context);
		mTxtChecked.setTextSize(TypedValue.COMPLEX_UNIT_SP, 10);
		// mTxtChecked.setText("OK");
		mTxtChecked.setMinLines(1);
		FrameLayout.LayoutParams txtCheckedParams = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.WRAP_CONTENT,
				FrameLayout.LayoutParams.WRAP_CONTENT, Gravity.CENTER_VERTICAL | Gravity.LEFT);
		txtCheckedParams.setMargins(10, 0, 0, 0);
		mTxtChecked.setLayoutParams(txtCheckedParams);
		mTxtChecked.setVisibility(View.GONE);

		mTxtNotChecked = new TextView(context);
		mTxtNotChecked.setTextSize(TypedValue.COMPLEX_UNIT_SP, 10);
		// mTxtNotChecked.setText("Cancel");
		mTxtNotChecked.setMinLines(1);
		FrameLayout.LayoutParams txtNotCheckedParams = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.WRAP_CONTENT,
				FrameLayout.LayoutParams.WRAP_CONTENT, Gravity.CENTER_VERTICAL | Gravity.RIGHT);
		txtNotCheckedParams.setMargins(0, 0, 10, 0);
		mTxtNotChecked.setLayoutParams(txtNotCheckedParams);

		// Initializes the sliding button
		mSliderButton = new ImageView(context);
		FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.WRAP_CONTENT, FrameLayout.LayoutParams.WRAP_CONTENT,
				Gravity.CENTER_VERTICAL);
		params.setMargins(5, 0, 5, 0);

		mSliderButton.setLayoutParams(params);
		mSliderButton.setImageDrawable(mSliderButtonImage);

		addView(mSwitchButton);
		addView(mTxtChecked);
		addView(mTxtNotChecked);
		addView(mSliderButton);

		mSliderButton.setOnClickListener(this);
		mSwitchButton.setOnClickListener(this);

		mOnSwipeListener = new OnSwipeTouchListener(context, false) {

			@Override
			public void onSwipeLeft() {
				/*
				 * if(mChecked){ enableClick = false; actionSwitch(true,
				 * USER_ACTION_EVENT); }
				 */
				super.onSwipeLeft();

			}

			@Override
			public void onSwipeRight() {
				if (!mChecked) {
					enableClick = false;
					actionSwitch(true, USER_ACTION_EVENT);
				}
				super.onSwipeRight();

			}
		};

		mSliderButton.setOnTouchListener(mOnSwipeListener);
		mSwitchButton.setOnTouchListener(mOnSwipeListener);

	}

	@Override
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);
	}

	@Override
	protected void dispatchDraw(Canvas canvas) {
		/*
		 * float radius = 28.0f; Path clipPath = new Path(); RectF rect = new
		 * RectF(0, 0, this.getWidth(), this.getHeight());
		 * clipPath.addRoundRect(rect, radius, radius, Path.Direction.CW);
		 * canvas.clipPath(clipPath);
		 */
		super.dispatchDraw(canvas);
	}

	@Override
	public void onAnimationEnd(Animation animation) {
		// TODO Auto-generated method stub

		if (mChecked) {

			mSwitchButton.setImageDrawable(mSwitchImageNotChecked);

			mChecked = false;

		} else {
			mSwitchButton.setImageDrawable(mSwitchImageChecked);

			mChecked = true;
		}

		// mScaleAnim = new MyScaleAnimation(mSwitchButton, 10, 10,
		// MyScaleAnimation.RESIZE_AND_RETURN_TO_ORIGINAL);
		// mScaleAnim.setDuration(mAnimDuration);
		// mSwitchButton.startAnimation(mScaleAnim);

		Handler handler = new Handler();
		handler.postDelayed(new Runnable() {

			@Override
			public void run() {
				enableClick = true;
			}
		}, mAnimDuration);

		if (onSwitchListener != null) {
			onSwitchListener.onSwitch(this, mChecked);
		}

	}

	@Override
	public void onAnimationRepeat(Animation animation) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onAnimationStart(Animation animation) {
		// TODO Auto-generated method stub
		enableClick = false;
		if (mChecked) {

			mTxtChecked.setVisibility(View.GONE);
			mTxtNotChecked.setVisibility(View.VISIBLE);
		} else {
			mTxtChecked.setVisibility(View.VISIBLE);
			mTxtNotChecked.setVisibility(View.GONE);
		}
	}

	@SuppressLint("NewApi")
	private void actionSwitch(boolean animate, int actionType) {

		if (mSlidingButtonOriginalWidth == 0) {
			mSlidingButtonOriginalWidth = mSliderButton.getWidth();

		}
		if (mSwitchOriginalWidth == 0) {
			mSwitchOriginalWidth = mSwitchButton.getWidth();

			mTxtChecked.setMaxWidth(mSwitchOriginalWidth - mSlidingButtonOriginalWidth);
			mTxtChecked.setMaxHeight(mSwitchButton.getHeight());
			mTxtNotChecked.setMaxWidth(mSwitchOriginalWidth - mSlidingButtonOriginalWidth);
			mTxtNotChecked.setMaxHeight(mSwitchButton.getHeight());
		}

		if (animate) {
			// Sets the animation for the sliding button
			if (mChecked) {
				mTranslateAnim = new MyTranslationAnimation(mSliderButton, -(mSwitchOriginalWidth - mSlidingButtonOriginalWidth), 0, true);

			} else {
				mTranslateAnim = new MyTranslationAnimation(mSliderButton, mSwitchOriginalWidth - mSlidingButtonOriginalWidth, 0, true);
			}

			mTranslateAnim.setDuration(mAnimDuration);
			mTranslateAnim.setAnimationListener(this);
			mSliderButton.startAnimation(mTranslateAnim);

		} else {
			// Happens when 'Checked' status changes from code and not by user
			// action
			// Or when no animation is needed, Or when we need to redraw the
			// SAME state again without switching
			if (mChecked) {
				mSliderButton.setImageDrawable(mSliderButtonImage);
				mSliderButton.setX(mSwitchOriginalWidth - mSlidingButtonOriginalWidth);
				mSwitchButton.setImageDrawable(mSwitchImageChecked);
				mTxtChecked.setVisibility(View.VISIBLE);
				mTxtNotChecked.setVisibility(View.GONE);

			} else {
				mSliderButton.setImageDrawable(mSliderButtonImage);
				mSliderButton.setX(0);
				mSwitchButton.setImageDrawable(mSwitchImageNotChecked);
				mTxtChecked.setVisibility(View.GONE);
				mTxtNotChecked.setVisibility(View.VISIBLE);
			}
		}

	}

	public void setTextColor(int color) {
		this.mTxtChecked.setTextColor(color);
		this.mTxtNotChecked.setTextColor(color);
	}

	public void setTextForCheckedStatus(String text) {
		this.mTxtChecked.setText(text);
	}

	public void setTextForNotCheckedStatus(String text) {
		this.mTxtNotChecked.setText(text);
	}

	// Click Event
	@Override
	public void onClick(View v) {
		if (v == mSliderButton) {
			return;
		}
		if (enableClick && checkbale) {
			actionSwitch(true, USER_ACTION_EVENT);
		}
	}

	/** @return the mSwitchImageChecked */
	public Drawable getSwitchImageChecked() {
		return mSwitchImageChecked;
	}

	/** @param mSwitchImageChecked
	 *            the mSwitchImageChecked to set */
	public void setSwitchImageChecked(Drawable mSwitchImageChecked) {
		this.mSwitchImageChecked = mSwitchImageChecked;
	}

	/** @return the mSwitchImageNotChecked */
	public Drawable getSwitchImageNotChecked() {
		return mSwitchImageNotChecked;
	}

	/** @param mSwitchImageNotChecked
	 *            the mSwitchImageNotChecked to set */
	public void setSwitchImageNotChecked(Drawable mSwitchImageNotChecked) {
		this.mSwitchImageNotChecked = mSwitchImageNotChecked;
	}

	/** @return the mSliderButtonImage */
	public Drawable getSliderButtonImage() {
		return mSliderButtonImage;
	}

	/** @param mSliderButtonImage
	 *            the mSliderButtonImage to set */
	public void setSliderButtonImage(Drawable mSliderButtonImage) {
		this.mSliderButtonImage = mSliderButtonImage;
	}

	/** @param mAnimDuration
	 *            the mAnimDuration to set */
	public void setAnimDuration(int mAnimDuration) {
		this.mAnimDuration = mAnimDuration;
	}

	/** @return the mIsChecked */
	public boolean isChecked() {
		return mChecked;
	}

	/** @param checked
	 *            - set checked status */
	public void setChecked(boolean checked) {
		if (mChecked == checked) {
			return;
		} else {
			this.mChecked = checked;
		}

		actionSwitch(false, SET_CHECKED_EVENT);

	}

	/** @param onSwitchListener
	 *            the SwitchListener to set */
	public void setOnSwitchListener(OnSwitchChangeListener onSwitchListener) {
		this.onSwitchListener = onSwitchListener;
	}

	public void setCheckbale(boolean checkbale) {
		this.checkbale = checkbale;

	}

	public class OnSwipeTouchListener implements OnTouchListener {

		private final GestureDetector gestureDetector;
		private boolean disableClickEvent;

		/** @param ctx
		 *            - the context
		 * @param disableClickEvent
		 *            - true to disable click events on this view. false - keep
		 *            the click option */
		public OnSwipeTouchListener(Context ctx, boolean disableClickEvent) {
			gestureDetector = new GestureDetector(ctx, new GestureListener());
			this.disableClickEvent = disableClickEvent;
		}

		private final class GestureListener extends SimpleOnGestureListener {

			private static final int SWIPE_THRESHOLD = 100;
			private static final int SWIPE_VELOCITY_THRESHOLD = 100;

			@Override
			public boolean onDown(MotionEvent e) {
				return true;
			}

			@Override
			public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
				boolean result = false;
				try {
					float diffY = e2.getY() - e1.getY();
					float diffX = e2.getX() - e1.getX();
					if (Math.abs(diffX) > Math.abs(diffY)) {
						if (Math.abs(diffX) > SWIPE_THRESHOLD && Math.abs(velocityX) > SWIPE_VELOCITY_THRESHOLD) {
							if (diffX > 0) {
								onSwipeRight();
							} else {
								onSwipeLeft();
							}
						}
					} else {
						if (Math.abs(diffY) > SWIPE_THRESHOLD && Math.abs(velocityY) > SWIPE_VELOCITY_THRESHOLD) {
							if (diffY > 0) {
								onSwipeBottom();
							} else {
								onSwipeTop();
							}
						}
					}
				} catch (Exception exception) {
					exception.printStackTrace();
				}
				return result;
			}
		}

		public void onSwipeRight() {}

		public void onSwipeLeft() {}

		public void onSwipeTop() {}

		public void onSwipeBottom() {}

		@SuppressLint("ClickableViewAccessibility")
		public boolean onTouch(View v, MotionEvent event) {
			gestureDetector.onTouchEvent(event);
			return disableClickEvent;
		}
	}
	
	public interface OnSwitchChangeListener {

		void onSwitch(View v, boolean isChecked);
			
	}
}
