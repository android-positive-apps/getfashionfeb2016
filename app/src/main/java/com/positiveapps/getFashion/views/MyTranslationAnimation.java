/**
 * 
 */
package com.positiveapps.getFashion.views;

import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Transformation;

/**
 * Smart Custom Animation class that also affects the view itself during animation
 * @author Maor
 *
 */
public class MyTranslationAnimation extends Animation {

	private View targetView;
	private float originalX;
	private float originalY;
	private int newX;
	private int newY;
	private boolean append;
	
	/**
	 * 
	 * @param targetView - the view to change - has to be at least 1dp width/height
	 * @param newWidth - the new width (when append - 0 for no changes)
	 * @param newHeight - the new height (when append - 0 for no changes)
	 * @param apend - if true then the view will grow. Else it will redraw itself to the wanted size
	 */
	public MyTranslationAnimation(View targetView, int newX, int newY, boolean append) {
				
		this.targetView = targetView;
		this.originalX = targetView.getX();
		this.originalY = targetView.getY();
		this.newX = newX;
		this.newY = newY;
		this.append = append;
	}


	@Override
	protected void applyTransformation(float interpolatedTime, Transformation t) {
		// TODO Auto-generated method stub
		super.applyTransformation(interpolatedTime, t);
		
        //ViewGroup.LayoutParams lp = targetView.getLayoutParams();
        if(append){
        	if(newX != 0){
        		targetView.setX((int) (originalX + (newX * interpolatedTime)));
        	}
        	if(newY != 0){
        		targetView.setY((int) (originalY + (newY * interpolatedTime)));
        	}
        }else{
        	if(newX > 0){
        		targetView.setX((int) (newX * interpolatedTime));
        	}

        	if(newY > 0){
        		targetView.setY((int) (newY * interpolatedTime));
        	}
        }

	}
	
}
