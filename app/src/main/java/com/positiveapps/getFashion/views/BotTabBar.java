package com.positiveapps.getFashion.views;

import android.content.Context;
import android.graphics.Color;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.positiveapps.getFashion.BuildConfig;
import com.positiveapps.getFashion.R;

/**
 * Created by NIVO on 13/01/2016.
 */
public class BotTabBar extends LinearLayout implements View.OnClickListener {

    private LinearLayout tab1;
    private LinearLayout tab2;
    private LinearLayout tab3;
    private LinearLayout tab4;
    private LinearLayout tab5;
    private ImageView homeButton;
    private ImageView dealsButton ;
    private ImageView menuButton  ;
    private ImageView callButton  ;
    private ImageView locateButton;
    private TextView homeText;
    private TextView dealsText ;
    private TextView menuText  ;
    private TextView callText  ;
    private TextView locateText;
    int selected =1;
    private onTabSelected onTabSelected;

    public BotTabBar(Context context) {
        super(context);
        init();
    }

    public BotTabBar(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    private void init() {
        LayoutInflater.from(getContext()).inflate(R.layout.bot_tabs, this);

        tab1        = (LinearLayout)findViewById(R.id.tab1);
        tab2        = (LinearLayout)findViewById(R.id.tab2);
        tab3        = (LinearLayout)findViewById(R.id.tab3);
        tab4        = (LinearLayout)findViewById(R.id.tab4);
        tab5        = (LinearLayout)findViewById(R.id.tab5);
        homeButton      =(ImageView)findViewById(R.id.homeButton);
        dealsButton     =(ImageView)findViewById(R.id.dealsButton);
        menuButton      =(ImageView)findViewById(R.id.menuButton);
        callButton      =(ImageView)findViewById(R.id.callButton);
        locateButton    =(ImageView)findViewById(R.id.locateButton);
        homeText        =(TextView)findViewById(R.id.homeText);
        dealsText        =(TextView)findViewById(R.id.dealsText);
        menuText         =(TextView)findViewById(R.id.menuText);
        callText         =(TextView)findViewById(R.id.callText);
        locateText          =(TextView)findViewById(R.id.locateText);


        tab1.setOnClickListener(this);
        tab2.setOnClickListener(this);
        tab3.setOnClickListener(this);
        tab4.setOnClickListener(this);
        tab5.setOnClickListener(this);


    }

    public void setTabListener(onTabSelected listener){
        onTabSelected = listener;
    }
    public int getSelected(){
        return selected;
    }

    public void setSelected(int selected) {
        this.selected = selected;
        switch (selected){
            case 1:
                tab1Focus();
                break;
            case 2:
                tab2Focus();
                break;
            case 3:
                tab3Focus();
                break;
            case 4:
                tab4Focus();
                break;
            case 5:
                tab5Focus();
                break;
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.tab1:
                tab1Focus();
                onTabSelected.onSelected(1);
                break;
            case R.id.tab2:
                selected=2;
                onTabSelected.onSelected(2);
                tab2Focus();



                break;
            case R.id.tab3:
                selected=3;
                onTabSelected.onSelected(3);
                tab3Focus();

                break;
            case R.id.tab4:
                selected=4;
                onTabSelected.onSelected(4);
                tab4Focus();

                break;
            case R.id.tab5:
                selected=5;
                onTabSelected.onSelected(5);
                tab5Focus();

                break;
        }
    }

    private void tab5Focus() {
        homeButton.setBackgroundResource(R.drawable.home_white_business_nav);
        dealsButton .setBackgroundResource(R.drawable.deals_white_business_nav);
        menuButton  .setBackgroundResource(R.drawable.menu_white_business_nav);
        callButton  .setBackgroundResource(R.drawable.call_white_business_nav);
        locateButton.setBackgroundResource(R.drawable.locate_white_business_nav);

        if (BuildConfig.FLAVOR.equals("getDesign")){
            locateButton.setBackgroundResource(R.drawable.locate_blue_business_nav3x);
        }

        tab5.setBackgroundResource(R.drawable.gradient_single_tab_lay);
        tab1.setBackgroundResource(0);
        tab2.setBackgroundResource(0);
        tab3.setBackgroundResource(0);
        tab4.setBackgroundResource(0);

        homeText .setTextColor(Color.WHITE);
        dealsText .setTextColor(Color.WHITE);
        menuText  .setTextColor(Color.WHITE);
        callText  .setTextColor(Color.WHITE);
//        locateText.setTextColor(Color.GREEN);
        locateText.setTextColor(getResources().getColor(R.color.MyGreen));
    }

    private void tab4Focus() {
        homeButton.setBackgroundResource(R.drawable.home_white_business_nav);
        dealsButton .setBackgroundResource(R.drawable.deals_white_business_nav);
        menuButton  .setBackgroundResource(R.drawable.menu_white_business_nav );
        callButton  .setBackgroundResource(R.drawable.call_white_business_nav );
        locateButton.setBackgroundResource(R.drawable.locate_white_business_nav);

        if (BuildConfig.FLAVOR.equals("getDesign")){
            callButton  .setBackgroundResource(R.drawable.call_blue_business_nav3x);
        }

        tab4.setBackgroundResource(R.drawable.gradient_single_tab_lay);
        tab1.setBackgroundResource(0);
        tab2.setBackgroundResource(0);
        tab3.setBackgroundResource(0);
        tab5.setBackgroundResource(0);

        homeText .setTextColor(Color.WHITE);
        dealsText .setTextColor(Color.WHITE);
        menuText  .setTextColor(Color.WHITE);
        callText  .setTextColor(getResources().getColor(R.color.MyGreen));
        locateText.setTextColor(Color.WHITE);
    }

    private void tab3Focus() {
        homeButton.setBackgroundResource(R.drawable.home_white_business_nav);
        dealsButton .setBackgroundResource(R.drawable.deals_white_business_nav);
        menuButton  .setBackgroundResource(R.drawable.menu_white_business_nav );
        callButton  .setBackgroundResource(R.drawable.call_white_business_nav );
        locateButton.setBackgroundResource(R.drawable.locate_white_business_nav);



        tab3.setBackgroundResource(R.drawable.gradient_single_tab_lay);
        tab1.setBackgroundResource(0);
        tab2.setBackgroundResource(0);
        tab4.setBackgroundResource(0);
        tab5.setBackgroundResource(0);

        homeText .setTextColor(Color.WHITE);
        dealsText .setTextColor(Color.WHITE);
        menuText  .setTextColor(getResources().getColor(R.color.MyGreen));
        callText  .setTextColor(Color.WHITE);
        locateText.setTextColor(Color.WHITE);
    }

    private void tab2Focus() {
        homeButton.setBackgroundResource(R.drawable.home_white_business_nav);
        dealsButton .setBackgroundResource(R.drawable.deals_white_business_nav);
        menuButton  .setBackgroundResource(R.drawable.menu_white_business_nav  );
        callButton  .setBackgroundResource(R.drawable.call_white_business_nav);
        locateButton.setBackgroundResource(R.drawable.locate_white_business_nav);


        if (BuildConfig.FLAVOR.equals("getDesign")){
            dealsButton .setBackgroundResource(R.drawable.packages_blue_business_nav3x);

        }

        tab2.setBackgroundResource(R.drawable.gradient_single_tab_lay);
        tab1.setBackgroundResource(0);
        tab3.setBackgroundResource(0);
        tab4.setBackgroundResource(0);
        tab5.setBackgroundResource(0);

        homeText .setTextColor(Color.WHITE);
        dealsText.setTextColor(getResources().getColor(R.color.MyGreen));
        menuText  .setTextColor(Color.WHITE);
        callText  .setTextColor(Color.WHITE);
        locateText.setTextColor(Color.WHITE);
    }

    private void tab1Focus() {
        selected=1;
        homeButton.setBackgroundResource(R.drawable.home_white_business_nav);
        dealsButton .setBackgroundResource(R.drawable.deals_white_business_nav);
        menuButton  .setBackgroundResource(R.drawable.menu_white_business_nav);
        callButton  .setBackgroundResource(R.drawable.call_white_business_nav);
        locateButton.setBackgroundResource(R.drawable.locate_white_business_nav);


        if (BuildConfig.FLAVOR.equals("getDesign")){
            homeButton.setBackgroundResource(R.drawable.home_blue_business_nav3x);

        }

        tab1.setBackgroundResource(R.drawable.gradient_single_tab_lay);
        tab2.setBackgroundResource(0);
        tab3.setBackgroundResource(0);
        tab4.setBackgroundResource(0);
        tab5.setBackgroundResource(0);

        homeText.setTextColor(getResources().getColor(R.color.MyGreen));
        dealsText .setTextColor(Color.WHITE);
        menuText  .setTextColor(Color.WHITE);
        callText  .setTextColor(Color.WHITE);
        locateText.setTextColor(Color.WHITE);
    }

    public interface onTabSelected{
        public void onSelected(int d);
    }
}
