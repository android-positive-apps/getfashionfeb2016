package com.positiveapps.getFashion.views;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.AutoCompleteTextView;

public class ImmideateAutocomleteTextView extends AutoCompleteTextView {

    private int myThreshold = 0; 

    public ImmideateAutocomleteTextView(Context context) {
        super(context);
        init();
    }

    public ImmideateAutocomleteTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public ImmideateAutocomleteTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }
    
    private void init(){
    	myThreshold = 0;
    }

    @Override
    public void setThreshold(int threshold) {
        if (threshold < 0) {
            threshold = 0;
        }
        myThreshold = threshold;
    }

    @Override
    public boolean enoughToFilter() {
        return getText().length() >= myThreshold;
    }

    @Override
    public int getThreshold() {
        return myThreshold;
    }
    
    

}