/**
 * 
 */
package com.positiveapps.getFashion;

import android.app.Application;
import android.content.Context;
import android.content.res.Configuration;
import android.support.multidex.MultiDex;

import com.crashlytics.android.Crashlytics;
import com.facebook.FacebookSdk;
import com.positiveapps.getFashion.backend.APIManager;
import com.positiveapps.getFashion.backend.BusinessRepository;
import com.positiveapps.getFashion.backend.Categories;
import com.positiveapps.getFashion.backend.Prefs;
import com.positiveapps.getFashion.backend.SettingManager;
import com.positiveapps.getFashion.backend.UserManager;

import java.util.Locale;

import io.fabric.sdk.android.Fabric;

/** @author Ilia Kaplounov */
public class App extends Application {

	public static BusinessRepository repository;
	public static Context applicationContext;

	// onCreate
	@Override
	public void onCreate() {
		super.onCreate();
		Fabric.with(this, new Crashlytics());
		FacebookSdk.sdkInitialize(getApplicationContext());
		Prefs.initPreference(getApplicationContext());
		Categories.init(getApplicationContext());
		UserManager.init(getApplicationContext());
		SettingManager.init(getApplicationContext());

		repository = BusinessRepository.getInstance();
		applicationContext = getApplicationContext();
		APIManager.initAPIManager(applicationContext);
		if(BuildConfig.FLAVOR.equals("getCoffeeShops")) {
			Prefs.setAppLanguage("en");
		}
		initAppLanguage();
		
	}


	public static void initAppLanguage() {
		Locale locale = new Locale(Prefs.getAppLanguage());
		Configuration oldConfig = applicationContext.getResources().getConfiguration();
		Locale.setDefault(locale);
		Configuration newConfig = new Configuration(oldConfig);

		newConfig.locale = locale;
		applicationContext.getResources().updateConfiguration(newConfig, applicationContext.getResources().getDisplayMetrics());
	}

	@Override
	protected void attachBaseContext(Context base) {
		super.attachBaseContext(base);
		MultiDex.install(this);
	}

}
