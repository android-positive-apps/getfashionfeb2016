package com.positiveapps.getFashion.adapters;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Paint;
import android.os.CountDownTimer;
import android.os.SystemClock;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.positiveapps.getFashion.App;
import com.positiveapps.getFashion.MainActivity;
import com.positiveapps.getFashion.R;
import com.positiveapps.getFashion.backend.BusinessRepository;
import com.positiveapps.getFashion.backend.Prefs;
import com.positiveapps.getFashion.backend.SettingManager;
import com.positiveapps.getFashion.backend.UserManager;
import com.positiveapps.getFashion.dialogs.MessageDialog.DialogListener;
import com.positiveapps.getFashion.jsonmodels.Business;
import com.positiveapps.getFashion.jsonmodels.Product;
import com.positiveapps.getFashion.jsonmodels.Promotion;
import com.positiveapps.getFashion.util.ActivityStarter;
import com.positiveapps.getFashion.util.Coordinates;
import com.positiveapps.getFashion.util.DialogHelper;
import com.positiveapps.getFashion.util.GeneralHelper;
import com.positiveapps.getFashion.util.ImageLoader;
import com.positiveapps.getFashion.util.StringFormatUtils;

import java.util.ArrayList;

public class BusinessListAdapter extends RecyclerView.Adapter<BusinessListAdapter.BusinessViewHolder> {
	ArrayList<Business> mBusinesses;
	FragmentManager mFragmentManager;
	static long categoryID;
	static Coordinates coordinates;
	
	public BusinessListAdapter(FragmentManager fragmentManager, ArrayList<Business> businesses, long categoryID) {
		this.mBusinesses = businesses;
		BusinessListAdapter.categoryID = categoryID;
		mFragmentManager = fragmentManager;
	}

	public void addAll(ArrayList<Business> businesses) {
		if (businesses == null || businesses.isEmpty())
			return;

		if (mBusinesses == null)
			mBusinesses = new ArrayList<Business>();

		mBusinesses.addAll(businesses);
		notifyDataSetChanged();
	}

	@Override
	public int getItemCount() {
		return mBusinesses.size();
	}

	@Override
	public void onBindViewHolder(BusinessViewHolder holder, int pos) {
		holder.bindBusiness(mFragmentManager, mBusinesses.get(pos));
		coordinates = new Coordinates(mBusinesses.get(pos).Lat, mBusinesses.get(pos).Lng);
		/*double distance = 0;*/
		double distance = GeneralHelper.distance(coordinates.lat, coordinates.lon, Double.parseDouble(UserManager.getAppUser().Lat), Double.parseDouble(UserManager.getAppUser().Lng));
		String distanceInString = String.format("%.1f", distance);
		holder.txtDistance.setText(

				App.applicationContext.getResources().getString(R.string.km)+" "+distanceInString  );
		
/*		if (mBusinesses.get(pos).EnableReservations == 0) {
			holder.btnReserveItem.setVisibility(View.INVISIBLE);
			// holder.btnReserveItem.setClickable(false);
			// holder.btnReserveItem.setAlpha(0.2f);
		} else {
			holder.btnReserveItem.setVisibility(View.VISIBLE);
			// holder.btnReserveItem.setClickable(true);
			// holder.btnReserveItem.setAlpha(1f);
		}*/
		
	}

	@Override
	public BusinessViewHolder onCreateViewHolder(ViewGroup parent, int pos) {
		View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_main_business, parent, false);
		return new BusinessViewHolder(v);
	}

	public class BusinessViewHolder extends RecyclerView.ViewHolder {
		Business business;

		private static final int BIND_MODE_NOTHING = 0;
		private static final int BIND_MODE_PROMOTION = 1;
		private static final int BIND_MODE_PRODUCT = 2;

		int bindMode;

		Context context;
		FragmentManager mFragmentManager;

		ImageView imgProduct;
		TextView txtEndsIn;
		TextView txtAmountLeft;
		ImageView imgLogo;
		TextView txtDistance;
		TextView txtAddress;
		TextView txtProductTitle;
		TextView txtNewPrice;
		TextView txtOldPrice;
		Button btnReserveItem;
		TextView txtMoreInfo;
		FrameLayout imgRibbon;
		TextView txtDiscountPrcentage;

		CountDownTimer promotionCountDown;
		OnClickListener viewBusinessListener;


		public BusinessViewHolder(View itemView) {
			super(itemView);

			context = itemView.getContext();

//			imgProduct = (ImageView) itemView.findViewById(R.id.imgSaleImage);
//			imgRibbon = (FrameLayout) itemView.findViewById(R.id.imgRibbon);
//			imgLogo = (ImageView) itemView.findViewById(R.id.imgLogo);
//			txtAmountLeft = (TextView) itemView.findViewById(R.id.txtAmountLeft);
//			txtDistance = (TextView) itemView.findViewById(R.id.txtDistanceFromYou);
//			txtAddress = (TextView) itemView.findViewById(R.id.txtAddress);
//			txtProductTitle = (TextView) itemView.findViewById(R.id.txtTitle);
//			txtNewPrice = (TextView) itemView.findViewById(R.id.txtNewPrice);
//			txtOldPrice = (TextView) itemView.findViewById(R.id.txtOldPrice);
//			txtEndsIn = (TextView) itemView.findViewById(R.id.txtEndsIn);
//			txtMoreInfo = (TextView) itemView.findViewById(R.id.txtMoreInfo);
//			txtDiscountPrcentage = (TextView) itemView.findViewById(R.id.txtDiscount);
//			btnReserveItem = (Button) itemView.findViewById(R.id.btnReserveItem);

			imgProduct = (ImageView) itemView.findViewById(R.id.imgProduct);
			imgRibbon = (FrameLayout) itemView.findViewById(R.id.imgRibbon);
			imgLogo = (ImageView) itemView.findViewById(R.id.imgLogo);
			txtAmountLeft = (TextView) itemView.findViewById(R.id.txtAmountLeft);
			txtDistance = (TextView) itemView.findViewById(R.id.txtDistance);
			txtAddress = (TextView) itemView.findViewById(R.id.txtAddress);
			txtProductTitle = (TextView) itemView.findViewById(R.id.txtProductTitle);
			txtNewPrice = (TextView) itemView.findViewById(R.id.txtNewPrice);
			txtOldPrice = (TextView) itemView.findViewById(R.id.txtOldPrice);
			txtEndsIn = (TextView) itemView.findViewById(R.id.txtEndsIn);
			txtMoreInfo = (TextView) itemView.findViewById(R.id.txtMoreInfo);
			txtDiscountPrcentage = (TextView) itemView.findViewById(R.id.txtDiscountPrcentage);
			btnReserveItem = (Button) itemView.findViewById(R.id.btnReserveItem_a);


			txtOldPrice.setPaintFlags(txtOldPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
		}

		public void bindBusiness(FragmentManager fragmentManager, Business b) {
			business = b;
			mFragmentManager = fragmentManager;
			
			resetView();
			ImageLoader.loadBusinessLogo(context, b.Logo, imgLogo);
			ImageLoader.loadProductImage(context, b.Logo, imgProduct);
			txtAddress.setSelected(true);
			txtDiscountPrcentage.setVisibility(View.GONE);
			imgRibbon.setVisibility(View.GONE);
			btnReserveItem.setVisibility(View.INVISIBLE);
			txtMoreInfo.setVisibility(View.GONE);
			txtAddress.setText(business.Name + ", " + StringFormatUtils.getBusinessFormattedAddress(Prefs.getAppLanguage(), b));
			
			viewBusinessListener = new OnClickListener() {
				public void onClick(View v) {
						System.out.println("--------------------------------------------->    " + categoryID);
						BusinessRepository.tempBusiness = business;
						ActivityStarter.viewBusiness(context, business.ID, business.tempCategory);					
				}
			};
 
			imgProduct.setOnClickListener(viewBusinessListener);
			determineBindMode();
		}

		private void determineBindMode() {
			if (business.hasPromotions()) {
				bindMode = BIND_MODE_PROMOTION;
				Promotion p = business.getNearestPromotion();
				bindPromotion(p);
			} else if (!business.noProducts()) {
				bindMode = BIND_MODE_PRODUCT;
				Product p = business.Products.get(0);
				bindProduct(p);
			} else {
				bindMode = BIND_MODE_NOTHING;
				bindNothing();
			}
		}

		private void resetView() {
			txtAmountLeft.setVisibility(View.VISIBLE);
			txtEndsIn.setVisibility(View.VISIBLE);
			txtNewPrice.setVisibility(View.VISIBLE);
			txtOldPrice.setVisibility(View.VISIBLE);

			btnReserveItem.setAlpha(1);

			if (promotionCountDown != null) {
				promotionCountDown.cancel();
				promotionCountDown = null;
			}
		}

		private void bindPromotion(final Promotion promotion) {
			
//			Product promotionProduct = business.getProductById(promotion.ProductID);

 
			ImageLoader.loadProductImage(context, promotion.Image, imgProduct);
			txtDiscountPrcentage.setText(StringFormatUtils.getFormattedDiscount(context, promotion.ProductPrice, promotion.DiscountPrice));

			setPromotionProductTitle(promotion);

			txtDiscountPrcentage.setVisibility(View.VISIBLE);
			imgRibbon.setVisibility(View.VISIBLE);
			txtAmountLeft.setText(StringFormatUtils.getAmountLeft(context, promotion.NumUnits));
			txtNewPrice.setText(StringFormatUtils.getFormattedPrice(context, promotion.DiscountPrice));
			txtOldPrice.setText(StringFormatUtils.getFormattedPrice(context, promotion.ProductPrice));
			btnReserveItem.setText(Prefs.getAppLanguage().matches("en") ?
					SettingManager.getSetting().order_promotion_EN : SettingManager.getSetting().order_promotion_HE); 
//			txtMoreInfo.setVisibility(View.VISIBLE);
			
			btnReserveItem.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					if(Prefs.isConnected()){
						DialogHelper.promotionReservationDialog(mFragmentManager, business, promotion);
					}else{
						displayNotConnectedDialog();
					}
				}
			});
			btnReserveItem.setVisibility(View.VISIBLE);
			txtMoreInfo.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					DialogHelper.promotionDescriptionDialog(mFragmentManager, business, promotion);
				}
			});

			promotionCountDown = new CountDownTimer(promotion.millisLeft(), 1000) {
				public void onTick(long millisUntilFinished) { 
					
					if(promotion.millisLeft() == -1){
//						txtEndsIn.setText(context.getResources().getString(R.string.promotion_soon_will_be_up));
						txtEndsIn.setText("---");
						btnReserveItem.setVisibility(View.INVISIBLE);
					}else{
						txtEndsIn.setText(StringFormatUtils.getPromotionEndsIn(context, millisUntilFinished));
					}
				}

				public void onFinish() {}
			};
			
			if(promotion.millisLeft() == -1){
//				txtEndsIn.setText(context.getResources().getString(R.string.promotion_soon_will_be_up));
				txtEndsIn.setText("---");
			}else{
				promotionCountDown.start();
			}
			
		}


		private void setProductTitle(Product product) {
			System.out.println("niv product id="+product.ID+" title= "+product.Title
					+" TitleEN= "+product.TitleEN
					+" TitleHE= "+product.TitleHE);

			String title= product.Title;
			if (title!=null) {
				if (!title.isEmpty())
					txtProductTitle.setText(title);
				else {
					title= product.TitleEN;
					if (title!=null){
						if (!title.isEmpty())
							txtProductTitle.setText(title);
						else {
							title= product.TitleHE;
							if (title!=null)
							if (!title.isEmpty())
								txtProductTitle.setText(title);
						}
					}else {
						title= product.TitleHE;
						if (title!=null)
						if (!title.isEmpty())
							txtProductTitle.setText(title);
					}
				}
				assert title != null;
				if (title.isEmpty()){
					txtProductTitle.setText("");
				}
			}else {
				title= product.TitleEN;
				if (title!=null){
					if (!title.isEmpty())
						txtProductTitle.setText(title);
					else {
						title= product.TitleHE;
						if (!title.isEmpty())
							txtProductTitle.setText(title);
					}
				}else {
					title= product.TitleHE;
					if (title!=null)
						if (!title.isEmpty())
							txtProductTitle.setText(title);
				}
			}
			if (title==null)
				txtProductTitle.setText("");
		}

		private void bindProduct(final Product product) {

			if (product.Image != null && !product.Image.startsWith("http://")) {
				product.Image = "http://getfashion.positive-apps.com/" + product.Image;
			}
			btnReserveItem.setVisibility(View.VISIBLE);
			ImageLoader.loadProductImage(context, product.Image, imgProduct);
			btnReserveItem.setText(Prefs.getAppLanguage().matches("en") ?
					SettingManager.getSetting().order_product_EN : SettingManager.getSetting().order_product_HE);
			txtDiscountPrcentage.setVisibility(View.GONE);
			txtOldPrice.setVisibility(View.INVISIBLE);
			txtAmountLeft.setVisibility(View.INVISIBLE);
			txtEndsIn.setVisibility(View.INVISIBLE);
//			txtProductTitle.setText(product.Title);
			setProductTitle(product);
//			txtMoreInfo.setVisibility(View.VISIBLE);
			imgRibbon.setVisibility(View.GONE);
			double d = product.Price;
			if(d % 1 == 0){
				int p = (int) product.Price;
				txtNewPrice.setText(context.getString(R.string.price_format, p));
			}else{
				txtNewPrice.setText(context.getString(R.string.nis) + product.Price);
			}
			btnReserveItem.setVisibility(View.INVISIBLE);
			
			btnReserveItem.setOnClickListener(new OnClickListener() {
				public void onClick(View v) { 
					if(Prefs.isConnected()){
						DialogHelper.productReservationDialog(mFragmentManager, business, product);
					}else{
						displayNotConnectedDialog();
					}
				}
			});
		 

			txtMoreInfo.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					DialogHelper.productDescriptionDialog(mFragmentManager, business, product);
				}
			});
		}


		private void setPromotionProductTitle(Promotion promotion) {
			System.out.println("niv id="+promotion.ID+" title= "+promotion.Title
					+" TitleEN= "+promotion.TitleEN
					+" TitleHE= "+promotion.TitleHE);

			String title= promotion.Title;
			if (title!=null) {
				if (!title.isEmpty())
					txtProductTitle.setText(title);
				else {
					title= promotion.TitleEN;
					if (title!=null){
						if (!title.isEmpty())
							txtProductTitle.setText(title);
						else {
							title= promotion.TitleHE;
							if (!title.isEmpty())
								txtProductTitle.setText(title);
						}
					}else {
						title= promotion.TitleHE;
						if (title!=null)
							if (!title.isEmpty())
								txtProductTitle.setText(title);
					}
				}
				assert title != null;
				if (title.isEmpty()){
					txtProductTitle.setText("");
				}
			}else {
				title= promotion.TitleEN;
				if (title!=null){
					if (!title.isEmpty())
						txtProductTitle.setText(title);
					else {
						title= promotion.TitleHE;
						if (!title.isEmpty())
							txtProductTitle.setText(title);
					}
				}else {
					title= promotion.TitleHE;
					if (title!=null)
						if (!title.isEmpty())
							txtProductTitle.setText(title);
				}
			}
			if (title==null)
				txtProductTitle.setText("");
		}


		private void bindNothing() {
//			ImageLoader.loadDefaultProductImage(context, imgProduct);
			txtAmountLeft.setVisibility(View.INVISIBLE);
			txtEndsIn.setVisibility(View.INVISIBLE);
			txtNewPrice.setVisibility(View.INVISIBLE);
			txtOldPrice.setVisibility(View.INVISIBLE);
			txtProductTitle.setVisibility(View.INVISIBLE);
//			btnReserveItem.setAlpha(0.2f);
			txtMoreInfo.setVisibility(View.GONE);
			btnReserveItem.setVisibility(View.INVISIBLE);

		}

		
	}

	private void displayNotConnectedDialog() {
		DialogHelper.messageDialog(mFragmentManager, App.applicationContext.getResources().getString(R.string.location_dialog_title),
				App.applicationContext.getResources().getString(R.string.login_to_system), new DialogListener() {

					@Override
					public void onOk(Dialog d) {
						DialogHelper.loginDialog(mFragmentManager, MainActivity.appuserListener);
						d.dismiss();
					}

					@Override
					public void onCancel(Dialog d) {
						d.dismiss();
					}
				});
	}

}
