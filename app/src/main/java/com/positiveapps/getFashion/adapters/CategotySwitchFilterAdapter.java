package com.positiveapps.getFashion.adapters;

import java.util.ArrayList;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.TextView;

import com.positiveapps.getFashion.R;
import com.positiveapps.getFashion.jsonmodels.Category;
import com.positiveapps.getFashion.views.SwitchButton;
 

public class CategotySwitchFilterAdapter  extends ArrayAdapter<Category>{
	
	ArrayList<Category> list;
	boolean[] select;
	Activity context;
	ArrayList<String> categories;
	
	
//	public CategotySwitchAdapter(Activity context,  ArrayList<Category> list) {
//		super(context, R.layout.item_category_filter, list);
//		this.list = list;
//		this.context = context;
//		select = new boolean[list.size()];
//	}
	
	public CategotySwitchFilterAdapter(Activity context,  ArrayList<Category> list, ArrayList<String> categories) {
		super(context, R.layout.item_category_filter_x, list);
		this.list = list;
		this.context = context;
		select = new boolean[list.size()];
		this.categories = categories;
	}
	int counter = 0;
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {

		ViewHolder holder;
		View view = convertView;
		if(convertView == null){
			view = context.getLayoutInflater().inflate(R.layout.item_category_filter_x, null); 
			holder = new ViewHolder();
			holder.catName = (TextView) view.findViewById(R.id.txt_cat_DisplayItems);
			holder.catSwitch = (SwitchButton) view.findViewById(R.id.swtCategory);
			view.setTag(holder);
		}else{
			holder = (ViewHolder) view.getTag();
		}
		
		final Category O = getItem(position);
		
		if(O != null){
			holder.catSwitch.setTag(O.ID);
			for (int i = 0; i < categories.size(); i++) {
				if(categories.get(i).matches(O.ID+"")){
					holder.catSwitch.setChecked(true);
					select[position] = true; 
				}
			}
			holder.catName.setText(O.Name);
			
			holder.catSwitch.setOnCheckedChangeListener(new OnCheckedChangeListener() {
				
				@Override
				public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
					select[position] = isChecked;
					 
				}
			});
		
		}
		
		return view;
	}
	
	public class ViewHolder{
		SwitchButton catSwitch;
		TextView catName;
	}
	
	public String getCategoriesArray() {
		StringBuilder builder = new StringBuilder();
		builder.append("[");
		for (int i = 0; i < select.length; i++) {
			if(select[i]){
				builder.append(list.get(i).ID);
				builder.append(",");
			}
		}
		builder.append("]");
		System.out.print("niv "+builder.toString().replace(",]", "]"));
		return builder.toString().replace(",]", "]");
	}

	public String resetCategoriesArray() {
		return "[]";
	}
	

}
