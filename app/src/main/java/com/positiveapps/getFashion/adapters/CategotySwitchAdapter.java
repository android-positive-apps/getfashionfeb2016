package com.positiveapps.getFashion.adapters;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.TextView;
import android.widget.Toast;

import com.positiveapps.getFashion.R;
import com.positiveapps.getFashion.backend.SettingManager;
import com.positiveapps.getFashion.jsonmodels.Category;
import com.positiveapps.getFashion.views.SwitchButton;

import java.util.ArrayList;
 

public class CategotySwitchAdapter  extends ArrayAdapter<Category>{
	
	ArrayList<Category> list;
	boolean[] select;
	Activity context;
	ArrayList<String> categories;
	
	
//	public CategotySwitchAdapter(Activity context,  ArrayList<Category> list) {
//		super(context, R.layout.item_category_filter, list);
//		this.list = list;
//		this.context = context;
//		select = new boolean[list.size()];
//	}
	
	public CategotySwitchAdapter(Activity context,  ArrayList<Category> list, ArrayList<String> categories) {
		super(context, R.layout.item_category_filter, list);
		this.list = list;
		this.context = context;
		select = new boolean[list.size()];
		this.categories = categories;
	}
	int counter = 0;
	int maxCat = 3;
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {

		final ViewHolder holder;
		View view = convertView;
		if(convertView == null){
			view = context.getLayoutInflater().inflate(R.layout.item_category_filter, null); 
			holder = new ViewHolder();
			holder.catName = (TextView) view.findViewById(R.id.txt_cat_DisplayItems);
			holder.catSwitch = (SwitchButton) view.findViewById(R.id.swtCategory);
			
			if(SettingManager.getSetting().max_categories != null){
				maxCat = Integer.parseInt(SettingManager.getSetting().max_categories);
				}
			
			view.setTag(holder);
		}else{
			holder = (ViewHolder) view.getTag();
		}
		
		final Category O = getItem(position);
		
		if(O != null){
			counter = categories.size();
			holder.catSwitch.setTag(O.ID);
			for (int i = 0; i < categories.size(); i++) {
				if(categories.get(i).matches(O.ID + "")){
					holder.catSwitch.setChecked(true);
					select[position] = true;
				}
			}
			holder.catName.setText(O.Name);
			
			holder.catSwitch.setOnCheckedChangeListener(new OnCheckedChangeListener() {

				@Override
				public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

					System.out.println("counter " + counter);
					System.out.println("max_categories " + maxCat);
					if(isChecked){
						counter++;
					}else{
						counter--;
					}

					if(counter > maxCat){
						holder.catSwitch.setChecked(false);
						Toast.makeText(context, context.getResources().getString(R.string.categories_number_limited_to) + " " +  maxCat,  Toast.LENGTH_LONG).show();
					}else{
						select[position] = isChecked;
					}

				}
			});
		
		}
		
		return view;
	}
	
	public class ViewHolder{
		SwitchButton catSwitch;
		TextView catName;
	}
	
	public String getCategoriesArray() {
		StringBuilder builder = new StringBuilder();
		builder.append("[");
		for (int i = 0; i < select.length; i++) {
			if(select[i]){
				builder.append(list.get(i).ID);
				builder.append(",");
			}
		}
		builder.append("]");
		return builder.toString().replace(",]", "]");
	}
	
	
	

}
