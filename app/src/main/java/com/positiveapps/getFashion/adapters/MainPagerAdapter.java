package com.positiveapps.getFashion.adapters;

import java.util.ArrayList;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.positiveapps.getFashion.backend.Categories;
import com.positiveapps.getFashion.fragments.BaseBusinessListFragment;
import com.positiveapps.getFashion.jsonmodels.Category;

public class MainPagerAdapter extends FragmentStatePagerAdapter {

	ArrayList<Category> mCategories;
	Context mContext;

	public MainPagerAdapter(Context context, ArrayList<Category> categories, FragmentManager fm) {
		super(fm);
		mContext = context;
		mCategories = categories;
	}

	/** add search and hot deals category as they are not a part of the list */

	@Override
	public CharSequence getPageTitle(int position) {
		return mCategories.get(position).Name;
	}

	@Override
	public Fragment getItem(int position) {
		if (position == Categories.getSearchFragmentPosition()) {

			return BaseBusinessListFragment.newSearchInstance();

		} else if (position == Categories.getHotDealsFragmentPosition()) {

			return BaseBusinessListFragment.newHotDealsInstance();

		} else {
			return BaseBusinessListFragment.newCategoryInstance(mCategories.get(position).ID);
		}
	}

	@Override
	public int getCount() {
		return mCategories.size();
	}

}
