package com.positiveapps.getFashion.adapters;

import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

import com.positiveapps.getFashion.R;
import com.positiveapps.getFashion.backend.APIManager;
import com.positiveapps.getFashion.backend.SettingManager;
import com.positiveapps.getFashion.backend.UserManager;
import com.positiveapps.getFashion.jsonmodels.Reservation;
import com.positiveapps.getFashion.jsonmodels.apiresponse.DataCheck;
import com.positiveapps.getFashion.util.ActivityStarter;
import com.positiveapps.getFashion.util.DateUtil;
import com.positiveapps.getFashion.util.ImageLoader2;
import com.positiveapps.getFashion.views.SwitchButton;

import android.app.Dialog;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class ReservationListAdapter extends BaseAdapter {
	
	public static final int PURCHASE_TYPE_PICKUP = 3;
	public static final int PURCHASE_TYPE_DELIVERY = 2;
	public static final int PURCHASE_TYPE_RESERVE = 1;
	
	// the TAG
	private final static String TAG = "Articles_list_adapter";
	// the Reservation list
    private final List<Reservation> reservations;
    private final Context context;
    // the radio button array
    boolean[] itemChecked;
    boolean[] reservationCheck;
	private Dialog loadingDialog; 
    
    public ReservationListAdapter(Context context, List<Reservation> reservations) {
        this.reservations = reservations;
        this.context = context;
        itemChecked = new boolean[reservations.size()];
        reservationCheck = new boolean[reservations.size()];  
        initReservationArray();
    }
    

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {

        	Log.d(TAG, "convertView == null");
        	
            convertView = View.inflate(context, R.layout.list_view_reservation_item, null);
            holder = new ViewHolder();
			// getting the reservation views
			holder.radioBtnReservation = (CheckBox) convertView.findViewById(R.id.checkBoxReservation);
			holder.imgUserImage = (ImageView) convertView.findViewById(R.id.imgUserImage);
			holder.txtUserName = (TextView) convertView.findViewById(R.id.txtUserName);
			holder.txtPhone = (TextView) convertView.findViewById(R.id.txtPhone);
			holder.txtQuantity = (TextView) convertView.findViewById(R.id.txtQuantity);
			holder.Param1t = (TextView) convertView.findViewById(R.id.Param1_reserve_text);
			holder.Param2t = (TextView) convertView.findViewById(R.id.Param2_reserve_text);
			holder.Param3t = (TextView) convertView.findViewById(R.id.Param3_reserve_text);
			holder.Param4t = (TextView) convertView.findViewById(R.id.Param4_reserve_text);
			holder.Param5t = (TextView) convertView.findViewById(R.id.Param5_reserve_text);
			holder.Param1 = (TextView) convertView.findViewById(R.id.Param1_reserve);
			holder.Param2 = (TextView) convertView.findViewById(R.id.Param2_reserve);
			holder.Param3 = (TextView) convertView.findViewById(R.id.Param3_reserve);
			holder.Param4 = (TextView) convertView.findViewById(R.id.Param4_reserve);
			holder.Param5 = (TextView) convertView.findViewById(R.id.Param5_reserve);
			holder.txtOrderDate = (TextView) convertView.findViewById(R.id.txtOrderDate);
			holder.txtReservationType = (TextView) convertView.findViewById(R.id.txtReservationType);
			holder.confirmOrder = (SwitchButton) convertView.findViewById(R.id.switchReservationStatus);
			holder.txtDeliveryType = (TextView) convertView.findViewById(R.id.txtDeliveryType);
			holder.txtSaveSeat = (TextView) convertView.findViewById(R.id.save_seat);
			holder.txtDeliveryAddress = (TextView) convertView.findViewById(R.id.txtDeliveryAddress);
			holder.txtDeliveryAddressTXT = (TextView) convertView.findViewById(R.id.txtDeliveryAddress_txt);
			holder.txtIsMemeber = (TextView) convertView.findViewById(R.id.txtIsMemeber);
			holder.txtComments = (TextView) convertView.findViewById(R.id.txtComments);
			holder.imgUserImage = (ImageView)convertView.findViewById(R.id.imgUserImage);

		/*	if(UserManager.getAppUser().FBID != null){

			}*/
			for (String key: SettingManager.getSetting().params.keySet()) {
				if(key.matches("Param1")){
					holder.Param1t.setText(SettingManager.getSetting().params.get(key) +": ");
				}
				if(key.matches("Param2")){
					holder.Param2t.setText(SettingManager.getSetting().params.get(key) +": ");
				}
				if(key.matches("Param3")){
					holder.Param3t.setText(SettingManager.getSetting().params.get(key) +": ");
				}
				if(key.matches("Param4")){
					holder.Param4t.setText(SettingManager.getSetting().params.get(key) +": ");
				}
				if(key.matches("Param5")){
					holder.Param5t.setText(SettingManager.getSetting().params.get(key) +": ");
				}
			}
            convertView.setTag(holder);
        } else {
        	Log.d(TAG, "convertView != null");
            holder = (ViewHolder) convertView.getTag();
//            holder.imgUserImage.setImageBitmap(reservations.get(position));    // not sure if this is needed?

        }
        
        final Reservation reservation = reservations.get(position);
        
        if(reservation != null){
    		holder.txtUserName.setText(reservation.UserName);
			ImageLoader2.ByUrl(reservation.AppuserImage, holder.imgUserImage);
//			Picasso.with(context).load(reservation.AppuserImage).into(holder.imgUserImage);
//			ImageLoader.loadProductImage(context,reservation.AppuserImage,holder.imgUserImage);
			if(reservation.Quantity == 0){
    			holder.txtQuantity.setVisibility(View.GONE);
    		}else if(reservation.Quantity != 0){
    			holder.txtQuantity.setVisibility(View.VISIBLE);
    			holder.txtQuantity.setText(reservation.Quantity+"");
    		}
    		
    		if(reservation.Param1.matches("")){
    			holder.Param1.setVisibility(View.GONE);
    			holder.Param1t.setVisibility(View.GONE);
    		}else if(!reservation.Param1.matches("")){
    			holder.Param1.setText(reservation.Param1);
    			holder.Param1.setVisibility(View.VISIBLE);
    			holder.Param1t.setVisibility(View.VISIBLE);
    		}
    		if(reservation.Param2.matches("")){
    			holder.Param2.setVisibility(View.GONE);
    			holder.Param2t.setVisibility(View.GONE);
    		}else if(!reservation.Param2.matches("")){
    			holder.Param2.setText(reservation.Param2);
    			holder.Param2.setVisibility(View.VISIBLE);
    			holder.Param2t.setVisibility(View.VISIBLE);
    		}
    		if(reservation.Param3.matches("")){
    			holder.Param3.setVisibility(View.GONE);
    			holder.Param3t.setVisibility(View.GONE);
    		}else if(!reservation.Param3.matches("")){
    			holder.Param3.setText(reservation.Param3);
    			holder.Param3.setVisibility(View.VISIBLE);
    			holder.Param3t.setVisibility(View.VISIBLE);
    		}
    		if(reservation.Param4.matches("")){
    			holder.Param4t.setVisibility(View.GONE);
    			holder.Param4.setVisibility(View.GONE);
    		}else if(!reservation.Param4.matches("")){
    			holder.Param4.setText(reservation.Param4);
    			holder.Param4.setVisibility(View.VISIBLE);
    			holder.Param4t.setVisibility(View.VISIBLE);
    		}
 
    		if(reservation.Param5.matches("")){
    			holder.Param5.setVisibility(View.GONE);
    			holder.Param5t.setVisibility(View.GONE);
    		}else if(!reservation.Param5.matches("")){
    			holder.Param5.setText(reservation.Param5);
    			holder.Param5.setVisibility(View.VISIBLE);
    			holder.Param5t.setVisibility(View.VISIBLE);
    		}
    		
    		if(reservation.AppuserAddress.matches("")){
    			holder.txtDeliveryAddress.setText(context.getString(R.string.addressed_not_entered));
    		}else if(!reservation.AppuserAddress.matches("")){
    			holder.txtDeliveryAddress.setText(reservation.AppuserAddress);
    		}
    		
    		if(reservation.UserPhone.matches("")){
    			holder.txtPhone.setText(context.getString(R.string.phone_not_entered));
				holder.txtPhone.setOnClickListener(null);
    		}else if(!reservation.UserPhone.matches("")){
    			holder.txtPhone.setText(reservation.UserPhone);
				holder.txtPhone.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						ActivityStarter.dialer(context, holder.txtPhone.getText().toString());
					}
				});
    		}
    		
    		OnCheckedChangeListener reservationListener = new OnCheckedChangeListener() {
				
				@Override
				public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) { 
					if(isChecked){
						verifyReservation(reservation.ID , holder.txtReservationType, holder.confirmOrder ,position);
						}
					}
			};
			
    		holder.txtOrderDate.setText(DateUtil.changeStringFormat(reservation.ReservationDate));
			
    		if (reservationCheck[position]) {
				holder.txtReservationType.setText(context.getString(R.string.reservation_varified));
				holder.confirmOrder.setEnabled(false);
				holder.confirmOrder.setClickable(false);
			} else if (!reservationCheck[position]) {
				holder.txtReservationType.setText(context.getString(R.string.reservation_not_varified));
				holder.confirmOrder.setChecked(false);
				holder.confirmOrder.setEnabled(true);
				holder.confirmOrder.setClickable(true);
			}
			
			holder.confirmOrder.setOnCheckedChangeListener(reservationListener);
			
			
			
			
			switch (reservation.SaleType) {
			case PURCHASE_TYPE_DELIVERY:
				holder.txtDeliveryAddress.setVisibility(View.VISIBLE);
				holder.txtDeliveryAddressTXT.setVisibility(View.VISIBLE); 
				holder.txtDeliveryType.setVisibility(View.GONE);
				holder.txtSaveSeat.setVisibility(View.GONE); 
				break;
			case PURCHASE_TYPE_PICKUP: 
				holder.txtDeliveryType.setVisibility(View.VISIBLE);
				holder.txtSaveSeat.setVisibility(View.GONE); 
				holder.txtDeliveryAddress.setVisibility(View.GONE);
				holder.txtDeliveryAddressTXT.setVisibility(View.GONE); 
				break;
			case PURCHASE_TYPE_RESERVE:
				holder.txtSaveSeat.setVisibility(View.VISIBLE); 
				holder.txtDeliveryType.setVisibility(View.GONE);
				holder.txtDeliveryAddress.setVisibility(View.GONE);
				holder.txtDeliveryAddressTXT.setVisibility(View.GONE); 
				break;
			default:
				break;
			}
    		holder.txtDeliveryAddress.setText(reservation.AppuserAddress);
    		if(reservation.ClubMember == 1){
    			holder.txtIsMemeber.setText(context.getString(R.string.yes));
    		}else if(reservation.ClubMember == 0){
    			holder.txtIsMemeber.setText(context.getString(R.string.no));
    		}
    		
    		holder.txtComments.setText(reservation.Comments);
    		
    		///////// making sure the radio button state wont change when it exits the screen /////////
    		holder.radioBtnReservation.setChecked(false);
    		 
    		
    		if (itemChecked[position]) {
    			holder.radioBtnReservation.setChecked(true);
    		} else if (!itemChecked[position]) {
    			holder.radioBtnReservation.setChecked(false);
    		}
    	    
    	    holder.radioBtnReservation.setOnClickListener(new OnClickListener() {
    	    	   @Override
    	    	   public void onClick(View v) {
    		    	    if (holder.radioBtnReservation.isChecked())
    		    	    	itemChecked[position] = true;
//    		    	    	holder.radioBtnReservation.setChecked(true); // might be needed if using RadioButton instead of CheckBox
    		    	    else
    		    	    	itemChecked[position] = false;
//    		    	    	holder.radioBtnReservation.setChecked(false); // might be needed if using RadioButton instead of CheckBox
    		    	   }
    	    	  });
    	    	
    	     //////////////////// can be used if an image is needed /////////////
//          	holder.imgUserImage.setTag(items.get(position));
    	    	 //can be a method for getting the image and putting it in place
//          	new FetchImage(holder.itemImage, items.get(position), context).execute(items.get(position).getImage());
//
//    			// the Switch listener
//    			holder.confirmOrder.setOnCheckedChangeListener(new OnCheckedChangeListener() {
//    				@Override
//    				public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//    					Toast.makeText(context, "" + isChecked, Toast.LENGTH_SHORT).show();
//    				}
//    			});

        }

        return convertView;
    }
 
    @Override
    public int getCount() {
    	Log.d(TAG, reservations.size()+"");
        return reservations.size();
    }
 
    @Override
    public Object getItem(int position) {
    	Log.d(TAG, "getItem");
        return reservations.get(position);
    }
 
    @Override
    public long getItemId(int id) {
        return id;
    }
 
    static class ViewHolder {
		public TextView txtSaveSeat;
		public TextView txtDeliveryAddressTXT;
		public TextView Param5t;
		public TextView Param4t;
		public TextView Param3t;
		public TextView Param2t;
		public TextView Param1t;
		CheckBox radioBtnReservation;
		ImageView imgUserImage;
		TextView txtUserName;
		TextView txtPhone;
		TextView txtQuantity;
		TextView Param1;
		TextView Param2;
		TextView Param3;
		TextView Param4;
		TextView Param5;
		TextView txtOrderDate;
		TextView txtReservationType;
		SwitchButton confirmOrder;
		TextView txtDeliveryType;
		TextView txtDeliveryAddress;
		TextView txtIsMemeber;
		TextView txtComments;
    }
    
    public boolean getRadioButton(int position){
    	
    	boolean isChecked = itemChecked[position];
    	
    	return isChecked;
    }
    
    public ArrayList<Reservation> clearSelected() {
		ArrayList<Reservation> temp = new ArrayList<Reservation>();
		for (int i = 0; i < reservations.size(); i++) {
			if (!itemChecked[i]) {
				temp.add(reservations.get(i));
			}
		}
		return temp;
	}

    public String getSelectedInStringArray() {
		StringBuilder sb = new StringBuilder();
		sb.append("[");
		for (int i = 0; i < reservations.size(); i++) {
			if (itemChecked[i]) {
				sb.append(reservations.get(i).ID+"");
				sb.append(",");
			}
		}
		sb.append("]");
		return sb.toString().replace(",]", "]");
	}
    
    public String getAllInStringArray() {
		StringBuilder sb = new StringBuilder();
		sb.append("[");
		for (int i = 0; i < reservations.size(); i++) {
			sb.append(reservations.get(i).ID+"");
			sb.append(",");
		}
		sb.append("]");
		return sb.toString().replace(",]", "]");
	}
    
    private void verifyReservation(long ReservationID, final TextView textView, final SwitchButton switchD, final int position) {
    	showLoadingDialog();
		APIManager.api.vaerifyReservation(UserManager.getAppUser().ID, ReservationID, new Callback<DataCheck>() {

			@Override
			public void failure(RetrofitError arg0) {
				dismissLoadingDialog();
				Toast.makeText(context, "failure", Toast.LENGTH_SHORT).show();
			}

			@Override
			public void success(DataCheck arg0, Response arg1) {
				if(arg0.error == 0){
					Toast.makeText(context, context.getString(R.string.reservation_varified), Toast.LENGTH_SHORT).show();
					textView.setText(context.getString(R.string.reservation_varified));
					switchD.setEnabled(false);
					switchD.setClickable(false);
					notifyDataSetChanged();
					reservationCheck[position] = true;
				}else{
					textView.setText(context.getString(R.string.reservation_not_varified));
					Toast.makeText(context, arg0.errdesc, Toast.LENGTH_SHORT).show();
				}
				dismissLoadingDialog();
			}
		});

	}
    
	 public void showLoadingDialog(){
			loadingDialog = new Dialog(context, R.style.loadingDialog);
			final View view = LayoutInflater.from(context).inflate(R.layout.dialog_loading, null);
			loadingDialog.setContentView(view);
			loadingDialog.setCancelable(false);
			loadingDialog.show();
		}

		
		public void dismissLoadingDialog(){ 
			loadingDialog.dismiss();
		} 
    
		private void initReservationArray() {
			for (int i = 0; i < reservations.size(); i++) {
				if(reservations.get(i).Status == 1){
					reservationCheck[i] = true;
				}
			} 
		}
		
		
}


