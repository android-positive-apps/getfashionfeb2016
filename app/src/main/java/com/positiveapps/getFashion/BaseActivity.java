package com.positiveapps.getFashion;

import java.security.MessageDigest; 
import java.security.NoSuchAlgorithmException;

import com.positiveapps.getFashion.backend.APIManager;
import com.positiveapps.getFashion.backend.Categories;
import com.positiveapps.getFashion.backend.Prefs;
import com.positiveapps.getFashion.backend.SettingManager;
import com.positiveapps.getFashion.backend.UserManager;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.Signature;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

public class BaseActivity extends AppCompatActivity { 
	public void cleanFragManagerStack() {
		FragmentManager fm = getSupportFragmentManager();
		if (fm.getBackStackEntryCount() > 0) {
			fm.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
		}
		
		if(Prefs.needInit())
			Prefs.initPreference(getApplicationContext());
		
		if(Categories.needInit())
			Categories.init(getApplicationContext());
		
		if(UserManager.needInit())
			UserManager.init(getApplicationContext());
		
		if(SettingManager.needInit())
			SettingManager.init(getApplicationContext());
		
		if(APIManager.needInit())
			APIManager.initAPIManager(getApplicationContext());
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		
	}
	
	protected void onResume() {
		super.onResume();
		
		
	};
	
	public void hideKeyboard(View v) {
		InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
	}

	public int getDimensionPixelSize(int id) {
		return getResources().getDimensionPixelSize(id);
	}

	protected void logHashKey() {
		try {
			PackageInfo info = getPackageManager().getPackageInfo(getPackageName(), PackageManager.GET_SIGNATURES);
			for (Signature signature : info.signatures) {
				MessageDigest md = MessageDigest.getInstance("SHA");
				md.update(signature.toByteArray());
				Log.e("MY_KEY_HASH:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
			}
		} catch (NameNotFoundException e) {} catch (NoSuchAlgorithmException e) {}
	}
}
