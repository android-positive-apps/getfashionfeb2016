/**
 * 
 */
package com.positiveapps.getFashion.database;



import com.positiveapps.getFashion.App;

import android.content.Context;
import android.content.SharedPreferences;


/**
 * @author Nati Gabay
 *
 */
public class AppPreference {

	
	public static final int UNITS_TYPE_KM = 1;
	public static final int UNITS_TYPE_MILES = 2;
	public static final int SKI_PASS_TYPE_AREA = 1;
	public static final int SKI_PASS_TYPE_RESORT = 2;
	
	
	private static AppPreference instance;
	private static Context context = App.applicationContext;
	
	private static PreferenceUserProfil userProfil;
	private static PreferenceUserSettings userSettings;
	private static PreferenceGeneralSettings generalSettings;
	
	private AppPreference (Context context){
		userSettings = PreferenceUserSettings.getUserSettingsInstance();
		userProfil = PreferenceUserProfil.getUserProfileInstance();
		generalSettings = PreferenceGeneralSettings.getGeneralSettingsInstance();
	}
	
	public static AppPreference getInstans(Context context){
		if(instance == null){
			instance = new AppPreference(context);
		}
		setInstancContext(context);
		return instance;
	}
	
	public static void removeInstance(){
		userSettings.removeInstance();
		userProfil.removeInstance();
		generalSettings.removeInstance();
		instance = null;
	}
	
	private static void setInstancContext (Context context){
		AppPreference.context = context;
	}
	
	
	
	/**
	 * @return the userProfil
	 */
	public PreferenceUserProfil getUserProfil() {
		return userProfil;
	}

	/**
	 * @param userProfil the userProfil to set
	 */
	public void setUserProfil(PreferenceUserProfil userProfil) {
		this.userProfil = userProfil;
	}

	/**
	 * @return the userSettings
	 */
	public PreferenceUserSettings getUserSettings() {
		return userSettings;
	}

	/**
	 * @param userSettings the userSettings to set
	 */
	public void setUserSettings(PreferenceUserSettings userSettings) {
		this.userSettings = userSettings;
	}
	
	/**
	 * @return the generalSettings
	 */
	public PreferenceGeneralSettings getGeneralSettings() {
		return generalSettings;
	}

	/**
	 * @param generalSettings the generalSettings to set
	 */
	public void setGeneralSettings(PreferenceUserSettings generalSettings) {
		this.userSettings = generalSettings;
	}


	public static class PreferenceGeneralSettings {

		public final String GENERAL_SETTINGS = "GeneralSettings";

		public static final String LAT = "Lat";
		public static final String LON = "Lon";
		public static final String GCM_KEY = App.applicationContext + "GcmKey";
	
		public static final String EMERGENCY_PHONE_1 = "EmergencyPhone1";
		public static final String EMERGENCY_PHONE_2 = "EmergencyPhone2";
		public static final String SCREEN_WIDTH = "ScreenWidth";
		public static final String SCREEN_HIGHT = "ScreenHight";
		public static final String LAST_READ_MESSAGE_COUNT = "LastReadMessageCount";
		

		private SharedPreferences generalSettings;
		public static PreferenceGeneralSettings generalSettingInstance;

		private PreferenceGeneralSettings() {
			generalSettings = context.getSharedPreferences(GENERAL_SETTINGS,
					Context.MODE_PRIVATE);
		}

		public static PreferenceGeneralSettings getGeneralSettingsInstance() {
			if (generalSettingInstance == null) {
				generalSettingInstance = new PreferenceGeneralSettings();
			}
			return generalSettingInstance;
		}
		
		public void removeInstance(){
			generalSettings.edit().clear().commit();
			generalSettingInstance = null;
		}
		

		public void setLat(String toSet) {
			generalSettings.edit().putString(LAT, toSet).commit();
		}

		public void setLon(String toSet) {
			generalSettings.edit().putString(LON, toSet).commit();
		}
		
		public void setGCMKey(String gcmKey) {
			generalSettings.edit().putString(GCM_KEY, gcmKey).commit();
		}
		
		public void setEmergencyPhone1(String phone) {
			generalSettings.edit().putString(EMERGENCY_PHONE_1, phone).commit();
		}
		
		public void setEmergencyPhone2(String phone) {
			generalSettings.edit().putString(EMERGENCY_PHONE_2, phone).commit();
		}
		
		public void setScreenWidth(int width) {
			generalSettings.edit().putInt(SCREEN_WIDTH, width).commit();
		}
		
		public void setScreenHight(int hight) {
			generalSettings.edit().putInt(SCREEN_HIGHT, hight).commit();
		}

		public void setLastReadMessageCount(String groupID,int count) {
			generalSettings.edit().putInt(groupID + LAST_READ_MESSAGE_COUNT, count).commit();
		}
		
		public String getLat() {
			return generalSettings.getString(LAT, "0.0");
		}

		public String getLon() {
			return generalSettings.getString(LON, "0.0");
		}
		
		
		public String getLocation () {
			return getLat()+","+getLon();
		}
		
		public String getGCMKey () {
			return generalSettings.getString(GCM_KEY, "");
		}
		
		public String getEmergencyPhone1 () {
			return generalSettings.getString(EMERGENCY_PHONE_1, "");
		}
		
		public String getEmergencyPhone2 () {
			return generalSettings.getString(EMERGENCY_PHONE_2, "");
		}
		
		public int getScreenWidth () {
			return generalSettings.getInt(SCREEN_WIDTH, 0);
		}
		
		public int getScreenHight () {
			return generalSettings.getInt(SCREEN_HIGHT, 0);
		}
		
		public int getLastReadMessageCount (String groupId) {
			return generalSettings.getInt(groupId + LAST_READ_MESSAGE_COUNT, 0);
		}
		
		public static final String REPORT_ID = "reportId";

		public void setReportId(int reportId) {
		   generalSettings.edit().putInt(REPORT_ID, reportId).commit();
		}

		public int getReportId(){
		   return generalSettings.getInt(REPORT_ID, 0);
		}
		
//		/***************************  temp  *********************/
//		
//		public static final String TEMP_SCHOOL_NAME = "tempSchoolName";
//
//		public void setTempSchoolName(String tempSchoolName) {
//		generalSettings.edit().putString(TEMP_SCHOOL_NAME, tempSchoolName).commit();
//		}
//
//		public String getTempSchoolName(){
//		return generalSettings.getString(TEMP_SCHOOL_NAME, "");
//		}
//		
//		public static final String TEMP_CLASS_OR_GARDEN = "tempClassOrGarden";
//
//		public void setTempClassOrGarden(int tempClassOrGarden) {
//		   generalSettings.edit().putInt(TEMP_CLASS_OR_GARDEN, tempClassOrGarden).commit();
//		}
//
//		public int getTempClassOrGarden(){
//		   return generalSettings.getInt(TEMP_CLASS_OR_GARDEN, 0);
//		}
//		
//		public static final String TEMP_KID_NAME = "tempKidName";
//
//		public void setTempKidName(String tempKidName) {
//		   generalSettings.edit().putString(TEMP_KID_NAME, tempKidName).commit();
//		}
//
//		public String getTempKidName(){
//		   return generalSettings.getString(TEMP_KID_NAME, "");
//		}
//		
//		public static final String TEMP_KID_SURNAME = "tempKidSurname";
//
//		public void setTempKidSurname(String tempKidSurname) {
//		   generalSettings.edit().putString(TEMP_KID_SURNAME, tempKidSurname).commit();
//		}
//
//		public String getTempKidSurname(){
//		   return generalSettings.getString(TEMP_KID_SURNAME, "");
//		}
//		
//		public static final String TEMP_KID_GENDER = "tempKidGender";
//
//		public void setTempKidGender(boolean tempKidGender) {
//		   generalSettings.edit().putBoolean(TEMP_KID_GENDER, tempKidGender).commit();
//		}
//
//		public boolean getTempKidGender(){
//		   return generalSettings.getBoolean(TEMP_KID_GENDER, true);
//		}
//		
//		public static final String TEMP_KID_IMAGE64 = "tempKidImage64";
//
//		public void setTempKidImage64(String tempKidImage64) {
//		   generalSettings.edit().putString(TEMP_KID_IMAGE64, tempKidImage64).commit();
//		}
//
//		public String getTempKidImage64(){
//		   return generalSettings.getString(TEMP_KID_IMAGE64, "");
//		}
//
//		public static final String TEMP_SCHOOL_ID = "tempSchoolId";
//
//		public void setTempSchoolId(String tempSchoolId) {
//		   generalSettings.edit().putString(TEMP_SCHOOL_ID, tempSchoolId).commit();
//		}
//
//		public String getTempSchoolId(){
//		   return generalSettings.getString(TEMP_SCHOOL_ID, "");
//		}
//		
//		public static final String TEMP_SCHOOL_GRADE = "tempSchoolGrade";
//
//		public void setTempSchoolGrade(int tempSchoolGrade) {
//		   generalSettings.edit().putInt(TEMP_SCHOOL_GRADE, tempSchoolGrade).commit();
//		}
//
//		public int getTempSchoolGrade(){
//		   return generalSettings.getInt(TEMP_SCHOOL_GRADE, 0);
//		}
//		
//		public static final String TEMP_SCHOOL_LEVEL = "tempSchoolLevel";
//
//		public void setTempSchoolLevel(int tempSchoolLevel) {
//		   generalSettings.edit().putInt(TEMP_SCHOOL_LEVEL, tempSchoolLevel).commit();
//		}
//
//		public int getTempSchoolLevel(){
//		   return generalSettings.getInt(TEMP_SCHOOL_LEVEL, 0);
//		}
//		
//		public static final String TEMP_GARDEN_CITY = "tempGardenCity";
//
//		public void setTempGardenCity(String tempGardenCity) {
//		   generalSettings.edit().putString(TEMP_GARDEN_CITY, tempGardenCity).commit();
//		}
//
//		public String getTempGardenCity(){
//		   return generalSettings.getString(TEMP_GARDEN_CITY, "");
//		}
//		
//		public static final String TEMP_INSTITUTE_ID = "tempInstituteId";
//
//		public void setTempInstituteId(String tempInstituteId) {
//		   generalSettings.edit().putString(TEMP_INSTITUTE_ID, tempInstituteId).commit();
//		}
//
//		public String getTempInstituteId(){
//		   return generalSettings.getString(TEMP_INSTITUTE_ID, "");
//		}
	}



	public static class PreferenceUserProfil {
		
		public static final String USER_PROFILE = "UserProfile";
		
		public static final  String USER_ID = "user_id";
		private static final String NAME = "Name";
		private static final String EMAIL = "Email";
		private static final String USER_PHONE = "UserPhone";
	    private static final String NOTIFICATION_COUNTER = "NotificationCounter";
	    private static final String LAST_INBOX_COUTNT = "LastInboxCount";
	    
	    private static final String PASS_CODE = "PassCode";
		private static final String PASS = "Password";
		private static final String SECRET = "Secret";
		private static final String ICON_URL = "IconUrl";
		private static final String COUNTRY_NAME = "CountryName";
		private static final String CITY = "City";
		private static final String LAST_LAT = "LastLat";
		private static final String LAST_LON = "LastLng";
		private static final String MY_PHUS_SERVER_ID = "MyPushServerID";
		private static final String FBID = "FBID";
		private static final String CREATED_AT = "Created";
		private static final String UPDATE_INTERVAL = "ApdateInterval";
		
		private SharedPreferences userProfile;
		private static PreferenceUserProfil userProfileInstance;
		
		private  PreferenceUserProfil (){
			userProfile = context.getSharedPreferences(USER_PROFILE, Context.MODE_PRIVATE);
		}
		
		
		public  static PreferenceUserProfil getUserProfileInstance (){
			if (userProfileInstance == null){
				userProfileInstance = new PreferenceUserProfil();
			}
			return userProfileInstance;
		}
		
		public  void removeInstance(){
			userProfile.edit().clear().commit();
			userProfileInstance = null;
		}
		
		//-----------------------------------------------
		
		public static final String LAST_NAME = "lastName";

		public void setLastName(String lastName) {
		   userProfile.edit().putString(LAST_NAME, lastName).commit();
		}

		public String getLastName(){
		   return userProfile.getString(LAST_NAME, "");
		}
		
		public static final String FIRST_NAME = "firstName";

		public void setFirstName(String firstName) {
		   userProfile.edit().putString(FIRST_NAME, firstName).commit();
		}

		public String getFirstName(){
		   return userProfile.getString(FIRST_NAME, "");
		}
		
		public static final String ADDITIONAL_PHONE = "additionalPhone";

		public void setAdditionalPhone(String additionalPhone) {
		   userProfile.edit().putString(ADDITIONAL_PHONE, additionalPhone).commit();
		}

		public String getAdditionalPhone(){
		   return userProfile.getString(ADDITIONAL_PHONE, "");
		}
		
		public static final String USER_HOUSE = "userHouse";

		public void setUserHouse(String userHouse) {
		   userProfile.edit().putString(USER_HOUSE, userHouse).commit();
		}

		public String getUserHouse(){
		   return userProfile.getString(USER_HOUSE, "");
		}
		
		
		public static final String USER_APARTMENT = "userApartment";

		public void setUserApartment(String userApartment) {
		   userProfile.edit().putString(USER_APARTMENT, userApartment).commit();
		}

		public String getUserApartment(){
		   return userProfile.getString(USER_APARTMENT, "");
		}
 
		public static final String USER_STREET = "userStreet";

		public void setUserStreet(String userStreet) {
		   userProfile.edit().putString(USER_STREET, userStreet).commit();
		}

		public String getUserStreet(){
		   return userProfile.getString(USER_STREET, "");
		}
		
		public static final String USER_IMAGE = "userImage";

		public void setUserImage(String userImage) {
		   userProfile.edit().putString(USER_IMAGE, userImage).commit();
		}

		public String getUserImage(){
		   return userProfile.getString(USER_IMAGE, "");
		}
		
		//-----------------------------------------------
		
		public void setUserId(String userId) {
			userProfile.edit().putString(USER_ID, userId).commit();
		}

		public void setUserName(String name) {
			userProfile.edit().putString(NAME, name).commit();
		}

		public void setUserEmail(String email) {
			userProfile.edit().putString(EMAIL, email).commit();
		}

		
		public void setNotificationCount(int credits) {
			userProfile.edit().putInt(NOTIFICATION_COUNTER, credits).commit();
		}
		
		public void setLastInboxCoutn(int credits) {
			userProfile.edit().putInt(LAST_INBOX_COUTNT, credits).commit();
		}
		
		public void setUserPhone(String phone) {
			userProfile.edit().putString(USER_PHONE, phone).commit();
		}
		
		public void setUserPassCode(String passCode) {
			userProfile.edit().putString(PASS_CODE, passCode).commit();
		}
		public void setUserPass(String pass) {
			userProfile.edit().putString(PASS, pass).commit();
		}
		public void setUserCountry(String country) {
			userProfile.edit().putString(COUNTRY_NAME, country).commit();
		}
		public void setUserCity(String city) {
			userProfile.edit().putString(CITY, city).commit();
		}
		public void setUserFBID(String fbid) {
			userProfile.edit().putString(FBID, fbid).commit();
		}
		public void setUserSecret(String secret) {
			userProfile.edit().putString(SECRET, secret).commit();
		}
		public void setUserLastLat(String lastlat) {
			userProfile.edit().putString(LAST_LAT, lastlat).commit();
		}
		public void setUserLastLon(String lastlon) {
			userProfile.edit().putString(LAST_LON, lastlon).commit();
		}
		public void setUserCreatedAt(String createdAt) {
			userProfile.edit().putString(CREATED_AT, createdAt).commit();
		}
		public void setUserPushId(String pushId) {
			userProfile.edit().putString(MY_PHUS_SERVER_ID, pushId).commit();
		}
		public void setUserIconUrl(String iconUrl) {
			userProfile.edit().putString(ICON_URL, iconUrl).commit();
		}
		public void setUserApdateInterval(int time) {
			userProfile.edit().putInt(UPDATE_INTERVAL, time).commit();
		}
		
		
		
		public String getUserId() {
			return userProfile.getString(USER_ID, "0");
		}

		public String getUserName() {
			return userProfile.getString(NAME, "");
		}

		public String getUserEmail() {
			return userProfile.getString(EMAIL, "");
		}

		public int getNotificationCounter() {
			return userProfile.getInt(NOTIFICATION_COUNTER,0);
		}
		
		public int getLastInboxCount() {
			return userProfile.getInt(LAST_INBOX_COUTNT,0);
		}
		
		public String getUserPhone() {
			return userProfile.getString(USER_PHONE, "0526097222");
		}
		
		public String getUserPassCode() {
			return userProfile.getString(PASS_CODE, "");
		}
		public String getUserPass() {
			return userProfile.getString(PASS, "");
		}
		public String getUserSecret() {
			return userProfile.getString(SECRET, "");
		}
		public String getUserCountry() {
			return userProfile.getString(COUNTRY_NAME, "");
		}
		public String getUserCity() {
			return userProfile.getString(CITY, "");
		}
		public String getUserLastLat() {
			return userProfile.getString(LAST_LAT, "");
		}
		public String getUserLastLon() {
			return userProfile.getString(LAST_LON, "");
		}
		public String getUserFBID() {
			return userProfile.getString(FBID, "");
		}
		public String getUserCreatedAt() {
			return userProfile.getString(CREATED_AT, "");
		}
		public String getUserIconUrl() {
			return userProfile.getString(ICON_URL, "");
		}
		public String getUserPushID() {
			return userProfile.getString(MY_PHUS_SERVER_ID, "");
		}
		public int getUserUpdateIntervall() {
			return userProfile.getInt(UPDATE_INTERVAL, 30);
		}
		
		
		


	}
	
	public static class PreferenceUserSettings {
		
		public  final String USER_SETTINGS = "UserSettings";
		
		private static final String CURRENT_SITE_ID = "CurrentSiteID";
		private static final String CURRENT_SITE_NAME = "CurrentSiteName";
		private static final String UNIT_DISTANCE = "UnitDistance";
		private static final String UNIT_TEMPERATURE = "UnitTemperature";
		private static final String SCREEN_AUTO_LOCK = "ScreenAutoLock";
		private static final String AVATR_INDEX = "AvatarIndex";
		private static final String EXPERIENCE = "Experience";
		

		private SharedPreferences userSettings;
		public static  PreferenceUserSettings userSettingInstance;
		
		private PreferenceUserSettings() {
			userSettings = context.getSharedPreferences(USER_SETTINGS, Context.MODE_PRIVATE);
		}
		
		public static PreferenceUserSettings getUserSettingsInstance() {
			if (userSettingInstance == null){
				userSettingInstance = new PreferenceUserSettings();
			}
			return userSettingInstance;
		}
		
		public void removeInstance(){
			userSettings.edit().clear().commit();
			userSettingInstance = null;
		}
		
		public static final String EVENT_PERMISSION = "eventPermission";

		public void setEventPermission(boolean eventPermission) {
		   userSettings.edit().putBoolean(EVENT_PERMISSION, eventPermission).commit();
		}

		public boolean getEventPermission(){
		   return userSettings.getBoolean(EVENT_PERMISSION, true);
		}
		
		// SET
		public void setCurrentSiteID(String siteID) {
			userSettings.edit().putString(CURRENT_SITE_ID, siteID).commit();
		}
		public void setCurrentSiteName(String siteName) {
			userSettings.edit().putString(CURRENT_SITE_NAME, siteName).commit();
		}
		public void setDistanceUnit(boolean isMile) {
			userSettings.edit().putBoolean(UNIT_DISTANCE, isMile).commit();
		}
		
		public void setTemperatureUnit(boolean isFahrenheit) {
			userSettings.edit().putBoolean(UNIT_TEMPERATURE, isFahrenheit).commit();
		}
		public void setScreenAutoLock(boolean isScreenAutoLock) {
			userSettings.edit().putBoolean(SCREEN_AUTO_LOCK, isScreenAutoLock).commit();
		}
		public void setAvatarIndex(int index) {
			userSettings.edit().putInt(AVATR_INDEX, index).commit();
		}
		public void setExperience(int experience) {
			userSettings.edit().putInt(EXPERIENCE, experience).commit();
		}
		
		
		// GET
		public String getCurrentSiteID() {
			return userSettings.getString(CURRENT_SITE_ID, "");
		}
		public String getCurrentSiteName() {
			return userSettings.getString(CURRENT_SITE_NAME, "None");
		}
		public boolean getDistanceUnit() {
			return userSettings.getBoolean(UNIT_DISTANCE, false);
		}
		public String getDistanceUnitName() {
			if (getDistanceUnit()){
				return "Mile";
			}
			return "Km";
		}
		public double getDistanceUnitFactor() {
			if (getDistanceUnit()){
				return 1609.34;
				
			}
			return 1000;
		}
		public boolean getTemperatureUnit() {
			return userSettings.getBoolean(UNIT_TEMPERATURE, false);
		}
		public boolean getScreenAutoLock() {
			return userSettings.getBoolean(SCREEN_AUTO_LOCK, true);
		}
		public int getAvatarIndex() {
			return userSettings.getInt(AVATR_INDEX, 0);
		}
		public int getExperience() {
			return userSettings.getInt(EXPERIENCE, 1);
		}		
		

	}
	
	
	
	
	
	
	
	

}
