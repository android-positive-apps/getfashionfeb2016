package com.positiveapps.getFashion.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DBHelper extends SQLiteOpenHelper {
	public static final String DB_NAME = "geteat.db";
	public static final int DB_VERSION = 2;

	public static class ReservationsTable {
		public static final String TABLE_NAME = "my_reservations";

		public static final String _ID = "_id";
		public static final String CLOUD_ID = "cloud_id";
		public static final String STATUS = "status";
		public static final String CREATED_TIMESTAMP = "created";
		public static final String USER_ID = "user_id";
		public static final String BUSINESS_ID = "business_id";
	}

	public DBHelper(Context context) {
		super(context, DB_NAME, null, DB_VERSION);

	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		String sqlCreateReservationsTable = "CREATE TABLE " + ReservationsTable.TABLE_NAME + "("
				+ ReservationsTable._ID
				+ " INTEGER PRIMARY KEY AUTOINCREMENT ,"
				+ ReservationsTable.CLOUD_ID + " INTEGER ,"
				+ ReservationsTable.CREATED_TIMESTAMP + " INTEGER ,"
				+ ReservationsTable.USER_ID + " INTEGER ,"
				+ ReservationsTable.BUSINESS_ID + " INTEGER UNIQUE,"
				+ ReservationsTable.STATUS + " INTEGER " + ")";
		
		db.execSQL(sqlCreateReservationsTable);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		String sqlDropReservationsTable = "DROP TABLE IF EXISTS " + ReservationsTable.TABLE_NAME;
		
		db.execSQL(sqlDropReservationsTable);
		onCreate(db);
	}

}
