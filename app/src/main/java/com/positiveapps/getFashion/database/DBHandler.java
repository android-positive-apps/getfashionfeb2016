package com.positiveapps.getFashion.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.positiveapps.getFashion.database.DBHelper.ReservationsTable;
import com.positiveapps.getFashion.jsonmodels._Business;

public class DBHandler {

	private DBHelper dbHelp;
	public static final int THREE_HOURS = 1000 * 60 * 60 * 3;
	private static DBHandler ref = null;

	public static synchronized DBHandler getInstance(Context ctx) {
		if (ref == null)
			ref = new DBHandler(ctx);

		return ref;
	}

	public DBHandler(Context ctx) {
		dbHelp = new DBHelper(ctx);
	}

	public void deleteOldReservations() {
		SQLiteDatabase database = dbHelp.getWritableDatabase();
		
//		String whereStr = "1=1";
		String whereStr = DBHelper.ReservationsTable.CREATED_TIMESTAMP + "<"
				+ (System.currentTimeMillis() - THREE_HOURS);
		database.execSQL("DELETE FROM " + DBHelper.ReservationsTable.TABLE_NAME
				+ " WHERE " + whereStr);
		// database.delete(DBHelper.ReservationsTable.TABLE_NAME, whereStr,
		// null);
		// database.close();
		
	}

	public Cursor fetchReservationsOfBusiness(String bid) {
		SQLiteDatabase database = dbHelp.getReadableDatabase();
		String selection = ReservationsTable.BUSINESS_ID + "=" + bid;
		Cursor c = database.query(DBHelper.ReservationsTable.TABLE_NAME, null,
				selection, null, null, null, null);
		// database.close();

		return c;
	}

	public int getReservationStatus(String bid) {
		SQLiteDatabase database = dbHelp.getReadableDatabase();
		
		if(bid.isEmpty()){
			bid="-1";
		}
		
		String selection = ReservationsTable.BUSINESS_ID + "=" + bid;
		Cursor c = database.query(DBHelper.ReservationsTable.TABLE_NAME, null,
				selection, null, null, null, null);

		if (c == null || c.getCount() <= 0)
			return _Business.RES_STATUS_NO_RESERVATION;

		c.moveToFirst();
		int status = c.getInt(c.getColumnIndex(ReservationsTable.STATUS));

		c.close();
		// database.close();

		return status;
	}

	public void insertOrUpdateReservation(int cloudId, String bId, int status) {
		SQLiteDatabase database = dbHelp.getWritableDatabase();

		String selection = ReservationsTable.BUSINESS_ID + "=" + bId;
		Cursor c = database.query(DBHelper.ReservationsTable.TABLE_NAME, null,
				selection, null, null, null, null);

		if (c == null || c.getCount() <= 0) {
			ContentValues content = new ContentValues();
			content.put(ReservationsTable.BUSINESS_ID, bId);
			content.put(ReservationsTable.CLOUD_ID, cloudId);
			content.put(ReservationsTable.STATUS, status);
			content.put(ReservationsTable.CREATED_TIMESTAMP,
					System.currentTimeMillis());

			database.insertWithOnConflict(ReservationsTable.TABLE_NAME, null,
					content, SQLiteDatabase.CONFLICT_REPLACE);
		} else {
			String whereClause = ReservationsTable.BUSINESS_ID + "=" + bId;
			ContentValues content = new ContentValues();
			content.put(ReservationsTable.STATUS, status);
			database.update(ReservationsTable.TABLE_NAME, content, whereClause,
					null);
		}

		c.close();

	}

}
