package com.positiveapps.getFashion;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v4.app.NavUtils;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.positiveapps.getFashion.backend.APIManager;
import com.positiveapps.getFashion.backend.Categories;
import com.positiveapps.getFashion.backend.Prefs;
import com.positiveapps.getFashion.backend.SettingManager;
import com.positiveapps.getFashion.dialogs.MessageDialog.DialogListener;
import com.positiveapps.getFashion.jsonmodels.Business;
import com.positiveapps.getFashion.jsonmodels.PriceLabel;
import com.positiveapps.getFashion.jsonmodels.Product;
import com.positiveapps.getFashion.jsonmodels.Promotion;
import com.positiveapps.getFashion.jsonmodels.apiresponse.BusinessGet;
import com.positiveapps.getFashion.util.ActivityStarter;
import com.positiveapps.getFashion.util.Coordinates;
import com.positiveapps.getFashion.util.DialogHelper;
import com.positiveapps.getFashion.util.ImageLoader;
import com.positiveapps.getFashion.util.StringFormatUtils;
import com.positiveapps.getFashion.views.BotTabBar;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class ViewBusinessActivity extends BaseActivity {
    public static final String EXTRA_BUSINESS_ID = "businessid";
    public static final String EXTRA_CATEGORY_ID = "categoryId";
    public static final String EXTRA_BUSSINESS_OBJECT = "bussinessObject";

    private static final String COMMA = ", ";


    private Toolbar mToolbar;
    private Business mBusiness;
    private ViewGroup mPromotionsContainer;
    private ViewGroup mProductContainer;
    private TextView txtTitle;
    private LayoutInflater mInflater;
    private long catId;
    private Coordinates coordinates;

    private String Param1 = "";
    private String Param2 = "";
    private String Param3 = "";
    private String Param4 = "";
    private String Param5 = "";
    private ArrayList<Promotion> promotionsArray;
    private ArrayList<Product> productsArray;
    private ViewGroup container;
    private LinearLayout linear_scroll_container;
    private TextView header_hot_deals;
    private TextView header_menu;
    private ScrollView mainScrollView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_business);

        mInflater = LayoutInflater.from(this);
        findViews();
        initActionbar();

        getBusinessInfoAndLoadBusiness();
        if (BuildConfig.FLAVOR.equals("getDesign")) {
            mainScrollView.setBackgroundColor(getResources().getColor(R.color.mainBackground));
        }
    }

    private void populateUIwithAll() {
        if (mBusiness.noProducts()) {
            return;
        }

  /*      for (int i = 0; i < mBusiness.Products.size(); i++) {
            mProductContainer.addView(getViewForProducts(
                    mBusiness.Products.get(i), mProductContainer));
            for (int k = 0; k < mBusiness.Promotions.size(); k++) {
                if (mBusiness.Promotions.get(k).ProductID == mBusiness.Products.get(i).ID) {

                    for (int j = 0; j < mBusiness.Products.get(i).ProductItems.size(); j++) {
                        System.out.println("ProductItems - > " + mBusiness.Products.get(i).ProductItems);
                        Param1 += mBusiness.Products.get(i).ProductItems.get(j).Param1 + ",";
                        Param2 += mBusiness.Products.get(i).ProductItems.get(j).Param2 + ",";
                        Param3 += mBusiness.Products.get(i).ProductItems.get(j).Param3 + ",";
                        Param4 += mBusiness.Products.get(i).ProductItems.get(j).Param4 + ",";
                        Param5 += mBusiness.Products.get(i).ProductItems.get(j).Param5 + ",";
                    }

                    mPromotionsContainer.addView(getViewForPromotion(
                            mBusiness.Promotions.get(k), mPromotionsContainer));
                }
            }
        }*/
    }

    boolean bussinessLoaded = false;
    /*LinearLayout layout = null;
    LinearLayout layout2 = null;
    int counter = 0;
    View v = LayoutInflater.from(this).inflate(R.layout.thick_green_line_with_arrow, null);
    ((TextView) v.findViewById(R.id.text_in_line)).setText(mBusiness.Products.get(i).Categories.get(j));
    LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.WRAP_CONTENT);
    v.setLayoutParams(param);
    layout2.addView(v);
    mProductContainer.addView(layout2);*/

/*    private void populateUIwithProductsAndPromotions() {

        if (mBusiness.noProducts()) {
            return;
        }

        for (int i = 0; i < mBusiness.Products.size(); i++) {
            if (mBusiness.Products.get(i).Categories == null) {
                continue;
            }
            for (int j = 0; j < mBusiness.Products.get(i).Categories.size(); j++) {

                if (!bussinessLoaded) {
                    showBusinessInfoLayout();
                    bussinessLoaded = true;
                }


                if (Long.parseLong(mBusiness.Products.get(i).Categories.get(j)) == catId) {
//                    mProductContainer.addView(getViewForProducts(mBusiness.Products.get(i), mProductContainer));
                    promotionsArray = new ArrayList<Promotion>();
                    for (int k = 0; k < mBusiness.Promotions.size(); k++) {
                        if (mBusiness.Products.get(i).ID == mBusiness.Promotions.get(k).ProductID) {
                            promotionsArray.add(mBusiness.Promotions.get(k));
//                            mPromotionsContainer.addView(getViewForPromotion( mBusiness.Promotions.get(k), mPromotionsContainer));
                        }
                    }
                }
            }
        }
    }*/


    private void populateUIwithProductsAndPromotions() {

        if (mBusiness.noProducts()) {
            return;
        }
        promotionsArray = new ArrayList<Promotion>();
        productsArray = new ArrayList<Product>();

        if (mBusiness.Products.size() != 0) {
            header_menu.setVisibility(View.VISIBLE);
//            header_menu.setBackgroundColor(getResources().getColor(R.color.mainBackground));

        }

        for (int i = 0; i < mBusiness.Products.size(); i++) {

            Log.i("RUN", "mBusiness.Products.get(i).getTitle() " + mBusiness.Products.get(i).getTitle());
            if (mBusiness.Products.get(i).Categories == null) {
                continue;
            }

            if (!bussinessLoaded) {
                showBusinessInfoLayout();
                bussinessLoaded = true;
            }

            productsArray.add(mBusiness.Products.get(i));
            if (mBusiness.Promotions.size() != 0) {
                header_hot_deals.setVisibility(View.VISIBLE);
//                header_hot_deals.setBackgroundColor(getResources().getColor(R.color.mainBackground));

            }
            for (int k = 0; k < mBusiness.Promotions.size(); k++) {
                if (mBusiness.Products.get(i).ID == mBusiness.Promotions.get(k).ProductID) {
                    promotionsArray.add(mBusiness.Promotions.get(k));
                }
            }
        }

        populateUI();
    }

    LinearLayout tempOpenLayout;
    HashMap<Long, LinearLayout> map = new HashMap<>();
    LinearLayout layout = null;

    private void populateUI() {
        int counter = 0; // populate promotion
        Log.i("RUN", "promotionsArray.size: " + promotionsArray.size());
        for (int i = 0; i < promotionsArray.size(); i++) {
            if (counter == 0) {
                layout = new LinearLayout(this);
                layout.setOrientation(LinearLayout.HORIZONTAL);
            }
            layout.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
            layout.addView(getViewForPromotion(promotionsArray.get(i), mPromotionsContainer));
//            layout.setBackgroundColor(getResources().getColor(R.color.mainBackground));
            counter++;
            if(counter != 2 && i == promotionsArray.size() - 1){
                View x = new View(this);
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.WRAP_CONTENT, 1f); // set weight
                params.gravity = Gravity.CENTER; // set gravity
                x.setLayoutParams(params);
                x.setPadding(5, 15, 5, 15); // set padding
                layout.addView(x);
                counter++;
            }
            if (counter == 2) {
                mPromotionsContainer.addView(layout);
                counter = 0;
            }
        }

        // populate categories
        HashMap<Long, ArrayList<View>> map2 = new HashMap<>(); // storing views bu categories
        LinearLayout productsMainLayout = null;
        Set<Long> set = new HashSet<Long>(); // set of categories for removing unnecessary categories
        for (int i = 0; i < productsArray.size(); i++) {
            for (int j = 0; j < productsArray.get(i).Categories.size(); j++) {
                set.add(Long.parseLong(productsArray.get(i).Categories.get(j)));
                map2.put(Long.parseLong(productsArray.get(i).Categories.get(j)), new ArrayList<View>());
            }
        }
        for (int i = 0; i < Categories.getCategories().size(); i++) {
            if (Categories.getCategories().get(i).ID == -1) {
                continue;
            }
            productsMainLayout = new LinearLayout(this);
            productsMainLayout.setOrientation(LinearLayout.VERTICAL);
//            productsMainLayout.setLayoutTransition(new LayoutTransition());
            productsMainLayout.setVisibility(set.contains(Categories.getCategories().get(i).ID) ? View.VISIBLE : View.GONE);
            View greemRow = LayoutInflater.from(this).inflate(R.layout.thick_green_line_with_arrow, null);
            greemRow.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
            final TextView row = (TextView) greemRow.findViewById(R.id.text_in_line);
            row.setText(Categories.getCategories().get(i).Name);
            row.setTag(Categories.getCategories().get(i).ID);
            row.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    toggleContents(map.get(row.getTag()));
                }
            });

            productsMainLayout.addView(greemRow);
            LinearLayout content = new LinearLayout(this);
            content.setOrientation(LinearLayout.VERTICAL);
            content.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
            content.setVisibility(View.GONE);
            map.put(Categories.getCategories().get(i).ID, content);
            productsMainLayout.addView(content);
            mProductContainer.addView(productsMainLayout);
        }


        for (int i = 0; i < productsArray.size(); i++) {
            for (int j = 0; j < productsArray.get(i).Categories.size(); j++) {
                map2.get(Long.parseLong(productsArray.get(i).Categories.get(j))).add(getViewForProducts(productsArray.get(i)));
            }
        }


        for (Map.Entry<Long, ArrayList<View>> entry : map2.entrySet()) {
            if ((entry.getValue().size() & 1) != 0) {
                View x = new View(this);
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT, 1f); // set weight
                params.gravity = Gravity.CENTER; // set gravity
                x.setLayoutParams(params);
                x.setPadding(5, 15, 5, 15); // set padding
                ArrayList<View> allViews = entry.getValue();
                allViews.add(x);
                map2.put(entry.getKey(), allViews);
            }
        }

        LinearLayout productsLayout = null;
        int counter2 = 0; // populate products
        System.out.println("set " + set.size());
        for (Long s : set) {
            System.out.println("set2 " + map2.get(s).size());
            for (int i = 0; i < map2.get(s).size(); i++) {
                if (counter2 == 0) {
                    System.out.println("set3 ");
                    productsLayout = new LinearLayout(this);
                    productsLayout.setOrientation(LinearLayout.HORIZONTAL);
                }
                productsLayout.addView(map2.get(s).get(i));
                counter2++;
                if (counter2 == 2) {
                    System.out.println("set4 ");
                    LinearLayout linearLayout = map.get(s);
                    try {
                        linearLayout.addView(productsLayout);
                    }catch (Exception e) {
                        e.printStackTrace();
                    }
                    counter2 = 0;
                }
            }
        }


    }


//		View categoryView;
//		TextView txtCategoryName;
//		ViewGroup productThumbnailsLayout;
//		long categoryID;
//		ArrayList<Product> productsForCategory;
//		String categoryName;
//		ArrayList<Category> categories = Categories.getCategories();
//		int numCategories = categories.size();

//		for (int i = 0; i < numCategories; i++) {
//
//			categoryID = categories.get(i).ID;
//			Log.e("CAT", "categoryID " + categoryID);
//			productsForCategory = mBusiness.getProductsForCategory(categoryID);
//
//			if (productsForCategory == null){
//				continue;
//			}
//
//			categoryName = Categories.getCategoryName(categoryID);
//			Log.e("CAT", "categoryName " + categoryName);
//			categoryView = mInflater.inflate(R.layout.item_list_sale_page_products_category, mProductContainer, false);
//			txtCategoryName = (TextView) categoryView.findViewById(R.id.txtCategoryName);
//			productThumbnailsLayout = (ViewGroup) categoryView.findViewById(R.id.productThumbnailsLayout);
//
//			txtCategoryName.setText(categoryName);
//			addProductsToLayout(productsForCategory, productThumbnailsLayout);
//
//
////			mPromotionsContainer.addView(categoryView);
//		}


//	private void addProductsToLayout(ArrayList<Product> productsForCategory, ViewGroup productThumbnailsLayout) {
//		for(int i=0; i<productsForCategory.size();i++){
//
//		}
//	}

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


    private void showBusinessInfoLayout() {
        View businessInfoView = mInflater.inflate(R.layout.item_bussines, mPromotionsContainer, false); // the view

        ((TextView) businessInfoView.findViewById(R.id.title)).setText(mBusiness.Name); // business name
        ((TextView) businessInfoView.findViewById(R.id.sub_title)).setText(mBusiness.Name); // business name extra
        ImageLoader.loadBusinessLogo(this, mBusiness.Logo, ((ImageView) businessInfoView.findViewById(R.id.imgLogo))); // business image logo_status_bar2
        ImageLoader.loadProductImage(this, mBusiness.CoverImage, (ImageView) businessInfoView.findViewById(R.id.imgProduct)); // business vover image

        /*handling identify categories

         */
        int[] customs;
        if (BuildConfig.FLAVOR.equals("getCoffeeShops")) {

//            Map map = SettingManager.getSetting().custom_filters;
//            Map<String, Integer> CustomMap = new HashMap<>();
//            CustomMap.put(map.get("Custom1").toString(), mBusiness.Custom1);
//            CustomMap.put(map.get("Custom2").toString(), mBusiness.Custom2);
//            CustomMap.put(map.get("Custom3").toString(), mBusiness.Custom3);
//            CustomMap.put(map.get("Custom4").toString(), mBusiness.Custom4);
            customs = new int[5];
//            customs[0] = CustomMap.get("Club");
//            customs[1] = CustomMap.get("Medical");
//            customs[2] = CustomMap.get("Recreational");
//            customs[3] = mBusiness.Delivery;
//            customs[4] = CustomMap.get("Coffeeshop");
            customs[0] =  mBusiness.Custom3;
            customs[1] = mBusiness.Custom1;
            customs[2] =  mBusiness.Custom2;
            customs[3] = mBusiness.Custom4;
            customs[4] = mBusiness.Custom5;


        }else {
            customs =
                    new int[]{mBusiness.Custom1, mBusiness.Custom2, mBusiness.Custom3,  mBusiness.Delivery, mBusiness.Custom5};
        }

//
//        int[] cat =
//                {mBusiness.Custom1,mBusiness.Custom2, mBusiness.Custom3, mBusiness.Custom4, mBusiness.Custom5};
        String[] str = getResources().getStringArray(R.array.txt_icons);
        TypedArray highRes = getResources().obtainTypedArray(R.array.high_icons);
        TypedArray lowRes = getResources().obtainTypedArray(R.array.low_icons);

        ((ImageView) businessInfoView.findViewById(R.id.cat1Img)).setImageDrawable(customs[0] == 1 ? highRes.getDrawable(0) : lowRes.getDrawable(0));
        ((TextView) businessInfoView.findViewById(R.id.cat1Txt)).setText(str[0]);
        ((ImageView) businessInfoView.findViewById(R.id.cat2Img)).setImageDrawable(customs[1] == 1 ? highRes.getDrawable(1) : lowRes.getDrawable(1));
        ((TextView) businessInfoView.findViewById(R.id.cat2Txt)).setText(str[1]);
        ((ImageView) businessInfoView.findViewById(R.id.cat3Img)).setImageDrawable(customs[2] == 1 ? highRes.getDrawable(2) : lowRes.getDrawable(2));
        ((TextView) businessInfoView.findViewById(R.id.cat3Txt)).setText(str[2]);
        ((ImageView) businessInfoView.findViewById(R.id.cat4Img)).setImageDrawable(customs[3] == 1 ? highRes.getDrawable(3) : lowRes.getDrawable(3));
        ((TextView) businessInfoView.findViewById(R.id.cat4Txt)).setText(str[3]);
        ((ImageView) businessInfoView.findViewById(R.id.cat5Img)).setImageDrawable(customs[4] == 1 ? highRes.getDrawable(4) : lowRes.getDrawable(4));
        ((TextView) businessInfoView.findViewById(R.id.cat5Txt)).setText(str[4]);

        ((TextView) businessInfoView.findViewById(R.id.phone)).setText(mBusiness.Phone); // business phone
        ((TextView) businessInfoView.findViewById(R.id.address)).setText(StringFormatUtils.getBusinessFormattedAddress(Prefs.getAppLanguage(), mBusiness)); // business address
        ((TextView) businessInfoView.findViewById(R.id.openingHour)).setText(getOpeningHours()); // business opening hours
        ((ImageView) businessInfoView.findViewById(R.id.wereOpen)).setVisibility(isStoreNowOpen() ? View.VISIBLE : View.GONE);

        ((TextView) businessInfoView.findViewById(R.id.phone)).setOnClickListener(new OnClickListener() { // call store onclick
            public void onClick(View v) {
                ActivityStarter.dialer(ViewBusinessActivity.this, mBusiness.Phone);
            }
        });

        ((ImageView) businessInfoView.findViewById(R.id.share)).setOnClickListener(new OnClickListener() { // share onclick
            @Override
            public void onClick(View v) {
                ActivityStarter.share(ViewBusinessActivity.this, mBusiness.Name);
            }
        });

        ((ImageView) businessInfoView.findViewById(R.id.report)).setOnClickListener(new OnClickListener() { // report onclick
            public void onClick(View v) {
                ActivityStarter.sendEmail(ViewBusinessActivity.this, "", "", "");
            }
        });


//		final PromotionViewHolder holder = new PromotionViewHolder(businessInfoView);
//		coordinates = new Coordinates(mBusiness.Lat, mBusiness.Lng);
//		double distance = GeneralHelper.distance(coordinates.lat, coordinates.lon, Double.parseDouble(UserManager.getAppUser().Lat), Double.parseDouble(UserManager.getAppUser().Lng));
//		String distanceInString = String.format("%.1f", distance);
//		holder.txtDistanceFromUser.setText(App.applicationContext.getResources().getString(R.string.distance_from_you)
//				+ " " + distanceInString + " " +
//				App.applicationContext.getResources().getString(R.string.km) );
//		holder.btnReserveItem.setVisibility(View.VISIBLE);
//		holder.btnReserveItem.setOnClickListener(new OnClickListener() {
//
//			@Override
//			public void onClick(View v) {
//				// TODO Auto-generated method stub
//				DialogHelper.productReservationDialog(
//						getSupportFragmentManager(), mBusiness, null);
//			}
//		});
//		holder.promotionInfoScrollView.setVisibility(View.GONE);
//		holder.promotionInfoLayout.setVisibility(View.INVISIBLE);
//		holder.promotionRibbonFrame.setVisibility(View.INVISIBLE);
//		holder.promotionPricesView.setVisibility(View.INVISIBLE);
//		holder.txtAddress.setText(StringFormatUtils.getBusinessFormattedAddress(Prefs.getAppLanguage(), mBusiness));


//		((ImageView)businessInfoView.findViewById(R.id.imgSaleImage)).txtCallStore.setOnClickListener(new OnClickListener() {
//			public void onClick(View v) {
//				ActivityStarter.dialer(ViewBusinessActivity.this, mBusiness.Phone);
//			}
//		});

//		holder.txtOpenHour.setOnClickListener(new OnClickListener() {
//			public void onClick(View v) {
//				openHoursDialog();
//			}
//		});
//
//		holder.txtNavigate.setOnClickListener(new OnClickListener() {
//			public void onClick(View v) {
//				ActivityStarter.openWaze(ViewBusinessActivity.this, holder.txtAddress.getText().toString());
//			}
//		});


//		if (mBusiness.EnableReservations == 0) {
//			holder.btnReserveItem.setVisibility(View.INVISIBLE);
//		} else {
//			holder.btnReserveItem.setVisibility(View.VISIBLE);
//		}
//
        container.addView(businessInfoView);

    }

    private View getViewForProducts(final Product product) {
        View v = mInflater.inflate(R.layout.item_inner_product, null);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT, 1f); // set weight
        params.gravity = Gravity.CENTER; // set gravity
        v.setLayoutParams(params);
        v.setPadding(2,2,2,2);


//        v.setPadding(5, 15, 5, 15); // set padding
        /*set data in view*/
        ImageLoader.loadProductImage(this, product.Image, ((ImageView) v.findViewById(R.id.prodImage))); // load product image
        ((TextView) v.findViewById(R.id.prod_text)).setText(product.getTitle());

        ArrayList<PriceLabel> pl= product.getPrices();

        ArrayList<TextView> valueTextViews1 = new ArrayList<>();
        valueTextViews1.add((TextView) v.findViewById(R.id.price_a));
        valueTextViews1.add((TextView) v.findViewById(R.id.price_b));
        valueTextViews1.add((TextView) v.findViewById(R.id.price_c));
        valueTextViews1.add((TextView) v.findViewById(R.id.price_d));
        valueTextViews1.add((TextView) v.findViewById(R.id.price_e));

        ArrayList<TextView> labelTextViews2 = new ArrayList<>();
        labelTextViews2.add((TextView) v.findViewById(R.id.price_lab1));
        labelTextViews2.add((TextView) v.findViewById(R.id.price_lab2));
        labelTextViews2.add((TextView) v.findViewById(R.id.price_lab3));
        labelTextViews2.add((TextView) v.findViewById(R.id.price_lab4));
        labelTextViews2.add((TextView) v.findViewById(R.id.price_lab5));

        if (pl!=null){
            for (int i=0;i<pl.size();i++){
                valueTextViews1.get(i).setText(pl.get(i).getValue());
                labelTextViews2.get(i).setText(pl.get(i).getLabel());
            }
        }
//            if (pl.size()==5) {
//
//                ((TextView) v.findViewById(R.id.price_a)).setText(pl.get(0).getValue());
//                ((TextView) v.findViewById(R.id.price_b)).setText(pl.get(1).getValue());
//                ((TextView) v.findViewById(R.id.price_c)).setText(pl.get(2).getValue());
//                ((TextView) v.findViewById(R.id.price_d)).setText(pl.get(3).getValue());
//                ((TextView) v.findViewById(R.id.price_e)).setText(pl.get(4).getValue());
//
//                ((TextView) v.findViewById(R.id.price_lab1)).setText(pl.get(0).getLabel());
//                ((TextView) v.findViewById(R.id.price_lab2)).setText(pl.get(1).getLabel());
//                ((TextView) v.findViewById(R.id.price_lab3)).setText(pl.get(2).getLabel());
//                ((TextView) v.findViewById(R.id.price_lab4)).setText(pl.get(3).getLabel());
//                ((TextView) v.findViewById(R.id.price_lab5)).setText(pl.get(4).getLabel());
//            }

        for (int j = 0; j < product.ProductItems.size(); j++) {
            System.out.println("ProductItems - > " + product.ProductItems);
            Param1 += product.ProductItems.get(j).Param1 + ",";
            Param2 += product.ProductItems.get(j).Param2 + ",";
            Param3 += product.ProductItems.get(j).Param3 + ",";
            Param4 += product.ProductItems.get(j).Param4 + ",";
            Param5 += product.ProductItems.get(j).Param5 + ",";
        }

        if(Param1.matches(",")){Param1 = "";}
        if(Param2.matches(",")){Param2 = "";}
        if(Param3.matches(",")){Param3 = "";}
        if(Param4.matches(",")){Param4 = "";}
        if(Param5.matches(",")){Param5 = "";}

        for (String key : SettingManager.getSetting().params.keySet()) {
            if (key.matches("Param1")) {
                ((TextView) v.findViewById(R.id.param1)).setText(SettingManager.getSetting().params.get(key));
                if (!Param1.isEmpty()) {
                    Param1 = checkEndingString(Param1);
                    ((TextView) v.findViewById(R.id.param1)).setVisibility(View.VISIBLE);
                    try {
                        ((TextView) v.findViewById(R.id.percentage_a)).setText(Param1.substring(0,Param1.indexOf(",")));
                    } catch (Exception e) {
                        e.printStackTrace();
                        ((TextView) v.findViewById(R.id.percentage_a)).setText(Param1);

                    }
                    ((TextView) v.findViewById(R.id.percentage_a)).setVisibility(View.VISIBLE);
                }
            }
            if (key.matches("Param2")) {
                ((TextView) v.findViewById(R.id.param2)).setText(SettingManager.getSetting().params.get(key));
                if (!Param2.isEmpty()) {
                    Param2 = checkEndingString(Param2);
                    ((TextView) v.findViewById(R.id.param2)).setVisibility(View.VISIBLE);
                    try {
                        ((TextView) v.findViewById(R.id.percentage_b)).setText(Param2.substring(0,Param2.indexOf(",")));
                    } catch (Exception e) {
                        e.printStackTrace();
                        ((TextView) v.findViewById(R.id.percentage_b)).setText(Param2);

                    }
                    ((TextView) v.findViewById(R.id.percentage_b)).setVisibility(View.VISIBLE);
                }
            }
            if (key.matches("Param3")) {
                ((TextView) v.findViewById(R.id.param3)).setText(SettingManager.getSetting().params.get(key));
                if (!Param3.isEmpty()) {
                    Param3 = checkEndingString(Param3);
                    ((TextView) v.findViewById(R.id.param3)).setVisibility(View.VISIBLE);
                    try {
                        ((TextView) v.findViewById(R.id.percentage_c)).setText(Param3.substring(0,Param3.indexOf(",")));
                    } catch (Exception e) {
                        e.printStackTrace();
                        ((TextView) v.findViewById(R.id.percentage_c)).setText(Param3);

                    }
                    ((TextView) v.findViewById(R.id.percentage_c)).setVisibility(View.VISIBLE);
                }
            }
            if (key.matches("Param4")) {
                ((TextView) v.findViewById(R.id.param4)).setText(SettingManager.getSetting().params.get(key));
                if (!Param4.isEmpty()) {
                    Param4 = checkEndingString(Param4);
                    ((TextView) v.findViewById(R.id.param4)).setVisibility(View.VISIBLE);
                    ((TextView) v.findViewById(R.id.percentage_d)).setText(Param4);
                    ((TextView) v.findViewById(R.id.percentage_d)).setVisibility(View.VISIBLE);
                }
            }
            if (key.matches("Param5")) {
                ((TextView) v.findViewById(R.id.param5)).setText(SettingManager.getSetting().params.get(key));
                if (!Param5.isEmpty()) {
                    Param5 = checkEndingString(Param5);
                    ((TextView) v.findViewById(R.id.param5)).setVisibility(View.VISIBLE);
                    ((TextView) v.findViewById(R.id.percentage_e)).setText(Param5);
                    ((TextView) v.findViewById(R.id.percentage_e)).setVisibility(View.VISIBLE);
                }
            }
        }

        ((ImageView) v.findViewById(R.id.prodImage)).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogHelper.productDescriptionDialog(getSupportFragmentManager(), mBusiness, product);
            }
        });
        return v;
    }

  /*  private View getViewForProducts(final Product product, ViewGroup container) {


        View productView = mInflater.inflate(R.layout.item_list_sale_page_, container, false);
        final PromotionViewHolder holder = new PromotionViewHolder(productView);
        holder.imgLogo.setVisibility(View.GONE);
        holder.promotionRibbonFrame.setVisibility(View.GONE);
        holder.txtAddress.setVisibility(View.GONE);
        holder.txtDistanceFromUser.setVisibility(View.GONE);
        holder.promotionInfoLayout.setVisibility(View.GONE);
        holder.llayoutContact.setVisibility(View.GONE);
        holder.upperLayout.setVisibility(View.GONE);
        holder.btnReserveItem.setVisibility(View.GONE);
        holder.description.setVisibility(View.VISIBLE);

        holder.txtTitle.setText(product.Title);
        holder.txtDesc.setText(product.Description);
        holder.description.setText(product.Description);
        holder.txtPrice.setText(StringFormatUtils.getFormattedPrice(this, product.Price));
        holder.txtAddress.setText(StringFormatUtils.getBusinessFormattedAddress(Prefs.getAppLanguage(), mBusiness));

        ImageLoader.loadBusinessLogo(this, mBusiness.Logo, holder.imgLogo);
        ImageLoader.loadProductImage(this, product.Image, holder.imgSaleImage);

        holder.txtShare.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                ActivityStarter.share(ViewBusinessActivity.this, mBusiness.Name);
            }
        });

        holder.txtCallStore.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                ActivityStarter.dialer(ViewBusinessActivity.this, mBusiness.Phone);
            }
        });

        holder.txtNavigate.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                ActivityStarter.openWaze(ViewBusinessActivity.this, holder.txtAddress.getText().toString());
            }
        });

        holder.txtReport.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                ActivityStarter.sendEmail(ViewBusinessActivity.this, "", "", "");
            }
        });


        return productView;
    }*/

    private View getViewForPromotion(final Promotion promotion, ViewGroup container) {

        View promotionView = mInflater.inflate(R.layout.item_inner_promotion, container, false);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.WRAP_CONTENT, 1f); // set weight
        params.gravity = Gravity.CENTER; // set gravity
        promotionView.setLayoutParams(params);
        promotionView.setPadding(5, 0, 5, 15); // set padding

        ((FrameLayout) promotionView.findViewById(R.id.badge)).setTranslationY(dp2px(5) * -1); // fixed badge size
        ((TextView) promotionView.findViewById(R.id.txtDiscount))
                .setText(StringFormatUtils.getFormattedDiscount(this, promotion.ProductPrice, promotion.DiscountPrice)); // set price in percentage
        ((TextView) promotionView.findViewById(R.id.hearts)).setVisibility(View.GONE); // for now
        ImageLoader.loadProductImage(this, promotion.Image, ((ImageView) promotionView.findViewById(R.id.promo_img))); // set promotion image
        setPromotionCountdown(promotion, ((Button) promotionView.findViewById(R.id.btnReserveItem)), ((TextView) promotionView.findViewById(R.id.timeLeft)));
        ((TextView) promotionView.findViewById(R.id.promoTitle)).setText(Prefs.getAppLanguage() == "en" ? promotion.TitleEN : promotion.TitleHE);
        ((TextView) promotionView.findViewById(R.id.txtOldPrice)).setText(StringFormatUtils.getFormattedPrice(this, promotion.ProductPrice));
        ((TextView) promotionView.findViewById(R.id.txtNewPrice)).setText(StringFormatUtils.getFormattedPrice(this, promotion.DiscountPrice));

        ((Button) promotionView.findViewById(R.id.btnReserveItem)).setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (Prefs.isConnected()) {
                    DialogHelper.promotionReservationDialog(getSupportFragmentManager(), mBusiness, promotion);
                } else {
                    displayNotConnectedDialog();
                }

            }
        });

/*        ((ImageView) promotionView.findViewById(R.id.promo_img)).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogHelper.promotionDescriptionDialog(getSupportFragmentManager(), mBusiness, promotion);
            }
        });*/
      /*  for (String key : SettingManager.getSetting().params.keySet()) {
            if (key.matches("Param1")) {
                holder.param1.setText(SettingManager.getSetting().params.get(key));
                holder.param1.setVisibility(View.VISIBLE);
                if (!Param1.isEmpty()) {
                    Param1 = checkEndingString(Param1);
                    holder.param1Txt.setText(Param1);
                    holder.param1Txt.setVisibility(View.VISIBLE);
                }
            }
            if (key.matches("Param2")) {
                holder.param2.setText(SettingManager.getSetting().params.get(key));
                holder.param2.setVisibility(View.VISIBLE);
                if (!Param2.isEmpty()) {
                    Param2 = checkEndingString(Param2);
                    holder.param2Txt.setText(Param2);
                    holder.param2Txt.setVisibility(View.VISIBLE);
                }
            }
            if (key.matches("Param3")) {
                holder.param3.setText(SettingManager.getSetting().params.get(key));
                holder.param3.setVisibility(View.VISIBLE);
                if (!Param3.isEmpty()) {
                    Param3 = checkEndingString(Param3);
                    holder.param3Txt.setText(Param3);
                    holder.param3Txt.setVisibility(View.VISIBLE);
                }
            }
            if (key.matches("Param4")) {
                holder.param4.setText(SettingManager.getSetting().params.get(key));
                holder.param4.setVisibility(View.VISIBLE);
                if (!Param4.isEmpty()) {
                    Param4 = checkEndingString(Param4);
                    holder.param4Txt.setText(Param4);
                    holder.param4Txt.setVisibility(View.VISIBLE);
                }
            }
            if (key.matches("Param5")) {
                holder.param5.setText(SettingManager.getSetting().params.get(key));
                holder.param5.setVisibility(View.VISIBLE);
                if (!Param5.isEmpty()) {
                    Param5 = checkEndingString(Param5);
                    holder.param5Txt.setText(Param5);
                    holder.param5Txt.setVisibility(View.VISIBLE);
                }
            }
        }


        holder.txtTitle.setText(promotion.Title);
        holder.txtDesc.setText(promotion.Description);
        holder.txtDiscount.setText(StringFormatUtils.getFormattedDiscount(this,
                promotion.ProductPrice,
                promotion.DiscountPrice));
        holder.txtOldPrice.setText(StringFormatUtils.getFormattedPrice(this, promotion.ProductPrice));
        holder.txtPrice.setText(StringFormatUtils.getFormattedPrice(this, promotion.DiscountPrice));
        holder.txtAddress.setText(StringFormatUtils.getBusinessFormattedAddress(Prefs.getAppLanguage(), mBusiness));
        holder.txtItemsLeft.setText(App.applicationContext.getResources().getString(R.string.number_of_item_in_storage) + " " + promotion.NumUnits);

        ImageLoader.loadBusinessLogo(this, mBusiness.Logo, holder.imgLogo);
        ImageLoader.loadProductImage(this, promotion.Image, holder.imgSaleImage);

        holder.txtShare.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                ActivityStarter.share(ViewBusinessActivity.this, mBusiness.Name);
            }
        });

        holder.txtCallStore.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                ActivityStarter.dialer(ViewBusinessActivity.this, mBusiness.Phone);
            }
        });

        holder.txtNavigate.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                ActivityStarter.openWaze(ViewBusinessActivity.this, holder.txtAddress.getText().toString());
            }
        });

        holder.txtReport.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                ActivityStarter.sendEmail(ViewBusinessActivity.this, "", "", "");
            }
        });

        holder.txtOpenHour.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                openHoursDialog();
            }
        });

        holder.btnReserveItem.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (Prefs.isConnected()) {
                    DialogHelper.promotionReservationDialog(getSupportFragmentManager(), mBusiness, promotion);
                } else {
                    displayNotConnectedDialog();
                }

            }
        });
        coordinates = new Coordinates(mBusiness.Lat, mBusiness.Lng);
        double distance = GeneralHelper.distance(coordinates.lat, coordinates.lon, Double.parseDouble(UserManager.getAppUser().Lat), Double.parseDouble(UserManager.getAppUser().Lng));
        String distanceInString = String.format("%.1f", distance);
        holder.txtDistanceFromUser.setText(App.applicationContext.getResources().getString(R.string.distance_from_you)
                + " " + distanceInString + " " +
                App.applicationContext.getResources().getString(R.string.km));

        if (mBusiness.EnableReservations == 0) {
            holder.btnReserveItem.setVisibility(View.INVISIBLE);
        } else {
            holder.btnReserveItem.setVisibility(View.VISIBLE);
        }*/

        return promotionView;
    }

    private void getBusinessInfoAndLoadBusiness() {
        long businessId = getIntent().getLongExtra(EXTRA_BUSINESS_ID, -1);
        catId = getIntent().getLongExtra(EXTRA_CATEGORY_ID, -1);
//		Log.d("CAT", "catId from Extra" + catId);
        Log.d("CAT", "businessId from Extra" + businessId);

        loadBusinessById(businessId);
//        if (mBusiness == null) {
//            return;
//        }


//
//        if (catId == 0) {
//
//            populateUIwithAll();
//        } else {
//
//        }


    }

    private void findViews() {
        mToolbar = (Toolbar) findViewById(R.id.actionbar);
        container = (ViewGroup) findViewById(R.id.containerer);
        mPromotionsContainer = (ViewGroup) findViewById(R.id.promotionsContainer);
        mProductContainer = (ViewGroup) findViewById(R.id.productContainer);
        txtTitle = (TextView) findViewById(R.id.txtBusinessTitle);
        linear_scroll_container = (LinearLayout) findViewById(R.id.linear_scroll_container);
        header_hot_deals = (TextView) findViewById(R.id.header_hot_deals);
        header_menu = (TextView) findViewById(R.id.header_menu);
        mainScrollView = (ScrollView) findViewById(R.id.mainScrollView);

    }

    private void initActionbar() {
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
    }

    private void handleTabClicks(){
        BotTabBar botTabBar = (BotTabBar) findViewById(R.id.BotTabBar);
        botTabBar.setTabListener(new BotTabBar.onTabSelected() {
            @Override
            public void onSelected(int d) {
                switch (d) {
                    case 1:
                        mainScrollView.scrollTo(0, 0);
                        break;
                    case 2:
                        mainScrollView.scrollTo(0, mPromotionsContainer.getTop() - (int) dp2px(60));
                        break;
                    case 3:
                        mainScrollView.scrollTo(0, mProductContainer.getTop() - (int)dp2px(60));
                        break;
                    case 4:
                        if (mBusiness != null) {

                            try {
                                ActivityStarter.dialer(ViewBusinessActivity.this, mBusiness.Phone);
                            } catch (Exception e) {
                                e.printStackTrace();
                                Toast.makeText(ViewBusinessActivity.this,"error while open caller",Toast.LENGTH_SHORT).show();
                            }
                            break;
                        }
                        break;
                    case 5:
                        Uri uri = Uri.parse("geo:0,0?q=" + StringFormatUtils.getBusinessFormattedAddress(Prefs.getAppLanguage(), mBusiness).replace(" ", "+"));
                        showMap(uri, ViewBusinessActivity.this);
                }
            }
        });
    }

    /*Uri uri = Uri.parse("geo:0,0?q=" + location.replace(" ", "+"));
				showMap(uri);
				*/
    public void showMap(Uri geoLocation, Activity activity) {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(geoLocation);
        if (intent.resolveActivity(activity.getPackageManager()) != null) {
            activity.startActivity(intent);
        }
    }

//	@Override
//	public void onFinishedLoadingBusiness(ArrayList<Business> businesses) {
//		mBusiness = businesses.get(0);
//		txtTitle.setText(mBusiness.Name);
//		populateUIWithPromotions();
//		populateUIwithProducts();
//	}
//
//	@Override
//	public void onNoMoreBusinessResults() {
//
//	}

//	@Override
//	public void onProgressLoadingBusinesses() {
//
//	}
//
//	@Override
//	public void onErrorLoadingBusinesses(Throwable throwable) {
//
//	}

    private static class PromotionViewHolder {
        public TextView txtDiscount;
        public TextView txtTitle;
        public TextView txtHeartsCounter;
        public TextView txtDistanceFromUser;
        public TextView txtAddress;
        public TextView txtDesc;
        public TextView txtAvailableColors;
        public TextView txtAvailableSizes;
        public TextView txtPrice;
        public TextView txtOldPrice;
        public TextView txtItemsLeft;
        public TextView txtCallStore;
        public TextView txtNavigate;
        public TextView txtShare;
        public TextView txtReport;
        public ImageView imgSaleImage;
        public ImageView imgHeartIcon;
        public ImageView imgLogo;
        public Button btnReserveItem;

        public View promotionInfoLayout;
        public View promotionRibbonFrame;
        public View promotionPricesView;
        private LinearLayout llayoutContact;
        private LinearLayout upperLayout;
        private TextView description;
        private TextView param1;
        private TextView param2;
        private TextView param3;
        private TextView param4;
        private TextView param5;
        private TextView param1Txt;
        private TextView param2Txt;
        private TextView param3Txt;
        private TextView param4Txt;
        private TextView param5Txt;
        private TextView txtOpenHour;
        private ScrollView promotionInfoScrollView;


        public PromotionViewHolder(View convertView) {
            txtDiscount = (TextView) convertView.findViewById(R.id.txtDiscount);
            txtTitle = (TextView) convertView.findViewById(R.id.txtTitle);
            txtHeartsCounter = (TextView) convertView.findViewById(R.id.txtHeartsCount);
            txtDistanceFromUser = (TextView) convertView.findViewById(R.id.txtDistance);
            txtAddress = (TextView) convertView.findViewById(R.id.txtAddress);
            txtDesc = (TextView) convertView.findViewById(R.id.txtDesc);
            txtAvailableColors = (TextView) convertView.findViewById(R.id.txtAvailableColors);
            txtAvailableSizes = (TextView) convertView.findViewById(R.id.txtAvailableSizes);
            txtPrice = (TextView) convertView.findViewById(R.id.txtPrice);
            txtOldPrice = (TextView) convertView.findViewById(R.id.txtOldPrice);
            txtItemsLeft = (TextView) convertView.findViewById(R.id.txtItemsLeft);
            txtCallStore = (TextView) convertView.findViewById(R.id.txtCallStore);
            txtNavigate = (TextView) convertView.findViewById(R.id.txtNavigate);
            txtShare = (TextView) convertView.findViewById(R.id.txtShare);
            txtOpenHour = (TextView) convertView.findViewById(R.id.txtOpenHour);
            txtReport = (TextView) convertView.findViewById(R.id.txtReport);
            imgSaleImage = (ImageView) convertView.findViewById(R.id.imgProduct);
            imgHeartIcon = (ImageView) convertView.findViewById(R.id.imgHeartIcon);
            imgLogo = (ImageView) convertView.findViewById(R.id.imgLogo);
            btnReserveItem = (Button) convertView.findViewById(R.id.btnReserveItem);
            llayoutContact = (LinearLayout) convertView.findViewById(R.id.llayoutContact);
            upperLayout = (LinearLayout) convertView.findViewById(R.id.upperLayout);
            description = (TextView) convertView.findViewById(R.id.description);
            promotionInfoScrollView = (ScrollView) convertView.findViewById(R.id.promotionInfoScrollView);

            param1 = (TextView) convertView.findViewById(R.id.param1);
            param2 = (TextView) convertView.findViewById(R.id.param2);
            param3 = (TextView) convertView.findViewById(R.id.param3);
            param4 = (TextView) convertView.findViewById(R.id.param4);
            param5 = (TextView) convertView.findViewById(R.id.param5);
            param1Txt = (TextView) convertView.findViewById(R.id.param1Txt);
            param2Txt = (TextView) convertView.findViewById(R.id.param2Txt);
            param3Txt = (TextView) convertView.findViewById(R.id.param3Txt);
            param4Txt = (TextView) convertView.findViewById(R.id.param4Txt);
            param5Txt = (TextView) convertView.findViewById(R.id.param5Txt);


            txtOldPrice.setPaintFlags(txtOldPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

            promotionInfoLayout = convertView.findViewById(R.id.promotionInfoLayout);
            promotionPricesView = convertView.findViewById(R.id.promotionPricesLayout);
            promotionRibbonFrame = convertView.findViewById(R.id.promotionRibbonFrame);

        }
    }

    private void openHoursDialog() {

        final Dialog dialog = new Dialog(this, R.style.myBackgroundStyle);
        final View view = this.getLayoutInflater().inflate(R.layout.dialog_opening_hours, null);

        TextView midDays = (TextView) view.findViewById(R.id.midDays);
        TextView friDays = (TextView) view.findViewById(R.id.friDays);
        TextView satDays = (TextView) view.findViewById(R.id.satDays);
        Button closeDialog = (Button) view.findViewById(R.id.closeDialog);


        closeDialog.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        StringBuilder midTime = new StringBuilder();
        StringBuilder friTime = new StringBuilder();
        StringBuilder satTime = new StringBuilder();

        midTime.append(getString(R.string.sun_thur));
        midTime.append(": ");
        if (mBusiness.OpeningTime.matches("00:00:00") && mBusiness.ClosingTime.matches("00:00:00")) {
            midTime.append(getString(R.string.close));
        } else {
            midTime.append(mBusiness.OpeningTime.substring(0,4));
            midTime.append(" - ");
            midTime.append(mBusiness.ClosingTime);
        }

        friTime.append(getString(R.string.fri));
        friTime.append(": ");
        if (mBusiness.OpeningTimeFriday.matches("00:00:00") && mBusiness.ClosingTimeFriday.matches("00:00:00")) {
            friTime.append(getString(R.string.close));
        } else {
            friTime.append(mBusiness.OpeningTimeFriday);
            friTime.append(" - ");
            friTime.append(mBusiness.ClosingTimeFriday);
        }


        satTime.append(getString(R.string.sat));
        satTime.append(": ");
        if (mBusiness.OpeningTimeSaturday.matches("00:00:00") && mBusiness.ClosingTimeSaturday.matches("00:00:00")) {
            satTime.append(getString(R.string.close));
        } else {
            satTime.append(mBusiness.OpeningTimeSaturday);
            satTime.append(" - ");
            satTime.append(mBusiness.ClosingTimeSaturday);
        }

        midDays.setText(midTime.toString());
        friDays.setText(friTime.toString());
        satDays.setText(satTime.toString());

        dialog.setContentView(view);
        dialog.show();
    }

    public boolean isStoreNowOpen() {
        Calendar cal = Calendar.getInstance();
        int dayOfMonth = cal.get(Calendar.DAY_OF_WEEK);
        int nowHour = cal.get(Calendar.HOUR_OF_DAY);
        int minute = cal.get(Calendar.MINUTE);
        if (dayOfMonth >= 1 && dayOfMonth <= 5) {
            if (mBusiness.OpeningTime.matches("00:00:00") && mBusiness.ClosingTime.matches("00:00:00")) {return false;}
            String[] open = mBusiness.OpeningTime.split(":");
            String[] close = mBusiness.ClosingTime.split(":");
            int ho = Integer.parseInt(open[0]);
            int mo = Integer.parseInt(open[1]); // hours and minutes opening hours
            int hc = Integer.parseInt(close[0]);
            int mc = Integer.parseInt(close[1]); // hours and minutes closing hours
            if (hc == 0) {
                hc = 24;
            }
            if (mc == 0) {
                mc = 60;
            } // handling zero in closig hours
            if ((nowHour > ho && nowHour < hc)) {
                return true;
            } else if ((nowHour == ho) && (minute >= mo)) {
                return true;
            } else if ((nowHour == hc) && (minute < mc)) {
                return true;
            }
            System.out.println("@@ho/hc " + ho + "/" + hc);
            System.out.println("@@mo/mc " + mo + "/" + mc);
            System.out.println("@@betHour " + (nowHour >= ho && nowHour < hc));
            System.out.println("@@betMinute " + (minute >= mo && minute < mc));
        } else if (dayOfMonth == 6) {
            if (mBusiness.OpeningTimeFriday.matches("00:00:00") && mBusiness.ClosingTimeFriday.matches("00:00:00")) {return false;}
            String[] open = mBusiness.OpeningTimeFriday.split(":");
            String[] close = mBusiness.ClosingTimeFriday.split(":");
            int ho = Integer.parseInt(open[0]);
            int mo = Integer.parseInt(open[1]); // hours and minutes opening hours
            int hc = Integer.parseInt(close[0]);
            int mc = Integer.parseInt(close[1]); // hours and minutes closing hours
            if (hc == 0) {
                hc = 24;
            }
            if (mc == 0) {
                mc = 60;
            } // handling zero in closig hours
            if ((nowHour > ho && nowHour < hc)) {
                return true;
            } else if ((nowHour == ho) && (minute >= mo)) {
                return true;
            } else if ((nowHour == hc) && (minute < mc)) {
                return true;
            }
            System.out.println("@@ho/hc " + ho + "/" + hc);
            System.out.println("@@mo/mc " + mo + "/" + mc);
            System.out.println("@@betHour " + (nowHour >= ho && nowHour < hc));
            System.out.println("@@betMinute " + (minute >= mo && minute < mc));
        } else if (dayOfMonth == 7) {
            if (mBusiness.OpeningTimeSaturday.matches("00:00:00") && mBusiness.ClosingTimeSaturday.matches("00:00:00")) {return false;}
            String[] open = mBusiness.OpeningTimeSaturday.split(":");
            String[] close = mBusiness.ClosingTimeSaturday.split(":");
            int ho = Integer.parseInt(open[0]);
            int mo = Integer.parseInt(open[1]); // hours and minutes opening hours
            int hc = Integer.parseInt(close[0]);
            int mc = Integer.parseInt(close[1]); // hours and minutes closing hours
            if (hc == 0) {
                hc = 24;
            }
            if (mc == 0) {
                mc = 60;
            } // handling zero in closig hours
            if ((nowHour > ho && nowHour < hc)) {
                return true;
            } else if ((nowHour == ho) && (minute >= mo)) {
                return true;
            } else if ((nowHour == hc) && (minute < mc)) {
                return true;
            }
            System.out.println("@@ho/hc " + ho + "/" + hc);
            System.out.println("@@mo/mc " + mo + "/" + mc);
            System.out.println("@@betHour " + (nowHour >= ho && nowHour < hc));
            System.out.println("@@betMinute " + (minute >= mo && minute < mc));
        }
        return false;
    }

    private String getOpeningHours() {

        StringBuilder midTime = new StringBuilder();
        StringBuilder friTime = new StringBuilder();
        StringBuilder satTime = new StringBuilder();

        midTime.append(getString(R.string.sun_thur));
        midTime.append(": ");
        if (mBusiness.OpeningTime.matches("00:00:00") && mBusiness.ClosingTime.matches("00:00:00")) {
            midTime.append(getString(R.string.close));
        } else {
            midTime.append(mBusiness.OpeningTime);
            midTime.append(" - ");
            midTime.append(mBusiness.ClosingTime);
        }

        friTime.append(getString(R.string.fri));
        friTime.append(": ");
        if (mBusiness.OpeningTimeFriday.matches("00:00:00") && mBusiness.ClosingTimeFriday.matches("00:00:00")) {
            friTime.append(getString(R.string.close));
        } else {
            friTime.append(mBusiness.OpeningTimeFriday);
            friTime.append(" - ");
            friTime.append(mBusiness.ClosingTimeFriday);
        }


        satTime.append(getString(R.string.sat));
        satTime.append(": ");
        if (mBusiness.OpeningTimeSaturday.matches("00:00:00") && mBusiness.ClosingTimeSaturday.matches("00:00:00")) {
            satTime.append(getString(R.string.close));
        } else {
            satTime.append(mBusiness.OpeningTimeSaturday);
            satTime.append(" - ");
            satTime.append(mBusiness.ClosingTimeSaturday);
        }

        return midTime.toString() + "\n" + friTime.toString() + "\n" + satTime.toString();
    }

    private void displayNotConnectedDialog() {
        DialogHelper.messageDialog(getSupportFragmentManager(), getString(R.string.location_dialog_title),
                getString(R.string.login_to_system), new DialogListener() {

                    @Override
                    public void onOk(Dialog d) {
                        DialogHelper.loginDialog(getSupportFragmentManager(), MainActivity.appuserListener);
                        d.dismiss();
                    }

                    @Override
                    public void onCancel(Dialog d) {
                        d.dismiss();
                    }
                });
    }

    private String checkEndingString(String param) {
        System.out.println("ProductDescriptionDialog: param " + param.charAt(param.length() - 1));
        System.out.println("ProductDescriptionDialog: param " + param.charAt(param.length() - 2));
        if (param.length() > 0 && param.charAt(param.length() - 2) == ',') {
            param = param.substring(0, param.length() - 2);
            System.out.println("ProductDescriptionDialog: param " + param);
            return param;
        }
        return param;
    }


    public void loadBusinessById(final long id) {
        APIManager.api.getBusiness(id, new Callback<BusinessGet>() {
            @Override
            public void success(BusinessGet result, Response response) {
                if (result.data == null) {
                    Log.d("RUN", "data == null");
                    return;
                }
                ((FrameLayout) findViewById(R.id.progressBar)).setVisibility(View.GONE);
                linear_scroll_container.animate().alpha(1);
                mBusiness = result.data.Business;
                txtTitle.setText(mBusiness.Name);
                handleTabClicks();
                populateUIwithProductsAndPromotions();



            }

            @Override
            public void failure(RetrofitError retrofitError) {
                Log.d("RUN", "failure " + retrofitError.toString());
            }
        });
        ;
    }

    private void setPromotionCountdown(final Promotion promotion, final Button btnReserveItem, final TextView counter) {
        CountDownTimer promotionCountDown = new CountDownTimer(promotion.millisLeft(), 1000) {
            public void onTick(long millisUntilFinished) {

                if (promotion.millisLeft() == -1) {
                    counter.setText(getString(R.string.promotion_soon_will_be_up));
                    btnReserveItem.setVisibility(View.INVISIBLE);
                } else {
                    counter.setText(StringFormatUtils.getPromotionEndsIn(ViewBusinessActivity.this, millisUntilFinished));
                }
            }

            public void onFinish() {
            }
        };

        if (promotion.millisLeft() == -1) {
            counter.setText(ViewBusinessActivity.this.getResources().getString(R.string.promotion_soon_will_be_up));
            btnReserveItem.setVisibility(View.VISIBLE); // for now
        } else {
            promotionCountDown.start();
        }
    }

    public static float dp2px(float dp) {
        Resources resources = App.applicationContext.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float px = dp * (metrics.densityDpi / 160f);
        return px;
    }

    public void slideDown(View v){

        Animation a = AnimationUtils.loadAnimation(this, R.anim.slide_down);

        if(a != null){
            a.reset();
            if(v != null){
                v.clearAnimation();
                v.startAnimation(a);
            }
        }
    }

    public void slideUp(View v){

        Animation a = AnimationUtils.loadAnimation(this, R.anim.slide_up);
        if(a != null){
            a.reset();
            if(v != null){
                v.clearAnimation();
                v.startAnimation(a);
            }
        }
    }

    private View lastShownView;
    public void toggleContents(View lowerLayout){
        if(lowerLayout.isShown()){
            slideUp(lowerLayout);
            lowerLayout.setVisibility(View.GONE);
        }
        else{
            if(lastShownView != null) {lastShownView.setVisibility(View.GONE);}
            slideDown(lowerLayout);
            lowerLayout.setVisibility(View.VISIBLE);
            lastShownView = lowerLayout;
        }
    }

}
