/**
 * 
 */
package com.positiveapps.getFashion.backend;

import com.positiveapps.getFashion.App;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * @author Ilia Kaplounov
 *
 */
public class Prefs {
	
	public static final String LANGUAGE_HEB = "he";
	public static final String LANGUAGE_ENG = "en";
	public static final String LANGUAGE_DEFAULT = LANGUAGE_HEB;
	public static final String GCM_KEY = App.applicationContext + "GcmKey";
	private static SharedPreferences mPrefs = null;

//	private static final String GCM = "gcm";
//	private static final String USER_ID = "user_id";
//	private static final String USER_PHONE = "user_phone";
//	private static final String FACEBOOK_ID = "facebook_id";
//	private static final String FACEBOOK_IMAGE = "facebook_image";
//	private static final String FACEBOOK_NAME = "facebook_name";
//	private static final String FACEBOOK_USERNAME = "user_name";
//	private static final String FACEBOOK_ACCESSTOKEN = "facebook_AccessToken";
//	private static final String RESTAURANT_RESERVED_SET = "rest_reserved";
//	private static final String LAST_DISH_SAVE_TIME = "cooldown_last_time";
	
//	private static final String SEARCH_HISTORY = "search_history";
	
	private static final String APP_LANGUAGE = "app_locale";
	private static final String IS_MANAGER = "isManager";
	private static final String IS_CONNECTED = "isConnected";
	public static final String FROM_NOTIFICATION = "isFromNotification";
	public static final String FROM_NOTIFICATION_TYPE = "isFromNotificationType";
	private static final String FROM_NOTIFICATION_MESSAGE = "notificationMessage";
	private static final String NOTIFICATION_BUSINESS_ID = "notificationBusinessId";
	private static final String MENU_FILTER_BUTTONS = "MenuFilterButton";
	private static final String SPLASH_LOADED = "splashLoaded";
	private static final String SPLASH_URI = "splashUri";
	private static final String LAST_NUMBER = "lastNumber";

	public static void initPreference(Context context){
		mPrefs = PreferenceManager.getDefaultSharedPreferences(context);
	}
	
	public static String getAppLanguage(){
		return mPrefs.getString(APP_LANGUAGE, LANGUAGE_DEFAULT);
	}
	
	public static void setAppLanguage(String langauge){
		mPrefs.edit().putString(APP_LANGUAGE, langauge).commit();
	}
	
	public static boolean isConnected(){
		return mPrefs.getBoolean(IS_CONNECTED, false);
	}
	
	public static void setIsConnected(boolean isConnected){
		mPrefs.edit().putBoolean(IS_CONNECTED, isConnected).commit();
	}
	
	public static boolean isManager(){
		return mPrefs.getBoolean(IS_MANAGER, false);
	}
	
	public static void setIsManager(boolean isManager){
		mPrefs.edit().putBoolean(IS_MANAGER, isManager).commit();
	}
	
	public static boolean needInit(){
		return mPrefs == null;
	}
	
	public static void setGCMKey(String gcmKey) {
		mPrefs.edit().putString(GCM_KEY, gcmKey).commit();
	}

	public static String getGCMKey () {
		return mPrefs.getString(GCM_KEY, "");
	}
	
	public static void setFromNotification(boolean isFromNotification) {
		mPrefs.edit().putBoolean(FROM_NOTIFICATION, isFromNotification).commit();
	}

	public static boolean getFromNotification () {
		return mPrefs.getBoolean(FROM_NOTIFICATION, false);
	}
	
	public static void setFromNotificationType(int isFromNotification) {
		mPrefs.edit().putInt(FROM_NOTIFICATION_TYPE, isFromNotification).commit();
	}

	public static int getFromNotificationType() {
		return mPrefs.getInt(FROM_NOTIFICATION_TYPE, 0);
	}
	
	public static void setNotificationMessage(String notificationMessage) {
		mPrefs.edit().putString(FROM_NOTIFICATION_MESSAGE, notificationMessage).commit();
	}

	public static String getNotificationMessage() {
		return mPrefs.getString(FROM_NOTIFICATION_MESSAGE, "");
	}
	
	public static void setNotificationBusinessId(long notificationBusinessId) {
		mPrefs.edit().putLong(NOTIFICATION_BUSINESS_ID, notificationBusinessId).commit();
	}

	public static long getNotificationBusinessId() {
		return mPrefs.getLong(NOTIFICATION_BUSINESS_ID, 0);
	}

	public static void setMenuFilterButtons(String buttonsCheckedString){
		mPrefs.edit().putString(MENU_FILTER_BUTTONS, buttonsCheckedString).commit() ;
	}
	public static String getMenuFilterButtons() {
		return mPrefs.getString(MENU_FILTER_BUTTONS, "0,0,0,0,0,1");
	}
//	public static  void setSplashLoaded(boolean isSplashLoaded){
//		mPrefs.edit().putBoolean(SPLASH_LOADED,isSplashLoaded).commit();
//
//	}
//	public static boolean getSplashLoaded() {
//		return mPrefs.getBoolean(SPLASH_LOADED, false);
//	}

		public static  void setSplashLoaded(String SplashLoaded){
		mPrefs.edit().putString(SPLASH_LOADED,SplashLoaded).commit();

	}
	public static String getSplashLoaded() {
		return mPrefs.getString(SPLASH_LOADED, "");
	}
	public static void setSplashUri(String path){
		mPrefs.edit().putString(SPLASH_URI,path).commit() ;

	}
	public static String getSplashUri() {
		return mPrefs.getString(SPLASH_URI, null);
	}

	public static void setLastNumber(String number){
		mPrefs.edit().putString(LAST_NUMBER,number).commit() ;

	}
	public static String getLastNumber() {
		return mPrefs.getString(LAST_NUMBER, null);
	}

}
