/**
 * 
 */
package com.positiveapps.getFashion.backend;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.positiveapps.getFashion.jsonmodels.Appuser;
import com.positiveapps.getFashion.jsonmodels.apiresponse.BusinessAsManager;
import com.positiveapps.getFashion.jsonmodels.apiresponse.BusinessGet;
import com.positiveapps.getFashion.jsonmodels.apiresponse.BusinessGetList;
import com.positiveapps.getFashion.jsonmodels.apiresponse.CategoryGetList;
import com.positiveapps.getFashion.jsonmodels.apiresponse.DataCheck;
import com.positiveapps.getFashion.jsonmodels.apiresponse.PromotionGetList;
import com.positiveapps.getFashion.jsonmodels.apiresponse.ReservationGetList;
import com.positiveapps.getFashion.jsonmodels.apiresponse.SettingGet;
import com.positiveapps.getFashion.jsonmodels.apiresponse.UserResponse;
import com.positiveapps.getFashion.util.DeviceUtil;
import com.positiveapps.getFashion.util.GsonHelper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RestAdapter.LogLevel;
import retrofit.converter.GsonConverter;
import retrofit.http.Field;
import retrofit.http.FieldMap;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Path;
import retrofit.http.Query;
import retrofit.http.QueryMap;

/** @author Ilia Kaplounov */
public class APIManager {

	public static String BASE_URL = "http://getweed.positive-apps.com/api/v1/" /*BuildConfig.FLAVOR.equals("getCoffeeShops")? "http://getweed.positive-apps.com/api/v1/": "http://getfashion.positive-apps.com/api/v1/"*/;
	public static final String DATE_FORMAT_PATTERN = "dd/MM/yyyy hh:mm";

	private static Context mApplicationContext;

	//@formatter:off
	private static Gson gson = new GsonBuilder().
			registerTypeAdapter(Boolean.class, new GsonHelper.BooleanAsIntAdapter()).
			registerTypeAdapter(boolean.class, new GsonHelper.BooleanAsIntAdapter()).
			registerTypeAdapter(Date.class, new GsonHelper.DateTypeAdapter(DATE_FORMAT_PATTERN)).
			create();

	
	public static API api = new RestAdapter.Builder().
			setEndpoint(BASE_URL).
			setLogLevel(LogLevel.FULL).
			setConverter(new GsonConverter(gson)).
			build().create(API.class);
	//@formatter:on
//	public static API restAdapter = new RestAdapter.Builder()
//			.setEndpoint(BASE_URL)
//			.setLogLevel(RestAdapter.LogLevel.FULL)
//			.build().create(API.class);

	public static final int PAGE_SIZE = 25;

	public static void initAPIManager(Context applicationContext) {
//		if(BuildConfig.FLAVOR.equals("getCoffeeShops")) {
//			BASE_URL = "http://getweed.positive-apps.com/api/v1/";
//		}else if(BuildConfig.FLAVOR.equals("getFashion")) {
//			BASE_URL = "http://getfashion.positive-apps.com/api/v1/";
//		}
		mApplicationContext = applicationContext;
	}

	public static boolean needInit() {
		return mApplicationContext == null;
	}

	//@formatter:off
	public interface API {
		  
		@GET("/business/getList" + "?ShowPromotions=1" + "&ShowProducts=1" + "&PageSize=" + PAGE_SIZE)
	
		public void getBusinessList(	@Query("Page") int pageNumber, 
										@Query("Categories") String Categories, 
										@QueryMap  Map<String, String> userParams, 
										Callback<BusinessGetList> callback);

		
		@GET("/business/getList" + "?ShowPromotions=1" + "&ShowProducts=1" + "&HasPromotions=1" + "&PageSize=" + PAGE_SIZE)

		public void getBusinessListHotDeals(	@Query("Page") int pageNumber, 
												@QueryMap  Map<String, String> userParams, 
												Callback<BusinessGetList> callback);
		
		@GET("/business/getList" + "?ShowPromotions=1" + "&ShowProducts=1" + "&HasPromotions=1" + "&PageSize=" + PAGE_SIZE)

		public void getFilteredHotDeals(	    @Query("Page") int pageNumber, 
												@Query("Radius") int Radius, 
												@Query("Categories") String Categories, 
												@Query("Recommended") int Recommended,
												@Query("Lng") double Lng, 
												@Query("Lat") double Lat, 
												@QueryMap  Map<String, String> userParams, 
												Callback<BusinessGetList> callback);

		
		@GET("/business/getList" + "?ShowPromotions=1" + "&ShowProducts=1" + "&PageSize=" + PAGE_SIZE)
		public void searchBusiness(		@Query("Keyword") String keyword, 
										@Query("Page") int pageNumber, 
										Callback<BusinessGetList> callback);

		
		@GET("/business/get/{id}")
		public void getBusiness(		@Path("id") long id,
										Callback<BusinessGet> callback);
		

		@GET("/category/getList")		
		public void getCategories(		@QueryMap  Map<String, String> userParams, 
										Callback<CategoryGetList> callback);

		
		@POST("/appuser/initApp")
		@FormUrlEncoded
		public void initApp(			@FieldMap Map<String, String> userParams, 
										Callback<UserResponse> callback);
		
		@POST("/reservation/getList")
		@FormUrlEncoded
		public void getReservation(		@Field("BusinessID") long BusinessID, 
										@Field("ProductID") long ProductID, 
										Callback<ReservationGetList> callback);
		
		@POST("/businessmanager/getBusinesses")
		@FormUrlEncoded
		public void getProductsAsManager(		@Field("AppuserID") long AppuserID, 
												@Field("Products") int Products, 
												Callback<BusinessAsManager> callback);
		
		@POST("/businessmanager/getBusinesses")
		@FormUrlEncoded
		public void getPromotionsAsManager(		@Field("AppuserID") long AppuserID, 
												@Field("Promotions") int Promotions, 
												@Field("Products") int Products,
												@FieldMap Map<String, String> userParams,
												   Callback<BusinessAsManager> callback);
		
		@POST("/reservation/verify")
		@FormUrlEncoded
		public void vaerifyReservation(			@Field("AppuserID") long AppuserID, 
												@Field("ReservationID") long ReservationID, 
												Callback<DataCheck> callback);
		
		@POST("/product/delete")
		@FormUrlEncoded
		public void deleteProduct(			@Field("ID") long ProductID, 
											Callback<DataCheck> callback);
		
		@POST("/reservation/delete")
		@FormUrlEncoded
		public void deleteReservation(			@Field("BusinessID") long BusinessID, 
												@Field("Reservations") String ReservationsArrays, 
												Callback<DataCheck> callback);
		
		@POST("/product/save")
		@FormUrlEncoded
		public void saveProductHe(		@Field("ID") long ID,
										@Field("BusinessID") long BusinessID, 
										@Field("TitleHE") String TitleHE, 
										@Field("DescriptionHE") String DescriptionHE,
										@Field("Price") double Price,
										@Field("Image") String Image,
										@Field("Categories") String CategoriesArrays,
										@Field("AvailableForSale") long AvailableForSale,
										Callback<DataCheck> callback);
		
		@POST("/promotion/getByFilters")
		@FormUrlEncoded
		public void getPromotionsByFilters(		@Field("Categories") String CategoriesArrays, 
												@Field("Lat") double Lat, 
												@Field("Lng") double Lng, 
												@Field("Radius") int Radius, 
												@Field("Recommended") long Recommended, 
												Callback<PromotionGetList> callback);
		
		@POST("/product/save")
		@FormUrlEncoded
		public void saveProductEn(		@Field("ID") long ID,
										@Field("BusinessID") long BusinessID,
										@Field("TitleEN") String TitleEN,
										@Field("DescriptionEN") String DescriptionEN,
										@Field("Price") double Price,
										@Field("Image") String Image,
										@Field("Categories") String CategoriesArrays,
										@Field("AvailableForSale") long AvailableForSale,
										Callback<DataCheck> callback);

		@POST("/promotion/save")
		@FormUrlEncoded
		public void savPromotion(		@Field("ID") long ID, 
										@Field("ProductID") long ProductID, 
										@Field("Status") int Status, 
										@Field("TitleHE") String TitleHE, 
										@Field("TitleEN") String TitleEN, 
										@Field("DescriptionHE") String DescriptionHE, 
										@Field("DescriptionEN") String DescriptionEN,  
										@Field("DiscountPrice") double DiscountPrice, 
										@Field("NumUnits") int NumUnits, 
										@Field("Image") String Image, 
										@Field("TimeStart") long TimeStart, 
//										@Field("TimeEnd") long TimeEnd, 
//										@Field("Repeated") int Repeated, 
//										@Field("RepeatTo") long RepeatTo, 
										Callback<DataCheck> callback);
		
		
		@POST("/reservation/save")
		@FormUrlEncoded
		public void saveReservation(	@Field("ReservationID") long ReservationID, 
										@Field("AppuserID") long AppuserID, 
										@Field("ProductID") long ProductID, 
										@Field("SaleType") int SaleType, 
										@Field("Param1") String Param1, 
										@Field("Param2") String Param2, 
										@Field("Param3") String Param3, 
										@Field("Param4") String Param4, 
										@Field("Param5") String Param5,
										@Field("Phone") String phone,
										@Field("Quantity") long Quantity, 
										@Field("Comments") String Comments, 
										@Field("ClubMember") String ClubMember, 
										@Field("AddressStreet") String AddressStreet, 
										@Field("AddressCity") String AddressCity, 
										@Field("AddressHouse") String AddressHouse, 
										@Field("AddressApt") String AddressApt, 
										Callback<ReservationGetList> callback);
		
		@POST("/promotion/getList")
		@FormUrlEncoded
		public void getPromotionList(   @Field("Radius") String Radius, 
										@Field("Categories") String Categories,
										@FieldMap Map<String, String> userParams, 
										Callback<UserResponse> callback);

		
		@POST("/appuser/setPushToken")
		@FormUrlEncoded
		public void setPushToken(       @Field("PushToken") String accessToken, 
										@Field("Device") String device,
										Callback<UserResponse> callback);

		@POST("/appuser/fbLogin")
		@FormUrlEncoded
		public void serverFacebookLogin(@Field("FBID") String fbID,
										@Field("FBAccessToken") String accessToken,
										@FieldMap Map<String, String> userParams,
										Callback<UserResponse> callback);



		@POST("/appuser/emailRegister")
		@FormUrlEncoded
		public void emailRegister(		@Field("Email") String email, 
										@Field("Password") String password, 
										@Field("FirstName") String firstname, 
										@Field("LastName") String lastname, 
										@FieldMap Map<String, String> userParams, 
										Callback<UserResponse> callback);
		
		@POST("/appuser/emailLogin")
		@FormUrlEncoded
		public void emailLogin(			@Field("Email") String email, 
										@Field("Password") String password,
										@FieldMap Map<String, String> userParams,
										Callback<UserResponse> callback);
		
		@POST("/appuser/logout")
		public void logout(				Callback<UserResponse> callback);
		
		@GET("/general/getSettings")
		public void getSettings(		Callback<SettingGet> callback);
	}
	//@formatter:on
 
	
	public static void initApp(Context context, Callback<UserResponse> callback) {
		api.initApp(makeUserParams(), callback);
	}

	public static void serverFacebookLogin(String fbID, String accessToken, Callback<UserResponse> callback) {
		api.serverFacebookLogin(fbID, accessToken, makeUserParams(), callback);
	}


	public static void emailRegister(String email, String password, String firstname, String lastname, Callback<UserResponse> callback) {
		api.emailRegister(email, password, firstname, lastname, makeUserParams(), callback);
	}
	
	public static void emailLogin(String email, String password, Callback<UserResponse> callback) {
		api.emailLogin(email, password, makeUserParams(), callback);
	}
	
	
	
	public static void logout(Callback<UserResponse> callback){
		api.logout(callback);
	}

 
	
	public static Map<String, String> makeUserParams() {
		Map<String, String> userParams = new HashMap<String, String>();
		Appuser user = UserManager.getAppUser();

		if (user != null) {
			userParams.put("_appuser_id", user.ID + "");
			userParams.put("_token", user.AuthToken);
			userParams.put("_lat", user.Lat);
			userParams.put("_lng", user.Lng);
			userParams.put("_lang", Prefs.getAppLanguage().toUpperCase());
		}
		userParams.put("_platform", "android");
		userParams.put("_device_id",
				DeviceUtil.getDeviceUDID(mApplicationContext));
		userParams.put("_http_error", "1");
		System.out.println("niv "+userParams.toString());

		return userParams;
	}

}
