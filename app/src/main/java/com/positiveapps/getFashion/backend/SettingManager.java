package com.positiveapps.getFashion.backend;

import java.io.IOException;

import android.content.Context;
import android.util.Log;

import com.positiveapps.getFashion.jsonmodels.apiresponse.Setting;
import com.positiveapps.getFashion.util.FilesUtil;

public class SettingManager {
	private static final String SETTING_FILENAME = "current.setting";
	private static final String TAG = "SettingManager";
	private static Setting mCurrentSetting = null;
	private static Context mContext;

	public static void init(Context context) {
		mContext = context;
		mCurrentSetting = getSetting();
	}
	
	public static boolean needInit(){
		return mContext==null || mCurrentSetting == null;
	}

	public static Setting getSetting() {
		if (mCurrentSetting != null)
			return mCurrentSetting;

		Setting setting =null;

		try {
			setting = getSettingFromStorage();
//			Log.d(TAG, "User instanciated from storage, ID " + user.ID);
		} catch (ClassCastException | ClassNotFoundException | IOException e) {
			e.printStackTrace();
		}
		
		mCurrentSetting = setting;
		return setting;
	}

	public static void setSetting(Setting setting) {
		mCurrentSetting = setting;

		try {
			saveSettingToStorage(setting);
//			Log.d(TAG, "Setting saved to storage with id " + user.ID);
		} catch (IOException e) {
			Log.wtf(TAG, "Error saving setting to storage", e);
			e.printStackTrace();
		}
	}

	public static void saveSettingToStorage(Setting setting) throws IOException {
		String userFilePath = mContext.getFilesDir().getAbsolutePath() + "/" + SETTING_FILENAME;

		FilesUtil.saveObjectToFile(setting, userFilePath);
	}

	public static Setting getSettingFromStorage() throws ClassCastException, ClassNotFoundException, IOException {
		String settingFilePath = mContext.getFilesDir().getAbsolutePath() + "/" + SETTING_FILENAME;
		Setting setting = FilesUtil.instaciateObjectFromFile(settingFilePath);

		return setting;
	}
}
