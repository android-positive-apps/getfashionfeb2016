package com.positiveapps.getFashion.backend;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import android.util.Log;

import com.positiveapps.getFashion.App;
import com.positiveapps.getFashion.interfaces.BusinessRepositoryCallbacks;
import com.positiveapps.getFashion.jsonmodels.Appuser;
import com.positiveapps.getFashion.jsonmodels.Business;
import com.positiveapps.getFashion.jsonmodels.apiresponse.BusinessGet;
import com.positiveapps.getFashion.jsonmodels.apiresponse.BusinessGetList;
import com.positiveapps.getFashion.util.DeviceUtil;

public class BusinessRepository {
	public static final String TAG = "BusinessRepository";
	private static BusinessRepository instance;
	
	private static HashSet<Business> mBusineses;
	private ArrayList<Business> mSearchResults;
	public static Business tempBusiness; 
	private HashSet<BusinessRepositoryCallbacks> mPendingCallbacks;

	public static BusinessRepository getInstance() {
		if (instance == null) {
			instance = new BusinessRepository();
		}

		return instance;
	}

	private BusinessRepository() {
		mBusineses = new HashSet<Business>();
		mPendingCallbacks = new HashSet<BusinessRepositoryCallbacks>();
		mSearchResults = new ArrayList<Business>();
	}

	public void loadBusinessList(int page, final long categoryID, final BusinessRepositoryCallbacks callbacks) {
		registerCallback(callbacks);

		onProgressLoadingBusinesses(callbacks);

		APIManager.api.getBusinessList(page, "["+categoryID+"]", makeUserParams(), new Callback<BusinessGetList>() {
			public void success(BusinessGetList result, Response response) {
				if (result.data.Records == null || result.data.Records.isEmpty()) {
					onNoMoreBusinessResults(callbacks);
					return;
				}
				mBusineses.clear();
				Log.e("product", "categoryID ---> " + categoryID + ": ");
				for (int i = 0; i < result.data.Records.size(); i++) {
					Log.e("product", "result.data.Records ---> " + result.data.Records.get(i).Name);
					result.data.Records.get(i).tempCategory = categoryID;
				}
				
   
				onFinishedLoadingBusiness(result.data.Records, callbacks, categoryID);
			}

			public void failure(RetrofitError retrofitError) {
				onErrorLoadingBusinesses(retrofitError, callbacks);

			}
		});
	}

	public void loadBusinessListHotDeals(int page, final BusinessRepositoryCallbacks callbacks) {
		registerCallback(callbacks);

		onProgressLoadingBusinesses(callbacks);
		
		Appuser user = UserManager.getAppUser();
		if(user.isFiltered){
		APIManager.api.getFilteredHotDeals(page, user.filteredRadius, user.filteredCategories, 0 , user.filteredLng, user.filteredLat, makeUserParams(), new Callback<BusinessGetList>(){

			@Override
			public void success(BusinessGetList result, Response response) {
				if (result.data.Records == null || result.data.Records.isEmpty()) {
					onNoMoreBusinessResults(callbacks);
					return;
				}
				mBusineses.clear();
				onFinishedLoadingBusiness(result.data.Records, callbacks);
			}

			@Override
			public void failure(RetrofitError retrofitError) {
				onErrorLoadingBusinesses(retrofitError, callbacks);
			}
			});
		}else{
		APIManager.api.getBusinessListHotDeals(page, makeUserParams(), new Callback<BusinessGetList>() {
			@Override
			public void success(BusinessGetList result, Response response) {
				if (result.data.Records == null || result.data.Records.isEmpty()) {
					onNoMoreBusinessResults(callbacks);
					return;
				}
				mBusineses.clear();
				onFinishedLoadingBusiness(result.data.Records, callbacks);
			}

			@Override
			public void failure(RetrofitError retrofitError) {
				onErrorLoadingBusinesses(retrofitError, callbacks);
			}
		});
		}
	}

	public void searchForBusinesses(String keyword, int page, final BusinessRepositoryCallbacks callbacks) {
		registerCallback(callbacks);
		APIManager.api.searchBusiness(keyword, page, new Callback<BusinessGetList>() {
			@Override
			public void success(BusinessGetList result, Response response) {
				mSearchResults.clear();

				if (result.data.Records == null || result.data.Records.isEmpty()) {
					onNoMoreBusinessResults(callbacks);
					return;
				}

				mSearchResults.addAll(result.data.Records);
				mBusineses.clear();
				onFinishedLoadingBusiness(result.data.Records, callbacks);
			}

			@Override
			public void failure(RetrofitError retrofitError) {
				onErrorLoadingBusinesses(retrofitError, callbacks);
			}
		});
	}
	
	public static Business findBussinessById(long id) {
		if (mBusineses != null) {
			for (Business business : mBusineses) {
				if (business.ID == id) {
					Log.d(TAG, "Found business with id " + business.ID);
					return business;
				}
			}
		}
		return null;
	}

	public void loadBusinessById(final long id, boolean forceNetwork, final BusinessRepositoryCallbacks callbacks) {
		Log.d(TAG, "loadBusinessById()");
		registerCallback(callbacks); 
		Log.d(TAG, "loadBusinessById() registered Callback");
		
		if (mBusineses != null && !forceNetwork) {
			for (Business business : mBusineses) {
				if (business.ID == id) {
					Log.d(TAG, "Found business with id " + business.ID);
					ArrayList<Business> result = new ArrayList<Business>();
					result.add(business);
					onFinishedLoadingBusiness(result, callbacks);
					return;
				}
			}
		}

		onProgressLoadingBusinesses(callbacks);
		Log.d(TAG, "loadBusinessById() failed to find business locally, requesting network");
		APIManager.api.getBusiness(id, new Callback<BusinessGet>() {
			@Override
			public void success(BusinessGet result, Response response) {
				if (result.data == null) {
					onNoMoreBusinessResults(callbacks);
					return;
				}
				
				ArrayList<Business> business = new ArrayList<Business>();
				business.add(result.data.Business);
				Log.d(TAG, "result.data.Business " + business.toString());
				onFinishedLoadingBusiness(business, callbacks);
			}

			@Override
			public void failure(RetrofitError retrofitError) {
				onErrorLoadingBusinesses(retrofitError, callbacks);
			}
		});;
	}

	public boolean isCallbackRegistered(BusinessRepositoryCallbacks callback) {
		if (callback == null)
			return false;

		return mPendingCallbacks.contains(callback);

	}

	// argument is not needed, but maybe better to implement with List of
	// callbacks
	public void unregisterCallback(BusinessRepositoryCallbacks callback) {
		mPendingCallbacks.remove(callback);
		Log.d(TAG, "unregistered callback");

	}

	private void registerCallback(BusinessRepositoryCallbacks callback) {
		mPendingCallbacks.add(callback);
		Log.d(TAG, "registered callback");

	}

	public void onNoMoreBusinessResults(BusinessRepositoryCallbacks callback) {
		if (isCallbackRegistered(callback)) {
			callback.onNoMoreBusinessResults();
			mPendingCallbacks.remove(callback);
		} else {
			Log.e(TAG, "No callback registered!");
		}
	}

	public void onFinishedLoadingBusiness(ArrayList<Business> businesses, BusinessRepositoryCallbacks callback) {
		mBusineses.addAll(businesses);

		if (isCallbackRegistered(callback)) {
			callback.onFinishedLoadingBusiness(businesses);
			mPendingCallbacks.remove(callback);
		} else {
			Log.e(TAG, "No callback registered!");
		}
	}
	
	public void onFinishedLoadingBusiness(ArrayList<Business> businesses, BusinessRepositoryCallbacks callback, long cartegoryID) {
		mBusineses.addAll(businesses);

		if (isCallbackRegistered(callback)) {
			callback.onFinishedLoadingBusiness(businesses);
			mPendingCallbacks.remove(callback);
		} else {
			Log.e(TAG, "No callback registered!");
		}
	}

	public void onProgressLoadingBusinesses(BusinessRepositoryCallbacks callback) {
		if (isCallbackRegistered(callback)) {
			callback.onProgressLoadingBusinesses();
		}
	}

	public void onErrorLoadingBusinesses(Throwable throwable, BusinessRepositoryCallbacks callback) {
		if (isCallbackRegistered(callback)) {
			callback.onErrorLoadingBusinesses(throwable);
			mPendingCallbacks.remove(callback);
		}
		
		
	}
	public static Map<String, String> makeUserParams() {
		Map<String, String> userParams = new HashMap<String, String>();
		Appuser user = UserManager.getAppUser();

		if (user != null) {
			userParams.put("_appuser_id", user.ID + "");
			userParams.put("_token", user.AuthToken);
			userParams.put("_lat", user.Lat);
			userParams.put("_lng", user.Lng);
			userParams.put("_lang", Prefs.getAppLanguage().toUpperCase());
		}
		userParams.put("_platform", "android");
		userParams.put("_device_id",
				DeviceUtil.getDeviceUDID(App.applicationContext));
		userParams.put("_http_error", "1");

		return userParams;
	}	
}
