package com.positiveapps.getFashion.backend;

import java.io.IOException;

import android.content.Context;
import android.util.Log;

import com.positiveapps.getFashion.jsonmodels.Appuser;
import com.positiveapps.getFashion.util.FilesUtil;

public class UserManager {
	private static final String USER_FILENAME = "current.user";
	private static final String TAG = "UserManager";
	private static Appuser mCurrentAppuser = null;
	private static Context mContext;

	public static void init(Context context) {
		mContext = context;
		mCurrentAppuser = getAppUser();
	}
	
	public static boolean needInit(){
		return mContext==null || mCurrentAppuser == null;
	}

	public static Appuser getAppUser() {
		if (mCurrentAppuser != null)
			return mCurrentAppuser;

		Appuser user=null;

		try {
			user = getUserFromStorage();
			Log.d(TAG, "User instanciated from storage, ID " + user.ID);
		} catch (ClassCastException | ClassNotFoundException | IOException e) {
			e.printStackTrace();
		}
		
		mCurrentAppuser = user;
		return user;
	}
	public static Appuser getBaseAppUser() {
		if (mCurrentAppuser != null)
			return mCurrentAppuser;

		Appuser user=null;

		try {
			user = getBaseUserFromStorage();
			Log.d(TAG, "User instanciated from storage, ID " + user.ID);
		} catch (ClassCastException | ClassNotFoundException | IOException e) {
			e.printStackTrace();
		}

		mCurrentAppuser = user;
		return user;
	}
	public static void setAppuser(Appuser user) {
		mCurrentAppuser = user;

		try {
			saveUserToStorage(user);
			Log.d(TAG, "User saved to storage with id " + user.ID);
		} catch (IOException e) {
			Log.wtf(TAG, "Error saving user to storage", e);
			e.printStackTrace();
		}
	}
	public static void setBaseAppuser(Appuser user) {
		mCurrentAppuser = user;

		try {
			saveBaseUserToStorage(user);
			Log.d(TAG, "User saved to storage with id " + user.ID);
		} catch (IOException e) {
			Log.wtf(TAG, "Error saving user to storage", e);
			e.printStackTrace();
		}
	}
	public static void saveBaseUserToStorage(Appuser user) throws IOException {
		String userFilePath = mContext.getFilesDir().getAbsolutePath() + "/" + "base.user";
		FilesUtil.saveObjectToFile(user, userFilePath);
	}

	public static void saveUserToStorage(Appuser user) throws IOException {
		String userFilePath = mContext.getFilesDir().getAbsolutePath() + "/" + USER_FILENAME;
		FilesUtil.saveObjectToFile(user, userFilePath);
	}

	public static Appuser getUserFromStorage() throws ClassCastException, ClassNotFoundException, IOException {
		String userFilePath = mContext.getFilesDir().getAbsolutePath() + "/" + USER_FILENAME;
		System.out.println("userFilePath " + userFilePath);
		Appuser user = FilesUtil.instaciateObjectFromFile(userFilePath);

		return user;
	}
	public static Appuser getBaseUserFromStorage() throws ClassCastException, ClassNotFoundException, IOException {
		String userFilePath = mContext.getFilesDir().getAbsolutePath() + "/" + "base.user";
		System.out.println("userFilePath " + userFilePath);
		Appuser user = FilesUtil.instaciateObjectFromFile(userFilePath);

		return user;
	}
}
