package com.positiveapps.getFashion.backend;

import android.content.Context;

import com.positiveapps.getFashion.R;
import com.positiveapps.getFashion.jsonmodels.Category;

import java.util.ArrayList;

public class Categories {
	private static ArrayList<Category> categories;
	public static final int FRAGMENT_CATEGORY_HOT_DEALS = -1;
	public static final int FRAGMENT_CATEGORY_SEARCH = -2;
	
	public static int mSearchFragmentPosition;
	public static int mHotDealsFragmentPosition;
	
	private static Context applicationContext;
	
	public static void init(Context context) {
		applicationContext = context;
	}
	
	public static ArrayList<Category> getCategories() {
		return categories;
	}

	public static ArrayList<Category> getRealCategories() {
		ArrayList<Category> tempCategories = new ArrayList<>();
		for (int i = 0; i < categories.size(); i++) {
			if(i == 0 || i == (categories.size() - 1)){
			continue;	
			}
			
			tempCategories.add(categories.get(i));
		}
		return tempCategories;
	}

	public static ArrayList<String> getRealCategoriesIds() {
		ArrayList<String> tempCategories = new ArrayList<>();
		for (int i = 0; i < categories.size(); i++) {
			if(i == 0 || i == (categories.size() - 1)){
				continue;
			}

			tempCategories.add(categories.get(i).ID+"");
		}
		return tempCategories;
	}
	
	public static void setCategories(ArrayList<Category> Categories) {
		ArrayList<Category> temp = new ArrayList<>();
		for (int i = 0; i < Categories.size(); i++) {
			if(Categories.get(i).Accessible == 1){
				temp.add(Categories.get(i));
			}
		}
		categories = temp;
		addFakeCategories(categories);
 
	}
	
	private static void addFakeCategories(ArrayList<Category> categories) {
		Category hotDeals = new Category();
		Category search = new Category();

		hotDeals.Name = applicationContext.getString(R.string.tabs_hot_deals);
		hotDeals.ID = FRAGMENT_CATEGORY_HOT_DEALS;

		search.Name = applicationContext.getString(R.string.category_search);
		search.ID = FRAGMENT_CATEGORY_SEARCH;

		categories.add(0, hotDeals);
		categories.add(search);

		mHotDealsFragmentPosition = 0;
		mSearchFragmentPosition = categories.size() - 1;
		
		
	}

	public static int getSearchFragmentPosition() {
		return mSearchFragmentPosition;
	}

	public static int getHotDealsFragmentPosition() {
		return mHotDealsFragmentPosition;
	}
	
	public static String getCategoryName(long categoryID) {
		for(int i=0; i<categories.size(); i++){
			Category category = categories.get(i);
			if(categoryID == category.ID){
				return category.Name;
			}
		}
		
		return null;
	}
	
	public static boolean needInit() {
		return applicationContext == null;
	}
}
