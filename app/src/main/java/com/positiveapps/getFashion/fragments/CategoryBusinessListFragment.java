package com.positiveapps.getFashion.fragments;

import android.os.Bundle;

import com.positiveapps.getFashion.R;
import com.positiveapps.getFashion.backend.BusinessRepository;


public class CategoryBusinessListFragment extends BaseBusinessListFragment{
	
	public static final String KEY_FRAGMENT_CATEGORY = "fragment_category";
	private long mFragmentCategoryID;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mFragmentCategoryID = getArguments().getLong(KEY_FRAGMENT_CATEGORY);
	}
	
	@Override
	protected void getBusinessListIfNeeded() {
		BusinessRepository.getInstance().loadBusinessList(getCurrentPage(), mFragmentCategoryID, this);
	}

	@Override
	protected int getEmptyViewResource() {
		return R.layout.empty_view_fragment_promotions;
	}


}
