/**
 * 
 */
package com.positiveapps.getFashion.fragments;

import com.positiveapps.getFashion.R;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.TextView;

/**
 * @author Ilia
 *
 */
public class BaseFragment extends Fragment {
	private ProgressDialog mProgressDialog;
	private Dialog loadingDialog;
	public boolean isFragmentLoadingData = false;
	//private ProgressDialog progressDialog;
	
	public void refreshFragment(){
		try{
			if(!isFragmentLoadingData){
				getFragmentManager().beginTransaction().detach(this).attach(this).commit();
			}
			
		}catch(Exception ex){
			
		}
		
	}
	
	
	protected void showProgressDialog(String title, String message){
		mProgressDialog = ProgressDialog.show(getActivity(), title, message);
	}

	protected void dismissProgressDialog() {
		if(mProgressDialog != null) 
			mProgressDialog.dismiss();
	}
	
	public void showLoadingDialog(){
		loadingDialog = new Dialog(getActivity(), R.style.loadingDialog);
		final View view = getActivity().getLayoutInflater().inflate(R.layout.dialog_loading, null);
		loadingDialog.setContentView(view);
		loadingDialog.setCancelable(false);
		loadingDialog.show();
	}
	
	public void showLoadingDialog(String text){
		loadingDialog = new Dialog(getActivity(), R.style.loadingDialog);
		final View view = getActivity().getLayoutInflater().inflate(R.layout.dialog_loading, null);
		((TextView) view.findViewById(R.id.loading_text)).setText(text);
		loadingDialog.setContentView(view);
		loadingDialog.setCancelable(false);
		loadingDialog.show();
	}
	
	public void dismissLoadingDialog(){ 
		loadingDialog.dismiss();
	}
}
