package com.positiveapps.getFashion.fragments;

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.positiveapps.getFashion.BuildConfig;
import com.positiveapps.getFashion.MainActivity;
import com.positiveapps.getFashion.R;
import com.positiveapps.getFashion.adapters.BusinessListAdapter;
import com.positiveapps.getFashion.backend.BusinessRepository;
import com.positiveapps.getFashion.backend.Prefs;
import com.positiveapps.getFashion.interfaces.BusinessRepositoryCallbacks;
import com.positiveapps.getFashion.interfaces.FilterSetListener;
import com.positiveapps.getFashion.jsonmodels.Business;
import com.positiveapps.getFashion.views.GridSpacingItemDecoration;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;

public abstract class BaseBusinessListFragment extends BaseFragment implements BusinessRepositoryCallbacks, FilterSetListener, OnRefreshListener {
	public static final String TAG = "BusinessListFragment";
	public boolean isHotDealPage = false;
	private static final int ITEMS_PER_ROW = 2;
	
	private SwipeRefreshLayout swipeLayout;
	private RecyclerView businessRecyclerView;

	// sets to false on teh first time businesses recieved
	private boolean mIsFirstLoadOfBusinesses = true;
	private BusinessListAdapter mAdapter;
	private View mEmptyView;
	private FrameLayout mEmptyViewFrame;
	 
	private int mCurrentPage = 0;
	private Handler handler;

	public BaseBusinessListFragment() {}
	

	protected abstract void getBusinessListIfNeeded();
	protected abstract int getEmptyViewResource();

	public static BaseBusinessListFragment newCategoryInstance(long fragmentCategory) {
		BaseBusinessListFragment fragment = new CategoryBusinessListFragment();
		Bundle b = new Bundle();
		b.putLong(CategoryBusinessListFragment.KEY_FRAGMENT_CATEGORY, fragmentCategory);
		fragment.setArguments(b);

		return fragment;
	}

	public static BaseBusinessListFragment newHotDealsInstance() {
		BaseBusinessListFragment fragment = new HotDealsBusinessListFragment();
		return fragment;
	}

	public static BaseBusinessListFragment newSearchInstance() {
		BaseBusinessListFragment fragment = new SearchBusinessListFragment();
		return fragment;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		return inflater.inflate(R.layout.fragment_business_list, container, false);
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		findViews(view);
		initRecyclerView();
//		initSwipeRefreshCallback();
		swipeLayout.setOnRefreshListener(this);

		initEmptyView();

		((MainActivity)getActivity()).setFilterSetListener(this);


	}


	@Override
	public void onResume() {
		super.onResume();
		Log.d(TAG, "onResume()");

	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

	}

	@Override
	public void onStart() {
		super.onStart();
		getBusinessListIfNeeded();
	}


	@Override
	public void onDestroy() {
		super.onDestroy();
	}

	@Override
	public void onStop() {
		super.onStop();
		BusinessRepository.getInstance().unregisterCallback(this);

	}

	@Override
	public void onPause() {
		super.onPause();
		Log.d(TAG, "onPause()");
	}

	private void findViews(View view) {
		businessRecyclerView = (RecyclerView) view.findViewById(R.id.businessList);
		swipeLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_container);
		mEmptyViewFrame = (FrameLayout) view.findViewById(R.id.emptyViewFrame);

	}
	
	public int getCurrentPage() {
		return mCurrentPage;
	}
	
	private void initEmptyView() {

		mEmptyView = LayoutInflater.from(getActivity()).inflate(getEmptyViewResource(), mEmptyViewFrame, false);
		FrameLayout.LayoutParams lp = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.WRAP_CONTENT, FrameLayout.LayoutParams.WRAP_CONTENT,
				Gravity.CENTER);
		mEmptyView.setLayoutParams(lp);

		mEmptyViewFrame.addView(mEmptyView);
		mEmptyViewFrame.setVisibility(View.INVISIBLE);
	}

	private void initRecyclerView() {
		businessRecyclerView.setHasFixedSize(true);
		if(BuildConfig.FLAVOR.equals("getFit")||BuildConfig.FLAVOR.equals("getMedical")) {
			businessRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
		} else {
			businessRecyclerView.setLayoutManager(new GridLayoutManager(getActivity(), ITEMS_PER_ROW));
			businessRecyclerView.addItemDecoration(new GridSpacingItemDecoration(ITEMS_PER_ROW, 20, true));
		}


	}

//	private void initSwipeRefreshCallback() {
//		swipeLayout.setOnRefreshListener(this);
//	}
	@Override
	public void onRefresh() {
		getBusinessListIfNeeded();
	}
	@Override
	public void onFilterSet() {

		onRefresh();

	}

	@Override
	public void onFinishedLoadingBusiness(ArrayList<Business> businesses) {
		String filter= Prefs.getMenuFilterButtons();
		String[] filterButtons = filter.split(",");
//		String filter1= filterButtons[0];
//		String filter2= filterButtons[1];
//		String filter3= filterButtons[2];
//		String filter4= filterButtons[3];
//		String filter5= filterButtons[4];
//		String checked = button_b_checked + ","
//				+ button_c_checked + ","
//				+ button_a_checked + ","
//				+ button_e_checked + ","
//				+ button_d_checked + ","
//				+ openNow
//				;
		String[] filterButtonsFix= {filterButtons[1],filterButtons[2],
				filterButtons[0],filterButtons[4],filterButtons[3],filterButtons[5],
		};

		System.out.println("niv "+ Arrays.toString(filterButtons));


		ArrayList<Business> businessesFiltered= new ArrayList<>();
		for (Business i : businesses){
			String cus1= i.Custom1+"";
			String cus2= i.Custom2+"";
			String cus3= i.Custom3+"";
			String cus4= i.Custom4+"";
			String cus5= i.Custom5+"";
			String[] c= {cus1,cus2,cus3,cus4,cus5};
			int num2=0;
			for (int num=0;num<=4;num++) {
				if (filterButtonsFix[num].equals("0")) {
					if (c[num].equals("1")) {
						num2++;
					}
				}
			}
			if (i.Custom4==1 && filterButtonsFix[4].equals("0")){
				num2++;
			}

			if(num2>0) {
				if (filterButtonsFix[5].equals("0")){
					if (isStoreNowOpen(i)) {
						businessesFiltered.add(i);
					}
				}else {
					businessesFiltered.add(i);
				}

				System.out.println("niv " + i.toString() + " :)" + cus1 + cus2 + cus3 + cus4 + cus5);

			}

		}

		System.out.println("niv "+businessesFiltered.toString());


		Log.d(TAG, "onFinishedLoadingBusiness() start");
		swipeLayout.setRefreshing(false);
		mEmptyViewFrame.setVisibility(View.INVISIBLE);
		long catId = 0;
		if(getArguments() != null){
			catId = getArguments().getLong(CategoryBusinessListFragment.KEY_FRAGMENT_CATEGORY);
		}
		Log.e("CAT", "catId " + catId);
//		if (mAdapter == null) {
			
//				mAdapter = new BusinessListAdapter(getFragmentManager(), getOnlyBussinessWithProductsOrPromotion(businesses), catId);
				mAdapter = new BusinessListAdapter(getFragmentManager(), getOnlyBussinessWithProductsOrPromotion(businessesFiltered), catId);
//		}

		businessRecyclerView.setAdapter(mAdapter);
		Log.d(TAG, "onFinishedLoadingBusiness() end");

		mIsFirstLoadOfBusinesses = false;
	}

	public void onNoMoreBusinessResults() {
		swipeLayout.setRefreshing(false);

		if (mAdapter == null || mAdapter.getItemCount() == 0) {
			 mEmptyViewFrame.setVisibility(View.VISIBLE);
		}
	};

	@Override
	public void onProgressLoadingBusinesses() {
		Log.d(TAG, "onProgressLoadingBusinesses() start");
		swipeLayout.post(new Runnable() {
			@Override
			public void run() {
				swipeLayout.setRefreshing(true);
			}
		});
		Log.d(TAG, "onProgressLoadingBusinesses() end");
	}

	@Override
	public void onErrorLoadingBusinesses(Throwable throwable) {
		Log.d(TAG, "onErrorLoadingBusinesses() start");
		throwable.printStackTrace();
		Log.d(TAG, "onErrorLoadingBusinesses() end");

	}

	private ArrayList<Business> getOnlyBussinessWithProductsOrPromotion(ArrayList<Business> businesses) {
		ArrayList<Business> temp = new ArrayList<>();
		for (Business x : businesses) {
			if(x.Promotions.size() != 0 || x.Products.size() != 0){
				temp.add(x);
			}
		} 
		return temp; 
	}

	public boolean isStoreNowOpen(Business mBusiness) {
		Calendar cal = Calendar.getInstance();
		int dayOfMonth = cal.get(Calendar.DAY_OF_WEEK);
		int nowHour = cal.get(Calendar.HOUR_OF_DAY);
		int minute = cal.get(Calendar.MINUTE);
		if (dayOfMonth >= 1 && dayOfMonth <= 5) {
			if (mBusiness.OpeningTime.matches("00:00:00") && mBusiness.ClosingTime.matches("00:00:00")) {return false;}
			String[] open = mBusiness.OpeningTime.split(":");
			String[] close = mBusiness.ClosingTime.split(":");
			int ho = Integer.parseInt(open[0]);
			int mo = Integer.parseInt(open[1]); // hours and minutes opening hours
			int hc = Integer.parseInt(close[0]);
			int mc = Integer.parseInt(close[1]); // hours and minutes closing hours
			if (hc == 0) {
				hc = 24;
			}
			if (mc == 0) {
				mc = 60;
			} // handling zero in closig hours
			if ((nowHour > ho && nowHour < hc)) {
				return true;
			} else if ((nowHour == ho) && (minute >= mo)) {
				return true;
			} else if ((nowHour == hc) && (minute < mc)) {
				return true;
			}
			System.out.println("@@ho/hc " + ho + "/" + hc);
			System.out.println("@@mo/mc " + mo + "/" + mc);
			System.out.println("@@betHour " + (nowHour >= ho && nowHour < hc));
			System.out.println("@@betMinute " + (minute >= mo && minute < mc));
		} else if (dayOfMonth == 6) {
			if (mBusiness.OpeningTimeFriday.matches("00:00:00") && mBusiness.ClosingTimeFriday.matches("00:00:00")) {return false;}
			String[] open = mBusiness.OpeningTimeFriday.split(":");
			String[] close = mBusiness.ClosingTimeFriday.split(":");
			int ho = Integer.parseInt(open[0]);
			int mo = Integer.parseInt(open[1]); // hours and minutes opening hours
			int hc = Integer.parseInt(close[0]);
			int mc = Integer.parseInt(close[1]); // hours and minutes closing hours
			if (hc == 0) {
				hc = 24;
			}
			if (mc == 0) {
				mc = 60;
			} // handling zero in closig hours
			if ((nowHour > ho && nowHour < hc)) {
				return true;
			} else if ((nowHour == ho) && (minute >= mo)) {
				return true;
			} else if ((nowHour == hc) && (minute < mc)) {
				return true;
			}
			System.out.println("@@ho/hc " + ho + "/" + hc);
			System.out.println("@@mo/mc " + mo + "/" + mc);
			System.out.println("@@betHour " + (nowHour >= ho && nowHour < hc));
			System.out.println("@@betMinute " + (minute >= mo && minute < mc));
		} else if (dayOfMonth == 7) {
			if (mBusiness.OpeningTimeSaturday.matches("00:00:00") && mBusiness.ClosingTimeSaturday.matches("00:00:00")) {return false;}
			String[] open = mBusiness.OpeningTimeSaturday.split(":");
			String[] close = mBusiness.ClosingTimeSaturday.split(":");
			int ho = Integer.parseInt(open[0]);
			int mo = Integer.parseInt(open[1]); // hours and minutes opening hours
			int hc = Integer.parseInt(close[0]);
			int mc = Integer.parseInt(close[1]); // hours and minutes closing hours
			if (hc == 0) {
				hc = 24;
			}
			if (mc == 0) {
				mc = 60;
			} // handling zero in closig hours
			if ((nowHour > ho && nowHour < hc)) {
				return true;
			} else if ((nowHour == ho) && (minute >= mo)) {
				return true;
			} else if ((nowHour == hc) && (minute < mc)) {
				return true;
			}
			System.out.println("@@ho/hc " + ho + "/" + hc);
			System.out.println("@@mo/mc " + mo + "/" + mc);
			System.out.println("@@betHour " + (nowHour >= ho && nowHour < hc));
			System.out.println("@@betMinute " + (minute >= mo && minute < mc));
		}
		return false;
	}
}
