package com.positiveapps.getFashion.fragments.manage;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.nfc.Tag;
import android.os.Bundle;
import android.os.Environment;
import android.os.SystemClock;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.ListPopupWindow;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.PopupMenu.OnMenuItemClickListener;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.positiveapps.getFashion.ManageActivity;
import com.positiveapps.getFashion.R;
import com.positiveapps.getFashion.backend.APIManager;
import com.positiveapps.getFashion.backend.BusinessRepository;
import com.positiveapps.getFashion.backend.Prefs;
import com.positiveapps.getFashion.backend.SettingManager;
import com.positiveapps.getFashion.backend.UserManager;
import com.positiveapps.getFashion.dialogs.DialogCallback;
import com.positiveapps.getFashion.dialogs.TimePickerFragment;
import com.positiveapps.getFashion.fragments.BaseFragment;
import com.positiveapps.getFashion.interfaces.ManagePromationResumeListener;
import com.positiveapps.getFashion.jsonmodels.Business;
import com.positiveapps.getFashion.jsonmodels.Promotion;
import com.positiveapps.getFashion.jsonmodels.apiresponse.BusinessAsManager;
import com.positiveapps.getFashion.jsonmodels.apiresponse.DataCheck;
import com.positiveapps.getFashion.util.BitmapUtil;
import com.positiveapps.getFashion.util.DateUtil;
import com.positiveapps.getFashion.util.DialogHelper;
import com.positiveapps.getFashion.util.GeneralHelper;
import com.positiveapps.getFashion.util.ImageLoader2;
import com.positiveapps.getFashion.views.SwitchButton;

import java.io.File;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class ManagePromotionFragment extends BaseFragment implements
        OnClickListener {

    private Button editButton;
    private Button langButton;
    private TextView picBranchTextView;
    private Button nextButton;
    private Button backButton;
    private Button editPictureButton;
    private EditText regPriceEditText;
    private EditText promPriceEditText;
    private EditText quantityEditText;
    private EditText promBeginEditText;
    private ImageView promImageImageView;
    private SwitchButton activationSwitchButton;
    private ListPopupWindow listPopupWindow;
    private String imageBase64String;
    private boolean isOnEditMode;
    private ListPopupWindow quantitiesWindow;
    private Button saveButton;
    private BusinessAsManager bussinessArray;
    private Business bussines;
    private PopupMenu mBranchPopupMenu;
    private PopupMenu mPromotionPopupMenu;
    private ArrayList<Promotion> promotion;
    private Resources resources;
    private String currentLang = Prefs.getAppLanguage().toUpperCase();
    private TextView promNameTextview;
    private TextView promBeginTextview;
    private TextView quantityTextview;
    private TextView regPriceTextview;
    private TextView promPriceTextview;
    private TextView promDescTextview;
    private TextView isActiveTextview;
    private Button previewButton;
    private EditText promBeginSelector;
    private TextView picPromoTextView;
    private Promotion promotion2;
    private LinearLayout mainLayout;
    private EditText descriptionEditTextHE;
    private EditText descriptionEditTextEN;
    private EditText promNameEditTextHE;
    private EditText promNameEditTextEN;
    private LinearLayout paramsLayout;
    private ArrayList<Promotion> promotionArray;
    private ImageView ic_left_arrow;
    private ImageView ic_right_arrow;
    private int currentIndex;
    Bitmap image = null;
    private boolean foundPromotion = false;
    private long promotionId;
    private ManagePromationResumeListener promotionCallback;

    public ManagePromotionFragment() {
    }

    public static ManagePromotionFragment newInstance() {
        ManagePromotionFragment instance = new ManagePromotionFragment();
        return instance;
    }

    public static ManagePromotionFragment newInstance(long ...strings) {
        ManagePromotionFragment instance = new ManagePromotionFragment();
        Bundle bundle = new Bundle();
        bundle.putLongArray("LongArr", strings);
        instance.setArguments(bundle);
        return instance;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_manage_promotion,
                container, false);
        return v;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
        super.onActivityResult(requestCode, resultCode, data);


        if (resultCode != -1) {
            return;
        }

        switch (requestCode) {
            case GeneralHelper.GALLARY_REQUEST_CODE:
                image = ImageLoader2.byUri(ImageLoader2.IMAGE_TYPE_FROM_GALLERAY,
                        data.getData(), promImageImageView);
                break;
            case GeneralHelper.CAMERA_REQUEST_CODE:
                File file = new File(Environment.getExternalStorageDirectory()
                        .getPath(), GeneralHelper.CAMERA_PROFILE_PATH);
                image = BitmapUtil.loadImageIntoByStringPath(
                        file.getAbsolutePath(), promImageImageView, 300, 2,
                        ImageLoader2.DEAFULT_PLACE_HOLDER);
                break;
            default:
                break;
        }
        imageBase64String = GeneralHelper.getProfileImageAsBase64(image);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        langButton = (Button) view
                .findViewById(R.id.managepromotionfragment_lang_button);
        picBranchTextView = (TextView) view
                .findViewById(R.id.managepromotionfragment_picBranch_textview);
        picPromoTextView = (TextView) view
                .findViewById(R.id.managepromotionfragment_picPromotion_textview);
        editPictureButton = (Button) view
                .findViewById(R.id.managepromotionfragment_editPicture_button);
        regPriceEditText = (EditText) view
                .findViewById(R.id.managepromotionfragment_regPrice_edittext);
        promPriceEditText = (EditText) view
                .findViewById(R.id.managepromotionfragment_promPrice_edittext);
        quantityEditText = (EditText) view
                .findViewById(R.id.managepromotionfragment_quantity_edittext);
        descriptionEditTextHE = (EditText) view
                .findViewById(R.id.managepromotionfragment_description_edittext_he);
        descriptionEditTextEN = (EditText) view
                .findViewById(R.id.managepromotionfragment_description_edittext_en);
        promBeginSelector = (EditText) view
                .findViewById(R.id.managepromotionfragment_promBegin_edittext);
        promNameEditTextHE = (EditText) view
                .findViewById(R.id.managepromotionfragment_promName_edittext_he);
        promNameEditTextEN = (EditText) view
                .findViewById(R.id.managepromotionfragment_promName_edittext_en);
        promImageImageView = (ImageView) view
                .findViewById(R.id.managepromotionfragment_promImage_imageview);
        activationSwitchButton = (SwitchButton) view
                .findViewById(R.id.managepromotionfragment_isActive_toggleButton);
        saveButton = (Button) view
                .findViewById(R.id.managepromotionfragment_save_button);
        regPriceTextview = (TextView) view
                .findViewById(R.id.managepromotionfragment_regPrice_textview);
        promNameTextview = (TextView) view
                .findViewById(R.id.managepromotionfragment_promName_textview);
        promPriceTextview = (TextView) view
                .findViewById(R.id.managepromotionfragment_promPrice_textview);
        quantityTextview = (TextView) view
                .findViewById(R.id.managepromotionfragment_quantity_textview);
        promBeginTextview = (TextView) view
                .findViewById(R.id.managepromotionfragment_promBegin_textview);
        promDescTextview = (TextView) view
                .findViewById(R.id.managepromotionfragment_description_textview);
        isActiveTextview = (TextView) view
                .findViewById(R.id.managepromotionfragment_isActive_textview);
        previewButton = (Button) view
                .findViewById(R.id.managepromotionfragment_preview_button);
        mainLayout = (LinearLayout) view
                .findViewById(R.id.managepromotionfragment_main_layout);
        paramsLayout = (LinearLayout) view
                .findViewById(R.id.params_layout);
        ic_left_arrow = (ImageView) view
                .findViewById(R.id.ic_left_arrow);
        ic_right_arrow = (ImageView) view
                .findViewById(R.id.ic_right_arrow);

        changeResLang(currentLang);

        previewButton.setOnClickListener(this);
        ic_right_arrow.setOnClickListener(this);
        ic_left_arrow.setOnClickListener(this);
        langButton.setOnClickListener(this);
        picBranchTextView.setOnClickListener(this);
        picPromoTextView.setOnClickListener(this);
        saveButton.setOnClickListener(this);
        editPictureButton.setOnClickListener(this);
        promBeginSelector.setOnClickListener(this);
        quantityEditText.setOnClickListener(this);
//		activationSwitchButton.setOnClickListener(this);

        init();
    }

    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub
        switch (v.getId()) {
//		case R.id.managepromotionfragment_edit_button:
//			handleEditMode(isOnEditMode);
//			break;
            case R.id.managepromotionfragment_lang_button:
                initLanguageUI();
                break;
            case R.id.ic_left_arrow:
                currentIndex--;
                promotionTravel(currentIndex);
                break;
            case R.id.ic_right_arrow:
                currentIndex++;
                promotionTravel(currentIndex);
                break;
            case R.id.managepromotionfragment_preview_button:
                savePromotionForPreview();
                DialogHelper.promotionDialog(getFragmentManager(), bussines, promotion2, image);
                break;
            case R.id.managepromotionfragment_picBranch_textview:
                if (!picBranchTextView.getText().toString().equals(getString(R.string.no_bussiness))) {
                    if (mBranchPopupMenu!=null)
                        mBranchPopupMenu.show();
                    else
                        Log.e("ManagePromotionFragment","mBranchPopupMenu = NULL...");
                }

                break;
            case R.id.managepromotionfragment_picPromotion_textview:
                if (!picBranchTextView.getText().toString().equals(getString(R.string.no_bussiness))) {
                    if (mPromotionPopupMenu!=null)
                    mPromotionPopupMenu.show();
                    else
                        Log.e("ManagePromotionFragment","mPromotionPopupMenu = NULL...");
                }
                break;
            case R.id.managepromotionfragment_editPicture_button:
                openDialogeWhenClickOnProfileImage();
                break;
            case R.id.managepromotionfragment_promBegin_edittext:
                TimePickerFragment(promBeginSelector);
                break;
            case R.id.managepromotionfragment_quantity_edittext:
                showQuantityList(quantityEditText, isOnEditMode);
                break;
            case R.id.managepromotionfragment_save_button:
                saveData();
                break;
            default:
                break;
        }
    }

    private void promotionTravel(int index) {
        initData(promotionArray.get(index), currentLang);
    }

    private void init() {
        getBussinessAsManager();
    }

    @Override
    public void onResume() {
        super.onResume();
        promotionCallback.onPromotionsResumed();
    }
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        // try to set the activity as the listener:
        try {
            promotionCallback = (ManageActivity) activity;
        } catch (ClassCastException e) {
            // the activity does not implement FragmentListListener!
            throw new ClassCastException(activity.toString()
                    + " must implement SearchCustomerFragmentListener!");
        }
    }

    @Override
    public void onPause() {
        // TODO Auto-generated method stub
        super.onPause();
        GeneralHelper.closeKeyboard(getActivity(), regPriceEditText,
                promPriceEditText, quantityEditText, descriptionEditTextEN,
                descriptionEditTextHE, promNameEditTextHE, promNameEditTextEN);
    }

    public void TimePickerFragment(TextView textView) {
        DialogFragment newFragment = new TimePickerFragment(textView);
        newFragment.show(getFragmentManager(), "timePicker");
    }


    private void showQuantityList(final EditText editText, boolean isOnEditMode) {

        final String[] quantities = {"1", "2", "3", "4", "5", "6", "7", "8",
                "9", "10"};

        quantitiesWindow = new ListPopupWindow(getActivity());
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),
                R.layout.item_text_spinner, quantities);
        quantitiesWindow.setAdapter(adapter);
        quantitiesWindow.setAnchorView(editText);
        quantitiesWindow.setHeight(400);
        quantitiesWindow.setModal(true);
        quantitiesWindow.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                editText.setText(quantities[position]);
                quantitiesWindow.dismiss();
            }
        });

        if (!isOnEditMode) {
            quantitiesWindow.show();
        }
    }

    private void openDialogeWhenClickOnProfileImage() {

        final int CAMERA = 0;
        final int GALLERY = 1;
        String[] options = {getString(R.string.camera), getString(R.string.gallery)};
        DialogHelper.showDialogChooser(this, options, new DialogCallback() {
            protected void onDialogOptionPressed(int which,
                                                 android.support.v4.app.DialogFragment dialog) {
                switch (which) {
                    case CAMERA:
                        GeneralHelper.openCamera(ManagePromotionFragment.this);
                        break;
                    case GALLERY:
                        GeneralHelper.openGallery(ManagePromotionFragment.this);
                        break;
                }
            }

            ;
        });
    }


    private void savePromotionForPreview() {

        try {
            promotion2.setTitleEN(promNameEditTextEN.getText().toString());
        } catch (Exception e) {
            e.printStackTrace();
            promotion2.setTitleHE(promNameEditTextHE.getText().toString());

        }
        try {
            promotion2.setTitleHE(promNameEditTextHE.getText().toString());
        } catch (Exception e) {
            e.printStackTrace();
            promotion2.setTitleEN(promNameEditTextEN.getText().toString());

        }


        promotion2.setStatus(activationSwitchButton.isChecked() ? 1 : 0);

        promotion2.setPrice(Double.parseDouble(regPriceEditText.getText().toString().replaceAll("[^\\d.]", "")));
        promotion2.setDiscountPrice(Double.parseDouble(promPriceEditText.getText().toString().replaceAll("[^\\d.]", "")));
        promotion2.setNumUnits(Integer.parseInt(quantityEditText.getText().toString()));
        promotion2.setTimeStart(DateUtil.stringHour2Millisecond(promBeginSelector.getText().toString()));
        promotion2.setDescriptionEN(descriptionEditTextEN.getText().toString() + getTextFromEditextInView());
        promotion2.setDescriptionHE(descriptionEditTextHE.getText().toString() + getTextFromEditextInView());

    }

    private void saveData() {

        Promotion promotion = promotion2;
        promotion.setTitleEN(promNameEditTextEN.getText().toString());
        promotion.setTitleHE(promNameEditTextHE.getText().toString());
        promotion.setStatus(activationSwitchButton.isChecked() ? 1 : 0);
        promotion.setImage(imageBase64String);
        promotion.setPrice(Double.parseDouble(regPriceEditText.getText().toString().replaceAll("[^\\d.]", "")));
        promotion.setDiscountPrice(Double.parseDouble(promPriceEditText.getText().toString().replaceAll("[^\\d.]", "")));
        promotion.setNumUnits(Integer.parseInt(quantityEditText.getText().toString()));
        promotion.setTimeStart(DateUtil.stringHour2Millisecond(promBeginSelector.getText().toString()));
        System.out.println("promBeginSelector.getText() - " + promBeginSelector.getText());


        promotion.setDescriptionEN(descriptionEditTextEN.getText().toString() + getTextFromEditextInView());
        promotion.setDescriptionHE(descriptionEditTextHE.getText().toString() + getTextFromEditextInView());
        saveDataQuery(promotion,true);
    }

    private void initData(Promotion promotion, String lang) {
        ic_left_arrow.setVisibility(View.VISIBLE);
        ic_right_arrow.setVisibility(View.VISIBLE);
        if (currentIndex <= 0) {
            ic_left_arrow.setVisibility(View.INVISIBLE);
        }
        if (currentIndex >= promotionArray.size() - 1) {
            ic_right_arrow.setVisibility(View.INVISIBLE);
        }
        Log.e("lang", "langX " + currentLang);
        if (lang.matches("HE")) {
            descriptionEditTextHE.setText(promotion.getDescriptionHE());
            promNameEditTextHE.setText(promotion.getTitleHE());
            promNameEditTextHE.setVisibility(View.VISIBLE);
            promNameEditTextEN.setVisibility(View.GONE);
            descriptionEditTextHE.setVisibility(View.VISIBLE);
            descriptionEditTextEN.setVisibility(View.GONE);
        } else if (lang.matches("EN")) {
            descriptionEditTextEN.setText(promotion.getDescriptionEN());
            promNameEditTextEN.setText(promotion.getTitleEN());
            promNameEditTextEN.setVisibility(View.VISIBLE);
            promNameEditTextHE.setVisibility(View.GONE);
            descriptionEditTextEN.setVisibility(View.VISIBLE);
            descriptionEditTextHE.setVisibility(View.GONE);
        }
        if (promotion.getTimeStartString() != null) {
            promBeginSelector.setText(changeStringFormat(
                    promotion.getTimeStartString(), false));
        } else {
            promBeginSelector.setText(nowString());
        }
        promPriceEditText.setText(promotion.getDiscountPrice() + "");
        regPriceEditText.setText(promotion.getPrice() + "");
        quantityEditText.setText(promotion.getNumUnits() + "");
        activationSwitchButton.setChecked(promotion.getStatus() == 0 ? false : true);
        ImageLoader2.ByUrl(promotion.getImage(), promImageImageView);
        addParamsFields();
        promotion2 = promotion;
    }

    private void initBranchMenus(final String[] branchArray) {

        mBranchPopupMenu = new PopupMenu(getActivity(), picBranchTextView);

        Menu branchMenu = mBranchPopupMenu.getMenu();

        branchMenu.clear();
        for (int i = 0; i < branchArray.length; i++) {
            branchMenu.add(branchArray[i]);
        }

        if (branchMenu.size() == 0) {
            picBranchTextView.setAlpha(0.4f);
        }

        mBranchPopupMenu.setOnMenuItemClickListener(new OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                picBranchTextView.setText(item.getTitle());
                onBranchesSelected(item.getTitle().toString());
                return true;
            }

        });

    }

    private void initPromoMenus(final String[] promoArray) {

        mPromotionPopupMenu = new PopupMenu(getActivity(), picPromoTextView);

        Menu promoMenu = mPromotionPopupMenu.getMenu();

        promoMenu.clear();
        for (int i = 0; i < promoArray.length; i++) {
            promoMenu.add(promoArray[i]);
        }

        if (promoMenu.size() == 0) {
            picPromoTextView.setAlpha(0.4f);
            picPromoTextView.setText(resources.getString(R.string.no_promotion));
            mainLayout.setVisibility(View.GONE);
        } else {
            picPromoTextView.setAlpha(1f);
            mainLayout.setVisibility(View.VISIBLE);
        }

        mPromotionPopupMenu.setOnMenuItemClickListener(new OnMenuItemClickListener() {


            @Override
            public boolean onMenuItemClick(MenuItem item) {

                if (item.toString()!=null)
                onPromotionSelected(item.toString());

                return true;
            }

        });

    }

    public static String changeStringFormat(String Date, boolean isLongTail) {
        SimpleDateFormat myFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat fromUser = new SimpleDateFormat("dd/MM/yyyy");
        String reformattedStr = "";
        try {
            if (isLongTail) {
                reformattedStr = fromUser.format(myFormat.parse(Date));
            } else {
                reformattedStr = myFormat.format(fromUser.parse(Date));
            }

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return reformattedStr;

    }

    public static Date convertStringToDate(String stringDate) {
        System.out.println("stringDate " + stringDate);
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = null;
        try {
            date = format.parse(stringDate);
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return date;
    }

    private void saveDataQuery(Promotion promotion, final boolean clicked) {
        showLoadingDialog();
        APIManager.api.savPromotion(promotion.getID(),
                promotion.getProductID(),
                promotion.getStatus(),
                promotion.getTitleHE(),
                promotion.getTitleEN(),
                promotion.getDescriptionHE(),
                promotion.getDescriptionEN(),
                promotion.getDiscountPrice(),
                promotion.getNumUnits(),
                promotion.getImage(),
                promotion.getTimeStart().getTime(),
                new Callback<DataCheck>() {

                    @Override
                    public void success(DataCheck arg0, Response arg1) {
                        if (arg0.error == 0) {
                            if (getArguments() != null) {
                                dismissLoadingDialog();
                                if (clicked){
                                    getActivity().finish();
                                }else {
                                    init();
                                }
                            } else {
                                Toast.makeText(getActivity(), getString(R.string.promotion_save), Toast.LENGTH_SHORT).show();
                                dismissLoadingDialog();
                                if (clicked){
                                    getActivity().finish();
                                }
                            }
                        } else {
                            dismissLoadingDialog();
                            Toast.makeText(getActivity(), arg0.errdesc, Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void failure(RetrofitError arg0) {
                        Toast.makeText(getActivity(), resources.getString(R.string.fail_to_load_data), Toast.LENGTH_SHORT).show();
                        dismissLoadingDialog();
                    }
                });
    }

    private void getBussinessAsManager() {
        showLoadingDialog();
        APIManager.api.getPromotionsAsManager(UserManager.getAppUser().ID, 1, 1, BusinessRepository.makeUserParams(), new Callback<BusinessAsManager>() {

            @Override
            public void failure(RetrofitError arg0) {
                dismissLoadingDialog();
                Toast.makeText(getActivity(), resources.getString(R.string.fail_to_load_data), Toast.LENGTH_SHORT).show();
                dismissLoadingDialog();
            }

            @Override
            public void success(BusinessAsManager arg0, Response arg1) {
                bussinessArray = arg0;
                int bussinessCount = bussinessArray.data.businesses.size();
                String[] branchArray = new String[bussinessCount];
                for (int i = 0; i < bussinessCount; i++) {
                    if(!currentLang.matches("HE")){
                        branchArray[i] = bussinessArray.data.businesses.get(i).NameEN;
                    }else{
                        branchArray[i] = bussinessArray.data.businesses.get(i).NameHE;
                    }

                    promotion = bussinessArray.data.businesses.get(i).Promotions;
                }

                if (getArguments() != null) {
                    System.out.println("getArguments() != null");
                    long[] data = getArguments().getLongArray("LongArr");
                    if (data == null) {
                        System.out.println("data= null");
                        return;
                    }
                    System.out.println("businesses id:" + data[0]);
                    System.out.println("product id:" + data[1]);

                    for (int i = 0; i < bussinessCount; i++) {
                        if (bussinessArray.data.businesses.get(i).ID == data[0]) {
                            System.out.println("matches buss id:" + bussinessArray.data.businesses.get(i).ID + "/" + data[0]);
                            ArrayList<Promotion> promotions = bussinessArray.data.businesses.get(i).Promotions;
                            for (int k = 0; k < promotions.size(); k++) {
                                System.out.println("matches Product id:" + promotions.get(k).getProductID() + "/" + data[1]);
                                if (promotions.get(k).getProductID() == data[1]) {
                                    System.out.println("promotions.get(k).getID() " + promotions.get(k).getID());
                                    promotionId = promotions.get(k).getID();
                                    picBranchTextView.setText(branchArray[i]);
                                    initBranchMenus(branchArray);
                                    onBranchesSelected(branchArray[i]);
                                    foundPromotion = true;
                                    break;
                                }
                            }
                        }
                    }

                    if (!foundPromotion) {
                        Promotion promotion = new Promotion();
                        promotion.setBusinessID(data[0]);
                        promotion.setProductID(data[1]);
                        promotion.setStatus(1);

                        Calendar ca = Calendar.getInstance();
                        ca.setTimeInMillis(System.currentTimeMillis()+60*60*1000);
                        promotion.setTimeStart(new Date()/*ca.getTime())*/);
//                        System.out.println("niv promotion time = "+ca.getTime());

                        dismissLoadingDialog();
                        saveDataQuery(promotion,false);

                    }

                } else if (bussinessCount != 0 && picBranchTextView!=null) {
                    picBranchTextView.setText(branchArray[0]);
                    initBranchMenus(branchArray);
                    onBranchesSelected(branchArray[0]);
                } else {
                    picBranchTextView.setText(getString(R.string.no_bussiness));
                    picBranchTextView.setEnabled(false);
                    picBranchTextView.setAlpha(0.4f);
                    picPromoTextView.setText(getString(R.string.no_promotion));
                    picPromoTextView.setEnabled(false);
                    picPromoTextView.setAlpha(0.4f);
                }
                dismissLoadingDialog();
            }
        });
    }

    private void changeResLang(String lang) {
        Configuration conf = getResources().getConfiguration();
        conf.locale = new Locale(lang);
        DisplayMetrics metrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(metrics);
        resources = new Resources(getActivity().getAssets(), metrics, conf);
    }

    private void initLanguageUI() {
        if (currentLang.matches("HE")) {
            currentLang = "EN";
        } else if (currentLang.matches("EN")) {
            currentLang = "HE";
        }
        Log.e("lang", "lang " + currentLang);
        changeResLang(currentLang);
        langButton.setText(resources.getString(R.string.language));
        promNameTextview.setText(resources.getString(R.string.promotion_name));
        editPictureButton.setText(resources.getString(R.string.edit_image));
        regPriceTextview.setText(resources.getString(R.string.regular_price));
        promPriceTextview.setText(resources.getString(R.string.sale_price));
        quantityTextview.setText(resources.getString(R.string.quantity));
        promBeginTextview.setText(resources.getString(R.string.the_beginning_of_operation));
        promDescTextview.setText(resources.getString(R.string.description_of_operation));
        isActiveTextview.setText(resources.getString(R.string.active_campaign));

        saveButton.setText(resources.getString(R.string.save));
        previewButton.setText(resources.getString(R.string.preview));
        if (promotion2 != null) {
            initData(promotion2, currentLang);
        }
    }

    int promotionPos = 0;
    private void onBranchesSelected(String Branch) {
        bussines = getBussinessByTitle(Branch);
        String[] promoArray = new String[1];
        if(bussines.Promotions != null){
            promoArray = new String[bussines.Promotions.size()];
        }
        for (int j = 0; j < bussines.Promotions.size(); j++) {
            String title= bussines.Promotions.get(j).ProductTitle;
            System.out.println("niv "+title);
            if (title==null){
                title= bussines.Promotions.get(j).TitleEN;
                System.out.println("niv 1"+title);
                if (title==null){

                    title= bussines.Promotions.get(j).TitleHE;
                    System.out.println("niv 2"+title);
                }
            }
//            promoArray[j] = bussines.Promotions.get(j).ProductTitle;
            promoArray[j] = title;
            if(bussines.Promotions.get(j).getID() == promotionId){
                promotionPos = j;
            }
//				 if(currentLang.matches("HE")){
//					 	promoArray[j] = bussines.Promotions.get(j).TitleHE;
//					}else if(currentLang.matches("EN")){
//						promoArray[j] = bussines.Promotions.get(j).TitleEN;
//					}
        }

        promoArray = new HashSet<String>(Arrays.asList(promoArray))
                .toArray(new String[0]);
        initPromoMenus(promoArray);

         if (bussines.Promotions.size() != 0) {
            onPromotionSelected(promoArray[promotionPos]);

        }


    }

    private void onPromotionSelected(String item) {
        picPromoTextView.setText(item);
        if(item != null){
            promotionArray = getPromotionsByTitle(item);
            if(promotionPos < promotionArray.size()){
                initData(promotionArray.get(promotionPos), currentLang);
            } else if(promotionArray.size() > 0) {
                initData(promotionArray.get(0), currentLang);
            }
        }else{
            Toast.makeText(getActivity(), "Oops, Item not defined.", Toast.LENGTH_SHORT).show();
        }



    }

    private ArrayList<Promotion> getPromotionsByTitle(String promotion) {
        ArrayList<Promotion> list = new ArrayList<Promotion>();
        for (int i = 0; i < bussines.Promotions.size(); i++) {
            String title= bussines.Promotions.get(i).ProductTitle;
            System.out.println("niv "+title);
            if (title==null){
                title= bussines.Promotions.get(i).TitleEN;
                System.out.println("niv 1"+title);
                if (title==null){
                    title= bussines.Promotions.get(i).TitleHE;
                    System.out.println("niv 2"+title);
                }
            }

            if(title == null){
                Toast.makeText(getActivity(), "Oops, Promotions not Correctly defined.", Toast.LENGTH_SHORT).show();
                return new ArrayList<Promotion>();
            }
            if (title.matches(promotion)
//						|| business.Promotions.get(i).TitleHE.matches(title)
                    ) {
                list.add(bussines.Promotions.get(i));
            }
        }
        return list;

    }

    private void addParamsFields() {
        paramsLayout.removeAllViews();
        View v;
        for (String key : SettingManager.getSetting().params.keySet()) {
            if (currentLang.matches("HE")) {
                v = getActivity().getLayoutInflater().inflate(R.layout.item_param_edit_text, null);
            } else {
                v = getActivity().getLayoutInflater().inflate(R.layout.item_param_edit_text_en, null);
            }
            TextView params = (TextView) v.findViewById(R.id.params_text_title);
            EditText paramsOutput = (EditText) v.findViewById(R.id.params_edittext_title);
            params.setText(SettingManager.getSetting().params.get(key));
            paramsLayout.addView(v);

        }

    }

    public Business getBussinessByTitle(String title) {
        for (int i = 0; i < bussinessArray.data.businesses.size(); i++) {
            String name;
            if(!currentLang.matches("HE")){
                name = bussinessArray.data.businesses.get(i).NameEN;
            }else{
                name = bussinessArray.data.businesses.get(i).NameHE;
            }
            if (name.matches(title)) {
                return bussinessArray.data.businesses.get(i);
            }
        }
        return null;
    }

    public Promotion getPromotionByTitle(String title, Business business) {
        for (int i = 0; i < business.Promotions.size(); i++) {
//				if(business.Promotions.get(i).TitleEN.matches(title) || business.Promotions.get(i).TitleHE.matches(title)){
//					return business.Promotions.get(i);
//				}
            if (business.Promotions.get(i).ProductID == Long.parseLong(title)) {
                return business.Promotions.get(i);
            }
        }
        return null;
    }

    private String getTextFromEditextInView() {
        String string = "";
        for (int i = 0; i < paramsLayout.getChildCount(); i++) {
            View v = paramsLayout.getChildAt(i);
            for (int j = 0; j < ((ViewGroup) v).getChildCount(); j++) {
                if (((ViewGroup) v).getChildAt(j) instanceof EditText) {
                    EditText editText = (EditText) ((ViewGroup) v).getChildAt(j);
                    string += "\n" + editText.getText().toString();
                }
            }

        }

        return string;

    }

    private String nowString() {
        DateFormat dateFormat = new SimpleDateFormat("HH:mm");
        Date date = new Date();
        return dateFormat.format(date);
    }

}
