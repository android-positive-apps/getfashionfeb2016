package com.positiveapps.getFashion.fragments;

import java.util.ArrayList;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;

import com.positiveapps.getFashion.R;
import com.positiveapps.getFashion.jsonmodels._Business;
import com.positiveapps.getFashion.util.ActivityStarter;

public abstract class AdminFragment extends BaseFragment {

	public static boolean mIsFragOn = false;

	protected abstract void populateUI(_Business mCurrentRestaurant);

	public static final int REQUEST_CODE_CAMERA = 990;
	public static final int REQUEST_CODE_CHOOSE_FILE = 991;
	private ProgressDialog mProgress;
	private ArrayList<_Business> mRestaurantsForShiftManager;
	private _Business mCurrentRestaurant;

	private String startUpRestaurantId = "";

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {

		super.onViewCreated(view, savedInstanceState);

		initUsersShiftManagerBusinesses();

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		return super.onCreateView(inflater, container, savedInstanceState);
	}

	@Override
	public void onStart() {

		super.onStart();
	}

	@Override
	public void onResume() {
		super.onResume();
		mIsFragOn = true;

	}

	@Override
	public void onPause() {
		super.onPause();
		mIsFragOn = false;
	}

	public void showPhotoGetChooserDialog() {
		String promptOptions[] = new String[2];
		promptOptions[0] = getString(R.string.take_picture_with_camera);
		promptOptions[1] = getString(R.string.choose_picture_from_phone);

		AlertDialog promptDialog = new AlertDialog.Builder(getActivity()).setItems(promptOptions, new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {

				switch (which) {
				case 0: // camera
					ActivityStarter.openCameraForResult(AdminFragment.this, REQUEST_CODE_CAMERA);
					break;
				case 1: // choose from phone
					ActivityStarter.openGallery(AdminFragment.this, REQUEST_CODE_CHOOSE_FILE);
					break;
				}

				dialog.dismiss();
			}
		}).create();

		promptDialog.show();
	}

	protected void initUsersShiftManagerBusinesses() {

		showProgressDialog(R.string.loading);

		mRestaurantsForShiftManager = new ArrayList<>();

//		final ArrayList<String> shiftManagerIn = App.appUser.getBuisnessManagerAt();

//		App.Log("shift manage in: " + shiftManagerIn.size());

	}

	public boolean handleDetach() {
		return false;
	}

	protected void showProgressDialog(int stringRecource) {
		mProgress = ProgressDialog.show(getActivity(), null, getString(stringRecource), true, true);
	}

	protected void dismissProgressDialog() {
		if (mProgress != null) {
			mProgress.dismiss();
			mProgress = null;
		}

	}

	protected _Business getRestaurantById(String id) {
		for (_Business r : mRestaurantsForShiftManager) {
			if (r.getID().equals(id)) {
				return r;
			}
		}

		return null;
	}

	protected void initRestaurantOnClickChooser() {
		TextView txtChooser = null;//(TextView) getView().findViewById(R.id.txtAnotherRestaurant);

		if (txtChooser == null)
			return;

		txtChooser.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				final ArrayList<String> restaurantIds = new ArrayList<>();
				ArrayList<String> restaurantNames = new ArrayList<>();

				for (_Business r : mRestaurantsForShiftManager) {
					restaurantNames.add(r.getName());
					restaurantIds.add(r.getID());
				}

				String[] items = (String[]) restaurantNames.toArray(new String[0]);

				AlertDialog d = new AlertDialog.Builder(getActivity()).setItems(items, new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						String restaurantId = restaurantIds.get(which);
						mCurrentRestaurant = getRestaurantById(restaurantId);

						populateUI(mCurrentRestaurant);

//						App.Log("chose restaurant: " + mCurrentRestaurant.getName());
						dialog.dismiss();
						// populateUI();
					}
				}).create();

				d.show();

			}
		});

	}

	protected _Business getCurrentRestaurant() {
		return mCurrentRestaurant;
	}

	public void setCurrentRestaurant(_Business mCurrentRestaurant) {
		this.mCurrentRestaurant = mCurrentRestaurant;
	}

	public void setStartUpRestaurantId(String id) {
		startUpRestaurantId = id;
	}
}
