package com.positiveapps.getFashion.fragments.manage;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.v7.widget.ListPopupWindow;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.PopupMenu.OnMenuItemClickListener;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.positiveapps.getFashion.R;
import com.positiveapps.getFashion.adapters.CategotySwitchAdapter;
import com.positiveapps.getFashion.backend.APIManager;
import com.positiveapps.getFashion.backend.Categories;
import com.positiveapps.getFashion.backend.Prefs;
import com.positiveapps.getFashion.backend.SettingManager;
import com.positiveapps.getFashion.backend.UserManager;
import com.positiveapps.getFashion.dialogs.DialogCallback;
import com.positiveapps.getFashion.fragments.BaseFragment;
import com.positiveapps.getFashion.jsonmodels.Business;
import com.positiveapps.getFashion.jsonmodels.Category;
import com.positiveapps.getFashion.jsonmodels.Product;
import com.positiveapps.getFashion.jsonmodels.apiresponse.BusinessAsManager;
import com.positiveapps.getFashion.jsonmodels.apiresponse.DataCheck;
import com.positiveapps.getFashion.util.BitmapUtil;
import com.positiveapps.getFashion.util.DialogHelper;
import com.positiveapps.getFashion.util.GeneralHelper;
import com.positiveapps.getFashion.util.ImageLoader2;
import com.positiveapps.getFashion.util.TextUtil;
import com.positiveapps.getFashion.views.SwitchButton;

import java.io.File;
import java.util.ArrayList;
import java.util.Locale;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class ManageProductsFragment extends BaseFragment implements OnClickListener {

	private String imageBase64String;
	private LinearLayout productContainer;
	private Button addProductButton;
	private ListPopupWindow listPopupWindow;
	private ScrollView scrollView;
	ImageView prodTempImage;
	private TextView picBranch;
	
	private BusinessAsManager bussinessArray;
	private PopupMenu mBranchPopupMenu;
	
	private String Image;
	String[] branchArray;
	String[] categoriesArray;
	private Button languageButton;
	private TextView noProductsTxt;
	private String currentLang = Prefs.getAppLanguage().toUpperCase();
	private int positionBranch;
	private Resources resources;
	private boolean isHebrew;
	private long bussinessId;
	private FrameLayout noProductLayout;
	private Button prodSave;

	public ManageProductsFragment() {
	}

	public static ManageProductsFragment newInstance() {
		ManageProductsFragment instance = new ManageProductsFragment();
		return instance;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.fragment_manage_product, container,
				false);
		return v;
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		
		productContainer = (LinearLayout)view.findViewById(R.id.products_container);
		addProductButton = (Button)view.findViewById(R.id.add_product_button);
		languageButton = (Button)view.findViewById(R.id.languageButton);
		picBranch = (TextView)view.findViewById(R.id.picBranch_textview);
		noProductsTxt = (TextView)view.findViewById(R.id.noProductsTxt);
		scrollView = ((ScrollView) view.findViewById(R.id.scrollView1));

		picBranch.setOnClickListener(this);
		addProductButton.setOnClickListener(this);
		languageButton.setOnClickListener(this);
		
		initCategories();
		getBussinessAsManager();
		changeResLang(currentLang);
		initLang(currentLang);
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);

		Bitmap image = null;
		if (resultCode != -1) {
			return;
		}
//		prodSave.setText(getResources().getString(R.string.save));
		switch (requestCode) {
		case GeneralHelper.GALLARY_REQUEST_CODE:
			image = ImageLoader2.byUri(ImageLoader2.IMAGE_TYPE_FROM_GALLERAY,
					data.getData(), prodTempImage);
			break;
		case GeneralHelper.CAMERA_REQUEST_CODE:
			File file = new File(Environment.getExternalStorageDirectory()
					.getPath(), GeneralHelper.CAMERA_PROFILE_PATH);
			image = BitmapUtil.loadImageIntoByStringPath(
					file.getAbsolutePath(), prodTempImage, 300, 2,
					ImageLoader2.DEAFULT_PLACE_HOLDER);
			break;
		default:
			break;
		}
		imageBase64String = GeneralHelper.getProfileImageAsBase64(image);
	}
	private void productItem(final Product product, String lang) {
		
		final View view = getActivity().getLayoutInflater().inflate(R.layout.item_product, null);
		view.setTag(0L);
		final EditText prodNameEditText = (EditText) view
				.findViewById(R.id.managepromotionfragment_prodName_edittext);
		final EditText prodDescriptionEditText = (EditText) view
				.findViewById(R.id.managepromotionfragment_prodDescription_edittext);
		final ImageView prodImageImageView = (ImageView) view
				.findViewById(R.id.managepromotionfragment_prodImage_imageview);
		final EditText prodPriceEditText = (EditText) view
				.findViewById(R.id.managepromotionfragment_prodPrice_edittext);
		final TextView selectCategoryTextView = (TextView) view
				.findViewById(R.id.managepromotionfragment_selectCategory_textview);
		Button prodRemove = (Button) view
				.findViewById(R.id.managepromotionfragment_prodRemove_button);
		prodSave = (Button) view
				.findViewById(R.id.managepromotionfragment_prodSave_button);
		final TextView prodDescriptionTextView = (TextView) view
				.findViewById(R.id.managepromotionfragment_prodDescription_textview);
		final TextView prodPriceTextView = (TextView) view
				.findViewById(R.id.managepromotionfragment_prodPrice_textview);
//		final TextView prodNameTextView = (TextView) view
//				.findViewById(R.id.managepromotionfragment_prodName_textview);
		final SwitchButton availableForSale = (SwitchButton) view
				.findViewById(R.id.managepromotionfragment_available_for_sale);
		final Button promote = (Button)view.findViewById(R.id.promote);

		onTextChangeListener(prodNameEditText, prodSave);
		onTextChangeListener(prodDescriptionEditText, prodSave);
		onTextChangeListener(prodPriceEditText, prodSave);
		availableForSale.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				// TODO Auto-generated method stub
				prodSave.setText(resources.getString(R.string.save));
			}
		});
				
		if(product != null){ // init Product
			view.setTag(product.getID());
			if(lang.matches("HE")){
				prodNameEditText.setText(product.getTitleHE());
				prodDescriptionEditText.setText(product.getDescriptionHE());
			}else if(lang.matches("EN")){
				prodNameEditText.setText(product.getTitleEN());
				prodDescriptionEditText.setText(product.getDescriptionEN());
			}
			
			prodPriceEditText.setText(product.getPrice() + "");
//			selectCategoryTextView.setText(product.getCategoryName());
			ImageLoader2.ByUrl(product.getImage(), prodImageImageView);
			availableForSale.setChecked(product.isAvailableForSale());
			prodDescriptionTextView.setText(resources.getString(R.string.item_description));
			prodPriceTextView.setText(resources.getString(R.string.price));
//			prodNameTextView.setText(resources.getString(R.string.item_name));
			selectCategoryTextView.setText(resources.getString(R.string.select_a_category));
			prodRemove.setText(resources.getString(R.string.remove));
			prodSave.setText(resources.getString(R.string.save));

		}
		
		prodImageImageView.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				prodTempImage = prodImageImageView;
				openDialogeWhenClickOnProfileImage();
			}
		});

		promote.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (TextUtil.baseFieldsValidation(prodNameEditText, prodPriceEditText)) {

					getFragmentManager().beginTransaction().replace(R.id.manageLayout, ManagePromotionFragment.newInstance(bussinessId, (long) view.getTag())).commit();
				}else {
					Toast.makeText(getContext(),"please fill in details and don't forget to save",Toast.LENGTH_LONG).show();

				}
			}
		});

		selectCategoryTextView.setOnClickListener(new OnClickListener() {
			
			private CategotySwitchAdapter adapter;

			@Override
			public void onClick(View v) {
				/*
				* niv-change
				* */


				final Dialog dialog = new Dialog(getActivity(), R.style.myBackgroundStyle);
				final View view = getActivity().getLayoutInflater().inflate(R.layout.dialog_categories, null);
				Button button = (Button) view.findViewById(R.id.on_choose_categories_BT);
				GridView gridView = (GridView) view.findViewById(R.id.categories_gridView);
				if(Categories.getCategories().size() != 0){
					ArrayList<Category> categories = Categories.getRealCategories();
					if(product != null) {
						System.out.println("product.Categories " + product.Categories);
						System.out.println("categories " + categories);
						adapter = new CategotySwitchAdapter(getActivity(), categories, product.Categories);
					}else{
						adapter = new CategotySwitchAdapter(getActivity(), categories, new ArrayList<String>());
					}
					gridView.setAdapter(adapter);
					}
				button.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						selectCategoryTextView.setTag(adapter.getCategoriesArray());
						dialog.dismiss();
					}
				});
				dialog.setContentView(view);
				dialog.show();
				/*
				* niv-change
				* */
			}
		});

		prodRemove.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				showDialog((long)view.getTag(), view);
			}
		});
		
		prodSave.setOnClickListener(new OnClickListener() {
 
			@Override
			public void onClick(View v) { 

				if (TextUtil.baseFieldsValidation(prodNameEditText, prodPriceEditText)) {

					saveProduct(prodSave, isHebrew,
							(long) view.getTag(),
							bussinessId,
							prodNameEditText.getText().toString(),
							prodDescriptionEditText.getText().toString(),
							Double.parseDouble(prodPriceEditText.getText().toString().replaceAll("[^\\d.]", "")),
							(String) selectCategoryTextView.getTag(), availableForSale.isChecked() ? 1 : 0);
				}else {
					if(prodPriceEditText.getText().toString().trim().isEmpty()) {
						prodPriceEditText.setHintTextColor(Color.RED);
					}
					Toast.makeText(getContext(), getResources().getString(R.string.please_fill_in_product_details),Toast.LENGTH_LONG).show();


				}
			}
		});
	
		
		productContainer.addView(view);
		
	}
	
 

	private void openDialogeWhenClickOnProfileImage() {

		final int CAMERA = 0;
		final int GALLERY = 1;
		String[] options = {getString(R.string.camera), getString(R.string.gallery) };
		DialogHelper.showDialogChooser(this, options, new DialogCallback() {
			protected void onDialogOptionPressed(int which,
					android.support.v4.app.DialogFragment dialog) {
				switch (which) {
				case CAMERA:
					GeneralHelper.openCamera(ManageProductsFragment.this);
					break;
				case GALLERY:
					GeneralHelper.openGallery(ManageProductsFragment.this);
					break;
				}
			};
		});
	}
	
	private void initProduct() {
		// TODO Auto-generated method stub

	}
	
	public class productItem{
		long id;
		View productView;
		EditText description;
		EditText title;
		EditText price;
		TextView categoty;
		ImageView image;
		Button save;
		Button remove;
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub

		switch (v.getId()) {
		case R.id.picBranch_textview:
			mBranchPopupMenu.show();
			break;
		case R.id.add_product_button:
			if(SettingManager.getSetting().max_products == null){
				return;
			}
			int maxProducts = Integer.parseInt(SettingManager.getSetting().max_products);
			if(productContainer.getChildCount() <= maxProducts){
			productItem(null, currentLang);
			}else{
				Toast.makeText(getActivity(), getString(R.string.products_number_limited_to) + " " + maxProducts, Toast.LENGTH_LONG).show();
			}


			scrollView.post(new Runnable() {

				@Override
				public void run() {
					scrollView.fullScroll(ScrollView.FOCUS_DOWN);
				}
			});
			break;
			case R.id.languageButton:
			initLanguageUI(currentLang);
 			break;
		default:
			break;
		}
	}

//	private void initListPopupWindow(final TextView textView) {
//
//		if(Categories.getCategories() != null){
//			if(Categories.getCategories().size() != 0){
//			ArrayList<Category> categories = Categories.getRealCategories();
//			categoriesArray = new String[categories.size()];
//				for (int i = 0; i < categories.size(); i++) {
//					categoriesArray[i] = categories.get(i).Name;
//				}
//			}
//		}
//
//
//		listPopupWindow = new ListPopupWindow(getActivity());
//		ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),
//				R.layout.item_list_text, categoriesArray);
//		listPopupWindow.setAdapter(adapter);
//		listPopupWindow.setAnchorView(textView);
//		listPopupWindow.setHeight(400);
//		listPopupWindow.setModal(true);
//		listPopupWindow.setOnItemClickListener(new OnItemClickListener() {
//
//			@Override
//			public void onItemClick(AdapterView<?> parent, View view,
//					int position, long id) {
//				textView.setText(categoriesArray[position]);
//				listPopupWindow.dismiss();
//			}
//		});
//		listPopupWindow.show();
//	}
	
//	private void initBussinessListPopupWindow(String[] branches, final TextView textView) {
//
//		final String[] products = { "Camera", "Laptop", "Watch", "Smartphone",
//				"Television" };
//
//		listPopupWindow = new ListPopupWindow(getActivity());
//		ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),
//				android.R.layout.simple_list_item_1, products);
//		listPopupWindow.setAdapter(adapter);
//		listPopupWindow.setAnchorView(textView);
//		listPopupWindow.setHeight(400);
//		listPopupWindow.setModal(true);
//		listPopupWindow.setOnItemClickListener(new OnItemClickListener() {
//
//			@Override
//			public void onItemClick(AdapterView<?> parent, View view,
//					int position, long id) {
//				textView.setText(products[position]);
//				listPopupWindow.dismiss();
//			}
//		});
//		listPopupWindow.show();
//	}
	
	@Override
	public void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		/*GeneralHelper.closeKeyboard(getActivity(), regPriceEditText,
				promPriceEditText, quantityEditText, descriptionEditText,
				promNameEditText);*/
	}
	
	private void initBranchMenus(final String[] branchArray) {
 
		mBranchPopupMenu = new PopupMenu(getActivity(), picBranch);

		Menu branchMenu = mBranchPopupMenu.getMenu();

 
		branchMenu.clear();
		for (int i = 0; i < branchArray.length ; i++) {
			branchMenu.add(branchArray[i]);

		}
 
		if (branchMenu.size() == 0) {
			picBranch.setAlpha(0.4f);
		}
 
		mBranchPopupMenu.setOnMenuItemClickListener(new OnMenuItemClickListener() {
 
			@Override
			public boolean onMenuItemClick(MenuItem item) {
				picBranch.setText(item.getTitle());
				positionBranch = getPositionByTitle(branchArray, item.getTitle().toString());
				onBranchesSelected(item.getTitle().toString());
				return true;
			}

		});

	}
	
	public Business getBussinessByTitle(String title){
		for (int i = 0; i < bussinessArray.data.businesses.size(); i++) {
			String name;
			if(!currentLang.matches("HE")){
				name = bussinessArray.data.businesses.get(i).NameEN;
			}else{
				name = bussinessArray.data.businesses.get(i).NameHE;
			}
			if (name.matches(title)) {
				return bussinessArray.data.businesses.get(i);
			}
		}
		return null;
	}
	
	private void initCategories() {
		if(Categories.getCategories() != null){
			if(Categories.getCategories().size() != 0){
			ArrayList<Category> categories = Categories.getRealCategories();
			categoriesArray = new String[categories.size()];
				for (int i = 0; i < categories.size(); i++) {
					categoriesArray[i] = categories.get(i).Name;
				}
			}
		}
	}
	
	private long getCategoryIdByTitle(String title) {
		long id = 0;
		for (int i = 0; i < Categories.getCategories().size(); i++) {
			if(Categories.getCategories().get(i).Name.matches(title)){
				id = Categories.getCategories().get(i).ID;
				break;
			}
		}
		return id;
	}
	
//	private PopupMenu initCategoryMenus(final String[] categories, final TextView textView) {
//	
//		
//		
//		PopupMenu mCategoryPopupMenu = new PopupMenu(getActivity(), textView);
// 
//		Menu CategoryMenu = mCategoryPopupMenu.getMenu();
// 
//		CategoryMenu.clear();
//		for (int i = 0; i < categories.length ; i++) {
//			CategoryMenu.add(categories[i]);
//		}
// 
//		if (CategoryMenu.size() == 0) {
//			textView.setAlpha(0.4f);
//		}
// 
//		mCategoryPopupMenu.setOnMenuItemClickListener(new OnMenuItemClickListener() {
//			@Override
//			public boolean onMenuItemClick(MenuItem item) {
//				textView.setText(item.getTitle());
//				int positionCategory = getPositionByTitle(categories, item.getTitle().toString());
////				onBranchesSelected(positionBranch);
//				return true;
//			}
//
//		});
//		return mCategoryPopupMenu;
//
//	}
	

	private void onBranchesSelected(String Branch) { 
		Business business = getBussinessByTitle(Branch);
		bussinessId = business.ID;
		if(business.Products.size() == 0){
		noProductsTxt.setVisibility(View.VISIBLE);
		}else{
			noProductsTxt.setVisibility(View.GONE);
		}
		productContainer.removeAllViews();
		for (int j = 0; j < business.Products.size(); j++) {
			productItem(business.Products.get(j), currentLang);
		}
	}
	
	private void getBussinessAsManager() {
		showLoadingDialog();
		APIManager.api.getProductsAsManager(UserManager.getAppUser().ID, 1, new Callback<BusinessAsManager>() {

			@Override
			public void failure(RetrofitError arg0) {
				// TODO Auto-generated method stub
				Toast.makeText(getActivity(), "failure", Toast.LENGTH_SHORT).show();
				dismissLoadingDialog();
			}

			@Override
			public void success(BusinessAsManager arg0, Response arg1) {
				dismissLoadingDialog();

				// TODO Auto-generated method stub
				bussinessArray = arg0;
				int bussinessCount = bussinessArray.data.businesses.size();
				String[] branchArray = new String[bussinessCount];
				for (int i = 0; i < bussinessCount; i++) {
					if(!currentLang.matches("HE")){
						branchArray[i] = bussinessArray.data.businesses.get(i).NameEN;
					}else{
						branchArray[i] = bussinessArray.data.businesses.get(i).NameHE;
					}
//					ArrayList<Product> products = bussinessArray.data.businesses.get(i).Products;
//					for (int j = 0; j < products.size(); j++) {
//						productItem(products.get(j));
//					}
				}

				if (bussinessCount != 0) {
					picBranch.setText(branchArray[0]);
					positionBranch = 0;
					initBranchMenus(branchArray);
					onBranchesSelected(branchArray[0]);
				} else {
					picBranch.setText(getString(R.string.no_bussiness));
					picBranch.setEnabled(false);
					picBranch.setAlpha(0.4f);
					addProductButton.setVisibility(View.GONE);
				}
			}
		});
	}
	
	private void saveProduct(final TextView prodSave, boolean isHebrew, 
			long ID, 
			long BusinessID, 
			String TitleHE, 
			String DescriptionHE,
			double Price,
			String CategoriesArrays, int available) {
		showLoadingDialog(getString(R.string.save_product));
		System.out.println("-------------> " + isHebrew);
		if(isHebrew ){
			APIManager.api.saveProductHe(ID, BusinessID, TitleHE, DescriptionHE, Price, imageBase64String, CategoriesArrays, available, new Callback<DataCheck>() {
				@Override
				public void success(DataCheck arg0, Response arg1) {
					// TODO Auto-generated method stub
					Toast.makeText(getActivity(), getString(R.string.product_save_succesfuly) , Toast.LENGTH_SHORT).show();
					dismissLoadingDialog();
					prodSave.setText(resources.getString(R.string.saved));

					new Handler().postDelayed(new Runnable() {
						@Override
						public void run() {
							getActivity().finish();
						}
					},200);
				}

				@Override
				public void failure(RetrofitError arg0) {
					Toast.makeText(getActivity(), getString(R.string.error_saving_product), Toast.LENGTH_SHORT).show();
					dismissLoadingDialog();
				}
			});
		}else{
			APIManager.api.saveProductEn(ID, BusinessID, TitleHE, DescriptionHE, Price, imageBase64String, CategoriesArrays, available, new Callback<DataCheck>() {

				@Override
				public void success(DataCheck arg0, Response arg1) {
					// TODO Auto-generated method stub
					Toast.makeText(getActivity(), getString(R.string.product_save_succesfuly) , Toast.LENGTH_SHORT).show();
					dismissLoadingDialog();
					prodSave.setText(resources.getString(R.string.saved));

					new Handler().postDelayed(new Runnable() {
						@Override
						public void run() {
							getActivity().finish();
						}
					},200);
				}

				@Override
				public void failure(RetrofitError arg0) {
					Toast.makeText(getActivity(), getString(R.string.error_saving_product), Toast.LENGTH_SHORT).show();
					dismissLoadingDialog();
				}
			});
		}

	}
	
	private int getPositionByTitle(String[] array, String title) {
		int pos = 0;
		for (int i = 0; i < array.length; i++) {
			if(array[i].matches(title)){
				pos = i;
				break;
			}
		}
		return pos;

	}
	
	private void showDialog(final long ProductID, final View view) {
		new AlertDialog.Builder(getActivity())
				.setMessage(getString(R.string.do_you_want_to_delet_product))
				.setPositiveButton(android.R.string.yes,
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,
									int which) {
								deleteProduct(ProductID, view);
							}
						})
				.setNegativeButton(android.R.string.no,
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,
									int which) {
								// do nothing
							}
						}).setIcon(android.R.drawable.ic_dialog_alert).show();
	}
	
	
	 private void deleteProduct(long ProductID, final View view) {
		 if(ProductID == 0L){
			 productContainer.removeView(view);
			 return;
		 }
		 showLoadingDialog(getString(R.string.delet_product));
		 APIManager.api.deleteProduct(ProductID, new Callback<DataCheck>() {

			 @Override
			 public void failure(RetrofitError arg0) {
				 // TODO Auto-generated method stub
				 Toast.makeText(getActivity(), "failure", Toast.LENGTH_SHORT).show();
				 dismissLoadingDialog();
			 }

			 @Override
			 public void success(DataCheck arg0, Response arg1) {
				 // TODO Auto-generated method stub
				 if (arg0.error == 0) {
					 Toast.makeText(getActivity(), getString(R.string.delete_confirm), Toast.LENGTH_SHORT).show();
					 productContainer.removeView(view);
				 } else {
					 Toast.makeText(getActivity(), arg0.errdesc, Toast.LENGTH_SHORT).show();
				 }
				 dismissLoadingDialog();
			 }
		 });
		 
		 
	    
		}

	private void initLang(String lang){
		if(lang.matches("HE")){
			isHebrew = true;
		}else if(lang.matches("EN")){
			isHebrew = false;
		}
	}

	 
	 private void initLanguageUI(String lang) {
		 if(lang.matches("HE")){
			 currentLang = "EN";
			 isHebrew = false;
		 }else if(lang.matches("EN")){
			 currentLang = "HE";
			 isHebrew = true;
		 }
		 changeResLang(currentLang);
		 languageButton.setText(resources.getString(R.string.language));
		 onBranchesSelected(mBranchPopupMenu.getMenu().getItem(positionBranch).getTitle().toString());
	}
	 
	 private void changeResLang(String lang){
			Configuration conf = getResources().getConfiguration();
			conf.locale = new Locale(lang);
			DisplayMetrics metrics = new DisplayMetrics();
			getActivity().getWindowManager().getDefaultDisplay().getMetrics(metrics);
			resources = new Resources(getActivity().getAssets(), metrics, conf);
		}
	 
	 private void onTextChangeListener(EditText editText, final TextView textView) {
		 editText.addTextChangedListener(new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				textView.setText(resources.getString(R.string.save));
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				
			}
		});

	}
	
}