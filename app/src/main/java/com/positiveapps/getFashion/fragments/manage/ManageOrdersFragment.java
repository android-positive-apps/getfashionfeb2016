package com.positiveapps.getFashion.fragments.manage;

import android.app.Dialog;
import android.os.Bundle;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.PopupMenu.OnMenuItemClickListener;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.positiveapps.getFashion.R;
import com.positiveapps.getFashion.adapters.ReservationListAdapter;
import com.positiveapps.getFashion.backend.APIManager;
import com.positiveapps.getFashion.backend.Prefs;
import com.positiveapps.getFashion.backend.UserManager;
import com.positiveapps.getFashion.fragments.BaseFragment;
import com.positiveapps.getFashion.jsonmodels.Reservation;
import com.positiveapps.getFashion.jsonmodels.apiresponse.BusinessAsManager;
import com.positiveapps.getFashion.jsonmodels.apiresponse.DataCheck;
import com.positiveapps.getFashion.jsonmodels.apiresponse.ReservationGetList;

import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class ManageOrdersFragment extends BaseFragment implements OnClickListener {		
	
	private PopupMenu mCampaignPopupMenu;
	private PopupMenu mBranchPopupMenu;
	private TextView btn_DeleteAll;
	private TextView btn_DeleteSelected;
	private TextView btn_CampaignPicker;
	private TextView btn_BranchPicker;
	private ListView reservationList;
	// my item list
	private List<Reservation> Reservations;
	// the item list booleans
	private boolean[] ReservationsBooleans;
	// the adapter
	private ReservationListAdapter mAdapter;
	
	// the TAG
	private static final String TAG = "ManageOrdersFragment";
	private long BusinessID;
	BusinessAsManager bussinessArray;
	private int positionBranch;
	private int positionProduct;
	private String[] branchArray;
	private String[] campaignArray;
	private Dialog loadingDialog;
	private LinearLayout deleteLayout;
	
	 public ManageOrdersFragment() {
	 }
												
	 public static ManageOrdersFragment newInstance() {											
	   ManageOrdersFragment instance = new ManageOrdersFragment();											
	   return instance;											
	 }											
												
	 @Override											
	 public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {													
	   View v = inflater.inflate(R.layout.fragment_manage_orders, container, false);											
	   return v;											
	 }											
												
	 @Override											
	 public void onViewCreated(View view, Bundle savedInstanceState) {											
	   super.onViewCreated(view, savedInstanceState);	
	   
	   findviews(view);
	   populateViews();

						
	   getBussinessAsManager();

	}

	private void populateViews() {
		   // setting the views listeners

		   btn_DeleteAll		.setOnClickListener(this);
		   btn_DeleteSelected.setOnClickListener(this);
		   btn_CampaignPicker.setOnClickListener(this);
		   btn_BranchPicker.setOnClickListener(this);
	}

	private void findviews(View view) {
		   // getting the views
		   reservationList = (ListView) view.findViewById(R.id.reservationList);
		   btn_DeleteAll = (TextView) view.findViewById(R.id.btn_DeleteAll);
		   btn_DeleteSelected = (TextView) view.findViewById(R.id.btn_DeleteSelected);
		   btn_CampaignPicker = (TextView) view.findViewById(R.id.txtCampaignPicker);
		   btn_BranchPicker = (TextView) view.findViewById(R.id.txtBranchPicker);
		   deleteLayout = (LinearLayout) view.findViewById(R.id.deleteLayout);
	}

	@Override
	public void onClick(View v) {

		switch (v.getId()) {
			case R.id.btn_DeleteAll:
				deleteQuary(BusinessID, mAdapter.getAllInStringArray());
				Reservations.clear();
				mAdapter.notifyDataSetChanged();
				deleteLayout.setVisibility(View.GONE);

				break;
				
			case R.id.btn_DeleteSelected:
				deleteQuary(BusinessID, mAdapter.getSelectedInStringArray());
				mAdapter = new ReservationListAdapter(getActivity(), mAdapter.clearSelected());
				reservationList.setAdapter(mAdapter);

				break;
				
			case R.id.txtCampaignPicker:
				mCampaignPopupMenu.show();
				break;
				
			case R.id.txtBranchPicker:
				mBranchPopupMenu.show();
				break;
		}
	}
	
	///  Initializing the CampaignPicker Menu and the BranchPicker Menu
	private void initMenus(final String[] branchArray, final String[] campaignArray) {
		// initializing Popup menus
		mCampaignPopupMenu = new PopupMenu(getActivity(), btn_CampaignPicker);
		mBranchPopupMenu = new PopupMenu(getActivity(), btn_BranchPicker);
		
		// getting the menus so we can populate them
		Menu campaignMenu = mCampaignPopupMenu.getMenu();
		Menu branchMenu = mBranchPopupMenu.getMenu();
 
		// populate the Popup menus
		campaignMenu.clear();
		for (int i = 0; i < campaignArray.length ; i++) {
			campaignMenu.add(campaignArray[i]);
		}
		branchMenu.clear();
		for (int i = 0; i < branchArray.length ; i++) {
			branchMenu.add(branchArray[i]);
		}

		// handling in case one of the popup menus is empty
		if (campaignMenu.size() == 0) {
			btn_CampaignPicker.setAlpha(0.4f);
		}else{
			btn_CampaignPicker.setAlpha(1f);
		}
		if (branchMenu.size() == 0) {
			btn_BranchPicker.setAlpha(0.4f);
		}else{
			btn_CampaignPicker.setAlpha(1f);
		}
		
		mCampaignPopupMenu.setOnMenuItemClickListener(new OnMenuItemClickListener() {
			

			@Override
			public boolean onMenuItemClick(MenuItem item) {
				btn_CampaignPicker.setText(item.getTitle());
				
				
				positionProduct = getPositionByTitle(campaignArray, item.getTitle()+"");
				onProductSelected(positionBranch, positionProduct);
				return true;
			}
		});
		
		mBranchPopupMenu.setOnMenuItemClickListener(new OnMenuItemClickListener() {
			@Override
			public boolean onMenuItemClick(MenuItem item) {
				btn_BranchPicker.setText(item.getTitle());
				positionBranch = getPositionByTitle(branchArray, item.getTitle().toString());
				onBranchesSelected(positionBranch);
				return true;
			}
		});

	}
	
	private void getBussinessAsManager() {
		showLoadingDialog();
		APIManager.api.getProductsAsManager(UserManager.getAppUser().ID, 1 ,new Callback<BusinessAsManager>(){

			

			@Override
			public void failure(RetrofitError arg0) {
				// TODO Auto-generated method stub
				Toast.makeText(getActivity(), getString(R.string.failure), Toast.LENGTH_SHORT).show();
//				Toast.makeText(getActivity(), arg0.getMessage(), Toast.LENGTH_SHORT).show();

				dismissLoadingDialog();
			}

			@Override
			public void success(BusinessAsManager arg0, Response arg1) {
				// TODO Auto-generated method stub
				bussinessArray = arg0;
				int bussinessCount = bussinessArray.data.businesses.size();
				
			 
				branchArray = new String[bussinessCount];
				for (int i = 0; i < bussinessCount; i++) {
					if(Prefs.getAppLanguage().matches("en")){
						branchArray[i] = bussinessArray.data.businesses.get(i).NameEN;
					}else{
						branchArray[i] = bussinessArray.data.businesses.get(i).NameHE;
					}
				}
				if(bussinessCount != 0){
					btn_BranchPicker.setText(branchArray[0]);
					dismissLoadingDialog();
					onBranchesSelected(0);
				}else{
					btn_BranchPicker.setText(getString(R.string.no_bussiness));
					btn_BranchPicker.setEnabled(false);
					btn_BranchPicker.setAlpha(0.4f);
					btn_CampaignPicker.setText(getString(R.string.no_product));
					btn_CampaignPicker.setEnabled(false);
					btn_CampaignPicker.setAlpha(0.4f); 
					
					dismissLoadingDialog();
				}
				
				
				
 
 
//				if(bussinessCount != 0){
//					btn_CampaignPicker.setText(campaignArray[0]);
//					
//					bussinessId = bussinessArray.data.businesses.get(0).ID;
//					if(bussinessArray.data.businesses.get(0).Products.size() != 0){
//					if(bussinessArray.data.businesses.get(0).Products != null){
//					porductId = bussinessArray.data.businesses.get(0).Products.get(0).ID;
//					}
//					getReservation(bussinessId,porductId);
//					}
//				}
//					
//					initMenus(branchArray, campaignArray);	
					dismissLoadingDialog();
			}});
	}

	private void getReservation(long BusinessID, long ProductID) {
		showLoadingDialog();
		APIManager.api.getReservation(BusinessID, ProductID, new Callback<ReservationGetList>() {

			@Override
			public void failure(RetrofitError arg0) {
				// TODO Auto-generated method stub
				Toast.makeText(getActivity(), "failure", Toast.LENGTH_SHORT).show();
				dismissLoadingDialog();
			}

			@Override
			public void success(ReservationGetList result, Response arg1) {
				// TODO Auto-generated method stub
				List<Reservation> reservations = result.data.Reservations;
				if(reservations != null){
						init(reservations);
					if (reservations.size()==0){
						deleteLayout.setVisibility(View.GONE);

					}
				}
				dismissLoadingDialog();
			}
		});

	}
		 
	private void init(List<Reservation> reservations) {
		
			Reservations = reservations;
			mAdapter = new ReservationListAdapter(getActivity(), Reservations);
			reservationList.setAdapter(mAdapter);
			if(Reservations.size() != 0){
				deleteLayout.setVisibility(View.VISIBLE);
			}
			
			Log.d(TAG, "mAdapter updated");
	}
	
	private void onBranchesSelected(int position) {  
		
				int campaignCount = bussinessArray.data.businesses.get(position).Products.size();
				campaignArray = new String[campaignCount+1];
				
				for (int j = 1; j <= campaignCount; j++) {
					campaignArray[j] = bussinessArray.data.businesses.get(position).Products.get(j-1).Title;
				}
				
				
				
		if (campaignCount != 0) {
			btn_CampaignPicker.setClickable(true);
			campaignArray[0] = getString(R.string.all_product);
			btn_CampaignPicker.setText(campaignArray[0]);
			initMenus(branchArray, campaignArray);
			initCampainArray(campaignArray);
			long bussinessId = bussinessArray.data.businesses.get(position).ID;
			getReservation(bussinessId, 0);
			
			
//			else{
//				btn_CampaignPicker.setText(campaignArray[0]);
//				initCampainArray(campaignArray);
//				long bussinessId = bussinessArray.data.businesses.get(position).ID;
//				long porductId = bussinessArray.data.businesses.get(position).Products
//						.get(0).ID;
//				getReservation(bussinessId, porductId);
//			}
			
		} else {
			if(Reservations != null){
			Reservations.clear();
			mAdapter.notifyDataSetChanged();
			}
			
			btn_CampaignPicker.setText(getString(R.string.no_product));
			btn_CampaignPicker.setClickable(false);
		} 
				
//				long bussinessId = bussinessArray.data.businesses.get(position).ID;
//				BusinessID = bussinessId;
//				if(bussinessArray.data.businesses.get(position).Products != null){
//				long porductId = bussinessArray.data.businesses.get(position).Products.get(0).ID;
//				getReservation(bussinessId,porductId);
//				}
//				}else{
//					btn_CampaignPicker.setText(getString(R.string.no_product));
//				}
		 
			
			
		}
	private void initCampainArray(String[] campaignArray) {
		Menu campaignMenu = mCampaignPopupMenu.getMenu();
		campaignMenu.clear();
		for (int i = 0; i < campaignArray.length ; i++) {
			campaignMenu.add(campaignArray[i]);
		}
	}
 
	private int getPositionByTitle(String[] array, String title) {
		System.out.println("positionBranch " + title);
		for (String string : array) {
		}
		int pos = 0;
		for (int i = 0; i < array.length; i++) {
			if(array[i].matches(title)){
				pos = i;
				break;
			}
		}
		return pos;

	}
	
	private void onProductSelected(int positionBranch, int positionProduct) {
		if (positionProduct == 0) {
			btn_CampaignPicker.setText(campaignArray[0]); 
			long bussinessId = bussinessArray.data.businesses.get(positionBranch).ID;
			getReservation(bussinessId, 0);
		}else{
			long bussinessId = bussinessArray.data.businesses.get(positionBranch).ID;
			System.out.println("bussinessId " + bussinessId);	
			if(bussinessArray.data.businesses.get(positionBranch).Products != null){
			long porductId = bussinessArray.data.businesses.get(positionBranch).Products.get(positionProduct-1).ID;
			getReservation(bussinessId,porductId);
		}
		}		
	}
 
	 private void deleteQuary(long BusinessID, String ReservationsArrays) {
		 showLoadingDialog();
	    	APIManager.api.deleteReservation(BusinessID, ReservationsArrays, new Callback<DataCheck>() {

				@Override
				public void failure(RetrofitError arg0) {
					// TODO Auto-generated method stub
					Toast.makeText(getActivity(), "failure", Toast.LENGTH_SHORT).show();
					dismissLoadingDialog();
				}

				@Override
				public void success(DataCheck arg0, Response arg1) {
					if(arg0.error == 0){
						Toast.makeText(getActivity(), getString(R.string.delete_confirm), Toast.LENGTH_SHORT).show();
					}else{
						Toast.makeText(getActivity(), arg0.errdesc, Toast.LENGTH_SHORT).show();
					}
					dismissLoadingDialog();
				}
			});
		}
	 

	 
}									