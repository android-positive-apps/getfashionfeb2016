package com.positiveapps.getFashion.fragments;

import com.positiveapps.getFashion.R;
import com.positiveapps.getFashion.backend.BusinessRepository;
import com.positiveapps.getFashion.interfaces.BusinessRepositoryCallbacks;

public class HotDealsBusinessListFragment extends BaseBusinessListFragment implements BusinessRepositoryCallbacks {
	public static final String TAG = "HotDealsBusinessListFragment";
	//int mPage = 0;
	public HotDealsBusinessListFragment() {
		isHotDealPage = true;
	}

	@Override
	protected void getBusinessListIfNeeded() {
		BusinessRepository.getInstance().loadBusinessListHotDeals(getCurrentPage(), this);
	}



	@Override
	protected int getEmptyViewResource() {
		return R.layout.empty_view_fragment_promotions;
	}

}
