package com.positiveapps.getFashion.fragments;

import android.app.Activity;
import android.text.TextUtils;

import com.positiveapps.getFashion.MainActivity;
import com.positiveapps.getFashion.R;
import com.positiveapps.getFashion.backend.BusinessRepository;


public class SearchBusinessListFragment extends BaseBusinessListFragment {
	public static final String TAG = "SearchBusinessListFragment";

	public SearchBusinessListFragment() {}
	
	@Override
	protected void getBusinessListIfNeeded() {
		String keyword = MainActivity.searchData.keyword;
		
		if(TextUtils.isEmpty(keyword))
			return;
		System.out.println("AAAAAAAAAAAAAAAAAAAA");
		BusinessRepository.getInstance().searchForBusinesses(keyword, 0, this);
	}
	
	@Override
	public void onResume() {
		
		super.onResume();
		
	}
	
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		
	}
	
	@Override
	public void onPause() {
		super.onPause();
		
	}

	@Override
	protected int getEmptyViewResource() {
		return R.layout.empty_view_fragment_promotions;
	}
	
	
}
